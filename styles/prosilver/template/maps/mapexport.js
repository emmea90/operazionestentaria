function getTrackDepart() {
	var trackDepart = document.getElementById('routedepart').value;
	if(trackDepart=="") {
		trackDepart = "Partenza";
	}
	return trackDepart;
}


function getTrackArrive() {
	var trackArrive = document.getElementById('routearrive').value;
	if(trackArrive=="") {
		trackArrive = "Arrivo";
	}
	return trackArrive;
}

function getTrackVisibility() {
	var trackVisibility = document.getElementById('routepublic').checked;
	if (trackVisibility == true) {
		return 1;
	} else {
		return 0;
	}
}

function getTrackRacetype() {
	var trackracetype = document.getElementById('racetype').value;
	return trackracetype;	
}

function getTracktype() {
	var stagetype = document.getElementById('stagetype').value;
	return stagetype;
}

function getStageId() {
	var stageid = document.getElementById('stageid').value;
	return stageid;
}

function getTrackDescription () {
	var trackDescr = document.getElementById('trackdescription').value;
	return trackDescr;
}


function getAsText(readFile) {
    reader.onload = fileloaded;
}

function fileloaded(evt) {
    var fileString = evt.target.result;
}

function gpxImport() {
    var file = document.getElementById('gpximport').files[0];
    if (file) {
    	var reader = new FileReader();	
    	reader.readAsText(file, "UTF-8");
		reader.onload = parseGpx;
    }	
}

function parseGpx(evt) {
	clearAllProfiles();
    var xml = evt.target.result;
	var first = $(xml).find("trkpt").first();
	var last = $(xml).find("trkpt").last();
	var firstlat = first.attr("lat");
	var firstlon = first.attr("lon");
	var lastlat = last.attr("lat");
	var lastlon = last.attr("lon");		
	var points = [];
	var pstart = new google.maps.LatLng(firstlat, firstlon);	
	var pend = new google.maps.LatLng(lastlat, lastlon);										
	var previous = pstart;
	var bounds = new google.maps.LatLngBounds();	
	var startMarker = insertMarker(pstart, depart);		
	markers.push(startMarker);
	var endMarker = insertMarker(pend, arrive);	
	markers.push(endMarker);
	var distance = 0;				
	$(xml).find("trkpt").each(function () {
		 var lat = $(this).attr("lat");
		 var lon = $(this).attr("lon");
		 var ele = $(this).children();
		 ele = Number(ele[0].innerText);
		 var p = new google.maps.LatLng(lat, lon);
		 distance = distance + distancePoints(previous, p);
		 previous = p;		 
		 var altpoint = {
		 	position: p,
			altitude: ele,
			distance: distance,
			canvasx: 0,
			canvasy: 0,
		 }
		 points.push(p);
		 calcroutealt.push(altpoint);
		 bounds.extend(p);			 	 
	});			  	 		
	calcroute.push(points);			
	currentpointer = 2;
	var poly = insertPoly(points, 0);
	polylines.push(poly);
	updateRouteInfos();
	elaborateRoute();
	map.fitBounds(bounds);	 
}

function importObjects() {
	var objects = '{TRACKSPRINTS}';
	if(objects != '') {
		var exportobjects = JSON.parse(objects);
		importobjectsFromVector(stageobjects.stagesprints, exportobjects.stagesprints);
		importobjectsFromVector(stageobjects.stageclimbs, exportobjects.stageclimbs);
		importobjectsFromVector(stageobjects.stageintermediates, exportobjects.stageintermediates);
		importobjectsFromVector(stageobjects.stagecobbles, exportobjects.stagecobbles);
		importobjectsFromVector(stageobjects.stagerests, exportobjects.stagerests);	
		updateStageObjects();
	}
	sprintcounter = getSprintCounter();
}

function importobjectsFromVector (dest, source) {
	for(var i = 0; i < source.length; i++) {
		var plat = source[i].location.k;
		var plong = source[i].location.A;
		if(!plong) {
			plong = source[i].location.B;
		}
		var p = new google.maps.LatLng(plat, plong);
		var marker = insertMarker(p, source[i].marker.icon);
		marker.labelClass = source[i].marker.labelClass;
		marker.labelAnchor = otherAncor;
		var object = {
			altitude: source[i].altitude,
			category: source[i].category,
			difficulty: source[i].difficulty,
			distance: source[i].distance,
			id: source[i].id,
			location: source[i].location,
			name: unescape(source[i].name),
			startdistance: source[i].startdistance,
			startaltitude: source[i].startaltitude,		
			marker: marker,
		}
		marker.labelContent = unescape(object.name);		
	dest.push(object);
	}	
}

function save() {
	if(calcroute.length) {
		if(!calcroutealt.length) {
			elaborateRoute();
		}
		var exportRoute = [];
		var exportSampleRoute = [];
	
		for (var i = 0; i < calcroute.length; i++) {
			var exportSampleRouteSection = [];
			for (var j = 0; j < calcroute[i].length; j++) {
				
				var position = {
					A: Number(calcroute[i][j].lng().toFixed(5)),
					k: Number(calcroute[i][j].lat().toFixed(5)),
				}	
				exportSampleRouteSection.push(position);		
			}	
			exportSampleRoute.push(exportSampleRouteSection);
		}	
		
		for (var i = 0; i < calcroutealt.length; i++) {
			var position = {
				A: Number(calcroutealt[i].position.lng().toFixed(5)),
				k: Number(calcroutealt[i].position.lat().toFixed(5)),
			}			
			var savePoint = {
				altitude: calcroutealt[i].altitude,
				distance: calcroutealt[i].distance,		
				position: position,
			}
			exportRoute.push(savePoint);
		}
		
		var exportobs = exportObjects();
		var stringRoute = JSON.stringify(exportSampleRoute);
		var stringRouteAlt = JSON.stringify(exportRoute);
		var stringSprints = JSON.stringify(exportobs);
		var data = new FormData();
		data.append('route', stringRoute);
		data.append('routewithalt', stringRouteAlt);
		data.append('sprints', stringSprints);
		data.append('name', getTrackname());
		data.append('depart', getTrackDepart());
		data.append('arrive', getTrackArrive());
		data.append('ispublic', getTrackVisibility());
		data.append('type', getTracktype());
		data.append('distance', fixdistance);
		data.append('stageid', getStageId());
		data.append('trackdescription', getTrackDescription());
		data.append('trackracetype', getTrackRacetype());
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("post", "save.php", true);
		document.getElementById("savestate").innerHTML= "{L_SAVING}";
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("stageid").value=xmlhttp.responseText;
				document.getElementById("savestate").innerHTML= "{L_SAVED}";
			} else {
				document.getElementById("savestate").innerHTML= "Errore " + xmlhttp.responseText;
			}
		}	
		xmlhttp.send(data);		
	} else {
		alert("La traccia è vuota");
	}
}

function exportObjects () {
	var exportobjects = {
		stagesprints: [],
		stageintermediates: [],
		stagecobbles: [],
		stageclimbs: [],
		stagerests: [],
	};	
	
	exportobjectsFromVector(stageobjects.stagesprints, exportobjects.stagesprints);
	exportobjectsFromVector(stageobjects.stageclimbs, exportobjects.stageclimbs);
	exportobjectsFromVector(stageobjects.stageintermediates, exportobjects.stageintermediates);
	exportobjectsFromVector(stageobjects.stagecobbles, exportobjects.stagecobbles);
	exportobjectsFromVector(stageobjects.stagerests, exportobjects.stagerests);
	return exportobjects;
}

function exportobjectsFromVector (source, dest) {
	for(var i = 0; i < source.length; i++) {
		var markerInfo = {
			icon: source[i].marker.icon,
			labelClass: source[i].marker.labelClass,
		}
		var object = {
			altitude: source[i].altitude,
			category: source[i].category,
			difficulty: source[i].difficulty,
			distance: source[i].distance,
			id: source[i].id,
			location: source[i].location,
			name: escape(source[i].name),
			startdistance: source[i].startdistance,
			startaltitude: source[i].startaltitude,				
			marker: markerInfo,
		}
	dest.push(object);
	}
}