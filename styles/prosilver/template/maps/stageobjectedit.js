var stageobjects = {
	stagesprints: [],
	stageintermediates: [],
	stagecobbles: [],
	stageclimbs: [],
	stagerests: [],
};
var finishAncor = new google.maps.Point(-20, 40);
var otherAncor = new google.maps.Point(-20, 25);
var climbfinishAncor = new google.maps.Point(-20, 5);
var sprintcounter = 0;

function getSprintCounter () {
	var counter = stageobjects.stagesprints.length + stageobjects.stageintermediates.length + stageobjects.stagecobbles.length + stageobjects.stageclimbs.length + stageobjects.stagerests.length;
	return counter;
}

function updateStartEnd () {
	var depart = document.getElementById('routedepart');
	markers[0].labelContent = depart.value;
	markers[0].labelClass = 'finishlabels';
	markers[0].labelAnchor = finishAncor;
	var arrive = document.getElementById('routearrive');
	markers[markers.length-1].labelContent = arrive.value;
	markers[markers.length-1].labelClass = 'finishlabels';
	markers[markers.length-1].labelAnchor = finishAncor;	
}

function setObjectMarkersDraggables (value) {
	setFieldObjectMarkersDraggables(value, stageobjects.stagesprints);
	setFieldObjectMarkersDraggables(value, stageobjects.stageintermediates);
	setFieldObjectMarkersDraggables(value, stageobjects.stagecobbles);
	setFieldObjectMarkersDraggables(value, stageobjects.stageclimbs);
	setFieldObjectMarkersDraggables(value, stageobjects.stagerests);
}

function removeAllSprintObjects () {
	removeMarkersFromVector(stageobjects.stagesprints);
	removeMarkersFromVector(stageobjects.stageintermediates);
	removeMarkersFromVector(stageobjects.stagecobbles);
	removeMarkersFromVector(stageobjects.stageclimbs);
	removeMarkersFromVector(stageobjects.stagerests);
	updateStageObjects();		
}

function updateObjectsPositions() {
	updateObjectsPositionFromVector(stageobjects.stagesprints);
	updateObjectsPositionFromVector(stageobjects.stageintermediates);
	updateObjectsPositionFromVector(stageobjects.stagecobbles);
	updateObjectsPositionFromVector(stageobjects.stageclimbs);
	updateObjectsPositionFromVector(stageobjects.stagerests);	
}

function updateObjectsPositionFromVector(vector) {
	for (var k = 0; k < vector.length; k++) {
		var originallocation = new google.maps.LatLng(vector[k].location.k, vector[k].location.B);
		var mindistance = 9999;
		var mindistancelocation = originallocation;
		for(var i=0; i<calcroute.length; i++) {
			for(var j=0; j<calcroute[i].length; j++) {
				var start = calcroute[i][j];
				var distance = distancePoints(start, originallocation);
				if (distance < mindistance) {
					mindistance = distance;
					mindistancelocation = start; 
				}
			}
		}
		vector.location = mindistancelocation;
		vector[k].marker.setPosition(mindistancelocation);
	}
}

function setFieldObjectMarkersDraggables (value, vector) {
	for (var i = 0; i < vector.length; i++) {
		vector[i].marker.setDraggable(value);
	}
}

function removeMarkersFromVector (vector) {
	for (var i = 0; i < vector.length; i++) {
		vector[i].marker.setMap(null);
	}
	vector.length = 0;
}


function updateObjectInfo (marker) {
		var location = marker.getPosition();
		var newlocation = getObjectPosition(location);
		marker.setPosition(newlocation);
		var distance = getDistance(newlocation);
		var sprintvector = stageobjects.stagesprints;	
		if (marker.getIcon()==sprint) {
			sprintvector = stageobjects.stagesprints;
		} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
			sprintvector = stageobjects.stageclimbs;
		} else if (marker.getIcon()==intermediate) {
			sprintvector = stageobjects.stageintermediates;					
		} else if (marker.getIcon()==rest) {
			sprintvector = stageobjects.stagerests;				
		} else if (marker.getIcon()==cobble) {
			sprintvector = stageobjects.stagecobbles;	
		}	
		var index = findIndexByMarker(sprintvector, marker);		
		sprintvector[index].location = location;			
		sprintvector[index].altitude = getPointAltitude(distance);	
		sprintvector[index].distance = distance;
		updateStageObjects();			
}

function findIndexByMarker (sprintvector, marker) {
	for (var i = 0; i < sprintvector.length; i++) {
		if(sprintvector[i].marker == marker) {
			return i;
		}
	}
}

function insertObjectPoint(marker, counter) {
	var location = marker.getPosition();
	var distance = getDistance(location);
	var objectpoint = {
		id: counter,
		name: null,
		location: location,
		distance: distance,
		marker: marker,
		altitude: getPointAltitude(distance),
		category: null,
		startdistance: null,
		startaltitude: null,
		difficulty: null,
	}
	marker.labelAnchor = otherAncor;
	return objectpoint;
}

function insertObjectPointWithDistance(marker, counter, distance) {
	var location = marker.getPosition();
	var objectpoint = {
		id: counter,
		name: null,
		location: location,
		distance: distance,
		marker: marker,
		altitude: getPointAltitude(distance),
		category: null,
		startdistance: null,
		startaltitude: null,
		difficulty: null,
	}
	marker.labelAnchor = otherAncor;
	return objectpoint;
}

function transformMarker (marker) {
	if(tracktype == 'labels') {
		var objectpoint = getObjectPoint(marker);
		if (marker.getIcon()==sprint) {
			marker.setIcon(climbNO);
			marker.labelClass = "climblabels";
			stageobjects.stageclimbs.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stagesprints);
		} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
			marker.setIcon(intermediate);
			marker.labelClass = "intermediatelabels";
			stageobjects.stageintermediates.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stageclimbs);
		} else if (marker.getIcon()==intermediate) {
			marker.setIcon(rest);
			marker.labelClass = "intermediatelabels";
			stageobjects.stagerests.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stageintermediates);
		} else if (marker.getIcon()==rest) {
			marker.setIcon(cobble);
			marker.labelClass = "cobblelabels";
			stageobjects.stagecobbles.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stagerests);
		} else if (marker.getIcon()==cobble) {
			marker.setIcon(sprint);
			marker.labelClass = "sprintlabels";
			stageobjects.stagesprints.push(objectpoint);		
			removeobjectpoint (objectpoint, stageobjects.stagecobbles);	
		}			
		updateStageObjects();
	}
}

function insertNewMarkerObject (position) {
	sprintcounter++;
	var marker = insertMarker(position, sprint);		
	marker.labelClass = "sprintlabels";
	var objectpoint = insertObjectPoint(marker, sprintcounter);
	stageobjects.stagesprints.push(objectpoint);		
	updateStageObjects();		
}

function insertNewMarkerObjectWithDistance (position, distance) {
	sprintcounter++;
	var marker = insertMarker(position, sprint);		
	marker.labelClass = "sprintlabels";
	var objectpoint = insertObjectPointWithDistance(marker, sprintcounter, distance);
	stageobjects.stagesprints.push(objectpoint);		
	updateStageObjects();		
}

function deleteMarkerObject (marker) {
	marker.setMap(null);
	var vector = null;
	if (marker.getIcon()==sprint) {
		vector = stageobjects.stagesprints;
	} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
		vector = stageobjects.stageclimbs;
	} else if (marker.getIcon()==intermediate) {
		vector = stageobjects.stageintermediates;			
	} else if (marker.getIcon()==rest) {
		vector = stageobjects.stagerests;
	} else if (marker.getIcon()==cobble) {
		vector = stageobjects.stagecobbles;
	}			
	var objectpoint = findobjectpoint(marker, vector);		
	removeobjectpoint (objectpoint, vector);		
	updateStageObjects();	
}

function findobjectpoint (marker, vector) {
	for(var i = 0; i<vector.length; i++) {
		if(vector[i].marker == marker) {
			return vector[i];
		}
	}
}

function getObjectPoint (marker) {
	var vector = null;
	if (marker.getIcon()==sprint) {
		vector = stageobjects.stagesprints;
	} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
		vector = stageobjects.stageclimbs;
	} else if (marker.getIcon()==intermediate) {
		vector = stageobjects.stageintermediates;			
	} else if (marker.getIcon()==rest) {
		vector = stageobjects.stagerests;
	} else if (marker.getIcon()==cobble) {
		vector = stageobjects.stagecobbles;
	}		
	var objectpoint = findobjectpoint(marker, vector);		
	return objectpoint;			
}

function updateStageObjects() {
	updateSprints();
	updateClimbs();
	updateRests();
	updateIntermediates();
	updateCobbles();
	elaborateZoom();
}

function cleanList(listname) {
	var sprintlist = document.getElementById(listname);
	while (sprintlist.firstChild) {
    	sprintlist.removeChild(sprintlist.firstChild);
	}	
	return sprintlist;	
}

function updateCobbles() {
	var cobblelist = cleanList("cobblelist");	
	
	var cobblehead = '\
            <tr class="bg1">\
                <td align="center" width="100px" colspan="7"><h3>{L_ROUTECOBBLES}</h3></td>\
            </tr>\
            <tr class="bg1">\
                <td align="center" width="100px"><h4>{L_COBBLESECTOR}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLEDIFFICULTY}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLENAME}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLESTART}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLEEND}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLELENGTH}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLELEVATION}</h4></td>\
            </tr>';	
	var cobbleheadref = cleanList("cobblehead");
	if(stageobjects.stagecobbles.length) {
		cobbleheadref.innerHTML = cobblehead;
	}
	stageobjects.stagecobbles.sort(distanceSortFunction);	
	for (var i = 0; i < stageobjects.stagecobbles.length; i++) {
		var cobble = stageobjects.stagecobbles[i];
		var cobblerow = cobblelist.insertRow(-1);
		cobblerow.setAttribute("class","bg1");	
		var cobblesector = cobblerow.insertCell(-1);
		var cobbledifficulty = cobblerow.insertCell(-1);
		var cobblename = cobblerow.insertCell(-1);
		var cobblestart = cobblerow.insertCell(-1);		
		var cobbleend = cobblerow.insertCell(-1);		
		var cobblelength = cobblerow.insertCell(-1);		
		var cobbleelevation = cobblerow.insertCell(-1);		
		var name = unescape(cobble.name);
		if(!cobble.name) {
			name = "";
		}
		var startdistance = cobble.startdistance;
		var distance = cobble.distance - cobble.startdistance;		
		if(!cobble.startdistance)
		{
			startdistance = "";
			distance = 0;
		}		
		cobblesector.innerHTML = i + 1;
		if(cobble.difficulty == null) {
			cobble.difficulty == 1;	
		}
		var stringdifficulty = "";
		for (var j = 1; j <= 5; j++) {
			stringdifficulty = stringdifficulty + "+";
			if(cobble.difficulty == j) {
				cobbledifficulty.innerHTML = cobbledifficulty.innerHTML + '<input type="radio" id="cobbledifficulty' + cobble.id + '" group="cobbledifficulty' + cobble.id + '" onclick="setCobbleDifficulty(' + cobble.id + ', ' + j + ')" value="' + j + '" checked="checked">&nbsp;' + stringdifficulty + '&nbsp;</input>';				
			} else {
				cobbledifficulty.innerHTML = cobbledifficulty.innerHTML + '<input type="radio" id="cobbledifficulty' + cobble.id + '" group="cobbledifficulty' + cobble.id + '" onclick="setCobbleDifficulty(' + cobble.id + ', ' + j + ')" value="' + j + '">&nbsp;' + stringdifficulty + '&nbsp;</input>';			
			}		
		}
		cobblename.innerHTML = '<input type="text" id="cobblename' + cobble.id + '" name="cobblename' + cobble.id + '" onchange="setCobbleName(' + cobble.id + ')" value="'+ name +'" />';
		cobblestart.innerHTML = '<input type="text" id="cobblestart' + cobble.id + '" name="cobblestart' + cobble.id + '" onchange="setCobbleDistance(' + cobble.id + ')" value="'+ startdistance +'" /> Km';
		cobbleend.innerHTML = cobble.distance.toFixed(2) + ' Km';	
		cobblelength.innerHTML = distance.toFixed(2) + ' Km';	
		cobbleelevation.innerHTML = cobble.altitude.toFixed(0) + ' m';
	}	
						
}

function updateIntermediates() {
	var intermediatelist = cleanList("intermediateslist");
	
	var intermediatehead = '\
	        <tr class="bg1">\
                <td align="center" width="100px" colspan="3"><h3>{L_ROUTEINTERMEDIATES}</h3></td>\
            </tr>\
            <tr class="bg1">\
                <td align="center" width="100px"><h4>{L_INTERMEDIATENAME}</h4></td>\
                <td align="center" width="100px"><h4>{L_INTERMEDIATEPOSITION}</h4></td>\
                <td align="center" width="100px"><h4>{L_INTERMEDIATEELEVATION}</h4></td>\
            </tr>';
	
	var intermediateheadref = cleanList("intermediateshead");
	if(stageobjects.stageintermediates.length) {
		intermediateheadref.innerHTML = intermediatehead;
	}				
	stageobjects.stageintermediates.sort(distanceSortFunction);	
	for (var i = 0; i < stageobjects.stageintermediates.length; i++) {
		var intermediate = stageobjects.stageintermediates[i];
		var intermediaterow = intermediatelist.insertRow(-1);
		intermediaterow.setAttribute("class","bg1");	
		var intermediatename = intermediaterow.insertCell(-1);
		var intermediatefinish = intermediaterow.insertCell(-1);
		var intermediatealtitude = intermediaterow.insertCell(-1);		
		var name = unescape(intermediate.name);
		if(!intermediate.name) {
			name = "";
		}
		intermediatename.innerHTML = '<input type="text" id="intermediatename' + intermediate.id + '" name="intermediatename' + intermediate.id + '" onchange="setIntermediateName(' + intermediate.id + ')" value="'+ name +'" />';
		intermediatefinish.innerHTML = intermediate.distance.toFixed(2) + ' Km';
		intermediatealtitude.innerHTML = intermediate.altitude.toFixed(0) + ' m';
	}
}

function updateRests() {
	var restlist = cleanList("restlist");
	stageobjects.stagerests.sort(distanceSortFunction);	
	
	var resthead = '<tr class="bg1">\
                <td align="center" width="100px" colspan="3"><h3>{L_ROUTERESTS}</h3></td>\
            </tr>\
            <tr class="bg1">\
                <td align="center" width="100px"><h4>{L_RESTNAME}</h4></td>\
                <td align="center" width="100px"><h4>{L_RESTPOSITION}</h4></td>\
                <td align="center" width="100px"><h4>{L_RESTELEVATION}</h4></td>\
            </tr>';
	
	var restheadref = cleanList("resthead");
	if(stageobjects.stagerests.length) {
		restheadref.innerHTML = resthead;
	}	
	for (var i = 0; i < stageobjects.stagerests.length; i++) {
		var rest = stageobjects.stagerests[i];
		var restrow = restlist.insertRow(-1);
		restrow.setAttribute("class","bg1");	
		var restname = restrow.insertCell(-1);
		var restfinish = restrow.insertCell(-1);
		var restaltitude = restrow.insertCell(-1);		
		var name = unescape(rest.name);
		if(!rest.name) {
			name = "";
		}
		restname.innerHTML = '<input type="text" id="restname' + rest.id + '" name="restname' + rest.id + '" onchange="setRestName(' + rest.id + ')" value="'+ name +'" />';
		restfinish.innerHTML = rest.distance.toFixed(2) + ' Km';
		restaltitude.innerHTML = rest.altitude.toFixed(0) + ' m';
	}
}

function updateSprints() {
	var sprintlist = cleanList("sprintlist");
	stageobjects.stagesprints.sort(distanceSortFunction);	
	
	var sprinthead = '\
	<tr class="bg1">\
		<td align="center" width="100px" colspan="3"><h3>{L_ROUTESPRINTS}</h3></td>\
	</tr>\
		<tr class="bg1">\
		<td align="center" width="100px"><h4>{L_SPRINTNAME}</h4></td>\
		<td align="center" width="100px"><h4>{L_SPRINTPOSITION}</h4></td>\
		<td align="center" width="100px"><h4>{L_SPRINTELEVATION}</h4></td>\
    </tr>';
	
	var sprintheadref = cleanList("sprinthead");
			
	if(stageobjects.stagesprints.length) {
		sprintheadref.innerHTML = sprinthead;
	}
	
	for (var i = 0; i < stageobjects.stagesprints.length; i++) {
		var sprint = stageobjects.stagesprints[i];
		var sprintrow = sprintlist.insertRow(-1);
		sprintrow.setAttribute("class","bg1");	
		var sprintname = sprintrow.insertCell(-1);
		var sprintfinish = sprintrow.insertCell(-1);
		var sprintaltitude = sprintrow.insertCell(-1);		
		var name = unescape(sprint.name);
		if(!sprint.name) {
			name = "";
		}
		sprintname.innerHTML = '<input type="text" id="sprintname' + sprint.id + '" name="sprintname' + sprint.id + '" onchange="setSprintName(' + sprint.id + ')" value="'+ name +'" />';
		sprintfinish.innerHTML = sprint.distance.toFixed(2) + ' Km';
		sprintaltitude.innerHTML = sprint.altitude.toFixed(0) + ' m';
	}
}

function updateClimbs() {
	var climblist = cleanList("climbfinlist");
	stageobjects.stageclimbs.sort(distanceSortFunction);
	
	var climbhead ='\
	<tr class="bg1">\
		<td align="center" width="100px" colspan="8"><h3>{L_ROUTECLIMBS}</h3></td>\
	</tr>\
	<tr class="bg1">\
		<td align="center" width="100px"><h4>{L_CLIMBNAME}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBSTART}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBFINISH}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBSTARTELE}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBFINISHELE}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBDISTANCE}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBCATEGORY}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBAVERAGE}</h4></td>\
	</tr>';
	var climbheadref = cleanList("climbhead");	
	
	if(stageobjects.stageclimbs.length) {
	
		climbheadref.innerHTML = climbhead;
	}
	for (var i = 0; i < stageobjects.stageclimbs.length; i++) {
		var climb = stageobjects.stageclimbs[i];
		var climbrow = climblist.insertRow(-1);
		climbrow.setAttribute("class","bg1");	
		var climbname = climbrow.insertCell(-1);
		var climbstart = climbrow.insertCell(-1);
		var climbfinish = climbrow.insertCell(-1);
		var climbstartele = climbrow.insertCell(-1);
		var climbfinishele = climbrow.insertCell(-1);
		var climbdistance = climbrow.insertCell(-1);
		var climbcategory = climbrow.insertCell(-1);
		var climbaverage = climbrow.insertCell(-1);	
		var startele = getPointAltitude(climb.startdistance);
		var distance = climb.distance - climb.startdistance;		
		var slope = 0.1*((climb.altitude - startele) / distance);
		slope = slope.toFixed(2);
		var name = unescape(climb.name);
		var startdistance = climb.startdistance;
		if(!climb.name) {
			name = "";
		}
		if(!climb.startdistance)
		{
			startdistance = "";
			slope = "";
			distance = 0;
		}
		climbname.innerHTML = '<input type="text" id="climbname' + climb.id + '" name="climbname' + climb.id + '" onchange="setClimbName(' + climb.id + ')" value="'+ name +'" />';
		climbstart.innerHTML = '<input type="text" id="climbstart' + climb.id + '" name="climbstart' + climb.id + '" onchange="setClimbDistance(' + climb.id + ')" value="'+ startdistance +'" /> Km';
		climbfinish.innerHTML = climb.distance.toFixed(2) + ' Km';
		climbstartele.innerHTML = startele +' m';
		climbfinishele.innerHTML = climb.altitude +' m';
		climbdistance.innerHTML = distance.toFixed(2) +' Km';
		if(climb.category == 6 || !climb.category) {
			climbcategory.innerHTML = '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 6)" value="6" checked="checked">&nbsp;None&nbsp;</input>';
		} else {
			climbcategory.innerHTML = '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 6)" value="6">&nbsp;None&nbsp;</input>';
		}
		
		if(climb.category == 5) {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 5)" value="HC" checked="checked">&nbsp;HC&nbsp;</input>';
		} else {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 5)" value="HC">&nbsp;HC&nbsp;</input>';			
		}

		if(climb.category == 1) {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 1)" value="1" checked="checked">&nbsp;1&nbsp;</input>';				
		} else {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 1)" value="1">&nbsp;1&nbsp;</input>';			
		}
				
		if(climb.category == 2) {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 2)" value="2" checked="checked">&nbsp;2&nbsp;</input>';		
		} else {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 2)" value="2">&nbsp;2&nbsp;</input>';					
		}

		if(climb.category == 3) {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 3)" value="3" checked="checked">&nbsp;3&nbsp;</input>';			
		} else {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 3)" value="3">&nbsp;3&nbsp;</input>';			
		}
		
		if(climb.category == 4) {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 4)" value="4" checked="checked">&nbsp;4&nbsp;</input>';			
		} else {
			climbcategory.innerHTML = climbcategory.innerHTML + '<input type="radio" id="climbcategory' + climb.id + '" group="climbcategory' + climb.id + '" onclick="setClimbCategory(' + climb.id + ', 4)" value="4">&nbsp;4&nbsp;</input>';			
		}											
		climbaverage.innerHTML = slope +' %';	
	}
}

function setClimbCategory (climbid, value) {
	for (var i = 0; i < stageobjects.stageclimbs.length; i++) {
		var climb = stageobjects.stageclimbs[i];
		if(climb.id == climbid) {
			climb.category = value.toString();
			switch (value) {
				case 1:
					climb.marker.setIcon(climb1);
					break;
				case 2:
					climb.marker.setIcon(climb2);
					break;
				case 3:
					climb.marker.setIcon(climb3);
					break;
				case 4:
					climb.marker.setIcon(climb4);
					break;
				case 5:
					climb.marker.setIcon(climbHC);
					break;
				case 6:
				default:
					climb.marker.setIcon(climbNO);
					break;				
			}
			updateClimbs();
			elaborateZoom();
			return;
		}
	}
}

function setCobbleDifficulty (cobbleid, value) {
	for (var i = 0; i < stageobjects.stagecobbles.length; i++) {
		var cobble = stageobjects.stagecobbles[i];
		if(cobble.id == cobbleid) {
			cobble.difficulty = value.toString();
			updateCobbles();
			elaborateZoom();
			return;
		}
	}
}

function setElementName (elementid, rowname, vector) {
	var name = document.getElementById(rowname + elementid).value;
	for (var i = 0; i < vector.length; i++) {
		var element = vector[i];
		if(element.id == elementid) {
			element.name = escape(name);
			element.marker.labelContent = unescape(name);
			elaborateZoom();
			return;
		}
	}		
}

function setIntermediateName (intermediateid) {
	setElementName (intermediateid, 'intermediatename', stageobjects.stageintermediates);
	updateIntermediates();
}

function setRestName (restid) {
	setElementName (restid, 'restname', stageobjects.stagerests);
	updateRests();
}

function setCobbleName (cobbleid) {
	setElementName (cobbleid, 'cobblename', stageobjects.stagecobbles);
	updateCobbles();
}

function setSprintName (sprintid) {
	setElementName (sprintid, 'sprintname', stageobjects.stagesprints);
	updateSprints();
}

function setClimbName (climbid) {
	setElementName (climbid, 'climbname', stageobjects.stageclimbs);
	updateClimbs();
}

function setClimbDistance (climbid) {
	var distance = document.getElementById('climbstart' + climbid).value
	for (var i = 0; i < stageobjects.stageclimbs.length; i++) {
		var climb = stageobjects.stageclimbs[i];
		if(climb.id == climbid) {
			climb.startdistance = Number(distance);
			updateClimbs();
			return;
		}
	}
}

function setCobbleDistance (cobbleid) {
	var distance = document.getElementById('cobblestart' + cobbleid).value
	for (var i = 0; i < stageobjects.stagecobbles.length; i++) {
		var cobble = stageobjects.stagecobbles[i];
		if(cobble.id == cobbleid) {
			cobble.startdistance = Number(distance);
			updateCobbles();
			return;
		}
	}
}

function distanceSortFunction (a, b) {
	if (a.distance > b.distance) {
		return 1;
	} else {
		return -1;
	}
}

function getDistance (location) {
	var lessdistance = 300;
	var lesskilometers = null;
	for (var i = 0; i < calcroutealt.length; i++) {	
		var position = calcroutealt[i].position;
		var distance = calcroutealt[i].distance;
		var dx = distancePoints(position, location);

		if(dx < lessdistance) {
			lessdistance = dx;
			lesskilometers = distance;			
		}
	}	
	return lesskilometers;
}

function getObjectPosition (location) {
	var lessdistance = 300;
	var lessposition = null;
	for (var i = 0; i < calcroutealt.length; i++) {	
		var position = calcroutealt[i].position;
		var distance = calcroutealt[i].distance;
		var dx = distancePoints(position, location);
		if(dx <= lessdistance) {
			lessdistance = dx;
			lessposition = position;
		} 	
	}	
	return lessposition;	
}

function removeobjectpoint (point, objectvector) {
	var index = myIndexOf(objectvector, point);
	if (index > -1) {
    	objectvector.splice(index, 1);
	}
}

function myIndexOf(arr, o) {    
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].id == o.id) {
            return i;
        }
    }
    return -1;
}

function plotObjects(context, imagesource, vector) {
	var j = 0;
	var sprintimage = new Image();
	sprintimage.src = imagesource;
	sprintimage.onload = function(){
		var sprintwidth = sprintimage.width;
		var sprintheight = sprintimage.height;	
		for (var i = 0; i < vector.length; i++) {
			if(vector[i].marker.getIcon()==imagesource) {
				var distance = vector[i].distance;
				while(calcroutealt[j].distance < distance) {
					j++;
				}
				displaySprintImage(context, sprintimage, calcroutealt[j].canvasx, calcroutealt[j].canvasy);					
			}
		}	
	}
}

function displaySprintImage (context, image, x, y) {
	var sprintwidth = image.width;
	var sprintheight = image.height;
	context.drawImage(image, x - 0.5*sprintwidth, y - 20 - 0.5*sprintheight);		
}
