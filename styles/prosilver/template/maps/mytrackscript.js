// JavaScript Document
function deleteTrack (trackid) {
	if (confirm('{L_SURETRACK}')) {
		var data = new FormData();
		data.append('track', trackid);	
			
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("post", "deletetrack.php", true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				var node = document.getElementById("track" + trackid);
				document.location.reload();
			} else {
				var error = document.getElementById("errorbox");	
				error.innerHTML = xmlhttp.responseText;			
			}
		}	
		xmlhttp.send(data);
	}
}

function cloneTrack (trackid) {
	if (confirm('{L_SURETRACKCOPY}')) {
		var data = new FormData();
		data.append('track', trackid);	
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("post", "clonetrack.php", true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.location.reload();
			} else {
				var error = document.getElementById("errorbox");	
				error.innerHTML = xmlhttp.responseText;
			}
		}	
		xmlhttp.send(data);
	}
}