var iconBase = '{LOGOFOLDER}';
var depart = iconBase + 'partenza.gif';
var arrive = iconBase + 'arrivo.gif';
var sprint = iconBase + 'sprintlogo.gif';
var climbHC = iconBase + 'HCCatLogo.gif';
var climbNO = iconBase + 'NOCatLogo.gif';
var climb1 = iconBase + '1CatLogo.gif';
var climb2 = iconBase + '2CatLogo.gif';
var climb3 = iconBase + '3CatLogo.gif';
var climb4 = iconBase + '4CatLogo.gif';
var cobble = iconBase + 'Cobblelogo.gif';
var departprofile = iconBase + 'departprofile.gif';
var arriveprofile = iconBase + 'arriveprofile.gif';
var intermediate = iconBase + 'intermediatelogo.gif';
var trackPoint = iconBase + 'TrackPointLogo.gif';
var trackPointMini = iconBase + 'TrackPointLogoMini.gif';
var rest = iconBase + 'restlogo.gif';
var lastkmprofile = iconBase + 'lastkmprofile.gif';
var arrivelastkmprofile = iconBase + 'arrivelastkm.gif';
var departplain = iconBase + 'departplain.gif';
var arriveplain = iconBase + 'arriveplain.gif';
var departarriveplain = iconBase + 'departarriveplain.gif';

var map;
var navigationmarker;
var minhundred;
var maxhundred;
var calcroutealt = [];
var exportroute = [];
var interpolationdistance = 0.2;
var climbs = [];
var drawonlyclimbs = 0;

function initialize() {
	var mapOptions = {
		zoom: 8,
		center: new google.maps.LatLng(0,0),
		mapTypeId: google.maps.MapTypeId.TERRAIN
	};
  
	navigationmarker = new google.maps.Marker({
		position: null,
		icon: trackPoint,
		map: null,
		draggable: false
	});
	  
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
}


function setOnlyClimbs(number) {
	drawonlyclimbs = number;
	resetclimb();
}

function setSlopes(number) {
	drawclimbs = number;
	resetclimb();
}

function zoomsector (start, end) {
	document.getElementById('fromkm').value = start;
	document.getElementById('tokm').value = end;
 	elaborateZoom ();
}

function elaborateZoom () {
	var start = document.getElementById('fromkm').value;
	var end = document.getElementById('tokm').value;
	start = Number(start);
	end = Number(end);	
	if(start=="" || start < 0 || start > calcroutealt[calcroutealt.length-1].distance) {
		start = 0;
	}
	if(end=="" || end > calcroutealt[calcroutealt.length-1].distance || end < start) {
		end = calcroutealt[calcroutealt.length-1].distance;
	}
	start = Number(start);
	end = Number(end);
	plotAltitude(minhundred, maxhundred, start, end);
}


function findFinishPointOrder(startorder, finishorder) {
	// Find Real start point
	for (var k = startorder; k < finishorder; k++) {
		var distance =  calcroutealt[finishorder].distance.toFixed(2) - calcroutealt[k].distance.toFixed(2);
		var elevation = calcroutealt[finishorder].altitude.toFixed(0) - calcroutealt[k].altitude.toFixed(0);
		var slope = Number(((0.001*elevation / distance)*100).toFixed(1));	
		if (slope <= 0.5) {
			finish = calcroutealt[k];
			return k;
		}
	}
	finish = calcroutealt[finishorder];
	return finishorder;
}

function findStartPointOrder(startorder, finishorder) {
	// Find Real start point
	for (var k = startorder; k < finishorder; k++) {
		var distance = calcroutealt[k].distance - calcroutealt[startorder].distance;
		var elevation = calcroutealt[k].altitude - calcroutealt[startorder].altitude;
		var slope = Number(((0.001*elevation / distance)*100).toFixed(1));	
		var difficulty = distance * (Math.pow(slope,2));
		if(slope <= 1) {
			startorder = k;
		}
	}
	start = calcroutealt[startorder];
	return startorder;
}

function findclimbs () {
	climbs.length = 0;
	var climblist = document.getElementById('climblist');
	if(climblist) {
		while (climblist.firstChild) {
			climblist.removeChild(climblist.firstChild);
		}		
	}
	for(var i=1; i<calcroutealt.length; i++) {
		var start = calcroutealt[i-1];
		startorder = i;
		var previous = start;
		var error = 0;
		var finish = start;
		if(calcroutealt[i].altitude > start.altitude) {
			for(var j=i; j<calcroutealt.length && error == 0; j++) {
				if(calcroutealt[j].altitude <= previous.altitude) {	
					if(calcroutealt[j].distance - previous.distance > 2) {
						error = 1;
						
					}
				} else if (calcroutealt[j].altitude > previous.altitude) {
					finish = calcroutealt[j];
					finishorder = j;
					i = j;
					previous = calcroutealt[j];
				}
			}
			startorder = findStartPointOrder(startorder, finishorder);
			finishorder = findFinishPointOrder(startorder, finishorder);			
			if(startorder!=finishorder) {	
				var start = calcroutealt[startorder];		
				var finish = calcroutealt[finishorder];				
				var distance = finish.distance.toFixed(2) - start.distance.toFixed(2);
				var elevation = finish.altitude.toFixed(0) - start.altitude.toFixed(0);
				var slope = Number(((0.001*elevation / distance)*100).toFixed(1));
				var difficulty = distance * (Math.pow(slope,2));
				if (difficulty > 40) {	
					if(climblist) {
						var climbrow = climblist.insertRow(-1);
						climbrow.setAttribute("class","bg1");
						var climbstart = climbrow.insertCell(-1);
						var climbfinish = climbrow.insertCell(-1);
						var climbdistance = climbrow.insertCell(-1);
						var climbdenivel = climbrow.insertCell(-1);
						var climbcategory = climbrow.insertCell(-1);
						var climbdifficulty = climbrow.insertCell(-1);
						var climbmaxfor500 = climbrow.insertCell(-1);
						var climbmaxfor1000 = climbrow.insertCell(-1);
						var climbmaxfor2000 = climbrow.insertCell(-1);
						var alt500 = findmaxpercentage(startorder, finishorder, 0.5);
						var alt1000 = findmaxpercentage(startorder, finishorder, 1);
						var alt2000 = findmaxpercentage(startorder, finishorder, 2);
						climbstart.innerHTML = "Km " + start.distance.toFixed(2) + " (" + start.altitude + " m" + ")";
						climbfinish.innerHTML = "Km " + finish.distance.toFixed(2) + " (" + finish.altitude + " m" + ")";
						climbdistance.innerHTML = distance.toFixed(2) + " Km" + " al " + slope + "%";
						climbcategory.innerHTML = getCategory(difficulty, alt1000);
						climbdifficulty.innerHTML = difficulty.toFixed(2);
						climbdenivel.innerHTML = elevation + " m";
						if(alt500.slope) {
							climbmaxfor500.innerHTML = alt500.slope + "%" + " (Km " + alt500.start + "-" + alt500.end + ")";
							climbmaxfor500.setAttribute("onmouseover","evidentiateclimb(" + alt500.startpoint + "," + alt500.endpoint + ")");
						} else {
							climbmaxfor500.innerHTML = "-";
						}
						if(alt1000.slope) {
							climbmaxfor1000.innerHTML = alt1000.slope + "%" + " (Km " + alt1000.start + "-" + alt1000.end + ")";
							climbmaxfor1000.setAttribute("onmouseover","evidentiateclimb(" + alt1000.startpoint + "," + alt1000.endpoint + ")");
						} else {
							climbmaxfor1000.innerHTML = "-";
						}
						if(alt2000.slope) {
							climbmaxfor2000.innerHTML = alt2000.slope + "%" + " (Km " + alt2000.start + "-" + alt2000.end + ")";
							climbmaxfor2000.setAttribute("onmouseover","evidentiateclimb(" + alt2000.startpoint + "," + alt2000.endpoint + ")");
						} else {
							climbmaxfor2000.innerHTML = "-";
						}	
						climbrow.setAttribute("onclick","zoomsector(" + start.distance.toFixed(2) + "," + finish.distance.toFixed(2) + ")");
						climbstart.setAttribute("onmouseover","evidentiateclimb(" + startorder + "," + finishorder + ")");
						climbfinish.setAttribute("onmouseover","evidentiateclimb(" + startorder + "," + finishorder + ")");
						climbdistance.setAttribute("onmouseover","evidentiateclimb(" + startorder + "," + finishorder + ")");
						climbdifficulty.setAttribute("onmouseover","evidentiateclimb(" + startorder + "," + finishorder + ")");
						climbdifficulty.setAttribute("onmouseover","evidentiateclimb(" + startorder + "," + finishorder + ")");
						climbdenivel.setAttribute("onmouseover","evidentiateclimb(" + startorder + "," + finishorder + ")");

						climbrow.setAttribute("onmouseout","resetclimb()");						
					}
					var climb = {
						start: startorder,
						finish: finishorder,
					}
					climbs.push(climb);
				}
			}			
		}
	}
}

function getMousePos(canvas, evt) {
var rect = canvas.getBoundingClientRect();
	return {
	  x: evt.clientX - rect.left,
	  y: evt.clientY - rect.top,
	  absx: evt.pageX,
	  absy: evt.pageY,
	};
}


function getPosition (targetdistance) {
	for (var i = 0; i < calcroutealt.length; i++) {	
		var position = calcroutealt[i].position;
		var distance = calcroutealt[i].distance;
		if(distance >= targetdistance) {
			return position;
		}
	}	
}

function getPointAltitude (targetdistance) {
	for (var i = 0; i < calcroutealt.length; i++) {	
		var altitude = calcroutealt[i].altitude;
		var distance = calcroutealt[i].distance;
		if(distance >= targetdistance) {
			return altitude;
		}
	}	
	return null;
}

function getSlope(targetdistance) {
	var dx = 0.5;
	var start = 0;
	var end = 0;
	var slope = 0;
	var startfound = 0;
	var endfound = 0;
	if(targetdistance < dx) {
		start = calcroutealt[0];		
		for (var i = 0; i < calcroutealt.length; i++) {	
			var distance = calcroutealt[i].distance;
			if(distance >=dx && !endfound) {
				end = calcroutealt[i];	
				endfound = 1;			
			}
		}		
	} else if (targetdistance > calcroutealt[calcroutealt.length-1].distance - dx) {
		end = calcroutealt[calcroutealt.length-1];
		for (var i = 0; i < calcroutealt.length; i++) {	
			var distance = calcroutealt[i].distance;
			if(distance >= calcroutealt[calcroutealt.length-1].distance - dx && !startfound) {
				start = calcroutealt[i];				
				startfound = 1;
			}
		}			
	} else {
		for (var i = 0; i < calcroutealt.length; i++) {	
			var distance = calcroutealt[i].distance;
			if(targetdistance-distance <= dx/2 && !startfound) {
				start = calcroutealt[i];
				startfound = 1;
			}
			if(distance-targetdistance >= dx/2 && !endfound) {
				end = calcroutealt[i];
				endfound = 1;
			}		
		}				
	}
	var pointdistance = end.distance.toFixed(2) - start.distance.toFixed(2);
	var elevation = end.altitude.toFixed(0) - start.altitude.toFixed(0);
	slope = Number(((0.001*elevation / pointdistance)*100).toFixed(1));		
	return slope;
}

function computeDenivel() {
	var previousaltitude = calcroutealt[0].altitude;
	var totalAlt = 0;
	var totalPosAlt = 0;
	var totalNegAlt = 0;
	for(var i=0; i<calcroutealt.length; i++) {
		var altitude = calcroutealt[i].altitude;
		var dalt = altitude - previousaltitude;
		if(dalt > 0) {
			totalPosAlt = totalPosAlt + dalt;
		}
		if(dalt < 0) {
			totalNegAlt = totalNegAlt - dalt;
		}
		previousaltitude = altitude;
		totalAlt = totalAlt + Math.abs(dalt);
	}	
	document.getElementById('route_distance').innerHTML = calcroutealt[calcroutealt.length-1].distance.toFixed(2) + " Km";
	document.getElementById('route_denivel').innerHTML = totalAlt.toFixed(0) + " m";
	document.getElementById('route_ascentdenivel').innerHTML = totalPosAlt.toFixed(0) + " m";
	document.getElementById('route_descentdenivel').innerHTML = totalNegAlt.toFixed(0) + " m";
}

function gpxExport() {
	var ns1 = 'http://www.topografix.com/GPX/1/0';
	var xsi = 'http://www.w3.org/2001/XMLSchema-instance';
	var doc = document.implementation.createDocument(ns1, 'gpx', null);
	
	doc.documentElement.setAttributeNS(xsi, 'xsi:schemaLocation', 'http://www.topografix.com/GPX/1/0/gpx.xsd');
	
	
	var trk = doc.createElementNS(ns1, 'trk');
	var name = doc.createElementNS(ns1, 'name');
	var trackname = getTrackname();
	name.appendChild(document.createTextNode(trackname));
	trk.appendChild(name);
	var trkseg = doc.createElementNS(ns1, 'trkseg');
	exportroute = calcroutealt;
	for(var i = 0; i < exportroute.length; i++) {
		var trkpt = doc.createElementNS(ns1, 'trkpt');
		trkpt.setAttribute('lat',exportroute[i].position.lat());
		trkpt.setAttribute('lon',exportroute[i].position.lng());
		var ele = doc.createElementNS(ns1, 'ele');
		ele.appendChild(document.createTextNode(exportroute[i].altitude));
		trkpt.appendChild(ele);
		trkseg.appendChild(trkpt);
	}
	trk.appendChild(trkseg);
	doc.documentElement.appendChild(trk);
	
    var serializer = new XMLSerializer();
    var output = serializer.serializeToString(doc);	
	
	var filename = trackname + ".gpx";
	download(filename, output);
	
}

function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);
	console.log(pom['download']);
    pom.click();
}


function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function distancePoints(start, end) {
	var dtor = Math.PI/180;
	var r = 6378.14;
	var rlat1 = start.lat() * dtor;
	var rlon1 = start.lng() * dtor;
	var rlat2 = end.lat() * dtor;
	var rlon2 = end.lng() * dtor;
	
	var dlon = rlon1 - rlon2;
	var dlat = rlat1 - rlat2;
	
	var a = Math.sin(dlat/2) * Math.sin(dlat/2) +
			Math.sin(dlon/2) * Math.sin(dlon/2) * Math.cos(rlat1) * Math.cos(rlat2); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = r * c;	 
	return d;			
}

function encodeQuotes(string) {
	if (!string) {
		return "";
	}	
	return string.replace("'","\'");
}

function decodeQuotes(string) {
	if (!string) {
		return "";
	}
	return string.replace("\'","'");
}
