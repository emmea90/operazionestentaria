// JavaScript Document
function updateStage(tourid, order) {
    var tourselect = document.getElementById("tourselect" + order);	
	var data = new FormData();
	data.append('tourid', tourid);	
	data.append('order', order);	
	data.append('stageid', tourselect.options[tourselect.selectedIndex].value);	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("post", "{U_STAGEUPDATE}", true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		} else {
			var error = document.getElementById("errorbox");	
			error.innerHTML = xmlhttp.responseText;			
		}
	}	
	xmlhttp.send(data);		
}

function maketrackspublic(tourid) {
	var data = new FormData();
	data.append('tourid', tourid);		
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("post", "{U_MAKESTAGEPUBLIC}", true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			alert("{L_TRACKSAREPUBLIC}");
		} else {
			var error = document.getElementById("errorbox");	
			error.innerHTML = xmlhttp.responseText;			
		}
	}	
	xmlhttp.send(data);		
}

function maketracksprivate(tourid) {
	var data = new FormData();
	data.append('tourid', tourid);		
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("post", "{U_MAKESTAGEPRIVATE}", true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			alert("{L_TRACKSAREPRIVATE}");
		} else {
			var error = document.getElementById("errorbox");	
			error.innerHTML = xmlhttp.responseText;			
		}
	}	
	xmlhttp.send(data);		
}

function updatePage() {
	document.location.reload();
}