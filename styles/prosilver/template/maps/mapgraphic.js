// JavaScript Document
function drawProfile() {
	altitudecalls = 0;
	totalcalls = 0;
	var wholeroute = [];
	var filteredroute = [];
	calcroutealt = [];
	var previouspoint = calcroute[0][0];
	
	// Create route
	var distance = 0;
	for(var i=0; i<calcroute.length; i++) {
		for(var j=0; j<calcroute[i].length; j++) {
			var point = {
				position: calcroute[i][j],
				distance: 0,
				altitude: 0,
				canvasx: 0,
				canvasy: 0				
			};
			distance = distance + distancePoints(previouspoint, calcroute[i][j]);
			point.distance = distance; 
			previouspoint = calcroute[i][j];
			wholeroute.push(point);
		}	
	}	
	calcroutealt = wholeroute;
	
	// Filter route
	var previousdistance = 0;
	filteredroute.push(wholeroute[0].position);
	for(var i=0; i<wholeroute.length; i++) {
		if(wholeroute[i].distance - previousdistance >= interpolationdistance || i==wholeroute.length-1) {
			previousdistance = wholeroute[i].distance;
			filteredroute.push(wholeroute[i].position);
		}
	}	
	
	if(distance > 300) {
		alert("Percorso troppo lungo");
	} else {
		var i,j,temparray,chunk = 256;
		var section = 0;
		for (i=0,j=filteredroute.length; i<j; i+=chunk) {
			temparray = filteredroute.slice(i,i+chunk);
			altitudecalls++;
			getAltitude(temparray, altitudecalls);
		}	
	}
}

function plotAltitude(minhundred, maxhundred, start, end) {
	// document.getElementById('altitude-space').style.display='block';
	var profileCanvas =document.getElementById("profileCanvas");
	profileCanvas.height = 290;
	profileCanvas.width = profileCanvas.width;
	var width = profileCanvas.width;
	var height = profileCanvas.height;
	var context = profileCanvas.getContext("2d");	
	var totalslope = maxhundred-minhundred;
	var maxdistance = calcroutealt[calcroutealt.length-1].distance;
	var prevx = (calcroutealt[0].distance/maxdistance)*width;
	var prevy = height - ((calcroutealt[0].altitude-minhundred)/totalslope)*height;
	var firstx = null;
	var route = [];
	var axisposition = height - axiszone;
	
	// Plot route
	context.beginPath();
	for(var i = 0; i<calcroutealt.length; i++) {
		// Debug
		route.push(calcroutealt[i].position); 
		if(calcroutealt[i].distance >= start && calcroutealt[i].distance <= end) {
			var x = ((calcroutealt[i].distance-start)/(end-start))*(width - legendzone) + legendzone;
			var y = (height - axiszone) - ((calcroutealt[i].altitude-minhundred)/totalslope)*(height - axiszone);
			context.lineTo(x,y);
			if(firstx == null) {
				firstx = x;
			}
			calcroutealt[i].canvasx = x;
			calcroutealt[i].canvasy = y;
			prevx = x;
			prevy = y;			
		} else {
			calcroutealt[i].canvasx = null;
			calcroutealt[i].canvasy = null;	
		}
	}		
	context.lineTo(x,height - axiszone);
	context.lineTo(firstx,height - axiszone);
    context.lineWidth = 3;
    context.strokeStyle = '#f7cc3f';
    context.fillStyle = '#fcefc4';
	context.stroke();
	context.fill();	
	context.save();
	context.closePath();	
	
	if(drawclimbs) {
		var begin = 0;
		var km = 0;
		if(drawonlyclimbs) {
			// Draw Slopes for each Km
			for(var i = 0; i < calcroutealt.length; i++) {
				var distance = calcroutealt[i].distance.toFixed(2) - km;
					if(distance >= 1 || i == calcroutealt.length-1) {
						km++;
						var elevation = calcroutealt[i].altitude.toFixed(0) - calcroutealt[begin].altitude.toFixed(0);
						var slope = Number(((0.001*elevation / distance)*100).toFixed(1));			
						var climbcolor;
						if(drawclimbs == 1) {
							if(slope < 4 && slope > -4) {
								climbcolor = "#5dc71f";
							} else if(slope < 6 && slope > -6) {
								climbcolor = "#055b8b";
							} else if(slope < 9 && slope > -9) {
								climbcolor = "#eb102e";
							} else {
								climbcolor = "#050608";
							}
							colorProfile(begin, i, climbcolor, climbcolor);						
						} else if (drawclimbs == 2) {
							if(slope < 4 && slope > -4) {
								climbcolor = "#00ff00";
							} else if(slope < 7 && slope > -7) {
								climbcolor = "#0000ff";
							} else if(slope < 10 && slope > -10) {
								climbcolor = "#ffff00";
							} else {
								climbcolor = "#ff0000";
							}						
							colorProfile(begin, i, climbcolor, climbcolor);							
						}
						begin = i - 1;
					}		
			}	
		}
		// Draw Climbs
		for (var i = 0; i < climbs.length; i++) {
			var startorder = climbs[i].start;
			var finishorder = climbs[i].finish;
			var begin = startorder;
			for (var j = startorder; j <= finishorder; j++) {
				var distance = calcroutealt[j].distance.toFixed(2) - calcroutealt[begin].distance.toFixed(2);
				if(distance >= 1 || j == finishorder) {
					var elevation = calcroutealt[j].altitude.toFixed(0) - calcroutealt[begin].altitude.toFixed(0);
					var slope = Number(((0.001*elevation / distance)*100).toFixed(1));			
					var climbcolor;
					if(drawclimbs == 1) {
						if(slope < 4) {
							climbcolor = "#5dc71f";
						} else if(slope < 6) {
							climbcolor = "#055b8b";
						} else if(slope < 9) {
							climbcolor = "#eb102e";
						} else {
							climbcolor = "#050608";
						}
						colorProfile(begin, j, climbcolor, climbcolor);						
					} else if (drawclimbs == 2) {
						if(slope < 4) {
							climbcolor = "#00ff00";
						} else if(slope < 7) {
							climbcolor = "#0000ff";
						} else if(slope < 10) {
							climbcolor = "#ffff00";
						} else {
							climbcolor = "#ff0000";
						}						
						colorProfile(begin, j, climbcolor, climbcolor);							
					}
					begin = j-1;
				}		
			}			
		}
	}
	
	// Draw Y-Axis
	context.beginPath();
	context.moveTo(legendzone,0);
	context.lineTo(legendzone,height - axiszone);
    context.lineWidth = 0.4;
    context.strokeStyle = '#000';	
	context.stroke();
	context.closePath();
	
	// Draw X-Axis
	context.beginPath();
	context.moveTo(0,axisposition);
	context.lineTo(width,axisposition);
    context.lineWidth = 0.4;
    context.strokeStyle = '#000';	
	context.stroke();
	context.closePath();
	
	// Plot legend
	context.font = "12px Arial";	
	context.textAlign="right"; 
	
	// Find step
	var step = Math.floor(totalslope/8);
	var step = Math.floor(step/50)*50;
	if (step ==0) {
		step = 50;
	}
	for(var i = minhundred; i<=maxhundred; i = i + step) {
		context.beginPath();
		var y = (height - axiszone) - ((i-minhundred)/totalslope)*(height- axiszone);	
		context.moveTo(legendzone,y);
		context.lineTo(width,y);
	    context.lineWidth = 0.4;
   		context.strokeStyle = '#000';	
		context.stroke();
		context.closePath();	
		if(i != minhundred) {
			context.beginPath();
    		context.fillStyle = '#000';
			context.fillText(i + "m", legendzone-4, y);	
			context.closePath();
		}
	}	
	
	// Plot distances
	var step = 10;
	var curdistance = step;
	for(var i = 0; i<calcroutealt.length; i++) {
		if(calcroutealt[i].distance >= curdistance) {
			if(curdistance >= start && curdistance <= end) {
				var x = calcroutealt[i].canvasx;
				context.beginPath();
				context.font = "12px Arial";	
				context.textAlign="center"; 
				context.moveTo(x,0);
				context.lineTo(x,height - axiszone);
				context.lineWidth = 0.4;
				context.strokeStyle = '#000';	
				context.stroke();			
				context.fillText(curdistance, x, height - axiszone + 15);	
				context.closePath();
			}
			curdistance = curdistance + step;
		}
	}		
	
	// Plot sprints 
	plotObjects(context, sprint, stageobjects.stagesprints);
	plotObjects(context, climbHC, stageobjects.stageclimbs);
	plotObjects(context, climbNO, stageobjects.stageclimbs);
	plotObjects(context, climb1, stageobjects.stageclimbs);
	plotObjects(context, climb2, stageobjects.stageclimbs);
	plotObjects(context, climb3, stageobjects.stageclimbs);
	plotObjects(context, climb4, stageobjects.stageclimbs);
	plotObjects(context, cobble, stageobjects.stagecobbles);
	plotObjects(context, intermediate, stageobjects.stageintermediates);
	plotObjects(context, rest, stageobjects.stagerests);

}




function evidentiateclimb (startpoint, endpoint) {
	elaborateZoom();
	colorProfile(startpoint, endpoint, "#cccccc", "#e3e3e3");
}

function colorProfile (startpoint, endpoint, lineColor, profileColor) {
	var start = document.getElementById('fromkm').value;
	var end = document.getElementById('tokm').value;
	if(start=="") {
		start = 0;
	}
	if(end=="") {
		end = calcroutealt[calcroutealt.length-1].distance;
	}
	start = Number(start);
	end = Number(end);	
	var profileCanvas =document.getElementById("profileCanvas");
	var height = profileCanvas.height;
	var context = profileCanvas.getContext("2d");	
	var firstx = calcroutealt[startpoint].canvasx;	
	context.beginPath();	
	for (var i = startpoint; i<endpoint; i++) {
		if(calcroutealt[i].distance >= start && calcroutealt[i].distance <= end) {	
			var x = calcroutealt[i].canvasx;
			if(firstx == null) {
				firstx = x;
			}			
			var y = calcroutealt[i].canvasy;
			context.lineTo(x,y);	
		}
	}
	context.lineTo(x,height - axiszone);
	context.lineTo(firstx,height - axiszone);
	context.lineWidth = 1;
	context.strokeStyle = lineColor;
	context.fillStyle = profileColor;
	context.stroke();	
	context.fill();		
}

function resetclimb () {
	var start = document.getElementById('fromkm').value;
	var end = document.getElementById('tokm').value;
	if(start=="") {
		start = 0;
	}
	if(end=="") {
		end = calcroutealt[calcroutealt.length-1].distance;
	}
	start = Number(start);
	end = Number(end);	
	plotAltitude(minhundred, maxhundred, start, end);	
}