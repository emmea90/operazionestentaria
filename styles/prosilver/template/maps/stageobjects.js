var stageobjects = {
	stagesprints: [],
	stageintermediates: [],
	stagecobbles: [],
	stageclimbs: [],
	stagerests: [],
};
var finishAncor = new google.maps.Point(-20, 40);
var otherAncor = new google.maps.Point(-20, 25);
var climbfinishAncor = new google.maps.Point(-20, 5);

function getSprintCounter () {
	var counter = stageobjects.stagesprints.length + stageobjects.stageintermediates.length + stageobjects.stagecobbles.length + stageobjects.stageclimbs.length + stageobjects.stagerests.length;
	return counter;
}

function updateStartEnd () {
	var depart = document.getElementById('routedepart');
	markers[0].labelContent = depart.value;
	markers[0].labelClass = 'finishlabels';
	markers[0].labelAnchor = finishAncor;
	var arrive = document.getElementById('routearrive');
	markers[markers.length-1].labelContent = arrive.value;
	markers[markers.length-1].labelClass = 'finishlabels';
	markers[markers.length-1].labelAnchor = finishAncor;	
}

function setObjectMarkersDraggables (value) {
	setFieldObjectMarkersDraggables(value, stageobjects.stagesprints);
	setFieldObjectMarkersDraggables(value, stageobjects.stageintermediates);
	setFieldObjectMarkersDraggables(value, stageobjects.stagecobbles);
	setFieldObjectMarkersDraggables(value, stageobjects.stageclimbs);
	setFieldObjectMarkersDraggables(value, stageobjects.stagerests);
}

function removeAllSprintObjects () {
	removeMarkersFromVector(stageobjects.stagesprints);
	removeMarkersFromVector(stageobjects.stageintermediates);
	removeMarkersFromVector(stageobjects.stagecobbles);
	removeMarkersFromVector(stageobjects.stageclimbs);
	removeMarkersFromVector(stageobjects.stagerests);
	updateStageObjects();		
}

function setFieldObjectMarkersDraggables (value, vector) {
	for (var i = 0; i < vector.length; i++) {
		vector[i].marker.setDraggable(value);
	}
}

function removeMarkersFromVector (vector) {
	for (var i = 0; i < vector.length; i++) {
		vector[i].marker.setMap(null);
	}
	vector.length = 0;
}


function updateObjectInfo (marker) {
		var location = marker.getPosition();
		var newlocation = getObjectPosition(location);
		marker.setPosition(newlocation);
		var distance = getDistance(newlocation);
		var sprintvector = stageobjects.stagesprints;	
		if (marker.getIcon()==sprint) {
			sprintvector = stageobjects.stagesprints;
		} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
			sprintvector = stageobjects.stageclimbs;
		} else if (marker.getIcon()==intermediate) {
			sprintvector = stageobjects.stageintermediates;					
		} else if (marker.getIcon()==rest) {
			sprintvector = stageobjects.stagerests;				
		} else if (marker.getIcon()==cobble) {
			sprintvector = stageobjects.stagecobbles;	
		}	
		var index = findIndexByMarker(sprintvector, marker);		
		sprintvector[index].location = location;	
		sprintvector[index].distance = distance;
		updateStageObjects();			
}

function findIndexByMarker (sprintvector, marker) {
	for (var i = 0; i < sprintvector.length; i++) {
		if(sprintvector[i].marker == marker) {
			return i;
		}
	}
}

function insertObjectPoint(marker, counter) {
	var location = marker.getPosition();
	var distance = getDistance(location);
	var objectpoint = {
		id: counter,
		name: null,
		location: location,
		distance: distance,
		marker: marker,
		altitude: getPointAltitude(distance),
		category: null,
		startdistance: null,
		startaltitude: null,
		difficulty: null,
	}
	marker.labelAnchor = otherAncor;
	return objectpoint;
}

function insertObjectPointWithDistance(marker, counter, distance) {
	var location = marker.getPosition();
	var objectpoint = {
		id: counter,
		name: null,
		location: location,
		distance: distance,
		marker: marker,
		altitude: getPointAltitude(distance),
		category: null,
		startdistance: null,
		startaltitude: null,
		difficulty: null,
	}
	marker.labelAnchor = otherAncor;
	return objectpoint;
}

function transformMarker (marker) {
	if(tracktype == 'labels') {
		var objectpoint = getObjectPoint(marker);
		if (marker.getIcon()==sprint) {
			marker.setIcon(climbNO);
			marker.labelClass = "climblabels";
			stageobjects.stageclimbs.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stagesprints);
		} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
			marker.setIcon(intermediate);
			marker.labelClass = "intermediatelabels";
			stageobjects.stageintermediates.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stageclimbs);
		} else if (marker.getIcon()==intermediate) {
			marker.setIcon(rest);
			marker.labelClass = "intermediatelabels";
			stageobjects.stagerests.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stageintermediates);
		} else if (marker.getIcon()==rest) {
			marker.setIcon(cobble);
			marker.labelClass = "cobblelabels";
			stageobjects.stagecobbles.push(objectpoint);
			removeobjectpoint (objectpoint, stageobjects.stagerests);
		} else if (marker.getIcon()==cobble) {
			marker.setIcon(sprint);
			marker.labelClass = "sprintlabels";
			stageobjects.stagesprints.push(objectpoint);		
			removeobjectpoint (objectpoint, stageobjects.stagecobbles);	
		}			
		updateStageObjects();
	}
}

function insertNewMarkerObject (position) {
	sprintcounter++;
	var marker = insertMarker(position, sprint);		
	marker.labelClass = "sprintlabels";
	var objectpoint = insertObjectPoint(marker, sprintcounter);
	stageobjects.stagesprints.push(objectpoint);		
	updateStageObjects();		
}

function insertNewMarkerObjectWithDistance (position, distance) {
	sprintcounter++;
	var marker = insertMarker(position, sprint);		
	marker.labelClass = "sprintlabels";
	var objectpoint = insertObjectPointWithDistance(marker, sprintcounter, distance);
	stageobjects.stagesprints.push(objectpoint);		
	updateStageObjects();		
}

function deleteMarkerObject (marker) {
	marker.setMap(null);
	var vector = null;
	if (marker.getIcon()==sprint) {
		vector = stageobjects.stagesprints;
	} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
		vector = stageobjects.stageclimbs;
	} else if (marker.getIcon()==intermediate) {
		vector = stageobjects.stageintermediates;			
	} else if (marker.getIcon()==rest) {
		vector = stageobjects.stagerests;
	} else if (marker.getIcon()==cobble) {
		vector = stageobjects.stagecobbles;
	}			
	var objectpoint = findobjectpoint(marker, vector);		
	removeobjectpoint (objectpoint, vector);		
	updateStageObjects();	
}

function findobjectpoint (marker, vector) {
	for(var i = 0; i<vector.length; i++) {
		if(vector[i].marker == marker) {
			return vector[i];
		}
	}
}

function getObjectPoint (marker) {
	var vector = null;
	if (marker.getIcon()==sprint) {
		vector = stageobjects.stagesprints;
	} else if (marker.getIcon()==climbNO || marker.getIcon()==climbHC || marker.getIcon()==climb1 || marker.getIcon()==climb2 || marker.getIcon()==climb3 || marker.getIcon()==climb4) {
		vector = stageobjects.stageclimbs;
	} else if (marker.getIcon()==intermediate) {
		vector = stageobjects.stageintermediates;			
	} else if (marker.getIcon()==rest) {
		vector = stageobjects.stagerests;
	} else if (marker.getIcon()==cobble) {
		vector = stageobjects.stagecobbles;
	}		
	var objectpoint = findobjectpoint(marker, vector);		
	return objectpoint;			
}

function updateStageObjects() {	
	var sprintdescr = document.getElementById("tracksprints");
	sprintdescr.innerHTML = "";
	updateSprints();
	updateClimbs();
	updateIntermediates();
	updateCobbles();
	updateRests();
}

function cleanList(listname) {
	var sprintlist = document.getElementById(listname);
	while (sprintlist.firstChild) {
    	sprintlist.removeChild(sprintlist.firstChild);
	}	
	return sprintlist;	
}

function updateCobbles() {
	var cobblelist = cleanList("cobblelist");	
	var sprintdescr = document.getElementById("tracksprints");
	
	var cobblehead = '\
            <tr class="bg1">\
                <td align="center" width="100px" colspan="7"><h3>{L_ROUTECOBBLES}</h3></td>\
            </tr>\
            <tr class="bg1">\
                <td align="center" width="100px"><h4>{L_COBBLESECTOR}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLEDIFFICULTY}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLENAME}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLESTART}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLEEND}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLELENGTH}</h4></td>\
                <td align="center" width="100px"><h4>{L_COBBLELEVATION}</h4></td>\
            </tr>';	
	var cobbleheadref = cleanList("cobblehead");
	if(stageobjects.stagecobbles.length) {
		cobbleheadref.innerHTML = cobblehead;
		if(stageobjects.stagecobbles.length == 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTECOBBLEDESCR}: </strong>';			
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTECOBBLESDESCR}: </strong>';			
		}			
	}
	stageobjects.stagecobbles.sort(distanceSortFunction);	
	for (var i = 0; i < stageobjects.stagecobbles.length; i++) {
		var cobble = stageobjects.stagecobbles[i];
		var cobblerow = cobblelist.insertRow(-1);
		cobblerow.setAttribute("class","bg1");	
		var cobblesector = cobblerow.insertCell(-1);
		var cobbledifficulty = cobblerow.insertCell(-1);
		var cobblename = cobblerow.insertCell(-1);
		var cobblestart = cobblerow.insertCell(-1);		
		var cobbleend = cobblerow.insertCell(-1);		
		var cobblelength = cobblerow.insertCell(-1);		
		var cobbleelevation = cobblerow.insertCell(-1);		
		var name = cobble.name;
		if(!cobble.name) {
			name = "";
		}
		var startdistance = cobble.startdistance;
		var distance = cobble.distance - cobble.startdistance;		
		if(!cobble.startdistance)
		{
			startdistance = "";
			distance = 0;
		}		
		cobblenumber = i + 1;
		cobblesector.innerHTML = cobblenumber;
		var cobbleicon = iconBase + 'Cobblelogo' + cobblenumber + '.gif';
		cobble.marker.setIcon(cobbleicon);
		if(cobble.difficulty == null) {
			cobble.difficulty == 1;	
		}
		var stringdifficulty = "";
		for (var j = 0; j < cobble.difficulty; j++) {
			stringdifficulty = stringdifficulty + "+";
		}
		cobbledifficulty.innerHTML = stringdifficulty;		
		cobblename.innerHTML = unescape(name);
		cobblestart.innerHTML = startdistance + ' Km';
		cobbleend.innerHTML = cobble.distance.toFixed(2) + ' Km';	
		cobblelength.innerHTML = distance.toFixed(2) + ' Km';	
		cobbleelevation.innerHTML = cobble.altitude.toFixed(0) + ' m';
		sprintdescr.innerHTML = sprintdescr.innerHTML + unescape(name) + ' (' + stringdifficulty + ', ' + Math.floor(Number(distance.toFixed(1))*10)*100 + ' m, Km ' + cobble.distance.toFixed(1) + ')';	
		if (i == stageobjects.stagecobbles.length - 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '.<br / >';
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + ', ';
		}				
	}	
						
}

function updateIntermediates() {
	var intermediatelist = cleanList("intermediateslist");
	var sprintdescr = document.getElementById("tracksprints");
	
	var intermediatehead = '\
	        <tr class="bg1">\
                <td align="center" width="100px" colspan="3"><h3>{L_ROUTEINTERMEDIATES}</h3></td>\
            </tr>\
            <tr class="bg1">\
                <td align="center" width="100px"><h4>{L_INTERMEDIATENAME}</h4></td>\
                <td align="center" width="100px"><h4>{L_INTERMEDIATEPOSITION}</h4></td>\
                <td align="center" width="100px"><h4>{L_INTERMEDIATEELEVATION}</h4></td>\
            </tr>';
	
	var intermediateheadref = cleanList("intermediateshead");
	if(stageobjects.stageintermediates.length) {
		intermediateheadref.innerHTML = intermediatehead;
		if(stageobjects.stageintermediates.length == 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTEINTERMEDIATEDESCR}: </strong>';			
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTEINTERMEDIATESDESCR}: </strong>';			
		}				
	}				
	stageobjects.stageintermediates.sort(distanceSortFunction);	
	for (var i = 0; i < stageobjects.stageintermediates.length; i++) {
		var intermediate = stageobjects.stageintermediates[i];
		var intermediaterow = intermediatelist.insertRow(-1);
		intermediaterow.setAttribute("class","bg1");	
		var intermediatename = intermediaterow.insertCell(-1);
		var intermediatefinish = intermediaterow.insertCell(-1);
		var intermediatealtitude = intermediaterow.insertCell(-1);		
		var name = intermediate.name;
		if(!intermediate.name) {
			name = "";
		}
		intermediatename.innerHTML = unescape(name);
		intermediatefinish.innerHTML = intermediate.distance.toFixed(2) + ' Km';
		intermediatealtitude.innerHTML = intermediate.altitude.toFixed(0) + ' m';
		sprintdescr.innerHTML = sprintdescr.innerHTML + unescape(name) + ' (' + intermediate.altitude.toFixed(0) + ' m, Km ' + intermediate.distance.toFixed(1) + ')';
		if (i == stageobjects.stageintermediates.length - 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '.<br / >';
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + ', ';
		}			
	}
}

function updateRests() {
	var restlist = cleanList("restlist");
	var sprintdescr = document.getElementById("tracksprints");
	stageobjects.stagerests.sort(distanceSortFunction);	
	
	var resthead = '<tr class="bg1">\
                <td align="center" width="100px" colspan="3"><h3>{L_ROUTERESTS}</h3></td>\
            </tr>\
            <tr class="bg1">\
                <td align="center" width="100px"><h4>{L_RESTNAME}</h4></td>\
                <td align="center" width="100px"><h4>{L_RESTPOSITION}</h4></td>\
                <td align="center" width="100px"><h4>{L_RESTELEVATION}</h4></td>\
            </tr>';
	
	var restheadref = cleanList("resthead");
	if(stageobjects.stagerests.length) {
		restheadref.innerHTML = resthead;
		if(stageobjects.stagerests.length == 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTERESTDESCR}: </strong>';			
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTERESTSDESCR}: </strong>';			
		}		
	}	
	for (var i = 0; i < stageobjects.stagerests.length; i++) {
		var rest = stageobjects.stagerests[i];
		var restrow = restlist.insertRow(-1);
		restrow.setAttribute("class","bg1");	
		var restname = restrow.insertCell(-1);
		var restfinish = restrow.insertCell(-1);
		var restaltitude = restrow.insertCell(-1);		
		var name = rest.name;
		if(!rest.name) {
			name = "";
		}
		restname.innerHTML = unescape(name);
		restfinish.innerHTML = rest.distance.toFixed(2) + ' Km';
		restaltitude.innerHTML = rest.altitude.toFixed(0) + ' m';
		sprintdescr.innerHTML = sprintdescr.innerHTML +  unescape(name) + ' (' + rest.altitude.toFixed(0) + ' m, Km ' + rest.distance.toFixed(1) + ')';
		if (i == stageobjects.stagerests.length - 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '.<br / >';
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + ', ';
		}		
	}
}

function updateSprints() {
	var sprintlist = cleanList("sprintlist");
	var sprintdescr = document.getElementById("tracksprints");
	stageobjects.stagesprints.sort(distanceSortFunction);	
	
	var sprinthead = '\
	<tr class="bg1">\
		<td align="center" width="100px" colspan="3"><h3>{L_ROUTESPRINTS}</h3></td>\
	</tr>\
		<tr class="bg1">\
		<td align="center" width="100px"><h4>{L_SPRINTNAME}</h4></td>\
		<td align="center" width="100px"><h4>{L_SPRINTPOSITION}</h4></td>\
		<td align="center" width="100px"><h4>{L_SPRINTELEVATION}</h4></td>\
    </tr>';
	
	var sprintheadref = cleanList("sprinthead");
			
	if(stageobjects.stagesprints.length) {
		sprintheadref.innerHTML = sprinthead;
		if(stageobjects.stagesprints.length == 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTESPRINTDESCR}: </strong>';			
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTESPRINTSDESCR}: </strong>';			
		}
	}
	
	for (var i = 0; i < stageobjects.stagesprints.length; i++) {
		var sprint = stageobjects.stagesprints[i];
		var sprintrow = sprintlist.insertRow(-1);
		sprintrow.setAttribute("class","bg1");	
		var sprintname = sprintrow.insertCell(-1);
		var sprintfinish = sprintrow.insertCell(-1);
		var sprintaltitude = sprintrow.insertCell(-1);		
		var name = sprint.name;
		if(!sprint.name) {
			name = "";
		}
		sprintname.innerHTML = unescape(name);
		sprintfinish.innerHTML = sprint.distance.toFixed(2) + ' Km';
		sprintaltitude.innerHTML = sprint.altitude.toFixed(0) + ' m';
		sprintdescr.innerHTML = sprintdescr.innerHTML +  unescape(name) + ' (' + sprint.altitude.toFixed(0) + ' m, Km ' + sprint.distance.toFixed(1) + ')';
		if (i == stageobjects.stagesprints.length - 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '.<br / >';
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + ', ';
		}
	}	
}

function updateClimbs() {
	var climblist = cleanList("climbfinlist");
	var sprintdescr = document.getElementById("tracksprints");
	stageobjects.stageclimbs.sort(distanceSortFunction);
	
	var climbhead ='\
	<tr class="bg1">\
		<td align="center" width="100px" colspan="8"><h3>{L_ROUTECLIMBS}</h3></td>\
	</tr>\
	<tr class="bg1">\
		<td align="center" width="100px"><h4>{L_CLIMBNAME}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBSTART}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBFINISH}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBSTARTELE}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBFINISHELE}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBDISTANCE}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBCATEGORY}</h4></td>\
		<td align="center" width="100px"><h4>{L_CLIMBAVERAGE}</h4></td>\
	</tr>';
	var climbheadref = cleanList("climbhead");	
	
	if(stageobjects.stageclimbs.length) {
		climbheadref.innerHTML = climbhead;
		climbheadref.setAttribute("onclick","drawclimbs=0; zoomsector(" + 0 + "," + calcroutealt[calcroutealt.length-1].distance + ")");	
		if(stageobjects.stageclimbs.length == 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTEGPMDESCR}: </strong>';
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '<strong>{L_ROUTEGPMSDESCR}: </strong>';			
		}
	}
	for (var i = 0; i < stageobjects.stageclimbs.length; i++) {
		var climb = stageobjects.stageclimbs[i];
		var climbrow = climblist.insertRow(-1);
		climbrow.setAttribute("class","bg1");	
		var climbname = climbrow.insertCell(-1);
		var climbstart = climbrow.insertCell(-1);
		var climbfinish = climbrow.insertCell(-1);
		var climbstartele = climbrow.insertCell(-1);
		var climbfinishele = climbrow.insertCell(-1);
		var climbdistance = climbrow.insertCell(-1);
		var climbcategory = climbrow.insertCell(-1);
		var climbaverage = climbrow.insertCell(-1);	
		var startele = getPointAltitude(climb.startdistance);
		var distance = climb.distance - climb.startdistance;		
		var slope = 0.1*((climb.altitude - startele) / distance);
		slopedescr = slope.toFixed(1);
		slope = slope.toFixed(2);
		var name = climb.name;
		var startdistance = climb.startdistance;
		if(!climb.name) {
			name = "";
		}
		if(!climb.startdistance)
		{
			startdistance = "";
			slope = "";
			distance = 0;
		}
		climbname.innerHTML = unescape(name);
		climbstart.innerHTML = startdistance + ' Km';
		climbfinish.innerHTML = climb.distance.toFixed(2) + ' Km';
		climbstartele.innerHTML = startele +' m';
		climbfinishele.innerHTML = climb.altitude +' m';
		climbdistance.innerHTML = distance.toFixed(2) +' Km';
		sprintdescr.innerHTML = sprintdescr.innerHTML + unescape(name) + ' (';
		if(climb.category == 6 || !climb.category) {
			climbcategory.innerHTML = '{L_NOC}';
		} else if(climb.category == 5) {
			climbcategory.innerHTML = '{L_HC}';
			sprintdescr.innerHTML = sprintdescr.innerHTML + '{L_HC}, ';
		} else if(climb.category == 1) {
			climbcategory.innerHTML = '{L_1C}';
			sprintdescr.innerHTML = sprintdescr.innerHTML + '{L_1C}, ';
		} else if(climb.category == 2) {
			climbcategory.innerHTML = '{L_2C}';
			sprintdescr.innerHTML = sprintdescr.innerHTML + '{L_2C}, ';
		} else if(climb.category == 3) {
			climbcategory.innerHTML = '{L_3C}';
			sprintdescr.innerHTML = sprintdescr.innerHTML + '{L_3C}, ';
		} else if(climb.category == 4) {
			climbcategory.innerHTML = '{L_4C}';
			sprintdescr.innerHTML = sprintdescr.innerHTML + '{L_4C}, ';
		}								
		climbaverage.innerHTML = slope +' %';
		if(Math.abs(Number(climb.distance.toFixed(1)) - Number('{TRACKDISTANCE}'))<1.5) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + climb.altitude + ' m, ' + distance.toFixed(1) + ' Km {L_AT} ' + slopedescr + '%, {L_ARRIVE})';			
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + climb.altitude + ' m, ' + distance.toFixed(1) + ' Km {L_AT} ' + slopedescr + '%, Km ' + climb.distance.toFixed(1) + ')';
		}
		if (i == stageobjects.stageclimbs.length - 1) {
			sprintdescr.innerHTML = sprintdescr.innerHTML + '.<br / >';
		} else {
			sprintdescr.innerHTML = sprintdescr.innerHTML + ', ';
		}				
		climbrow.setAttribute("onclick","drawclimbs=1; zoomsector("+startdistance+","+climb.distance.toFixed(2)+")");	
	}
}

function setClimbCategory (climbid, value) {
	for (var i = 0; i < stageobjects.stageclimbs.length; i++) {
		var climb = stageobjects.stageclimbs[i];
		if(climb.id == climbid) {
			climb.category = value.toString();
			switch (value) {
				case 1:
					climb.marker.setIcon(climb1);
					break;
				case 2:
					climb.marker.setIcon(climb2);
					break;
				case 3:
					climb.marker.setIcon(climb3);
					break;
				case 4:
					climb.marker.setIcon(climb4);
					break;
				case 5:
					climb.marker.setIcon(climbHC);
					break;
				case 6:
				default:
					climb.marker.setIcon(climbNO);
					break;				
			}
			updateClimbs();
			elaborateZoom();
			return;
		}
	}
}

function setCobbleDifficulty (cobbleid, value) {
	for (var i = 0; i < stageobjects.stagecobbles.length; i++) {
		var cobble = stageobjects.stagecobbles[i];
		if(cobble.id == cobbleid) {
			cobble.difficulty = value.toString();
			updateCobbles();
			elaborateZoom();
			return;
		}
	}
}

function setElementName (elementid, rowname, vector) {
	var name = document.getElementById(rowname + elementid).value;
	for (var i = 0; i < vector.length; i++) {
		var element = vector[i];
		if(element.id == elementid) {
			element.name = unescape(name);
			element.marker.labelContent = name;
			elaborateZoom();
			return;
		}
	}		
}

function setIntermediateName (intermediateid) {
	setElementName (intermediateid, 'intermediatename', stageobjects.stageintermediates);
	updateIntermediates();
}

function setRestName (restid) {
	setElementName (restid, 'restname', stageobjects.stagerests);
	updateRests();
}

function setCobbleName (cobbleid) {
	setElementName (cobbleid, 'cobblename', stageobjects.stagecobbles);
	updateCobbles();
}

function setSprintName (sprintid) {
	setElementName (sprintid, 'sprintname', stageobjects.stagesprints);
	updateSprints();
}

function setClimbName (climbid) {
	setElementName (climbid, 'climbname', stageobjects.stageclimbs);
	updateClimbs();
}

function setClimbDistance (climbid) {
	var distance = document.getElementById('climbstart' + climbid).value
	for (var i = 0; i < stageobjects.stageclimbs.length; i++) {
		var climb = stageobjects.stageclimbs[i];
		if(climb.id == climbid) {
			climb.startdistance = Number(distance);
			updateClimbs();
			return;
		}
	}
}

function setCobbleDistance (cobbleid) {
	var distance = document.getElementById('cobblestart' + cobbleid).value
	for (var i = 0; i < stageobjects.stagecobbles.length; i++) {
		var cobble = stageobjects.stagecobbles[i];
		if(cobble.id == cobbleid) {
			cobble.startdistance = Number(distance);
			updateCobbles();
			return;
		}
	}
}

function distanceSortFunction (a, b) {
	if (a.distance > b.distance) {
		return 1;
	} else {
		return -1;
	}
}

function getDistance (location) {
	var lessdistance = 300;
	var lesskilometers = null;
	for (var i = 0; i < calcroutealt.length; i++) {	
		var position = calcroutealt[i].position;
		var distance = calcroutealt[i].distance;
		var dx = distancePoints(position, location);
		if(dx < lessdistance) {
			lessdistance = dx;
			lesskilometers = distance;			
		}
	}	
	return lesskilometers;
}

function getObjectPosition (location) {
	var lessdistance = 300;
	var lessposition = null;
	for (var i = 0; i < calcroutealt.length; i++) {	
		var position = calcroutealt[i].position;
		var distance = calcroutealt[i].distance;
		var dx = distancePoints(position, location);
		if(dx <= lessdistance) {
			lessdistance = dx;
			lessposition = position;
		} 	
	}	
	return lessposition;	
}

function removeobjectpoint (point, objectvector) {
	var index = myIndexOf(objectvector, point);
	if (index > -1) {
    	objectvector.splice(index, 1);
	}
}

function myIndexOf(arr, o) {    
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].id == o.id) {
            return i;
        }
    }
    return -1;
}

function plotObjects(context, imagesource, vector) {
	var j = 0;
	var sprintimage = new Image();
	var cobblesector = [];
	var icon = imagesource.substr(0, imagesource.length - 4) + "profile" + imagesource.substr(imagesource.length - 4);
	sprintimage.src = icon;
	sprintimage.onload = function(){
		var sprintwidth = sprintimage.width;
		var sprintheight = sprintimage.height;	
		for (var i = 0; i < vector.length; i++) {
			var startdistance = vector[i].startdistance;
			if(vector[i].marker.getIcon()==imagesource) {
				var distance = vector[i].distance;
				while(calcroutealt[j].distance < distance) {
					j++;
					if(startdistance && vector[i].marker.labelClass=="cobblelabels" && calcroutealt[j].distance > startdistance) {
						cobblesector.push(calcroutealt[j].position);
					}					
				}
				if(vector[i].marker.getIcon()==climbHC || vector[i].marker.getIcon()==climbNO || vector[i].marker.getIcon()==climb1 || vector[i].marker.getIcon()==climb2 || vector[i].marker.getIcon()==climb3 || vector[i].marker.getIcon()==climb4) {
					displaySprintImage(context, sprintimage, calcroutealt[j].canvasx, calcroutealt[j].canvasy, calcroutealt[j], vector[i].name);						
				} else {
					displaySprintImage(context, sprintimage, calcroutealt[j].canvasx, calcroutealt[j].canvasy, calcroutealt[j], vector[i].name.toUpperCase());	
				}	
				if(vector[i].marker.labelClass=="cobblelabels") {
					var line = new google.maps.Polyline({
						path: cobblesector,
						strokeColor: "#aaa",
						strokeOpacity: .9,
						strokeWeight: 8,					 
					});		
					line.setMap(map);								
				}								
			}
		}	
	}
}

function displaySprintImage (context, image, x, y, object, name, isclimb) {
	var sprintwidth = image.width;
	var sprintheight = image.height;
	var profileCanvas = document.getElementById("altitudeCanvas");
	var maxwidhth = profileCanvas.width - rightmargin;		
	var maxheight = profileCanvas.height ;		
	if(x + 0.5*sprintwidth >= maxwidhth) {
		context.drawImage(image, maxwidhth - sprintwidth - 2, toplinemargin + 36 + 20 + 1);	
	} else {
		if(name.length <= 12) {
			context.drawImage(image, x - 0.5*sprintwidth, y - 53 - 0.5*sprintheight);	
			context.beginPath();
			context.moveTo(x,  y - 8 - 0.5*sprintheight);	
			context.lineTo(x, object.canvasy);	
			context.lineWidth = 1;
			context.strokeStyle = '#000';	
			context.stroke();		
			context.closePath();			
			context.beginPath();
			context.moveTo(x, object.canvasy);
			context.lineTo(x, maxheight - bottommargin);	
			context.lineWidth = 1;
			context.strokeStyle = '#fff';	
			context.stroke();	
			context.closePath();			
			context.beginPath();
			context.font = "bold 11px Arial";	
			context.textAlign="center"; 
			context.fillStyle = '#fff';
			context.fillText(Number(object.distance).toFixed(0), x, maxheight - bottomborder - 3);	
			context.closePath();	
			context.beginPath();
			context.font = "10px Arial";	
			context.textAlign="center"; 
			context.fillStyle = '#000';
			context.fillText(object.altitude + " m", x, y - 23 - 0.5*sprintheight);	
			context.fillText(unescape(name), x, y - 12 - 0.5*sprintheight);	
			context.closePath();			
		} else {
			context.drawImage(image, x - 0.5*sprintwidth, y - 63 - 0.5*sprintheight);	
			context.beginPath();
			context.moveTo(x,  y - 8 - 0.5*sprintheight);	
			context.lineTo(x, object.canvasy);	
			context.lineWidth = 1;
			context.strokeStyle = '#000';	
			context.stroke();		
			context.closePath();			
			context.beginPath();
			context.moveTo(x, object.canvasy);
			context.lineTo(x, maxheight - bottommargin);	
			context.lineWidth = 1;
			context.strokeStyle = '#fff';	
			context.stroke();	
			context.closePath();			
			context.beginPath();
			context.font = "bold 11px Arial";	
			context.textAlign="center"; 
			context.fillStyle = '#fff';
			context.fillText(Number(object.distance).toFixed(0), x, maxheight - bottomborder - 3);	
			context.closePath();	
			context.beginPath();
			context.font = "10px Arial";	
			context.textAlign="center"; 
			context.fillStyle = '#000';
			context.fillText(object.altitude + " m", x, y - 34 - 0.5*sprintheight);	
			var namevector = unescape(name).split(" ");
			var words = namevector.length;
			var firstpart = [];
			var secondpart = [];
			firstpart.push(" ");
			secondpart.push(" ");			
			for (var i = 0; i < words; i++) {
				if(i < Math.floor(words) / 2) {		
					firstpart.push(namevector[i]);
					firstpart.push(" ");					
				} else {	
					secondpart.push(namevector[i]);
					secondpart.push(" ");							
				}

			}
			firstpart = firstpart.join("");
			secondpart = secondpart.join("");
			if(firstpart.length > 12 && secondpart.length < 4) {
				namevector = unescape(name).split("-");
				words = namevector.length;
				firstpart = [];
				secondpart = [];
				firstpart.push(" ");
				secondpart.push(" ");			
				for (var i = 0; i < words; i++) {
					if(i < Math.floor(words) / 2) {
						firstpart.push(namevector[i]);
						firstpart.push(" ");					
					} else {
						secondpart.push(namevector[i]);
						secondpart.push(" ");							
					}
	
				}
				firstpart = firstpart.join("");
				secondpart = secondpart.join("");			
			}
			context.fillText(firstpart, x, y - 23 - 0.5*sprintheight);			
			context.fillText(secondpart, x, y - 12 - 0.5*sprintheight);				
		}
	}
	
}
