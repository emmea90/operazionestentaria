// JavaScript Document
var finishAncor = new google.maps.Point(-15, 20);
var bigfinishAncor = new google.maps.Point(-20, 40);

google.setOnLoadCallback(function() {
  $(function() {
	initialize();
	loadTour();
  });
});

function loadTour() {
	var lineSymbol = {
	  path: 'M 0,-1 0,1',
	  strokeOpacity: 1,
	  scale: 4
	};	
	
	// Load stages
	var bounds = new google.maps.LatLngBounds();
	var end = 0;
	var previousarrive = 0;
	var isfirst = 1;
	<!-- BEGIN stages -->
	<!-- IF stages.CANVIEWSTAGE and stages.STAGEROUTE -->
	var calcroute = JSON.parse('{stages.STAGEROUTE}');
	var points = [];
	var transfer = [];
	for(var i = 0; i < calcroute.length; i++) {
		for(var j = 0; j < calcroute[i].length; j++) {
			var p = new google.maps.LatLng(calcroute[i][j].k, calcroute[i][j].A);
			calcroute[i][j] = p;
			points.push(p);
			bounds.extend(p);
		}
	}
	var startlat = calcroute[0][0].k;
	var startlong = calcroute[0][0].A;
	if (!startlong) {
		startlong = calcroute[0][0].B;
	}
	var start = new google.maps.LatLng(startlat, startlong);
	transfer.push(end);
	transfer.push(start);
	currentdepart = "{stages.STAGEDEPART}";
	if (isfirst) {
		startmarker = addMarker(start, depart, "{stages.STAGEDEPART}", "finishlabels", bigfinishAncor);	
		isfirst = 0;	
	} else {
		// Use the previous end
		if (end) {
			if (previousarrive.toLowerCase()!=currentdepart.toLowerCase()) {
				var line = new google.maps.Polyline({
				  path: transfer,
				  strokeOpacity: 0,
				  icons: [{
					icon: lineSymbol,
					offset: '0',
					repeat: '20px'
				  }],
				  map: map
				});		
				var routeairdistance = distancePoints (previousstart, end);
				if (routeairdistance > 2) {
					endmarker = addMarker(end, arriveplain, previousarrive, "finishminilabels", finishAncor);
				}									
				startmarker = addMarker(start, departplain, "{stages.STAGEDEPART}", "finishminilabels", finishAncor);	
			} else {
				midmarker = addMarker(end, departarriveplain, previousarrive, "finishminilabels", finishAncor);			
			}
		}		
	}

	var endlat = calcroute[calcroute.length-1][calcroute[calcroute.length-1].length-1].k;
	var endlong = calcroute[calcroute.length-1][calcroute[calcroute.length-1].length-1].A;
	if (!endlong) {
		endlong = calcroute[calcroute.length-1][calcroute[calcroute.length-1].length-1].B;
	}
	var end = new google.maps.LatLng(endlat, endlong);
	previousarrive = "{stages.STAGEARRIVE}";
	previousstart = start;

	var poly = new google.maps.Polyline({
		 // use your own style here
		 path: points,
		 strokeColor: "#FFCC33",
		 strokeOpacity: .9,
		 strokeWeight: 8,
	});	
	poly.setMap(map);		
	<!-- ENDIF -->
	<!-- END stages -->
	if (end) {
		endmarker = addMarker(end, arrive, previousarrive, "finishlabels", bigfinishAncor);		
	}	
	map.fitBounds(bounds);
}

function handleNoGeolocation(errorFlag) {
  var options = {
    map: map,
    position: new google.maps.LatLng(48.865497, 2.321022),
	mapTypeId: google.maps.MapTypeId.TERRAIN		
  };
  map.setCenter(options.position);
}

function addMarker(location, icon, content, lblclass, lblAnchor) {
	var marker = new MarkerWithLabel({
		position: location,
		icon: icon,
		map: map,
		draggable: false,
		raiseOnDrag: false,
		labelContent: content,
		labelAnchor: lblAnchor,
		labelClass: lblclass, // the CSS class for the label
		labelInBackground: true					
	});
	return marker;
}