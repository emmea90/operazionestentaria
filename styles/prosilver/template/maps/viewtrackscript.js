var startmarker;
var endmarker;
var drawclimbs=0;
var finishAncor = new google.maps.Point(-20, 40);

// Provisorian variables
var departLocation = "{TRACKDEPART}";
var arriveLocation = "{TRACKARRIVE}";
var servicecontrol = 1;

// Canvas info
var bottommargin = 40;
var bottomborder = 26;
var profiledefaultmargin = 10;
var toplinemargin = 5;
var bottomlinemargin = 5;
var linelong = 15;
var leftmargin = 7;
var rightmargin = 7;

// Route type
var routeType = "{TRACKRACETYPE}";
var trackType = Number("{TRACKTYPE}");
			
$(document).ready(function(){
	initialize();
	loadTrack();
	elaborateRoute();
	generateCanvasEvents();
});

function loadTrack() {
	var calcrouteStringAlt = '{TRACKROUTEALT}';
	var path = [];
	calcroutealt = JSON.parse(calcrouteStringAlt);	
	var bounds = new google.maps.LatLngBounds();
	// Re-parse positions
	for(var i=0; i<calcroutealt.length; i++) {
		var poslat = calcroutealt[i].position.k;
		var poslong = calcroutealt[i].position.A;
		if(!poslong) {
			poslong = calcroutealt[i].position.B;
		}
		calcroutealt[i].position = new google.maps.LatLng(poslat, poslong);
		bounds.extend(calcroutealt[i].position);
		path.push(calcroutealt[i].position);
	}
	startmarker = addMarker(calcroutealt[0].position, depart, departLocation, "finishlabels", finishAncor);
	endmarker = addMarker(calcroutealt[calcroutealt.length-1].position, arrive, arriveLocation, "finishlabels", finishAncor);

	var poly = new google.maps.Polyline({
		 // use your own style here
		 path: path,
		 strokeColor: "#FFCC33",
		 strokeOpacity: .9,
		 strokeWeight: 8
	});
	poly.setMap(map);
	importObjects();
	map.fitBounds(bounds);	 
}

function insertMarker(location, icon) {
	var marker = new MarkerWithLabel({
		position: location,
		icon: icon,
		map: map,
		draggable: false
	});	
	return marker;
}

// Function for adding a marker to the page.
function addMarker(location, icon, content, lblclass, lblAnchor) {
	var marker = new MarkerWithLabel({
		position: location,
		icon: icon,
		map: map,
		draggable: false,
		raiseOnDrag: false,
		labelContent: content,
		labelAnchor: lblAnchor,
		labelClass: lblclass, // the CSS class for the label
		labelInBackground: true					
	});
	return marker;
}

function elaborateRoute() {
	// Compute denivel 
	computeDenivel();

	var minelevation = 8000;
	var maxelevation = 0;
	// Fill data
	for (var i = 0; i < calcroutealt.length; i++) {		
		var elevation = calcroutealt[i].altitude;
		if(elevation <= minelevation) {
			minelevation = elevation.toFixed(2);
		}
		if(elevation >= maxelevation) {
			maxelevation = elevation.toFixed(2);
		}		
	}
	
	// Let fix elevations
	if(minelevation < 0) {
		minhundred = -100;
	} else {
		minhundred = 100*Math.floor(minelevation/100) - 100;
	}
	maxhundred = 100*Math.floor(maxelevation/100) + 100;
	if (minhundred < 0) {
		minhundred = 0;
	}
	if (maxhundred-minhundred < 1000) {
		maxhundred = minhundred + 1000;	
	} else if (trackType == 2) {
		maxhundred = maxhundred + 800;
	} else if (trackType == 3) {
		maxhundred = maxhundred + 600;
	} 			
	
	plotLastKms();
	findclimbs();
	plotAltitude(minhundred, maxhundred, 0, calcroutealt[calcroutealt.length-1].distance);	
}

function plotLastKms() {
	var bottomLastKmMargin = 5;
	var bottomProfileMargin = 26;
	var topLastKmMargin = 100;
	var imageMargin = 20;
	var lastKmLineLength = 40;
	var leftLastKmMargin = 47;
	var rightLastKmMargin = 30;
	var defaultSizeMargin = 20;
	
	var last5KmRouteAlt = [];
	var length = calcroutealt[calcroutealt.length-1].distance;
	
	if(length > 5) {
		for (var k = calcroutealt.length-1; calcroutealt[k].distance > length-5; k--) {
			var newPoint = jQuery.extend({}, calcroutealt[k]);
			last5KmRouteAlt.unshift(newPoint);
		}
	} else {
		last5KmRouteAlt = calcroutealt;
	}

	// Recalc Min and Max last5KmRouteAlt
	var minelevation = 8000;
	var maxelevation = 0;
	for (var i = 0; i < last5KmRouteAlt.length; i++) {		
		var elevation = last5KmRouteAlt[i].altitude;
		if(elevation <= minelevation) {
			minelevation = elevation.toFixed(2);
		}
		if(elevation >= maxelevation) {
			maxelevation = elevation.toFixed(2);
		}		
	}	
	
	var localMinHundred;
	var localMaxHundred;
	
	// Let fix elevations
	localMinHundred = 100*Math.floor(minelevation/100) - 100;
	localMaxHundred = 100*Math.floor(maxelevation/100) + 100;
	if (localMinHundred < 0) {
		localMinHundred = 0;
	} else 	if (localMaxHundred - localMinHundred < 500) {
		localMaxHundred = 500 + localMinHundred;
	}
	
	var localTotalSlope = localMaxHundred-localMinHundred;
	
	var background = new Image();
	background.src = getProfileImage(); 
	background.onload = function(){
		var pattern = context.createPattern(this, "repeat");
		context.fillStyle = pattern;
		context.fill();

		// Draw Km
		var distance = last5KmRouteAlt[last5KmRouteAlt.length-1].distance;
		var distanceToPlot = 1;
		var distanceMaxToPlot = 5;
		var haveInserted = {};
		
		for(var i = 0; i < distanceMaxToPlot; i++) {
			haveInserted[i] = 0;
		}
		
		for(var i = last5KmRouteAlt.length-1; i>0; i--) {
			if(distance - distanceToPlot >= last5KmRouteAlt[i].distance && haveInserted[distanceToPlot]==0) {
				context.font = "bold 20px Arial";	
				context.textAlign="center"; 
				context.fillStyle = '#fff';
				context.fillText(distanceToPlot.toString(), last5KmRouteAlt[i].canvasx, height - bottomLastKmMargin - 2);			
				haveInserted[distanceToPlot]=1;
				if(distanceToPlot==1) {
					var firstKmX = last5KmRouteAlt[i].canvasx;
					var firstKmY = last5KmRouteAlt[i].canvasy;
					var firstKmalt = last5KmRouteAlt[i].altitude.toFixed(0);
					context.beginPath();	
					context.moveTo(last5KmRouteAlt[i].canvasx, height - bottomProfileMargin);
					context.lineTo(last5KmRouteAlt[i].canvasx, last5KmRouteAlt[i].canvasy - lastKmLineLength);
					context.lineWidth = 2;
					context.strokeStyle = '#82182f';
					context.stroke();
					lastkm = new Image();
					lastkm.src = lastkmprofile;
					lastkm.onload = function(){
						var lastkmwidth = lastkm.width;
						var lastkmheight = lastkm.height;						
						context.drawImage(lastkm, firstKmX - 0.5*lastkmwidth, firstKmY - lastKmLineLength - imageMargin - 0.5*lastkmheight);
						context.save();
						var lastKmAltitudeText = firstKmalt + " m";
						var lastKmAltitudeTextLenght = context.measureText(lastKmAltitudeText).width;
						context.translate(firstKmX + 5, firstKmY - lastKmLineLength - 1.2*lastkmheight);
						context.rotate(-0.5*Math.PI);
						context.font = "normal 16px Arial";	
						context.textAlign="start"; 
						context.fillStyle = '#000';	
						context.fillText(lastKmAltitudeText, 0, 0);		
						context.restore();
					}				
					
				}
				distanceToPlot++;
			}
		}		
		context.beginPath();
		context.font = "bold 20px Arial";	
		context.textAlign="end"; 
		context.fillStyle = '#000';
		context.fillText(distanceMaxToPlot, leftLastKmMargin - 2, height - bottomLastKmMargin - 2);			
		arrivelastkm = new Image();
		arrivelastkm.src = arrivelastkmprofile;
		arrivelastkm.onload = function(){
			context.save();
			var lastKmAltitudeText = last5KmRouteAlt[last5KmRouteAlt.length-1].altitude.toFixed(0) + " m";
    		var lastKmAltitudeTextLenght = context.measureText(lastKmAltitudeText).width;
			context.translate(last5KmRouteAlt[last5KmRouteAlt.length-1].canvasx + 5, last5KmRouteAlt[last5KmRouteAlt.length-1].canvasy - 10);
			context.rotate(-0.5*Math.PI);
			context.font = "normal 16px Arial";	
			context.textAlign="start"; 
			context.fillStyle = '#000';	
			context.fillText(lastKmAltitudeText, 0, 0);		
			context.restore();
			var arrivelastkmwidth = arrivelastkm.width;
			var arrivelastkmheight = arrivelastkm.height;						
			context.drawImage(arrivelastkm, last5KmRouteAlt[last5KmRouteAlt.length-1].canvasx - 0.5*arrivelastkmwidth, last5KmRouteAlt[last5KmRouteAlt.length-1].canvasy - 1.5*arrivelastkmheight - lastKmAltitudeTextLenght - 10);			
		}		
		
	// Plot grid
	
	// Find step
	var step = Math.floor(localTotalSlope/5);
	var step = Math.floor(step/50)*50;
	if (step ==0) {
		step = 50;
	}
	for(var i = localMinHundred; i<=localMaxHundred; i = i + step) {
		context.beginPath();
		var y = (height - bottomProfileMargin) - ((i-localMinHundred)/localTotalSlope)*(height- bottomProfileMargin);	
		if(i != localMinHundred) {
		context.moveTo(leftLastKmMargin - 2, y);
		context.lineTo(width,y);
		context.lineWidth = 2;
		context.strokeStyle = '#fff';	
		context.stroke();
		context.closePath();	
			context.beginPath();
			context.font = "normal 12px Arial";	
			context.textAlign="end"; 
			context.fillStyle = '#000';
			context.fillText(i + "m", leftLastKmMargin-2, y);	
			context.closePath();
		}
	}			
		
	};		

	var lastKmsCanvas = document.getElementById("lastKmsCanvas");
	lastKmsCanvas.width = lastKmsCanvas.width;
	var width = lastKmsCanvas.width;
	var height = lastKmsCanvas.height;
	var profileSize = height - bottomProfileMargin - topLastKmMargin - imageMargin - defaultSizeMargin;
	var context = lastKmsCanvas.getContext("2d");		
	var maxdistance = last5KmRouteAlt[last5KmRouteAlt.length-1].distance - last5KmRouteAlt[0].distance;
	var prevx = (last5KmRouteAlt[0].distance/maxdistance)*width;
	var prevy = height - ((last5KmRouteAlt[0].altitude-minhundred)/localTotalSlope)*height;	
	
	var firstx = null;
	var route = [];
	var axisposition = height - bottomLastKmMargin;
	
	// Plot km axis
	context.beginPath();
	context.rect(leftLastKmMargin - 1, height-bottomLastKmMargin, width-rightLastKmMargin-leftLastKmMargin +1, bottomLastKmMargin-bottomProfileMargin);
	context.fillStyle = getAxisColor();
	context.fill(); 	
	
	var start = last5KmRouteAlt[0].distance;
	var end = last5KmRouteAlt[last5KmRouteAlt.length-1].distance;
	
	// Calculate x-y
	for(var i = 0; i<last5KmRouteAlt.length; i++) {
		// Debug
		route.push(last5KmRouteAlt[i].position); 
		var x = ((last5KmRouteAlt[i].distance-start)/(end-start))*(width - leftLastKmMargin - rightLastKmMargin) + leftLastKmMargin;
		var y = height - ((last5KmRouteAlt[i].altitude-localMinHundred)/localTotalSlope)*(profileSize) - bottomProfileMargin - defaultSizeMargin;
		if(firstx == null) {
			firstx = x;
		}
		last5KmRouteAlt[i].canvasx = x;
		last5KmRouteAlt[i].canvasy = y;
		prevx = x;
		prevy = y;			
	}			
	

	
	// Draw profile
	var step = 0.5
	var lastplotdist = 0;
	context.beginPath();	
	for(var i = 0; i<last5KmRouteAlt.length; i++) {
		var x = last5KmRouteAlt[i].canvasx;
		var y = last5KmRouteAlt[i].canvasy;	
		if(last5KmRouteAlt[i].distance -last5KmRouteAlt[lastplotdist].distance >= step || i == 0 || i == last5KmRouteAlt.length - 1) {
			lastplotdist = i;
			context.lineTo(x,y);
		}
	}	
	
	context.lineTo(x, height - bottomProfileMargin);
	context.lineTo(firstx, height - bottomProfileMargin);
	context.closePath();
    context.lineWidth = 2;
    context.strokeStyle = getLinesColor();
	context.stroke();
	context.save();
}

function getTrackname() {
	return "{TRACKNAME}";
}

function plotAltitude(minhundred, maxhundred, start, end) {
	var background = new Image();
	background.src = getProfileImage();
	background.onload = function(){
	
	var profileCanvas = document.getElementById("altitudeCanvas");
	profileCanvas.width = profileCanvas.width;
	var width = profileCanvas.width;
	var height = profileCanvas.height;
	var context = profileCanvas.getContext("2d");	
	var totalslope = maxhundred-minhundred;
	var maxdistance = calcroutealt[calcroutealt.length-1].distance;
	var prevx = (calcroutealt[0].distance/maxdistance)*width;
	var prevy = height - ((calcroutealt[0].altitude-minhundred)/totalslope)*height;
	var firstx = null;
	var route = [];
	var axisposition = height - bottommargin;
	
	if(servicecontrol==1) {
		// Plot km axis
		context.beginPath();
		context.rect(leftmargin, height-bottommargin, width-rightmargin-leftmargin, bottommargin-bottomborder);
		context.fillStyle = getAxisColor();
		context.fill(); 
		
		drawGreyLines(context, width, height, start, end);		
	}
	
	// Calculate x-y
	for(var i = 0; i<calcroutealt.length; i++) {
		// Debug
		route.push(calcroutealt[i].position); 
		if(calcroutealt[i].distance >= start && calcroutealt[i].distance <= end) {
			var x = ((calcroutealt[i].distance-start)/(end-start))*(width - leftmargin - rightmargin) + leftmargin;
			var y = (height - bottommargin - toplinemargin - profiledefaultmargin - 64) - ((calcroutealt[i].altitude-minhundred)/totalslope)*(height - bottommargin - toplinemargin - profiledefaultmargin - 64) + profiledefaultmargin + 64;

			if(firstx == null) {
				firstx = x;
			}
			calcroutealt[i].canvasx = x;
			calcroutealt[i].canvasy = y;
			prevx = x;
			prevy = y;			
		} else {
			calcroutealt[i].canvasx = null;
			calcroutealt[i].canvasy = null;	
		}
	}		
	
	var shadowlength = 16;
	if (servicecontrol==1 || servicecontrol==0) {
		// Draw 3d shadow
		for(var i = 0; i<calcroutealt.length; i++) {
			if(calcroutealt[i].distance >= start && calcroutealt[i].distance <= end) {
				context.beginPath();				
				var x = calcroutealt[i].canvasx;
				var y = calcroutealt[i].canvasy;	
				context.moveTo(x-1,y);
				var linex;	
				if (x-shadowlength-1 < leftmargin) {
					linex = leftmargin;
				} else {
					linex = x-shadowlength-1;			
				}
				context.lineTo(linex,y);	
				context.lineWidth = 3;
				context.strokeStyle = getLinesColor();
				context.stroke();
				context.closePath();	
			}
		}	
	}
			
	// Draw profile
	context.beginPath();	
	var lastx = null;
	for(var i = 0; i<calcroutealt.length; i++) {
		if(calcroutealt[i].distance >= start && calcroutealt[i].distance <= end) {
			var x = calcroutealt[i].canvasx;
			var y = calcroutealt[i].canvasy;	
			context.lineTo(x,y);
			lastx = x;
		}
	}	

	context.lineTo(lastx,height - bottommargin);
	context.lineTo(firstx,height - bottommargin);
    context.lineWidth = 1;
    context.strokeStyle = getMainLineColor();
	context.stroke();
	var pattern = context.createPattern(this, "repeat");
	context.fillStyle = pattern;
	context.fill();
	context.save();
	context.closePath();	
	if(servicecontrol==1) {
		// PLot Objects
	
		// Plot sprints 
		plotObjects(context, sprint, stageobjects.stagesprints);
		plotObjects(context, climbHC, stageobjects.stageclimbs);
		plotObjects(context, climbNO, stageobjects.stageclimbs);
		plotObjects(context, climb1, stageobjects.stageclimbs);
		plotObjects(context, climb2, stageobjects.stageclimbs);
		plotObjects(context, climb3, stageobjects.stageclimbs);
		plotObjects(context, climb4, stageobjects.stageclimbs);
		plotObjects(context, intermediate, stageobjects.stageintermediates);
		plotObjects(context, rest, stageobjects.stagerests);
		for(i = 1; i < 31; i++) {
			plotObjects(context, iconBase + "Cobblelogo"+i+".gif", stageobjects.stagecobbles);
		}		
	}
	
	if (servicecontrol==2) {
		var color = document.getElementById("profilecolor").value;
		colorProfile(0, calcroutealt.length-1, color, color);				
	} else if(drawclimbs) {
		var begin = 0;
		var km = 0;
		
		if(drawonlyclimbs) {
			// Draw Slopes for each Km
			for(var i = 0; i < calcroutealt.length; i++) {
				var distance = calcroutealt[i].distance.toFixed(2) - km;
					if(distance >= 1 || i == calcroutealt.length-1) {
						km++;
						var elevation = calcroutealt[i].altitude.toFixed(0) - calcroutealt[begin].altitude.toFixed(0);
						var slope = Number(((0.001*elevation / distance)*100).toFixed(1));			
						var climbcolor;
						if(drawclimbs == 1) {
							if(slope < 4 && slope > -4) {
								climbcolor = "#5dc71f";
							} else if(slope < 6 && slope > -6) {
								climbcolor = "#055b8b";
							} else if(slope < 9 && slope > -9) {
								climbcolor = "#eb102e";
							} else {
								climbcolor = "#050608";
							}
							colorProfile(begin, i, climbcolor, climbcolor);						
						} else if (drawclimbs == 2) {
							if(slope < 4 && slope > -4) {
								climbcolor = "#00ff00";
							} else if(slope < 7 && slope > -7) {
								climbcolor = "#0000ff";
							} else if(slope < 10 && slope > -10) {
								climbcolor = "#ffff00";
							} else {
								climbcolor = "#ff0000";
							}						
							colorProfile(begin, i, climbcolor, climbcolor);							
						}
						begin = i - 1;
					}		
			}
		}
		// Draw Climbs
		for (var i = 0; i < climbs.length; i++) {
			var startorder = climbs[i].start;
			var finishorder = climbs[i].finish;
			var begin = startorder;
			var km = 0;
			for (var j = startorder; j <= finishorder; j++) {
				var distance = calcroutealt[j].distance.toFixed(2) - calcroutealt[begin].distance.toFixed(0);
				if(distance >= 1 || j == finishorder) {
					var elevation = calcroutealt[j].altitude.toFixed(0) - calcroutealt[begin].altitude.toFixed(0);
					var slope = Number(((0.001*elevation / distance)*100).toFixed(1));			
					var climbcolor;
					km++;
					if(drawclimbs == 1) {
						if(slope < 4) {
							climbcolor = "#5dc71f";
						} else if(slope < 6) {
							climbcolor = "#055b8b";
						} else if(slope < 9) {
							climbcolor = "#eb102e";
						} else {
							climbcolor = "#050608";
						}
						colorProfile(begin, j, climbcolor, climbcolor);						
					} else if (drawclimbs == 2) {
						if(slope < 4) {
							climbcolor = "#00ff00";
						} else if(slope < 7) {
							climbcolor = "#0000ff";
						} else if(slope < 10) {
							climbcolor = "#ffff00";
						} else {
							climbcolor = "#ff0000";
						}						
						colorProfile(begin, j, climbcolor, climbcolor);							
					}
					begin = j-1;
				}		
			}			
		}
	}

	};			
}



function resetclimb () {
	var start = document.getElementById('fromkm').value;
	var end = document.getElementById('tokm').value;
	if(start=="") {
		start = 0;
	}
	if(end=="") {
		end = calcroutealt[calcroutealt.length-1].distance;
	}
	start = Number(start);
	end = Number(end);
	plotAltitude(minhundred, maxhundred, start, end);	
}

function colorProfile (startpoint, endpoint, lineColor, profileColor) {
	var start = document.getElementById('fromkm').value;
	var end = document.getElementById('tokm').value;
	if(start=="") {
		start = 0;
	}
	if(end=="") {
		end = calcroutealt[calcroutealt.length-1].distance;
	}
	start = Number(start);
	end = Number(end); 
	var profileCanvas = document.getElementById("altitudeCanvas");
	var height = profileCanvas.height;
	var context = profileCanvas.getContext("2d");	
	var firstx = calcroutealt[startpoint].canvasx;	
	context.beginPath();	
	for (var i = startpoint; i<endpoint; i++) {
		if(calcroutealt[i].distance >= start && calcroutealt[i].distance <= end) {	
			var x = calcroutealt[i].canvasx;
			if(firstx == null) {
				firstx = x;
			}
			var y = calcroutealt[i].canvasy;
			context.lineTo(x,y);	
		}
	}
	context.lineTo(x,height - bottommargin);
	context.lineTo(firstx,height - bottommargin);
	context.lineWidth = 1;
	if(servicecontrol==2) {
		context.strokeStyle = document.getElementById("bordercolor").value;
	} else {
		context.strokeStyle = lineColor;
	}
	context.fillStyle = profileColor;
	context.stroke();	
	context.fill();		
}


function drawGreyLines(context, width, height, start, end) {
	// Plot grey lines
	context.beginPath();
	context.moveTo(leftmargin+linelong, toplinemargin);
	context.lineTo(leftmargin, toplinemargin);	
	context.lineTo(leftmargin, height-bottomlinemargin);	
	context.lineTo(leftmargin+linelong, height-bottomlinemargin);	
    context.strokeStyle = '#b8b8b8';
	context.stroke();
	context.beginPath();
	context.moveTo(width - rightmargin-linelong, toplinemargin);
	context.lineTo(width - rightmargin, toplinemargin);	
	context.lineTo(width - rightmargin, height-bottomlinemargin);	
	context.lineTo(width - rightmargin-linelong, height-bottomlinemargin);	
    context.strokeStyle = '#b8b8b8';
	context.stroke();	
	
	var step = 2;
	// Plot start infos
	context.beginPath();
	context.font = "bold 18px Arial";	
	context.textAlign="start"; 
	context.fillStyle = '#000';
	context.fillText(departLocation.toUpperCase(), leftmargin + step, toplinemargin + 18);	
	context.beginPath();
	context.font = "normal 14px Arial";	
	context.textAlign="start"; 
	context.fillStyle = '#000';
	context.fillText(calcroutealt[0].altitude.toFixed(0) + " m", leftmargin + step, toplinemargin + 32);		
	context.beginPath();
	context.font = "bold 12px Arial";	
	context.textAlign="start"; 
	context.fillStyle = '#000';
	context.fillText(start, leftmargin + step, height - bottomlinemargin - step - 1);	
	departimage = new Image();
	departimage.src = departprofile;
	departimage.onload = function(){
		context.drawImage(departimage, leftmargin + step, toplinemargin + 36);
	}	
		
	// Plot end infos
	context.beginPath();
	context.font = "bold 18px Arial";	
	context.textAlign="end"; 
	context.fillStyle = '#000';
	context.fillText(arriveLocation.toUpperCase(), width - rightmargin - step, toplinemargin + 18);	
	context.beginPath();
	context.font = "normal 14px Arial";	
	context.textAlign="end"; 
	context.fillStyle = '#000';
	context.fillText(calcroutealt[calcroutealt.length-1].altitude.toFixed(0) + " m", width - rightmargin - step, toplinemargin + 32);		
	context.beginPath();
	context.font = "bold 12px Arial";	
	context.textAlign="end"; 
	context.fillStyle = '#000';
	context.fillText(end + " Km", width - rightmargin - step, height - bottomlinemargin - step - 1);		
	arriveimage = new Image();
	arriveimage.src = arriveprofile;
	arriveimage.onload = function(){
		context.drawImage(arriveimage, width - rightmargin - step - 20, toplinemargin + 36);
	}		
}

function plotGrid(context) {
	// Draw Y-Axis
	context.beginPath();
	context.moveTo(leftmargin,0);
	context.lineTo(leftmargin,height - bottommargin);
    context.lineWidth = 0.4;
    context.strokeStyle = '#000';	
	context.stroke();
	context.closePath();
	
	// Draw X-Axis
	context.beginPath();
	context.moveTo(0,axisposition);
	context.lineTo(width,axisposition);
    context.lineWidth = 0.4;
    context.strokeStyle = '#000';	
	context.stroke();
	context.closePath();
	
	// Plot legend
	context.font = "12px Arial";	
	context.textAlign="right"; 
	
}

function generateCanvasEvents (c) {
    var c = $('#altitudeCanvas');

	c.get(0).addEventListener('mousemove', function(evt) {
		var mousePos = getMousePos(c.get(0), evt);
		var profileBar = document.getElementById("navigation-bar");
		profileBar.style.left = mousePos.x + 'px';
		profileBar.style.top = c.get(0).height + 'px';
		var selected = 0;
		for(var i = 0; i < calcroutealt.length; i++) {
			if(mousePos.x > leftmargin && mousePos.x < c.get(0).width - rightmargin && calcroutealt[i].canvasx>=mousePos.x && selected==0) {
				selected = 1;
				// Compute distance
				var prevdist = calcroutealt[i-1].distance;
				var nextdist = calcroutealt[i].distance;
				var prevx = calcroutealt[i-1].canvasx;
				var nextx = calcroutealt[i].canvasy;
				var percentage = mousePos.x / (prevx+nextx);
				var distance = (nextdist - prevdist)*percentage + prevdist;
				document.getElementById('point_distance').innerHTML = distance.toFixed(2) + " Km";
				document.getElementById('point_elevation').innerHTML = calcroutealt[i].altitude.toFixed(0) + " m";
				document.getElementById('point_latitude').innerHTML = calcroutealt[i].position.lat().toFixed(3) + " °";
				document.getElementById('point_longitude').innerHTML = calcroutealt[i].position.lng().toFixed(3) + " °";	
				document.getElementById('point_slope').innerHTML = getSlope(distance) + " %";					
				profileBar.style.top = calcroutealt[i].canvasy + 'px';
				profileBar.style.height = c.get(0).height - calcroutealt[i].canvasy - bottommargin + 'px';
				var position = new google.maps.LatLng(calcroutealt[i-1].position.lat(), calcroutealt[i-1].position.lng());
				navigationmarker.setMap(map);
				navigationmarker.setPosition(position);		
				profileBar.style.border = "1px solid gray";		
			} else if(mousePos.x < leftmargin || mousePos.x > c.get(0).width - rightmargin) {
				profileBar.style.top = 0 + 'px';
				profileBar.style.left = 0 + 'px';
				profileBar.style.height = 0 + 'px';	
				profileBar.style.border = "0px solid white";			
			}
		}
	}, false);
	
	c.get(0).onmouseout = function(event) {
		navigationmarker.setMap(null);
		navigationmarker.setPosition(null);
		var profileBar = document.getElementById("navigation-bar");	
		profileBar.style.top = 0 + 'px';
		profileBar.style.left = 0 + 'px';
		profileBar.style.height = 0 + 'px';
		profileBar.style.border = "0px solid white";	
	}
	
	c.get(0).onmousedown = function(event){
   		event.preventDefault();
	};
}

// Set Type
function setType(type) {
	routeType = type;
	elaborateRoute();
}

function getProfileImage() {
	if(routeType == 1) {
		return 'profile.gif';
	} else {
		return 'profileother.gif';
	}
}

function getLinesColor() {
	if(routeType == 1) {
		return '#a47d14';
	} else {
		return '#94918c';
	}	
}

function getMainLineColor() {
	if(routeType == 1) {
		return '#f7cc3f';
	} else {
		return '#e2d6ca';
	}		
}

function getBottomBorderColor() {
	if(routeType == 1) {
		return '#000';
	} else {
		return '#e2d6ca';
	}		
}

function getAxisColor() {
	if(routeType == 1) {
		return '#010101';
	} else if(routeType == 2) {
		return '#0098d6';
	} else if(routeType == 3) {
		return '#afb900';
	} else if(routeType == 4) {
		return '#c40832';
	} else if(routeType == 5) {
		return '#ac9e6d';
	} else if(routeType == 6) {
		return '#0e4a02';
	} else if(routeType == 7) {
		return '#046fb7';
	} else if(routeType == 8) {
		return '#d64b13';
	} else if(routeType == 9) {
		return '#00978e';
	}													
}

function setServiceControl(value) {
	servicecontrol = value;
	elaborateRoute();
}