// JavaScript Document
function editTrackProperties(trackid) {
	var data = new FormData();
	var name = document.getElementById("trackname").value;
	var depart = document.getElementById("trackdepart").value;
	var arrive = document.getElementById("trackarrive").value;
	var tracktype = document.getElementById("tracktype").value;
	var racetype = document.getElementById("racetype").value;
	var trackowner = document.getElementById("trackowner").value;
	var trackdescription = document.getElementById("trackdescription").value;
	var public = getTrackPublic();
	data.append('trackname', name);	
	data.append('trackdepart', depart);	
	data.append('trackarrive', arrive);	
	data.append('tracktype', tracktype);	
	data.append('racetype', racetype);	
	data.append('trackowner', trackowner);	
	data.append('trackpublic', public);	
	data.append('trackdescription', trackdescription);	
	data.append('trackid', trackid);	
	data.append('editing', 1);	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("post", "{EDITRACKLINK}", true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.location.reload();
		} else {
			var error = document.getElementById("errorbox");	
			error.innerHTML = xmlhttp.responseText;			
		}
	}	
	xmlhttp.send(data);				
}

function getTrackPublic() {
	var trackPublic = document.getElementById('trackpublic').checked;
	if (trackPublic == true) {
		return 1;
	} else {
		return 0;
	}
}