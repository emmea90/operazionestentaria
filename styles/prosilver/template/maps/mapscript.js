google.setOnLoadCallback(function() {
  $(function() {
	start();
  });
});

var drawclimbs=1;
var markers = [];
var calcroute = [];
var routedistances = {};
var routealtitudes = {};
var polylines = [];
var directionsService = new google.maps.DirectionsService();
var elevationService = new google.maps.ElevationService();
var chart;
var currentpointer = 0;
var altitudecalls = 0;
var totalcalls = 0;
var filter = 10;
var fixdistance = 0;
var pathSamples = 64;
var trackmode = 'auto';
var legendzone = 50;
var axiszone = 20;
var fails = 0;
var precision = 5;
var tracktype = 'track';

function start() {
	initialize();
	generateTrackEvents();
	var action = getURLParameter("action");
	if (action == "edit") {
		loadTrack();
	} else {
		geolocateMap();
	}	
}

function generateTrackEvents() {
	google.maps.event.addListener(map, 'click', function(event) {
		if(tracktype == 'track') {
			if(currentpointer==0) {
				placeMarker(event.latLng, depart);
			} else {
				placeMarker(event.latLng, arrive);	
				if(currentpointer>2) {
					markers[currentpointer-2].setIcon(trackPointMini);
				}
			}			
		}
	});	
}

function geolocateMap() {
	// Try HTML5 geolocation
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
		  var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		  map.setCenter(pos);
	}, function() {
	  handleNoGeolocation(true);
	});
	} else {
		// Browser doesn't support Geolocation
		handleNoGeolocation(false);
	}	
}

function getTrackname() {
	var trackname = document.getElementById('routename').value;
	if(trackname=="") {
		trackname = "track";
	}
	return trackname;
}

function loadTrack() {
	var calcrouteString = '{TRACKROUTE}';
	var calcrouteStringAlt = '{TRACKROUTEALT}';
	calcroute = JSON.parse(calcrouteString);
	calcroutealt = JSON.parse(calcrouteStringAlt);	
	var bounds = new google.maps.LatLngBounds();
	// Re-parse calcroute
	for(var i=0; i<calcroute.length; i++) {
		for(var j=0; j<calcroute[i].length; j++) {
			var p = new google.maps.LatLng(calcroute[i][j].k, calcroute[i][j].A);
			calcroute[i][j] = p;
			bounds.extend(p);
		}
	}
	// Re-parse positions
	for(var i=0; i<calcroutealt.length; i++) {
		calcroutealt[i].position = new google.maps.LatLng(calcroutealt[i].position.k, calcroutealt[i].position.A);
	}
			
	for(var i=0; i<calcroute.length; i++) {
		var marker;
		if(i==0) {
			marker = insertMarker(calcroute[i][0], depart);
		} else {
			marker = insertMarker(calcroute[i][0], trackPointMini);			
		}
		currentpointer++;
		markers.push(marker);		
		var route = calcroute[i];
		drawRoute(route);	
		if(i==calcroute.length-1) {
			var marker = insertMarker(calcroute[i][calcroute[i].length-1], arrive);
			currentpointer++;
			markers.push(marker);	
		}			
	}
	updateRouteInfos();
	if(calcroutealt.length) {
		elaborateRoute();
		importObjects();
		updateStartEnd();
	}
	map.fitBounds(bounds);	 
}

function searchAddress() {
	var address = document.getElementById('addressbox').value;
	var geocoder = new google.maps.Geocoder();
	var request = {
		address: address
	}
	geocoder.geocode(request, searchCallback);
}

function searchCallback(result, status) {
	if(status==google.maps.GeocoderStatus.OK) {
		var center = map.getCenter();
		map.setCenter(result[0].geometry.location);
		map.setZoom(15);
	} else if(status==google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
		searchCallback(result, status);
	} else if(status==google.maps.GeocoderStatus.ZERO_RESULTS) {
		alert("{L_NORESULT}");
	}
}

function setFilter(value) {
	filter = value;
}
function setFreeMode() {
	trackmode = 'free';
}

function setRouteMode() {
	trackmode = 'auto';
}

function setTrackModeType(type) {
	tracktype = type;
	/* if(type=='track') {
		setMarkersDraggables(true);
		removeAllSprintObjects();
	} else if(type=='labels') {
		setMarkersDraggables(false);
	} */
}

function setMarkersDraggables(value) {
	for (var i=0; i<markers.length; i++) {
		markers[i].setDraggable(value);
	}
}

function clearAllProfiles() {
	currentpointer = 0;
	//Remove all Markers	
	for(var i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}
	//Remove all Polylines	
	for(var i = 0; i < polylines.length; i++) {
		polylines[i].setMap(null);
	}	
	markers.length = 0;
	calcroute.length = 0;
	polylines.length = 0;
}

function updateRouteInfos() {
	var distance = 0;
	var start = calcroute[0][0];
	routedistances[start] = 0;
	for(var i=0; i<calcroute.length; i++) {
		for(var j=0; j<calcroute[i].length-1; j++) {
			var start = calcroute[i][j];
			var end = calcroute[i][j+1];
			distance = distance + distancePoints(start, end);
			routedistances[calcroute[i][j+1].toUrlValue(3)] = distance;
		}
	}
	fixdistance=distance.toFixed(2);
	document.getElementById('route_distance').innerHTML = fixdistance + " Km";
	updateObjectsPositions();
}

function insertMarker(location, icon) {
	var marker = new MarkerWithLabel({
		position: location,
		icon: icon,
		map: map,
		draggable: true
	});
	google.maps.event.addListener(marker, 'dragend', function(event) {updateRoute(marker)} );	
	google.maps.event.addListener(marker, 'rightclick', function(event) {deleteMarker(marker)} );	
	google.maps.event.addListener(marker, 'click', function(event) {transformMarker(marker)} );			
	return marker;
}

function placeMarker(location, icon) {
	var marker = insertMarker(location, icon);
	markers.push(marker);
	if (currentpointer > 0) {
		addRouteSection(currentpointer);
	}
	currentpointer = currentpointer + 1;	
}

function updateRoute(marker) {
	var order = markers.indexOf(marker);
	if(order!=-1) {
		if(order!=0){
			editRouteSection(order);		
		}
		if(order!=currentpointer-1){
			editRouteSection(order+1);	
		}		
		updateMarkers(marker);				
	} else {
		updateObjectInfo(marker);			
	}
}

function updateMarkers(marker) {
	var order = markers.indexOf(marker);
	marker.setPosition(marker.getPosition());
}

function clearline (line) {
	polylines[line].setMap(null);	
}

function addRouteSection (sectionnumber) {
	computeRouteSection(sectionnumber, 'add');
}

function addRouteCallback (route) {
	calcroute.push(route);	
	drawRoute(route);	
	updateRouteInfos();
}

function insertPoly(route, order) {
	var poly = new google.maps.Polyline({
		 // use your own style here
		 path: route,
		 strokeColor: "#FFCC33",
		 strokeOpacity: .9,
		 strokeWeight: 8,
	});	
	poly.setMap(map);	
	google.maps.event.addListener(poly, 'click', function(event) {insertRoute(event.latLng, poly)} );		
	return poly;	
}

function drawRoute(route) {
	var poly = insertPoly(route, polylines.length);
	polylines.push(poly);	
}

function deleteMarker(marker) {
	//Update all Markers	
	var index = markers.indexOf(marker);
	if(index != -1) {
		if(index != 0) {
			currentpointer--;
			markers[index].setMap(null);	
			markers.splice(index, 1);		
			if(index == currentpointer) {	
				if (index != 1) {
					markers[index-1].setIcon(arrive);
				}
				polylines[index-1].setMap(null);	
				polylines.splice(index-1, 1);	
				calcroute.splice(index-1, 1);			
			} else {
				polylines[index].setMap(null);	
				polylines.splice(index, 1);	
				calcroute.splice(index, 1);				
			}
			var nextMarker = markers[index-1];	
			updateRoute(nextMarker);					
		}			
	} else {
		deleteMarkerObject(marker);
	}
}

function insertRoute(position, polyline) {
	if(tracktype == 'track') {
		var order = polylines.indexOf(polyline);
		currentpointer++;
		//Update all Markers	
		var marker = insertMarker(position, trackPointMini);
		markers.splice(order+1, 0, marker);
		var route = [];
		var poly = insertPoly(route, order+1);
		polylines.splice(order+1, 0, poly);
		calcroute.splice(order+1, 0, route);
		updateRoute(marker);		
	} else {
		insertNewMarkerObject(position);				
	}
}

function editRouteCallback (route, sectionnumber) {
	calcroute[sectionnumber] = route;
	polylines[sectionnumber].setPath(route);
	updateRouteInfos();
}

function editRouteSection (sectionnumber) {
	computeRouteSection (sectionnumber, 'edit');
}

function computeRouteSection (sectionnumber, mode) {
	var start = markers[sectionnumber-1].getPosition();
	var end = markers[sectionnumber].getPosition();		


	if (trackmode == 'auto') {
		// Create a request
		var request = {
		  origin: start,
		  destination: end,
		  avoidHighways: true,
		  travelMode: google.maps.TravelMode.DRIVING
		};	
		var driveroute = [];	
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				var routeLegs = response.routes[0].legs;	
				var startLocation = routeLegs[0].start_location;	
				var endLocation = routeLegs[routeLegs.length-1].end_location;
				markers[sectionnumber-1].setPosition(startLocation);	
				markers[sectionnumber].setPosition(endLocation);	
				for (i=0;i<routeLegs.length;i++) {
					var steps = routeLegs[i].steps;
					for (j=0;j<steps.length;j++) {
						var nextSegment = steps[j].path;
						for (k=0;k<nextSegment.length;k++) {
							var lat = nextSegment[k].lat();
							var lng = nextSegment[k].lng();
							var p = new google.maps.LatLng(lat.toFixed(5), lng.toFixed(5));
							driveroute.push(p); 		
						}
					}
				}		
				if(mode == 'add') {
					addRouteCallback(driveroute);
				} else if (mode == 'edit') {		
					editRouteCallback(driveroute, sectionnumber-1);
				}
			} else {
				if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
					setTimeout(function() {
						computeRouteSection (sectionnumber, mode);
					}, 2000);
				} else if (status == google.maps.DirectionsStatus.ZERO_RESULTS) {
					alert("{L_ZERODIRECTIONS}");					
					deleteMarker(markers[markers.length-1]);
				} else {
					alert("Errore: " + status);
				}
			} 
		});	
	} else if (trackmode == 'free') {
		var start = markers[sectionnumber-1].getPosition();
		var end = markers[sectionnumber].getPosition();		
		var driveroute = [];
		driveroute.push(start);	
		driveroute.push(end);		
		if(mode == 'add') {
			addRouteCallback(driveroute);
		} else if (mode == 'edit') {		
			editRouteCallback(driveroute, sectionnumber-1);
		}		
	}
}

function getAltitude (pathToPlot, section) {
	var PathRequest = {
		path: pathToPlot,
		samples: pathSamples
	};
	var LocationRequest = {
		locations: pathToPlot
	};
	// elevationService.getElevationAlongPath(PathRequest, function (response, status) {
	elevationService.getElevationForLocations(LocationRequest, function (response, status) {
		if(status == google.maps.ElevationStatus.OK) {
			for (var i = 0; i < response.length; i++) {
				var position = response[i].location.lat().toFixed(precision) + "," + response[i].location.lng().toFixed(precision);
				routealtitudes[position] = Math.floor(response[i].elevation);	
			}	
			getAltitudeCallback();
		} else {
			if (status == google.maps.ElevationStatus.OVER_QUERY_LIMIT) {
				setTimeout(function() {
					getAltitude (pathToPlot, section);
					fails++;
				}, 2000);			
			}	
		}
	});	
}

function getAltitudeCallback() {
	totalcalls++
	if (totalcalls==altitudecalls) {
		interpolateAltitudes(calcroutealt, routealtitudes);		
		smoothElevation(calcroutealt, filter);		
		elaborateRoute(); 
	}
}

function truncate(n) {
  return n | 0; // bitwise operators convert operands to 32-bit integers
}

function compare(a,b) {
  if (a.order < b.order)
     return -1;
  if (a.order > b.order)
    return 1;
  return 0;
}

// values:    an array of numbers that will be modified in place
// smoothing: the strength of the smoothing filter; 1=no change, larger values smoothes more
function smoothElevation(calcroutealt, smoothing){
  var value = calcroutealt[0].altitude; // start with the first input
  for (var i=1, len=calcroutealt.length; i<len; ++i){
    var currentValue = calcroutealt[i].altitude;
    value += (currentValue - value) / smoothing;
    calcroutealt[i].altitude = Math.floor(value);
  }
}

function interpolateAltitudes (destination, altitudeMaps) {
	// Step1: load altitudes
	var pointwithaltitudes = [];
	for (var i = 0; i < destination.length; i++) {	
		var position = destination[i].position.lat().toFixed(precision) + "," + destination[i].position.lng().toFixed(precision);
		var altitude = routealtitudes[position];
		if(altitude) {
			destination[i].altitude = Math.floor(altitude);
			var pointalt = {
				distance: destination[i].distance,
				altitude: Math.floor(altitude),
			}
			pointwithaltitudes.push(pointalt);
		}
	}
	
	// Step2: interpolation
	var j = 1;
	var prevpoint = pointwithaltitudes[0];
	var nextpoint = pointwithaltitudes[1];
	for (var i = 0; i < destination.length; i++) {	
		if(destination[i].distance >= nextpoint.distance) {
			j++;
			prevpoint = nextpoint;
			nextpoint = pointwithaltitudes[j];
		}
		if(destination[i].altitude == 0) {
			destination[i].altitude = prevpoint.altitude + (nextpoint.altitude - prevpoint.altitude)*(destination[i].distance - prevpoint.distance)/(nextpoint.distance - prevpoint.distance);
		}
	}
}

function elaborateRoute () {
	// Compute denivel
	computeDenivel();
	var distance = 0;
	var previous = calcroutealt[0].position;
	var minelevation = 8000;
	var maxelevation = 0;
	// Fill data
	for (var i = 0; i < calcroutealt.length; i++) {	
		var position = calcroutealt[i].position;
		// Calculate distance
		if(i == 0) {
			distance = 0;
		} else {
			distance = distance + distancePoints(previous, position);
		}
		previous = position;
		calcroutealt[i].distance = Math.round(distance * 100) / 100;	
		var elevation = calcroutealt[i].altitude;
		if(elevation <= minelevation) {
			minelevation = elevation.toFixed(2);
		}
		if(elevation >= maxelevation) {
			maxelevation = elevation.toFixed(2);
		}		
	}
	exportroute = calcroutealt;
	
	
	// Filter route
	var filteredroute = [];
	var previousdistance = 0;
	filteredroute.push(calcroutealt[0]);
	for(var i=0; i<calcroutealt.length; i++) {
		if(calcroutealt[i].distance - previousdistance >= interpolationdistance || i==calcroutealt.length-1) {
			previousdistance = calcroutealt[i].distance;
			filteredroute.push(calcroutealt[i]);
		}
	}		
	// calcroutealt = filteredroute;
	
	// Let fix elevations
	if(minelevation < 0) {
		minhundred = -100;
	} else {
		minhundred = 100*Math.floor(minelevation/100) - 100;
	}
	maxhundred = 100*Math.floor(maxelevation/100) + 300;
	if (minhundred < 0) {
		minhundred = 0;
	}
	if (maxhundred-minhundred < 1000) {
		maxhundred = minhundred + 900;
	}
	findclimbs();
	plotAltitude(minhundred, maxhundred, 0, calcroutealt[calcroutealt.length-1].distance);
}

function updateClimbName (position, destination) {
	var geocoder = new google.maps.Geocoder();
	var request = {
		location: position
	}
	geocoder.geocode(request, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
		if (results[1]) {
			destination.innerHTML=results[1].formatted_address;
		} else {
			destination.innerHTML=" ";
		}
	} else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
		updateClimbName (position, destination);	
	} else {
		destination.innerHTML=" ";		
	}
  });
}

function getCategory (difficulty, alt1000) {
	if(difficulty < 100) {
		return "{L_4C}"
	}
	if(difficulty < 170) {
		return "{L_3C}"
	}	
	if(difficulty < 400) {
		return "{L_2C}"
	}	
	if(difficulty < 800) {
		return "{L_1C}"
	} else {
		return "{L_HC}"		
	}
}

function findmaxpercentage(startorder, finishorder, distanceinterpolation) {
	var maxpercentage = {
		slope: 0,
		start: 0
	};
	var begin = 0;
	for (var i = startorder; i < finishorder; i++) {
		var distance = calcroutealt[i].distance.toFixed(2) - calcroutealt[begin].distance.toFixed(2);
		if(distance >= distanceinterpolation) {
			var elevation = calcroutealt[i].altitude.toFixed(0) - calcroutealt[begin].altitude.toFixed(0);
			var slope = Number(((0.001*elevation / distance)*100).toFixed(1));						
			if(slope > maxpercentage.slope) {
				maxpercentage.slope = slope;
				maxpercentage.start = calcroutealt[begin].distance;
				maxpercentage.end = calcroutealt[i].distance;
				maxpercentage.startpoint = begin;
				maxpercentage.endpoint = i;
			}
			begin = i;
		}		
	}	
	var length = calcroutealt[finishorder].distance.toFixed(2) - calcroutealt[startorder].distance.toFixed(2);	
	if(length < distanceinterpolation) {
		return "-"
	} else {
		return maxpercentage;
	}
}

function handleNoGeolocation(errorFlag) {
  var options = {
    map: map,
    position: new google.maps.LatLng(48.865497, 2.321022),
	mapTypeId: google.maps.MapTypeId.TERRAIN		
  };
  map.setCenter(options.position);
}

$(document).ready( function(){
    //Get the canvas & context
    var c = $('#profileCanvas');
    var ct = c.get(0).getContext('2d');
    var container = $(c).parent();
	
    //Run function when browser resizes
    $(window).resize(profileCanvas);

    function respondCanvas(){ 
        c.attr('width', $(container).width() ); //max width
        c.attr('height', $(container).height() ); //max height

        //Call a function to redraw other content (texts, images etc)
    }

	c.get(0).addEventListener('click', function(evt) {
		if(tracktype == 'labels') {
			var mousePos = getMousePos(c.get(0), evt);		
			for(var i = 0; i < calcroutealt.length; i++) {
				if(mousePos.x > legendzone && calcroutealt[i].canvasx>=mousePos.x) {
					var position = calcroutealt[i].position;
					var distance = calcroutealt[i].distance;
					insertNewMarkerObjectWithDistance(position, distance);
					return;
				}
			}	
		}	
	});
	
	c.get(0).addEventListener('mousemove', function(evt) {
		var mousePos = getMousePos(c.get(0), evt);
		var profileBar = document.getElementById("navigation-bar");
		profileBar.style.left = mousePos.x + 'px';
		profileBar.style.top = c.get(0).height + 'px';
		var selected = 0;
		for(var i = 0; i < calcroutealt.length; i++) {
			if(mousePos.x > legendzone && calcroutealt[i].canvasx>=mousePos.x && selected==0) {
				selected = 1;
				// Compute distance
				var prevdist = calcroutealt[i].distance;
				var nextdist = calcroutealt[i].distance;
				var prevx = calcroutealt[i].canvasx;
				var nextx = calcroutealt[i].canvasy;
				var percentage = mousePos.x / (prevx+nextx);
				var distance = (nextdist - prevdist)*percentage + prevdist;
				document.getElementById('point_distance').innerHTML = distance.toFixed(2) + " Km";
				document.getElementById('point_elevation').innerHTML = calcroutealt[i].altitude.toFixed(0) + " m";
				document.getElementById('point_latitude').innerHTML = calcroutealt[i].position.lat().toFixed(3) + " °";
				document.getElementById('point_longitude').innerHTML = calcroutealt[i].position.lng().toFixed(3) + " °";
				document.getElementById('point_slope').innerHTML = getSlope(distance) + " %";	
				profileBar.style.top = calcroutealt[i].canvasy + 'px';
				profileBar.style.height = c.get(0).height - calcroutealt[i].canvasy - axiszone + 'px';
				var position = new google.maps.LatLng(calcroutealt[i-1].position.lat(), calcroutealt[i].position.lng());
				navigationmarker.setMap(map);
				navigationmarker.setPosition(position);
				profileBar.style.border = "1px solid gray";		
			}
		}
	}, false);
	
	c.get(0).onmouseout = function(event) {
		navigationmarker.setMap(null);
		navigationmarker.setPosition(null);
		var profileBar = document.getElementById("navigation-bar");	
		profileBar.style.top = 0 + 'px';
		profileBar.style.left = 0 + 'px';
		profileBar.style.height = 0 + 'px';
		profileBar.style.border = "0px solid white";	
	}
	
	c.get(0).onmousedown = function(event){
   		event.preventDefault();
	};
	
    //Initial call 
    respondCanvas();
}); 