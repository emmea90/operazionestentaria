// JavaScript Document
function addTour() {
	var data = new FormData();
	var name = document.getElementById("tourname").value;
	var description = document.getElementById("tourdescription").value;
	var stages = document.getElementById("tourstages").value;
	var public = getTourPublic();
	data.append('tourname', name);	
	data.append('tourdescription', description);	
	data.append('tourpublic', public);		
	data.append('tourstages', stages);	
	data.append('mode', 'add');
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("post", "{U_CONFIRMTOUR}", true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			window.location.href='{U_TOURS}'
		} else {
			var error = document.getElementById("errorbox");	
			error.innerHTML = xmlhttp.responseText;			
		}
	}	
	xmlhttp.send(data);				
}

function editTour(tourid) {
	var data = new FormData();
	var name = document.getElementById("tourname").value;
	var description = document.getElementById("tourdescription").value;
	var stages = document.getElementById("tourstages").value;
	var public = getTourPublic();
	data.append('tourname', name);	
	data.append('tourdescription', description);	
	data.append('tourpublic', public);	
	data.append('tourid', tourid);	
	data.append('tourstages', stages);	
	data.append('mode', 'edit');
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("post", "{U_CONFIRMTOUR}", true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			window.location.href='{U_TOURS}'
		} else {
			var error = document.getElementById("errorbox");	
			error.innerHTML = xmlhttp.responseText;			
		}
	}	
	xmlhttp.send(data);				
}


function getTourPublic() {
	var trackPublic = document.getElementById('tourpublic').checked;
	if (trackPublic == true) {
		return 1;
	} else {
		return 0;
	}
}