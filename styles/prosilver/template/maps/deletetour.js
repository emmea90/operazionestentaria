// JavaScript Document
function deleteTour (tourid) {
	if (confirm('{L_SURETOUR}')) {
		var data = new FormData();
		data.append('tour', tourid);				
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("post", "deletetour.php", true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				var node = document.getElementById("tour" + tourid);
				document.location.reload();
			} else {
				var error = document.getElementById("errorbox");	
				error.innerHTML = xmlhttp.responseText;			
			}
		}	
		xmlhttp.send(data);
	}
}