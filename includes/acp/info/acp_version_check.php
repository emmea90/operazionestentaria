<?php
/**
*
* @package acp
* @version $Id: acp_version_check.php 50 2007-10-14 03:49:13Z Handyman $
* @copyright (c) 2007 Handyman - StarTrekGuide
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @package mod_version_check
*/
class acp_version_check_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_version_check',
			'title'		=> 'ACP_MOD_VERSION_CHECK',
			'version'	=> '1.0.2',
			'modes'		=> array(
				'version_check'		=> array('title' => 'ACP_MOD_VERSION_CHECK', 'auth' => 'acl_a_board', 'cat' => array('ACP_AUTOMATION')),
			),
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}

?>