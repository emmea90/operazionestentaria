<?php
/**
*
* @package acp SafeGT
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @package module_install
*/
class acp_safegt_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_safegt',
			'title'		=> 'ACP_CAT_SAFEGT',
			'version'	=> '2.3.4',
			'modes'		=> array(
				'main'		=> array('title' => 'ACP_SAFEGT_CFG', 'auth' => 'acl_a_safegt', 'cat' => array('ACP_CAT_DOT_MODS'),
				),
			),
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}

?>