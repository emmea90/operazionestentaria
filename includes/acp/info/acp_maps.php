<?php

/**
*
* @package - Maps
* @version $Id: acp_maps.php 
* @copyright (c) emmea90
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package module_install
*/
class acp_maps_info
{
    function module()
    {
        return array(
            'filename'    => 'acp_maps',
            'title'        => 'ACP_MAPS',
            'version'    => '1.0.0',
			'modes'		=> array(
				'config_maps'	=> array(
				'title'		=> 'ACP_MAPS_INDEX_TITLE',
				'auth'		=> 'acl_a_can_manage_maps',
				'cat'		=> array('ACP_BOARD_CONFIGURATION'),
				),
			),			
        );
    }


}
?>