<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @package Advanced Rules Page
* @version $Id$
* @copyright (c) 2012 Dugi ( http://imgit.org )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package module_install
*/
class acp_rules_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_rules',
			'title'		=> 'ACP_RULES_MOD',
			'version'	=> '1.0.0',
			'modes'		=> array(
				'manage'	=> array('title' => 'ACP_RULES', 'auth' => 'acl_a_rules', 'cat' => array('ACP_CAT_RULES')),
			),
		);
	}

	function install(){}
	function uninstall(){}		
}
?>