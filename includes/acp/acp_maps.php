<?php
/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package acp
*/
class acp_maps
{
	var $u_action;
	
	function main($id, $mode)
	{
		global $db, $user, $auth, $template;
		global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx;
		
		add_form_key('acp_maps');
		
		$this->page_title = 'ACP_MAPS';
		$this->tpl_name = 'acp_maps';
		$submit = (isset($_POST['submit'])) ? true : false;	
			
		if ($submit)
		{
			if (!check_form_key('acp_maps'))
			{
				trigger_error('FORM_INVALID');
			}
	
			set_config('tracks_apikey', request_var('tracks_apikey', '', true));
			trigger_error($user->lang['TRACKS_SAVED_SETTINGS'] . adm_back_link($this->u_action));
		}
			
		$template->assign_vars(array(
			'TRACKS_APIKEY'			=> $config['tracks_apikey'],
			'U_ACTION'				=> $this->u_action,
		));						
		
	}
	
}
?>