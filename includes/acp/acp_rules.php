<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @package Advanced Rules Page
* @version $Id$
* @copyright (c) 2012 Dugi ( http://imgit.org )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

class acp_rules
{
	var $u_action;
	var $parent_id;
	
	function main($id, $mode)
	{
		global $phpbb_root_path, $phpbb_admin_path, $phpEx;
		global $config, $db, $user, $template, $cache;
		
		$user->add_lang('mods/info_acp_rules');
		
		// Initialize properties
		$this->tpl_name		= 'acp_rules';
		$this->page_title 	= 'RULES_MANAGE';
		$this->parent_id 	= request_var('parent_id', 0);
		$this->u_action		= $this->u_action . (($this->parent_id) ? "&amp;parent_id={$this->parent_id}" : '');
		
		$form_key = 'acp_rules';
		add_form_key($form_key);
		
		// Installer was never started
		if (!isset($config['advanced_rules_page_version']))
		{
			trigger_error($user->lang['RULES_NOT_INSTALLED'] . adm_back_link(append_sid("{$phpbb_admin_path}index.$phpEx")), E_USER_WARNING);		
		}
		
		include($phpbb_root_path . 'includes/functions_display.' . $phpEx);
		include($phpbb_root_path . 'includes/functions_rules.' . $phpEx);	
		
		$action		= request_var('action', '');
		$rule_id 	= request_var('r', 0);
		
		$submit	= (isset($_POST['submit'])) ? true : false;
		$update = (isset($_POST['update'])) ? true : false;
		$create = (isset($_POST['create'])) ? true : false;
		
		$error 			= array();
		$alpha_string	= 'abcdefghijklmnopqrstuvwxyz';
		$alpha_count 	= 0;
		$cat_count 		= 1;
		
		if ($submit)
		{
			if (!check_form_key($form_key))
			{
				$error[] = 'FORM_INVALID';
			}
			
			if (empty($error))
			{
				$enable_disable = request_var('enable_board_rules', 0);
				set_config('enable_board_rules', $enable_disable);
				
				$message = ($enable_disable) ? 'RULES_PAGE_ENABLED' : 'RULES_PAGE_DISABLED';
				trigger_error($user->lang[$message] . adm_back_link($this->u_action));
			}
		}
		
		switch ($action)
		{
			case 'create':
				$this->page_title = 'ACP_CREATE_RULE';
				
				// Main variables	
				$rule_name 	= request_var('rule_name', '');
				$rule_type	= (!$this->parent_id) ? 'cat' : 'rule';
				
				// Blanks vars
				$error = array();
				$is_enabled	= true;
				$cats = $rules = 0;
				$rule_title = $rule_desc = '';
				
				// Get highest rule position
				$sql = 'SELECT MAX(' . $rule_type . '_position) + 1 AS max_position
					FROM ' . RULES_TABLE . 
					(($rule_type == 'rule') ? " WHERE parent_id = {$this->parent_id}" : '');
				$result = $db->sql_query($sql);
				$rule_pos = $db->sql_fetchfield('max_position');
				$db->sql_freeresult($result);
				
				if ($rule_type == 'rule')
				{
					$sql = 'SELECT public FROM ' . RULES_TABLE .
						" WHERE rule_id = {$this->parent_id}";
					$result = $db->sql_query($sql);
					$is_enabled = $db->sql_fetchfield('public');
					$db->sql_freeresult($result);
					
					$sql = 'SELECT COUNT(rule_id) AS rules FROM ' . RULES_TABLE .
						" WHERE rule_type = 'rule'
							AND parent_id = {$this->parent_id}";
					$result = $db->sql_query($sql);
					$rules	= (int) $db->sql_fetchfield('rules');
					$db->sql_freeresult($result);
					
				}
				
				if ($create)
				{
					// Submitted variables
					$rule_public	= request_var('enable', false);
					$rule_title 	= utf8_normalize_nfc(request_var('rule_title', '', true));
					$rule_desc		= utf8_normalize_nfc(request_var('rule_description', '', true));
					$group_ids		= request_var('group_ids', array(''));
					
					$rules_parse_bbcode		= request_var('rules_parse_bbcode', false);
					$rules_parse_links		= request_var('rules_parse_urls', false);
					$rules_parse_smilies	= request_var('rules_parse_smilies', false);
					
					if ($rule_type == 'rule' && $rules == 26)
					{
						$error[] = 'MAXIMUM_RULES';	
					}
					
					// Populate the $error array
					if (!$is_enabled && $rule_public)
					{
						$error[] = 'CANNOT_ENABLE';	
					}
					
					if (!check_form_key($form_key))
					{
						$error[] = 'FORM_INVALID';	
					}
					
					if ($rule_type == 'cat' && !request_var('rule_title', ''))
					{
						$error[] = 'EMPTY_FIELDS';
					}
					
					if ($rule_type == 'rule' && !request_var('rule_title', '') && !request_var('rule_description', ''))
					{
						$error[] = 'EMPTY_FIELDS';
					}
					
					if ($rule_type == 'rule' && !request_var('rule_description', ''))
					{
						$error[] = 'DESCRIPTION_REQUIRED';
					}
					
					if ($rule_type == 'rule' && !request_var('rule_title', ''))
					{
						$error[] = 'TITLE_REQUIRED';
					}
					
					if (utf8_strlen(request_var('rule_title', '')) > 255)
					{
						$error[] = 'RULE_TITLE_TOO_LONG';
					}
					
					if ($rule_type == 'rule' && utf8_strlen(request_var('rule_description', '')) > 2000)
					{
						$error[] = 'RULE_DESC_TOO_LONG';
					}
					
					if ($rule_type == 'cat' && utf8_strlen(request_var('rule_title', '')) < 3)
					{
						$error[] = 'CAT_TITLE_TOO_SHORT';
					}
					
					if ($rule_type == 'rule' && utf8_strlen(request_var('rule_description', '')) < 3)
					{
						$error[] = 'RULE_TOO_SHORT';
					}
					
					if (empty($error))
					{
						// Create rule or category
						$rule_id = create_rule(
									$this->parent_id, 
									$rule_pos, 
									$rule_title, 
									$rule_desc, 
									$rule_type, 
									$rule_public, 
									$rules_parse_bbcode, 
									$rules_parse_links, 
									$rules_parse_smilies, 
									$group_ids
								);
							
						if ($rule_id)
						{
							// Logs and messages
							$log_message 	= (!$this->parent_id) ? 'LOG_CAT_CREATED' : 'LOG_RULE_CREATED';
							$message 		= (!$this->parent_id) ? 'CAT_CREATED' : 'RULE_CREATED';
							
							add_log('admin', $log_message, $rule_title);
							trigger_error($user->lang[$message] . adm_back_link($this->u_action));
						}
					}
					
					// Replace "error" strings with their real, localised form
					$error = preg_replace('#^([A-Z_]+)$#e', "(!empty(\$user->lang['\\1'])) ? \$user->lang['\\1'] : '\\1'", $error);
				}
					
				$template->assign_vars(array(
					'CREATE_RULE'	=> true,
					'ERROR'			=> (sizeof($error)) ? true : false,
					'ERROR_MSG'		=> (sizeof($error)) ? implode('<br />', $error) : '',	
					
					'S_RULE_TITLE'		=> (!empty($rule_name)) ? $rule_name : $rule_title,
					'S_RULE_DESC'		=> $rule_desc,
					'S_IS_RULE'			=> ($rule_type == 'rule') ? true : false,
					'S_PARENT_PUBLIC' 	=> $is_enabled,
					'S_GROUP_OPTIONS'	=> multi_group_select_options(array(1, 2, 7)),
					
					'U_ACTION'	=> $this->u_action . "&amp;action=create",
					'U_BACK'	=> $this->u_action
				));
				
				return;
			break;
			
			case 'edit':
				if (!$rule_id || !rule_exists($rule_id))
				{
					trigger_error($user->lang['NO_RULE'] . adm_back_link($this->u_action), E_USER_WARNING);	
				}
				
				$this->page_title = 'ACP_RULE_EDIT';
				
				// Blank variables that will receive values somewhere below
				$rule_title = $rule_description = $rule_type = $new_group_ids = $group_ids = '';
				$is_enabled	= true;
				$error = array();
				
				// Get all the info we have about this rule or category based on rule_id & parent_id
				$rule = get_rule_info($rule_id, $this->parent_id);
				
				// Prepare the $group_ids string.
				// We take it and explode the whitespace
				// and make it an array which we can use
				if (!empty($rule['group_ids']))
				{
					$group_ids = array_map('intval', explode(' ', $rule['group_ids']));
				}
				
				if ($update)
				{
					// Submitted variables
					$cat_enable		= ($rule['rule_type'] == 'cat') ? $rule['public'] : false;
					$rule_pos		= $rule['rule_position'];
					$rule_public	= request_var('enable', (int) $cat_enable);
					$rule_title 	= utf8_normalize_nfc(request_var('rule_title', '', true));
					$rule_desc		= utf8_normalize_nfc(request_var('rule_description', '', true));
					$to_parent_id	= request_var('rule_parent', 0);
					$new_group_ids	= request_var('group_ids', $group_ids);
					$parent_id 		= ($this->parent_id == $to_parent_id) ? $this->parent_id : $to_parent_id;
					
					$rules_parse_bbcode		= request_var('rules_parse_bbcode', false);
					$rules_parse_links		= request_var('rules_parse_urls', false);
					$rules_parse_smilies	= request_var('rules_parse_smilies', false);
					
					// We cannot enable RULES which have their category/parent disabled
					if ($rule['rule_type'] == 'rule' && $rule_public)
					{
						$sql = 'SELECT public FROM ' . RULES_TABLE .
							' WHERE rule_id = ' . (int) $to_parent_id;
						$result = $db->sql_query($sql);
						$is_enabled = $db->sql_fetchfield('public');
						$db->sql_freeresult($result);
						
						if (!$is_enabled)
						{
							$error[] = 'CANNOT_ENABLE';	
						}
					}
					
					// Fill the $error array with error strings
					if (!check_form_key($form_key))
					{
						$error[] = 'FORM_INVALID';	
					}
					
					if ($rule['rule_type'] == 'cat' && !request_var('rule_title', ''))
					{
						$error[] = 'EMPTY_FIELDS';
					}
					
					if ($rule['rule_type'] == 'rule' && !request_var('rule_title', '') && !request_var('rule_description', ''))
					{
						$error[] = 'EMPTY_FIELDS';
					}
					
					if ($rule['rule_type'] == 'rule' && !request_var('rule_description', ''))
					{
						$error[] = 'DESCRIPTION_REQUIRED';
					}
					
					if ($rule['rule_type'] == 'rule' && !request_var('rule_title', ''))
					{
						$error[] = 'TITLE_REQUIRED';
					}
					
					if (utf8_strlen(request_var('rule_title', '')) > 255)
					{
						$error[] = 'RULE_TITLE_TOO_LONG';
					}
					
					if ($rule['rule_type'] == 'rule' && utf8_strlen(request_var('rule_description', '')) > 2000)
					{
						$error[] = 'RULE_DESC_TOO_LONG';
					}
					
					if ($rule['rule_type'] == 'cat' && utf8_strlen(request_var('rule_title', '')) < 3)
					{
						$error[] = 'CAT_TITLE_TOO_SHORT';
					}
					
					if ($rule['rule_type'] == 'rule' && utf8_strlen(request_var('rule_description', '')) < 3)
					{
						$error[] = 'RULE_TOO_SHORT';
					}
					
					if (empty($error))
					{			
						// Update position if we're moving this rule
						if ($rule['rule_type'] == 'rule' && ($this->parent_id != $to_parent_id))
						{				
							$sql = 'SELECT MAX(rule_position) + 1 AS max_position
								FROM ' . RULES_TABLE . 
								' WHERE parent_id = ' . $to_parent_id;
							$result = $db->sql_query($sql);
							$rule_pos = $db->sql_fetchfield('max_position');
							$db->sql_freeresult($result);
						}
						
						// Update rule or category
						update_rule(
							$rule_id, 
							$parent_id, 
							$rule_pos, 
							$rule_title, 
							$rule_desc, 
							$rule_public, 
							$rules_parse_bbcode, 
							$rules_parse_links, 
							$rules_parse_smilies, 
							$new_group_ids
						);
						
						// Now update the old parent - after updating
						if ($rule['rule_type'] == 'rule' && ($this->parent_id != $to_parent_id))
						{
							$sql = 'UPDATE ' . RULES_TABLE . ' 
								SET rule_position = rule_position - 1 
								WHERE parent_id = ' . $this->parent_id .
									' AND rule_position > ' . $rule['rule_position'];
							$db->sql_query($sql);
						}
							
						// Logs and messages
						$log_message 	= ($rule['rule_type'] == 'rule') ? 'LOG_ALTERED_RULE' : 'LOG_ALTERED_CAT';
						$message 		= ($rule['rule_type'] == 'rule') ? 'RULE_UPDATED' : 'CAT_UPDATED';
						
						$new_location	= str_replace("parent_id={$this->parent_id}", "parent_id=$to_parent_id", $this->u_action);
						$back_to		= ($this->parent_id == $to_parent_id) ? $this->u_action : $new_location;
						
						add_log('admin', $log_message, $rule['rule_title']);
						trigger_error($user->lang[$message] . adm_back_link($back_to));
					}
					
					// Replace "error" strings with their real, localised form
					$error = preg_replace('#^([A-Z_]+)$#e', "(!empty(\$user->lang['\\1'])) ? \$user->lang['\\1'] : '\\1'", $error);
				}
				
				$template->assign_vars(array(
					'EDIT_RULE'	=> true,
					'ERROR'		=> (sizeof($error)) ? true : false,
					'ERROR_MSG'	=> (sizeof($error)) ? implode('<br />', $error) : '',	
					
					'S_RULE_ENABLED'		=> ($rule['public']) ? true : false,
					'S_RULE_TITLE'			=> (!empty($rule_title)) ? $rule_title : $rule['rule_title'],
					'S_RULE_DESCRIPTION'	=> (!empty($rule_desc)) ? $rule_desc : $rule['rule_description'],
					'S_PARENT_SELECT'		=> rule_parent_options($this->parent_id),
					'S_GROUP_OPTIONS'		=> multi_group_select_options((!empty($new_group_ids)) ? $new_group_ids : $group_ids),
					
					'S_IS_RULE'			=> ($rule['rule_type'] == 'rule') ? true : false,
					'S_PARSE_BBCODE'	=> ($rule['parse_bbcode']) ? true : false,
					'S_PARSE_LINKS'		=> ($rule['parse_links']) ? true : false,
					'S_PARSE_SMILIES'	=> ($rule['parse_smilies']) ? true : false,
					
					'U_ACTION'	=> $this->u_action . "&amp;action=edit&amp;r=$rule_id",
					'U_BACK'	=> $this->u_action
				));
				
				return;
			break;
			
			case 'delete':
				if (!$rule_id || !rule_exists($rule_id))
				{
					trigger_error($user->lang['NO_RULE'] . adm_back_link($this->u_action), E_USER_WARNING);	
				}
			
				// Using confirm_box here for sensitive actions
				if (confirm_box(true))
				{	
					$rule_type 	= (!$this->parent_id) ? 'cat' : 'rule';
				
					// Get the position of the rule we're deleting
					$sql = 'SELECT rule_title, ' . $rule_type . '_position
						FROM ' . RULES_TABLE . "
						WHERE rule_id = $rule_id";
					$result = $db->sql_query($sql);
					$row = $db->sql_fetchrow($result);
					$db->sql_freeresult($result);
										
					// Now, change the positions of all the rules that come after it
					$sql = 'UPDATE ' . RULES_TABLE . '
						SET ' . $rule_type . '_position = ' . $rule_type . '_position - 1
						WHERE ' . $rule_type . '_position > ' . $row[$rule_type . '_position'] .
							(($rule_type == 'rule') ? " AND parent_id = {$this->parent_id}" : '');
					$db->sql_query($sql);
					
					// Delete rule
					$deleted = delete_rule($rule_id, $rule_type);
					
					if ($deleted)
					{					
						$log_message 	= ($rule_type == 'rule') ? 'LOG_DELETED_RULE' : 'LOG_DELETED_CAT';
						$message 		= ($rule_type == 'rule') ? 'RULE_DELETED' : 'CAT_DELETED';
						
						add_log('admin', $log_message, $row['rule_title']);
						trigger_error($user->lang[$message] . adm_back_link($this->u_action));
					}
				}
				else
				{	
					$message = (!$this->parent_id) ? 'CONFIRM_CAT_DELETE' : 'CONFIRM_RULE_DELETE';
					confirm_box(false, $user->lang[$message], build_hidden_fields(array(
						'r'			=> $rule_id,
						'action'	=> $action))
					);	
				}
			break;
			
			case 'enable':
			case 'disable':
				if (!$rule_id || !rule_exists($rule_id))
				{
					trigger_error($user->lang['NO_RULE'] . adm_back_link($this->u_action), E_USER_WARNING);	
				}
				
				$rule_type 	= (!$this->parent_id) ? 'cat' : 'rule';
			
				// We cannot enable RULES which have their category/parent disabled
				if ($rule_type == 'rule' && $action == 'enable')
				{
					$sql = 'SELECT public FROM ' . RULES_TABLE .
						' WHERE rule_id = ' . (int) $this->parent_id;
					$result = $db->sql_query($sql);
					$is_enabled = $db->sql_fetchfield('public');
					$db->sql_freeresult($result);
					
					if (!$is_enabled)
					{
						trigger_error($user->lang['CANNOT_ENABLE'] . adm_back_link($this->u_action), E_USER_WARNING);
					}
				}
					
				$changed_visibility = enable_rule($rule_id, $action, $rule_type);
						
				if ($changed_visibility)
				{
					$log_message 	= ($action == 'enable') ? 'LOG_ENABLED_RULE' : 'LOG_DISABLED_RULE';
					$message 		= ($action == 'enable') ? 'RULE_ENABLED' : 'RULE_DISABLED';
					
					add_log('admin', $log_message, $rule_title);
					
					// Instant redirect - no message here
					redirect($this->u_action);
				}
			break;
			
			case 'move_up':
			case 'move_down':
				if (!$rule_id || !rule_exists($rule_id))
				{
					trigger_error($user->lang['NO_RULE'] . adm_back_link($this->u_action), E_USER_WARNING);	
				}
				
				$rule = get_rule_info($rule_id, $this->parent_id);
				$move_rule = move_rule_by($rule, $action, 1);
				
				if ($move_rule !== false)
				{
					$cache->destroy('sql', RULES_TABLE);	
				}
			break;
		}
		
		// Default page
		if (!$this->parent_id)
		{
			$sql = 'SELECT rule_title, rule_description, rule_type, rule_id, public
				FROM ' . RULES_TABLE . 
				" WHERE rule_type = 'cat'
					AND parent_id = 0
				ORDER BY cat_position ASC";
			$result = $db->sql_query($sql);
			
			$navigation = $user->lang['ACP_CAT_CATS'];
		}
		else
		{
			$sql = 'SELECT rule_title, rule_description, rule_type, rule_id, public
				FROM ' . RULES_TABLE . 
				" WHERE rule_type = 'rule'
					AND parent_id = {$this->parent_id}
				ORDER BY rule_position ASC";
			$result = $db->sql_query($sql);
			
			$navigation = '<a href="' . str_replace("&amp;parent_id={$this->parent_id}", '', $this->u_action) . '">' . $user->lang['ACP_CAT_CATS'] . '</a>';
			$navigation .= ' -&gt; ' . get_rule_name($this->parent_id);
		}
		
		while ($row = $db->sql_fetchrow($result))
		{
			// Reset the counter once we reach 26. 
			// This should never occur, but in case someone changes something in this file.	
			$alpha_count = ($alpha_count == 26) ? 0 : $alpha_count;
			
			$template->assign_block_vars('rules', array(
				'RULE_NAME'			=> $row['rule_title'],
				'RULE_DESCRIPTION'	=> $row['rule_description'],
				'FOLDER_IMG'		=> ($row['rule_type'] == 'cat') ? 
										'<img src="images/icon_folder.gif" alt="{L_CAT}" />' : 
										'<img src="images/icon_subfolder.gif" alt="{L_RULE}" />',
				'RULE_ENABLED'	=> ($row['public']) ? true : false,
				'IS_RULE'		=> ($row['rule_type'] == 'rule') ? true : false,
				'COUNTER'		=> ($row['rule_type'] == 'rule') ? $alpha_string{$alpha_count} . '. ' : $cat_count . '. ',
				
				'U_RULE'		=> ($row['rule_type'] == 'cat') ? $this->u_action . "&amp;parent_id={$row['rule_id']}" : '',
				'U_EDIT'		=> $this->u_action . "&amp;action=edit&amp;r={$row['rule_id']}",
				'U_DELETE'		=> $this->u_action . "&amp;action=delete&amp;r={$row['rule_id']}",
				'U_ENABLE'		=> $this->u_action . "&amp;action=enable&amp;r={$row['rule_id']}&amp;type={$row['rule_type']}",
				'U_DISABLE'		=> $this->u_action . "&amp;action=disable&amp;r={$row['rule_id']}&amp;type={$row['rule_type']}",
				'U_MOVE_UP'		=> $this->u_action . "&amp;r={$row['rule_id']}&amp;action=move_up",
				'U_MOVE_DOWN' 	=> $this->u_action . "&amp;r={$row['rule_id']}&amp;action=move_down"
			));
			
			($row['rule_type'] == 'rule') ? $alpha_count++ : $cat_count++;
		}
		$db->sql_freeresult($result);
		
		$template->assign_vars(array(
			'ERROR'		=> (sizeof($error)) ? true : false,
			'ERROR_MSG'	=> (sizeof($error)) ? implode('<br />', $error) : '',
		
			'U_ACTION'		=> $this->u_action,
			
			'NAVIGATION' 	=> $navigation,
			'HAS_PARENT'	=> (!empty($this->parent_id)) ? true : false,
			'S_BOARD_RULES'		=> (isset($config['enable_board_rules']) && $config['enable_board_rules']) ? true : false,
			
			'ICON_ENABLE' 	=> '<img src="' . $phpbb_admin_path . 'images/icon_visible.gif" alt="' . $user->lang['RULE_DISABLE'] . '" title="' . $user->lang['DISABLE'] . '" />',		
			'ICON_DISABLE' 	=> '<img src="' . $phpbb_admin_path . 'images/icon_invisible.gif" alt="' . $user->lang['RULE_ENABLE'] . '" title="' . $user->lang['ENABLE'] . '" />',
		));			
	}
}
?>