<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @package Advanced Rules Page
* @version $Id$
* @copyright (c) 2012 Dugi ( http://imgit.org )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* Creates a rule or category
*
* @param int $parent_id The parent id of the RULE we are creating
* @param int $rule_position The position of the rule/category we are creating (bottom)
* @param string $rule_title The name of the rule/category we're creating
* @param string $rule_desc The description of the rule we're creating
* @param string $rule_type The type of the rule we're creating ('cat' or 'rule')
* @param bool $enabled Should this rule or category be enabled
* @param bool $parse_bbcode Should this rule/cat parse bbcode
* @param bool $parse_links Should this rule/cat parse links
* @param bool $parse_smilies Should this rule/cat parse smilies
* @param array $group_ids The group ids that should see this rule
*
* @return int The inserted id
*/
function create_rule($parent_id, $rule_position, $rule_title, $rule_desc = '', $rule_type, $enabled = true, $parse_bbcode = true, $parse_links = true, $parse_smilies = true, $group_ids = array(1, 2, 7))
{
	global $db;
	
	$position_col 	= ($rule_type == 'cat') ? 'cat_position' : 'rule_position';
	$group_ids 		= (is_array($group_ids)) ? $group_ids : array($group_ids);
	$group_ids 		= implode(' ', $group_ids);
	
	if (!$parent_id && $rule_type == 'rule')
	{
		$sql = 'SELECT group_ids FROM ' . RULES_TABLE .
			' WHERE rule_id = ' . (int) $parent_id;
		$result = $db->sql_query($sql);
		$group_ids = $db->sql_fetchfield('group_ids');
		$db->sql_freeresult($result);
	}
	
	$sql = 'INSERT INTO ' . RULES_TABLE . $db->sql_build_array('INSERT', array(
		'parent_id'			=> (int) $parent_id,
		$position_col		=> (int) $rule_position,
		'rule_type'			=> $rule_type,
		'rule_title'		=> $rule_title,
		'rule_description'	=> $rule_desc,
		'public'			=> (int) ($enabled) ? true : false,
		'parse_bbcode'		=> (int) ($parse_bbcode) ? true : false,
		'parse_links'		=> (int) ($parse_links) ? true : false,
		'parse_smilies'		=> (int) ($parse_smilies) ? true : false,
		'group_ids'			=> $group_ids));
	$db->sql_query($sql);
	
	return (int) $db->sql_nextid();
}

/**
* updates a rule or category
*
* @param int $rule_id The the rule id we're updating
* @param int $parent_id The new parent id of the RULE we are updating
* @param int $rule_position The position of the rule/category we are updating (bottom)
* @param string $rule_title The name of the rule/category we're updating
* @param string $rule_desc The description of the rule we're updating
* @param bool $enabled Should this rule or category be enabled
* @param bool $parse_bbcode Should this rule/cat parse bbcode
* @param bool $parse_links Should this rule/cat parse links
* @param bool $parse_smilies Should this rule/cat parse smilies
* @param array $group_ids The group ids that should see this rule
*
* @return bool True on successful update
*/
function update_rule($rule_id, $parent_id, $rule_position, $rule_title, $rule_desc = '', $enabled = true, $parse_bbcode = true, $parse_links = true, $parse_smilies = true, $group_ids = array(1, 2, 7))
{
	global $db;
	
	$group_ids = (is_array($group_ids)) ? $group_ids : array($group_ids);
	$group_ids = implode(' ', $group_ids);
	
	if (!$parent_id)
	{
		$sql = 'UPDATE ' . RULES_TABLE . 
			" SET group_ids = '" . $db->sql_escape($group_ids) . "'
			 WHERE rule_id = " . (int) $rule_id . 
				" OR parent_id = " . (int) $rule_id;
		$db->sql_query($sql);
		
		$sql = 'UPDATE ' . RULES_TABLE . 
			' SET public = ' . (int) (($enabled) ? true : false) . 
			' WHERE rule_id = ' . (int) $rule_id .
				" OR parent_id = " . (int) $rule_id;
		$db->sql_query($sql);
	}
	
	$sql = 'UPDATE ' . RULES_TABLE . ' SET ' . $db->sql_build_array('UPDATE', array(
			'parent_id'			=> (int) ($parent_id != false) ? $parent_id : false,
			'rule_position'		=> (int) $rule_position,
			'rule_title'		=> $rule_title,
			'rule_description'	=> $rule_desc,
			'public'			=> (int) ($enabled) ? true : false,
			'parse_bbcode'		=> (int) ($parse_bbcode) ? true : false,
			'parse_links'		=> (int) ($parse_links) ? true : false,
			'parse_smilies'		=> (int) ($parse_smilies) ? true : false)) .
		' WHERE rule_id = ' . (int) $rule_id;
	$db->sql_query($sql);
}

/**
* Deletes a rule or category
*
* @param int $rule_id The the rule id we're deleting
* @param string $type What are we deleting? Rule or category
*
* @return bool True on successful delete
*/
function delete_rule($rule_id, $type)
{
	global $db;
	
	$sql = 'DELETE FROM ' . RULES_TABLE .	
		' WHERE rule_id = ' . (int) $rule_id . 
			(($type == 'cat') ? ' OR parent_id = ' . (int) $rule_id : '');
	$db->sql_query($sql);
	
	return ($db->sql_affectedrows() > 0) ? true : false;
}

/**
* Checks if a rule or category exists
*/
function rule_exists($rule_id)
{
	global $db;
	
	$sql = 'SELECT * FROM ' . RULES_TABLE .
		' WHERE rule_id = ' . (int) $rule_id;
	$result = $db->sql_query($sql);
	$exists = $db->sql_fetchfield('rule_type');
	$db->sql_freeresult($result);
	
	return ($exists) ? true : false;
}

/**
* Returns the name of the rule/category based on rule_id
*/
function get_rule_name($rule_id)
{
	global $db;
	
	$sql = 'SELECT rule_title FROM ' . RULES_TABLE	.
		' WHERE rule_id = ' . (int) $rule_id;
	$result = $db->sql_query($sql);
	$rule_name	= $db->sql_fetchfield('rule_title');
	$db->sql_freeresult($result);
	
	return $rule_name;
}

/**
* Enable/disable a rule or category
*/
function enable_rule($rule_id, $enable, $type)
{
	global $db;
	
	$enable = ($enable == 'enable') ? true : false;
	
	$sql = 'UPDATE ' . RULES_TABLE . 
		' SET public = ' . (int) $enable . 
		' WHERE rule_id = ' . (int) $rule_id .
			(($type == 'cat') ? ' OR parent_id = ' . (int) $rule_id : '');
	$db->sql_query($sql);
	
	return ($db->sql_affectedrows() > 0) ? true : false;
}

/**
* Get rule details
*/
function get_rule_info($rule_id, $parent_id = false)
{
	global $db;

	$sql = 'SELECT *
		FROM ' . RULES_TABLE .
		' WHERE rule_id = ' . (int) $rule_id .
			' AND rule_type = ' . ((!$parent_id) ? "'cat'" : "'rule'");
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	if (!$row)
	{
		trigger_error("Rule #$rule_id does not exist", E_USER_ERROR);
	}

	return $row;
}

/**
* Moves a rule up and down
*
* I am not the original author of this function, I just modified it to my needs.
* All credits go to Tom (www.phpbb.com/community/memberlist.php?mode=viewprofile&un=Tom) for this function.
*/
function move_rule_by($rule_row, $action, $steps = 1)
{
	global $db, $user;

	$user->add_lang('mods/info_acp_rules');
	$type = $rule_row['rule_type'];
	
	if ($action == 'move_up')
	{
		$rule_position = $rule_row[$type . '_position'] - 1;
	}
	else
	{
		$rule_position = $rule_row[$type . '_position'] + 1;
	}

	$sql = 'SELECT rule_title
		FROM ' . RULES_TABLE . ' 
		WHERE ' . $type . "_position = $rule_position" . 
			(($type == 'rule') ? ' AND parent_id = ' . $rule_row['parent_id'] : '') .
			' AND rule_type = ' . (($type == 'rule') ? "'rule'" : "'cat'");
	$result = $db->sql_query_limit($sql, $steps);
	$target = array();
	while ($row = $db->sql_fetchrow($result))
	{
		$target = $row;
	}
	$db->sql_freeresult($result);

	if (!sizeof($target))
	{
		return false;
	}

	$sql = 'UPDATE ' . RULES_TABLE . '
		SET ' .  $type . '_position = ' . $rule_row[$type . '_position'] .
		' WHERE ' . $type . "_position = $rule_position" .
			(($type == 'rule') ? ' AND parent_id = ' . $rule_row['parent_id'] : '');	
	$db->sql_query($sql);

	$sql = 'UPDATE ' . RULES_TABLE . "
		SET " . $type . "_position = $rule_position
		 WHERE rule_id = {$rule_row['rule_id']}" . 
		 	(($type == 'rule') ? ' AND parent_id = ' . $rule_row['parent_id'] : '');
	$db->sql_query($sql);

	return $target['rule_title'];
}

/**
* Generate list of groups (option fields without select)
*
* @param array $group_ids The default group ids to mark as selected
*
* @return string The list of options.
*/
function multi_group_select_options($group_ids)
{
	global $db, $user;
	
	$group_ids 	= (is_array($group_ids)) ? $group_ids : array($group_ids);
	$sql_where 	= ($user->data['user_type'] == USER_FOUNDER) ? '' : 'WHERE group_founder_manage = 0';
	
	$sql = 'SELECT group_id, group_name, group_type
		FROM ' . GROUPS_TABLE . " 
		$sql_where 
		ORDER BY group_type DESC, group_name ASC";
	$result = $db->sql_query($sql);

	$s_group_options = '';
	
	while ($row = $db->sql_fetchrow($result))
	{			
		$selected = (is_array($group_ids) && in_array($row['group_id'], $group_ids)) ? ' selected="selected"' : '';
		$s_group_name = ($row['group_type'] == GROUP_SPECIAL) ? $user->lang['G_' . $row['group_name']] : $row['group_name'];
		$s_group_options .= '<option'  . (($row['group_type'] == GROUP_SPECIAL) ? ' class="sep"' : '') . ' value="' . $row['group_id'] . '"' . $selected . '>' . $s_group_name . '</option>';
	}
	$db->sql_freeresult($result);

	return $s_group_options;
}

/**
* Generate list of rule categories (option fields without select)
*
* @param array $parent_id The default rule id to mark as selected
*
* @return string The list of options.
*/
function rule_parent_options($parent_id)
{
	global $db;
	
	$sql = 'SELECT rule_id, rule_title FROM ' . RULES_TABLE .
		" WHERE rule_type = 'cat'";
	$result = $db->sql_query($sql);
			
	$s_rule_parent_options = '';	
	while ($row = $db->sql_fetchrow($result))
	{	
		$s_rule_parent_options .= '<option value="' . $row['rule_id'] . '"' . (($parent_id == $row['rule_id']) ? 'selected="selected"' : '') . '>' . $row['rule_title'] . '</option>';
	}
	
	return $s_rule_parent_options;	
}
?>