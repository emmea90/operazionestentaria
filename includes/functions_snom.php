<?php
/**
*
* @package - Sequential number on memberlist MOD
* @version $Id$
* @copyright (c) 2012 all4phone (http://phpbbmods.cba.pl/)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* Include only once.
*/
if (!defined('INCLUDES_FUNCTIONS_SNOM_PHP'))
{
	define('INCLUDES_FUNCTIONS_SNOM_PHP', true);
	
	if (!empty($config['snom_enable']))
	{
		$template->assign_vars(array(
			'SNOM_ENABLE'		            => $config['snom_enable'],
		));
	}
}
?>