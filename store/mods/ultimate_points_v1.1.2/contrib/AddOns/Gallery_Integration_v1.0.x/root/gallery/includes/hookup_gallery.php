<?php
/**
*
* @package phpBB Gallery
* @version $Id: hookup_gallery.php 816 2012-02-18 04:34:04Z femu $
* @copyright (c) 2007 nickvergessen nickvergessen@gmx.de http://www.flying-bits.org
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/

if (!defined('IN_PHPBB'))
{
	exit;
}


/**
* Hookup image counter
*
* This function is called, after an image was/multiple images were uploaded/deleted.
* You can add your code here, to get/substruct cash on Cash-MODs or what ever.
*
* @param int $user_id		ID of the user, who ownes the images
* @param int $num_images	Number of images which are handled. (positive on add, negative on delete)
*/
function gallery_hookup_image_counter($user_id, $num_images)
{
	global $config, $db, $user, $phpbb_root_path, $phpEx;

	// Read out Ultimate Points values data
	$sql_array = array(
		'SELECT'    => '*',
		'FROM'      => array(
			POINTS_VALUES_TABLE => 'v',
		),
	);
	$sql = $db->sql_build_query('SELECT', $sql_array);
	$result = $db->sql_query($sql);
	$points_values = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	// Edit your amounts here.
	define('GALLERY_ADD_CASH', $points_values['gallery_upload']);
	define('GALLERY_DELETE_CASH', $points_values['gallery_remove']);
	define('GALLERY_VIEW_CASH', $points_values['gallery_view']);

	/**
	* Adding/Substracting points
	*/
	
	if (defined('IN_ULTIMATE_POINTS') && $config['points_enable'])
	{
		if (!function_exists('add_points') || !function_exists('substract_points'))
		{
			includes($phpbb_root_path . 'includes/points/functions_points.' . $phpEx);
		}
		if ($num_images > 0)
		{
			// Add cash for uploading
			add_points($user_id, ($num_images * GALLERY_ADD_CASH));
		}
		else
		{
			// Substract cash for deleting
			substract_points($user_id, (abs($num_images) * GALLERY_DELETE_CASH));
		}
	}
}

/**
* Hookup image view
*
* This function is called, when an image was viewed in fullsize.
* You can add your code here, to substruct cash on Cash-MODs or what ever.
*
* @param int $user_id		ID of the user, who viewed the images
*/
function gallery_hookup_image_view($user_id)
{
	global $config, $db, $user, $phpbb_root_path, $phpEx;

	// Read out Ultimate Points values data
	$sql_array = array(
		'SELECT'    => '*',
		'FROM'      => array(
			POINTS_VALUES_TABLE => 'v',
		),
	);
	$sql = $db->sql_build_query('SELECT', $sql_array);
	$result = $db->sql_query($sql);
	$points_values = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	// Edit your amounts here.
	define('GALLERY_ADD_CASH', $points_values['gallery_upload']);
	define('GALLERY_DELETE_CASH', $points_values['gallery_remove']);
	define('GALLERY_VIEW_CASH', $points_values['gallery_view']);

	// Read out the config data
	$sql_array = array(
		'SELECT'    => 'config_name, config_value',
		'FROM'      => array(
			POINTS_CONFIG_TABLE => 'c',
		),
	);
	$sql = $db->sql_build_query('SELECT', $sql_array);
	$result = $db->sql_query($sql);

	while ($row = $db->sql_fetchrow($result))
	{
		$points_config[$row['config_name']] = $row['config_value'];
	}
	$db->sql_freeresult($result);
	
	/**
	* Substracting points for viewing images in full-screen mode
	*/
	if (defined('IN_ULTIMATE_POINTS') && $config['points_enable'])
	{
		if (!function_exists('substract_points'))
		{
			includes($phpbb_root_path . 'includes/points/functions_points.' . $phpEx);
		}
		substract_points($user_id, GALLERY_VIEW_CASH);

		// If the user has negative cash now (would be needed on return from the cash-mods function, you can deny to view the image at all,
		// by removing the // in the next lines:
		if ($user->data['user_points'] < $points_values['gallery_view'] && $points_config['gallery_deny_view'])
		{
			// readd the cash to users-cash
			add_points($user_id, abs(GALLERY_VIEW_CASH));
			return false;
		}
	}
	return true;
}

?>