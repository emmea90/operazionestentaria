<?php
/**
*
* @package phpBB3 User Blog Ultimate Points Plugin
* @version $Id: functions.php 794 2011-12-24 04:34:10Z femu $
* @copyright (c) 2009 femu - http://die-muellers.org
* @copyright (c) 2008 Doktor_x and McGod based on User Blog System Points by EXreaction, Lithium Studios
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function sp_acp_main_settings(&$settings)
{
	global $user;

	$user->add_lang('mods/blog/plugins/ultimate_points');

	$settings['legend_sp'] = 'UP_PLUGIN';
	$settings['user_blog_sp_blog_points'] = array('lang' => 'UP_BLOG_POINTS', 'validate' => 'int', 'type' => 'text:5:5', 'explain' => true);
	$settings['user_blog_sp_reply_points'] = array('lang' => 'UP_REPLY_POINTS', 'validate' => 'int', 'type' => 'text:5:5', 'explain' => true);
	$settings['user_blog_cp_points'] = array('lang' => 'UP_CP_POINTS', 'validate' => 'bool', 'type' => 'radio:yes_no', 'explain' => true);
}

function sp_blog_add_after_sql()
{
	global $auth, $config, $db, $user;

	if ($auth->acl_get('u_blognoapprove') && $config['user_blog_sp_blog_points'] && $config['points_enable'])
	{
		$db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_points = user_points + ' . $config['user_blog_sp_blog_points'] . ' WHERE user_id = ' . $user->data['user_id']);
	}
}

function sp_blog_approve_confirm()
{
	global $config, $db, $user_id;

	if ($config['user_blog_sp_blog_points'] && $config['points_enable'])
	{
		$db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_points = user_points + ' . $config['user_blog_sp_blog_points'] . ' WHERE user_id = ' . intval($user_id));
	}
}

function sp_reply_add_after_sql()
{
	global $auth, $config, $db, $user;

	if ($auth->acl_get('u_blogreplynoapprove') && $config['user_blog_sp_reply_points'] && $config['points_enable'])
	{
		$db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_points = user_points + ' . $config['user_blog_sp_reply_points'] . ' WHERE user_id = ' . $user->data['user_id']);
	}
}

function sp_reply_approve_confirm()
{
	global $config, $db, $reply_user_id;

	if ($config['user_blog_sp_reply_points'] && $config['points_enable'])
	{
		$db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_points = user_points + ' . $config['user_blog_sp_reply_points'] . ' WHERE user_id = ' . intval($reply_user_id));
	}
}

function sp_user_handle_data(&$output_data)
{
	global $config, $user, $ultimate_points;

	if ($config['user_blog_cp_points'] && $config['points_enable'])
	{
		$output_data['custom_fields'][] = array(
			'PROFILE_FIELD_NAME'	=> $config['points_name'],
			'PROFILE_FIELD_VALUE'	=> blog_data::$user[$output_data['USER_ID']]['user_points'],
		);
	}
}

?>