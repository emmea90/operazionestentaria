<?php
/**
*
* @package phpBB3 User Blog Ultimate Points
* @version $Id: uninstall.php 794 2011-12-24 04:34:10Z femu $
* @copyright (c) 2009 femu - http://die-muellers.org
* @copyright (c) 2008 Doktor_x and McGod based on User Blog System Points by EXreaction, Lithium Studios
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$db->sql_query('DELETE FROM ' . CONFIG_TABLE . ' WHERE config_name = \'user_blog_sp_blog_points\'');
$db->sql_query('DELETE FROM ' . CONFIG_TABLE . ' WHERE config_name = \'user_blog_sp_reply_points\'');
$db->sql_query('DELETE FROM ' . CONFIG_TABLE . ' WHERE config_name = \'user_blog_cp_points\'');

?>