<?php
/**
*
* @package phpBB3 User Blog Ultimate Points Plugin
* @version $Id: info_ultimate_points.php 794 2011-12-24 04:34:10Z femu $
* @copyright (c) 2009 femu - http://die-muellers.org
* @copyright (c) 2008 Doktor_x and McGod based on User Blog System Points by EXreaction, Lithium Studios
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/blog/plugins/ultimate_points');

self::$available_plugins[$name]['plugin_title'] = $user->lang['BLOG_UP_TITLE'];
self::$available_plugins[$name]['plugin_description'] = $user->lang['BLOG_UP_DESCRIPTION'];

self::$available_plugins[$name]['plugin_copyright'] = 'femu';
self::$available_plugins[$name]['plugin_version'] = '1.0.0';

$to_do = array(
	'acp_main_settings'			=> array('sp_acp_main_settings'),
	'blog_add_after_sql'		=> array('sp_blog_add_after_sql'),
	'blog_approve_confirm'		=> array('sp_blog_approve_confirm'),
	'reply_add_after_sql'		=> array('sp_reply_add_after_sql'),
	'reply_approve_confirm'		=> array('sp_reply_approve_confirm'),
	'user_handle_data'			=> array('sp_user_handle_data'),
);

foreach($to_do as $do => $what)
{
	if (!array_key_exists($do, self::$to_do))
	{
		self::$to_do[$do] = $what;
	}
	else
	{
		self::$to_do[$do] = array_merge(self::$to_do[$do], $what);
	}
}

include($blog_plugins_path . $name . '/functions.' . $phpEx);

?>