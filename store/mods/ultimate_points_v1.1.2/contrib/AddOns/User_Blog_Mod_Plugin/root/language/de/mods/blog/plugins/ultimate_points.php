<?php
/**
*
* @package phpBB3 User Blog Ultimate Points Plugin [German]
* @version $Id: ultimate_points.php 794 2011-12-24 04:34:10Z femu $
* @copyright (c) 2009 femu - http://die-muellers.org 
* @copyright (c) 2008 Doktor_x and McGod, based on User Blog System Points by EXreaction, Lithium Studios
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Create the lang array if it does not already exist
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// Merge the following language entries into the lang array
$lang = array_merge($lang, array(
	'BLOG_UP_DESCRIPTION'		=> 'Erlaubt es den Benutzer Punkte für das Schreiben eines neuen Blogs oder eines Kommentars zu erhalten.<br /><br />Einstellungen findest du unter ACP->Blog Einstellungen.<br /><br /><strong>Du benötigst für dieses Plugin den <a href="http://www.phpbb.com/community/viewtopic.php?f=70&t=1643875">Ultimate Points Mod</a>. Dieser muss erst installiert sein!</strong>',
	'BLOG_UP_TITLE'				=> 'Ultimate Points',
	'BLOG_UP_POINTS'			=> 'Punkte',
	'UP_BLOG_POINTS'			=> 'Punkte pro Blog',
	'UP_BLOG_POINTS_EXPLAIN'	=> 'Anzahl Punkte, die ein Benutzer pro genehmigten Blog erhält',
	'UP_CP_POINTS'				=> 'Punkte im Profil anzeigen',
	'UP_CP_POINTS_EXPLAIN'		=> 'Setze auf JA, um die Anzahl der Punkte im Benutzerprofil anzuzeigen',
	'UP_PLUGIN'					=> 'Ultimate Points Plugin',
	'UP_REPLY_POINTS'			=> 'Punkte pro Kommentar',
	'UP_REPLY_POINTS_EXPLAIN'	=> 'Anzahl Punkte, die ein Benutzer für das Schreiben eines Kommentars erhält',
));

?>