<?php
/**
*
* @package phpBB3 User Blog Ultimate Points Plugin [English]
* @version $Id: ultimate_points.php 794 2011-12-24 04:34:10Z femu $
* @copyright (c) 2009 femu - http://die-muellers.org 
* @copyright (c) 2008 Doktor_x and McGod, based on User Blog System Points by EXreaction, Lithium Studios
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Create the lang array if it does not already exist
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// Merge the following language entries into the lang array
$lang = array_merge($lang, array(
	'BLOG_UP_DESCRIPTION'		=> 'Allows users to gain points by posting a new blog or posting a reply.<br /><br />Options for the amount of points given will be shown in ACP->Blog Settings.<br /><br /><strong>You MUST have the <a href="http://www.phpbb.com/community/viewtopic.php?f=70&t=1643875">Ultimate Points Mod</a> installed, before you can install this plugin.</strong>',
	'BLOG_UP_TITLE'				=> 'Ultimate Points',
	'BLOG_UP_POINTS'			=> 'Points',
	'UP_BLOG_POINTS'			=> 'Points Per Blog',
	'UP_BLOG_POINTS_EXPLAIN'	=> 'The number of points a user will gain per approved blog.',
	'UP_CP_POINTS'				=> 'Points to display in profile',
	'UP_CP_POINTS_EXPLAIN'		=> 'Set to YES to display the number of points in the user\'s profile on the User Blog pages.',
	'UP_PLUGIN'					=> 'Ultimate Points Plugin',
	'UP_REPLY_POINTS'			=> 'Points Per Comment',
	'UP_REPLY_POINTS_EXPLAIN'	=> 'The number of points a user will gain per approved comment on a blog.',
));

?>