<?php
/**
*
* @package Board3 Portal v2 - Lottery
* @copyright (c) Sony Reader Forum ( www.sonyreaderboards.com )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package Leaders
*/
class portal_lottery_module
{
	/**
	* Allowed columns: Just sum up your options (Exp: left + right = 10)
	* top		1
	* left		2
	* center	4
	* right		8
	* bottom	16
	*/
	public $columns = 10;

	/**
	* Default modulename
	*/
	public $name = 'LOTTERY_TITLE';

	/**
	* Default module-image:
	* file must be in "{T_THEME_PATH}/images/portal/"
	*/
	public $image_src = 'portal_lottery.png';

	/**
	* module-language file
	* file must be in "language/{$user->lang}/mods/portal/"
	*/
	public $language = 'portal_lottery_module';

	public function get_template_side($module_id)
	{
		global $config, $template, $user, $auth, $db, $phpEx, $phpbb_root_path;

		// Set variables
		$no_of_tickets = $no_of_players = $last_winner = $last_winner_id = '';

		// Read out the config data
		$sql_array = array(
			'SELECT'	=> 'config_name, config_value',
			'FROM'		=> array(
				POINTS_CONFIG_TABLE => 'c',
			),
		);
		$sql = $db->sql_build_query('SELECT', $sql_array);
		$result = $db->sql_query($sql);

		while ($row = $db->sql_fetchrow($result))
		{
			$points_config[$row['config_name']] = $row['config_value'];
		}
		$db->sql_freeresult($result);

		// Read out values data
		$sql_array = array(
			'SELECT'	=> '*',
			'FROM'		=> array(
				POINTS_VALUES_TABLE => 'v',
			),
		);
		$sql = $db->sql_build_query('SELECT', $sql_array);
		$result = $db->sql_query($sql);
		$points_values = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		// Select the total number of tickets
		$sql_array = array(
			'SELECT'	=> 'COUNT(ticket_id) AS number_of_tickets',
			'FROM'		=> array(
				POINTS_LOTTERY_TICKETS_TABLE => 't',
			),
		);
		$sql = $db->sql_build_query('SELECT', $sql_array);
		$result = $db->sql_query($sql);
		$no_of_tickets = $db->sql_fetchfield('number_of_tickets');
		$db->sql_freeresult($result);

		// Select the total number of players
		$sql_ary = array(
			'SELECT'	=> 'user_id',
			'FROM'	=> array(
				POINTS_LOTTERY_TICKETS_TABLE => 't',
			),
		);
		$sql = $db->sql_build_query('SELECT_DISTINCT', $sql_ary);
		$result = $db->sql_query($sql);
		$no_of_players = 0;
		while ($row = $db->sql_fetchrow($result))
		{
			$no_of_players += 1;
		}
		$db->sql_freeresult($result);

		// Select the last winner id
		$sql_array = array(
			'SELECT'	=> 'user_id',
			'FROM'		=> array(
				POINTS_LOTTERY_HISTORY_TABLE => 'h',
			),
			'ORDER_BY'  => 'id DESC'
		);
		$sql = $db->sql_build_query('SELECT', $sql_array);
		$result = $db->sql_query_limit($sql, 1);
		$last_winner_id = $db->sql_fetchfield('user_id');
		$db->sql_freeresult($result);

		// Check, if a user won or nobody
		if ($last_winner_id != 0)
		{
			// Select the usernames from the user table to reflect user colors
			$sql_array = array(
				'SELECT'	=> 'u.user_id, u.username, u.user_colour, l.id',

				'FROM'		=> array(
					USERS_TABLE	=> 'u',
				),
				'LEFT_JOIN' => array(
					array(
						'FROM'	=> array(POINTS_LOTTERY_HISTORY_TABLE => 'l'),
						'ON'	=> 'u.user_id = l.user_id'
					)
				),
				'ORDER_BY'  => 'l.id DESC'
			);
			$sql = $db->sql_build_query('SELECT', $sql_array);
			$result = $db->sql_query_limit($sql, 1);
			$row = $db->sql_fetchrow($result);

			$winner_name = get_username_string('full', $row['user_id'], $row['username'], $row['user_colour']);
		}
		else
		{
			$winner_name = $user->lang['LOTTERY_NO_WINNER'];
		}

		// Send everything to the template
		$template->assign_vars(array(
			'LAST_WINNER'		=> $winner_name,
			'NO_OF_TICKETS'		=> $no_of_tickets,
			'NO_OF_PLAYERS'		=> $no_of_players,
			'JACKPOT'			=> number_format($points_values['lottery_jackpot'], 2, ",", "."),
			'CASH_NAME'			=> $config['points_name'],
			'NEXT_DRAWING'		=> $user->format_date($points_values['lottery_last_draw_time'] + $points_values['lottery_draw_period']),
			'S_DRAWING_ENABLED'	=> ($points_values['lottery_draw_period']) ? true : false,
			'S_LOTTERY_ENABLED'	=> ($points_config['lottery_enable']) ? true : false,
			'U_LOTTERY'			=> append_sid("{$phpbb_root_path}points.$phpEx", 'mode=lottery'),
		));

		return 'lottery_side.html';
	}

	public function get_template_acp($module_id)
	{
		return array(
			'title'	=> 'PORTAL_LOTTERY',
			'vars'	=> array(),
		);
	}

	/**
	* API functions
	*/
	public function install($module_id)
	{
		return true;
	}

	public function uninstall($module_id)
	{
		return true;
	}
}
