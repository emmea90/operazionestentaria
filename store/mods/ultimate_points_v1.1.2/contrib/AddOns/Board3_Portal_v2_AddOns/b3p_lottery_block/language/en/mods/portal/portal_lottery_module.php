<?php
/**
*
* @package Board3 Portal v2 - Lottery
* @copyright (c) Sony Reader Forum ( www.sonyreaderboards.com )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'PORTAL_LOTTERY'			=> 'Lottery',
	// Lottery Module
    'LOTTERY_TITLE'				=> 'Lottery',
	'LOTTERY_LAST_WINNER'		=> 'Last winner was',
	'LOTTERY_JACKPOT'			=> 'The Jackpot currently holds',
	'LOTTERY_NEXT_DRAW'			=> 'Next draw will be on',
	'LOTTERY_GOTO'				=> 'Goto Lottery',
	'LOTTERY_TICKETS'			=> 'Sold tickets up to now',
	'LOTTERY_PLAYERS'			=> 'Number of players up to now',
	'LOTTERY_NO_WINNER'			=> 'No winner',
));
