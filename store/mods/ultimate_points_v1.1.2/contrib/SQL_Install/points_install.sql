## $Id: points_install.sql 794 2011-12-24 04:34:10Z femu $

ALTER TABLE phpbb_users ADD user_points DECIMAL( 20, 2 ) NOT NULL;
ALTER TABLE phpbb_users ADD user_robbery_pm TINYINT( 1 ) NOT NULL DEFAULT '1';

ALTER TABLE phpbb_users ADD INDEX ( user_points );

ALTER TABLE phpbb_posts ADD points_received DECIMAL( 20, 2 ) NOT NULL default '0.00';
ALTER TABLE phpbb_posts ADD points_poll_received DECIMAL( 20, 2 ) NOT NULL default '0.00';
ALTER TABLE phpbb_posts ADD points_attachment_received DECIMAL( 20, 2 ) NOT NULL default '0.00';
ALTER TABLE phpbb_posts ADD points_topic_received DECIMAL( 20, 2 ) NOT NULL default '0.00';
ALTER TABLE phpbb_posts ADD points_post_received DECIMAL( 20, 2 ) NOT NULL default '0.00';
ALTER TABLE phpbb_posts ADD points_post_edit TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE phpbb_posts ADD points_post_edit_temp DECIMAL( 20, 2 ) NOT NULL DEFAULT '0.00';

ALTER TABLE phpbb_extensions ADD points_extension TINYINT( 1 ) NOT NULL DEFAULT '1';
ALTER TABLE phpbb_extensions ADD points_extension_costs DECIMAL( 20, 2 ) NOT NULL default '1.00';

INSERT INTO phpbb_config (config_name, config_value, is_dynamic) VALUES
('points_name', 'Points', 0),
('points_enable', '1', 0),
('ultimate_points_version', '1.1.2', 0);

ALTER TABLE phpbb_forums ADD forum_perpost DECIMAL( 10, 2 ) NOT NULL default '5.00';
ALTER TABLE phpbb_forums ADD forum_peredit DECIMAL( 10, 2 ) NOT NULL default '0.05';
ALTER TABLE phpbb_forums ADD forum_pertopic DECIMAL( 10, 2 ) NOT NULL default '15.00';
ALTER TABLE phpbb_forums ADD forum_costs TINYINT( 1 ) NOT NULL DEFAULT '1';


INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('u_use_points', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('u_use_bank', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('u_use_logs', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('u_use_robbery', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('u_use_lottery', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('u_use_transfer', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('m_chg_points', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('m_chg_bank', 1, 0, 0);
INSERT INTO phpbb_acl_options (auth_option, is_global, is_local, founder_only) VALUES ('a_points', 1, 0, 0);


DROP TABLE IF EXISTS phpbb_points_bank;
CREATE TABLE IF NOT EXISTS phpbb_points_bank (
  id int(10) unsigned NOT NULL auto_increment,
  user_id int(10) unsigned NOT NULL default '0',
  holding decimal(20,2) NOT NULL default '0.00',
  totalwithdrew decimal(20,2) NOT NULL default '0.00',
  totaldeposit decimal(20,2) NOT NULL default '0.00',
  opentime int(10) unsigned NOT NULL default '0',
  fees char(5) collate utf8_bin NOT NULL default 'on',
  PRIMARY KEY (id),
  KEY holding (holding)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS phpbb_points_config;
CREATE TABLE IF NOT EXISTS phpbb_points_config (
  config_name varchar(255) collate utf8_bin NOT NULL default '',
  config_value varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (config_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO phpbb_points_config (config_name, config_value) VALUES
('transfer_enable', '1'),
('transfer_pm_enable', '1'),
('comments_enable', '1'),
('pertopic_enable', '1'),
('perpost_enable', '1'),
('peredit_enable', '1'),
('logs_enable', '1'),
('images_topic_enable', '1'),
('images_memberlist_enable', '1'),
('lottery_enable', '1'),
('bank_enable', '1'),
('robbery_enable', '1'),
('points_disablemsg', 'Ultimate Points is currently disabled!'),
('stats_enable', '1'),
('lottery_multi_ticket_enable', '1'),
('robbery_sendpm', '1'),
('gallery_deny_view', '0'),
('robbery_usage', '1'),
('display_lottery_stats', '1');


DROP TABLE IF EXISTS phpbb_points_log;
CREATE TABLE IF NOT EXISTS phpbb_points_log (
  id int(11) unsigned NOT NULL auto_increment,
  point_send int(11) unsigned NOT NULL,
  point_recv int(11) unsigned NOT NULL,
  point_amount decimal(20,2) NOT NULL default '0.00',
  point_sendold decimal(20,2) NOT NULL default '0.00',
  point_recvold decimal(20,2) NOT NULL default '0.00',
  point_comment mediumtext collate utf8_bin NOT NULL,
  point_type int(11) unsigned NOT NULL,
  point_date int(11) unsigned NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS phpbb_points_lottery_history;
CREATE TABLE IF NOT EXISTS phpbb_points_lottery_history (
  id int(11) unsigned NOT NULL auto_increment,
  user_id mediumint(8) unsigned NOT NULL default '0',
  user_name varchar(255) collate utf8_bin NOT NULL default '',
  time int(11) unsigned NOT NULL default '0',
  amount decimal(20,2) NOT NULL default '0.00',
  PRIMARY KEY  (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS phpbb_points_lottery_tickets;
CREATE TABLE IF NOT EXISTS phpbb_points_lottery_tickets (
  ticket_id int(11) unsigned NOT NULL auto_increment,
  user_id int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (ticket_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS phpbb_points_values;
CREATE TABLE IF NOT EXISTS phpbb_points_values (
  bank_cost decimal(10,2) NOT NULL default '0.00',
  bank_fees decimal(10,2) NOT NULL default '0.00',
  bank_interest decimal(10,2) NOT NULL default '0.00',
  bank_interestcut decimal(20,2) NOT NULL default '0.00',
  bank_last_restocked int(11) unsigned NOT NULL,
  bank_min_deposit decimal(10,2) NOT NULL default '0.00',
  bank_min_withdraw decimal(10,2) NOT NULL default '0.00',
  bank_name varchar(100) collate utf8_bin NOT NULL,
  bank_pay_period int(10) unsigned NOT NULL default '2592000',
  forum_edit decimal(10,2) NOT NULL default '0.00',
  forum_post decimal(10,2) NOT NULL default '0.00',
  forum_topic decimal(10,2) NOT NULL default '0.00',
  gallery_upload decimal(10,2) NOT NULL default '0.00',
  gallery_remove decimal(10,2) NOT NULL default '0.00',
  gallery_view decimal(10,2) NOT NULL default '0.00',
  lottery_base_amount decimal(10,2) NOT NULL default '50.00',
  lottery_chance decimal(5,2) NOT NULL default '50.00',
  lottery_draw_period int(10) unsigned NOT NULL default '3600',
  lottery_jackpot decimal(20,2) NOT NULL default '50.00',
  lottery_last_draw_time int(11) unsigned NOT NULL,
  lottery_max_tickets int(10) unsigned NOT NULL default '10',
  lottery_name varchar(100) collate utf8_bin NOT NULL default '',
  lottery_pm_from int(10) unsigned NOT NULL default '0',
  lottery_prev_winner varchar(255) collate utf8_bin NOT NULL default '',
  lottery_prev_winner_id int(10) unsigned NOT NULL default '0',
  lottery_ticket_cost decimal(10,2) NOT NULL default '0.00',
  lottery_winners_total mediumint(8) unsigned NOT NULL default '0',
  number_show_per_page int(10) unsigned NOT NULL default '0',
  number_show_top_points mediumint(8) unsigned NOT NULL default '0',
  points_per_attach decimal(10,2) NOT NULL default '0.00',
  points_per_attach_file decimal(10,2) NOT NULL default '0.00',
  points_per_poll decimal(10,2) NOT NULL default '0.00',
  points_per_poll_option decimal(10,2) NOT NULL default '0.00',
  points_per_post_character decimal(10,2) NOT NULL default '0.00',
  points_per_post_word decimal(10,2) NOT NULL default '0.00',
  points_per_topic_character decimal(10,2) NOT NULL default '0.00',
  points_per_topic_word decimal(10,2) NOT NULL default '0.00',
  points_per_warn decimal(10,2) NOT NULL default '0.00',
  reg_points_bonus decimal(10,2) NOT NULL default '0.00',
  robbery_chance decimal(5,2) NOT NULL default '0.00',
  robbery_loose decimal(5,2) NOT NULL default '0.00',
  robbery_max_rob decimal(5,2) NOT NULL default '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



INSERT INTO phpbb_points_values (bank_cost, bank_fees, bank_interest, bank_interestcut, bank_last_restocked, bank_min_deposit, bank_min_withdraw, bank_name, bank_pay_period, forum_edit, forum_post, forum_topic, gallery_upload, gallery_remove, gallery_view, lottery_base_amount, lottery_chance, lottery_draw_period, lottery_jackpot, lottery_last_draw_time, lottery_max_tickets, lottery_name, lottery_pm_from, lottery_prev_winner, lottery_prev_winner_id, lottery_ticket_cost, lottery_winners_total, number_show_per_page, number_show_top_points, points_per_attach, points_per_attach_file, points_per_poll, points_per_poll_option, points_per_post_character, points_per_post_word, points_per_topic_character, points_per_topic_word, points_per_warn, reg_points_bonus, robbery_chance, robbery_loose, robbery_max_rob) VALUES
(0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 'BANK NAME', 2592000, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 50.00, 50.00, 3600, 50.00, 0, 10, 'LOTTERY NAME', 0, 0, 0, 10.00, 0, 15, 10, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 50.00, 50.00, 50.00, 10.00);


