<?php
/**
* permissions_dm_partners [English]
*
* @package language
* @version $Id: permissions_dm_partners.php 202 2009-12-17 08:40:11Z femu $
* @copyright (c) 2008 lefty74, femu
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
   $lang = array();
}

// Adding new category
$lang['permission_cat']['dm_mods']   = 'DM Mods';

// Adding the permissions
$lang = array_merge($lang, array(
	'acl_a_dm_partners_view'	=> array('lang'	=> 'Admin can view DM Partners', 'cat' => 'dm_mods',),
	'acl_u_dm_partners_view'	=> array('lang'	=> 'User can view DM Partners', 'cat' => 'dm_mods',),
));

?>