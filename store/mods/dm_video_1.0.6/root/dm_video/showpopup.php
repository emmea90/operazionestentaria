<?php
/**
*
* @package DM Video
* @version $Id: showpopup.php 251 2010-12-29 17:14:51Z femu $
* @copyright (c) 2008, 2009 femu - http://die-muellers.org
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/message_parser.' . $phpEx);

// Some mod related variables and includes
$dm_video_path = (defined('DM_VIDEO_ROOT_PATH')) ? DM_VIDEO_ROOT_PATH : 'dm_video/';
include($phpbb_root_path . $dm_video_path . 'includes/common_dm_video.' . $phpEx);
include($phpbb_root_path . $dm_video_path . 'includes/functions_dm_video.' . $phpEx);

// Do the session handling
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/dm_video');

// Check authorisation
$is_authorised = ($auth->acl_get('u_dm_video_view') || $auth->acl_get('a_dm_video_view')) ? true : false;

if (!$is_authorised)
{
	trigger_error('NOT_AUTHORISED');
}

// Check, if Highslide JS is installed
if (file_exists($phpbb_root_path . 'highslide/highslide-full.js'))
{
	$template_html = $dm_video_path . 'video_popup_hs_body.html';
}
else
{
	$template_html = $dm_video_path . 'video_popup_body.html';
}

// Set some variables
$video_id	= request_var('v', 0);

// Select some data for the template
$sql = 'SELECT *
	FROM ' . DM_VIDEO_TABLE . '
	WHERE video_id = ' . (int) $video_id;
$result = $db->sql_query($sql);
$row = $db->sql_fetchrow($result);
$db->sql_freeresult($result);

$video_title 	= (string) $row['video_title'];
$video_singer 	= (string) $row['video_singer'];
$video_url 		= (string) $row['video_url'];

// Send the variables to the template
$template->assign_vars(array(
	'VIDEO_TITLE' 	=> $video_title,
	'VIDEO_SINGER'	=> $video_singer,
	'VIDEO_URL'		=> htmlspecialchars_decode($row['video_url']),
	)
);
$db->sql_freeresult($result);

// Header information
page_header($user->lang['DMV_VIDEO'] . ' &bull; ' . censor_text($row['video_title']));

// Template file to use
$template->set_filenames(array(
   'body' => $template_html,
));

// Footer information
page_footer();

?>