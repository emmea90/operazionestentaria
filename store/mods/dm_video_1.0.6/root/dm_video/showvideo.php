<?php
/**
*
* @package DM Video
* @version $Id: showvideo.php 251 2010-12-29 17:14:51Z femu $
* @copyright (c) 2008, 2009 femu - http://die-muellers.org
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);
include($phpbb_root_path . 'includes/bbcode.' . $phpEx);
include($phpbb_root_path . 'includes/message_parser.' . $phpEx);

// Some mod related variables and includes
$dm_video_path = (defined('DM_VIDEO_ROOT_PATH')) ? DM_VIDEO_ROOT_PATH : 'dm_video/';
$dm_facebook_path = (defined('DM_FACEBOOK_ROOT_PATH')) ? DM_FACEBOOK_ROOT_PATH : ($phpbb_root_path . 'dm_video/');
include($phpbb_root_path . $dm_video_path . 'includes/common_dm_video.' . $phpEx);
include($phpbb_root_path . $dm_video_path . 'includes/functions_dm_video.' . $phpEx);

// Do the session handling
$user->session_begin();
$auth->acl($user->data);
$user->setup(array('viewtopic', 'viewforum', 'mods/dm_video'));

// Check authorisation
$is_authorised = ($auth->acl_get('u_dm_video_view') || $auth->acl_get('a_dm_video_view')) ? true : false;

if (!$is_authorised)
{
	trigger_error('NOT_AUTHORISED');
}

// Set some variables
$video_id	= request_var('v', 0);
$cat_id 	= request_var('c', 0);
$number  	= $dmv_config['video_comments_page1'];
$start 	 	= request_var('start', 0);
$sort_key1	= request_var('sk', '');
$sort_dir1	= request_var('sd', '');
$start1		= request_var('start', 0);
add_form_key('dm_video_rating');

// Main template variables for the navigation
$template->assign_block_vars('navlinks', array(
	'FORUM_NAME'	=> $user->lang['DMV_VIDEO'],
	'U_VIEW_FORUM'	=> append_sid("{$phpbb_root_path}{$dm_video_path}index.$phpEx"),
));

// We need som informations about the category we are in for the navigation
$cat_data = get_cat_info($cat_id);

// Generate the navigation
generate_cat_nav($cat_data);

// Generate the sub categories
generate_cat_list($cat_id);

/**
* Select the needed data to show the rating
*/
$sql = 'SELECT video_votetotal, video_votesum
	FROM ' . DM_VIDEO_TABLE . '
	WHERE video_id = ' . (int) $video_id;
$result = $db->sql_query($sql);
$row = $db->sql_fetchrow($result);
$db->sql_freeresult($result);

$votes = $row['video_votetotal'];
$sum = $row['video_votesum'];
$avg_score = ($row['video_votetotal'] == 0) ? 0 : round($row['video_votesum'] / $row['video_votetotal'], 2);

// Do the rating stuff
if (!$auth->acl_get('u_dm_video_rate'))
{
	trigger_error('DMV_RATING_NO_PERM');
}

if ($auth->acl_get('u_dm_video_rate'))
{
	if ( isset($_POST['submit']) )
	{
		if (!check_form_key('dm_video_rating'))
		{
			trigger_error($user->lang['FORM_INVALID']);
		}

		$sql_array = array(
			'SELECT'    => 'v.video_votetotal, v.video_votesum, r.*',

			'FROM'      => array(
				DM_VIDEO_TABLE  => 'v'
			),

			'LEFT_JOIN' => array(
				array(
					'FROM'  => array(DM_VIDEO_RATE_TABLE => 'r'),
					'ON'    => 'v.video_id = r.video_id'
				)
			),

			'WHERE'     => 'v.video_id = ' . (int) $video_id . '
				AND r.user_id = ' . (int) $user->data['user_id'],
		);

		$sql = $db->sql_build_query('SELECT', $sql_array);

		// now run the query...
		$result = $db->sql_query($sql);

		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		if ( !$row['video_rating'] )
		{
			$already_rated = false;
		}
		else
		{
			$already_rated = true;
		}
		
		$rating = request_var('video_rating', 0);

		$old_rating = (int) $row['video_rating'];
		$video_votesum = (int) $row['video_votesum'];

		// Lets do the video ratings...
		if ($rating != 0 && !$already_rated)
		{
			$sql_ary = array(
				'video_id'		=> (int) $video_id,
				'user_id'		=> (int) $user->data['user_id'],
				'video_rating'	=> (int) $rating,
				'rating_date'	=> time(),
				'user_ip'		=> $user->ip,
			);

			$sql = 'INSERT INTO ' . DM_VIDEO_RATE_TABLE . ' ' . $db->sql_build_array('INSERT', $sql_ary);
			$db->sql_query($sql);
			add_log('user', (int) $user->data['user_id'], 'LOG_USER_VIDEO_RATE', str_replace('%', '*', (int) $video_id));

			$sql = 'UPDATE ' . DM_VIDEO_TABLE . '
					SET video_votetotal = video_votetotal + 1,
						video_votesum = video_votesum + ' . $rating . '
					WHERE video_id = ' . (int) $video_id;
			$db->sql_query($sql);
		}
		else if ($rating != 0 && $already_rated)
		{
			$rating_old = $row['video_rating'];
			$sql_ary = array(
				'video_rating'	=> $rating,
			);

			$sql = 'UPDATE ' . DM_VIDEO_RATE_TABLE . ' 
					SET ' . $db->sql_build_array('UPDATE', $sql_ary) . ' 
					WHERE video_id = ' . (int) $video_id . ' 
						AND user_id = ' . (int) $user->data['user_id'];
			$db->sql_query($sql);

			$new_video_votesum = ($video_votesum-$old_rating+$rating);
			
			$sql = 'UPDATE ' . DM_VIDEO_TABLE . '
					SET video_votesum = ' . $new_video_votesum . '
					WHERE video_id = ' . (int) $video_id;
			$db->sql_query($sql);
			add_log('user', (int) $user->data['user_id'], 'LOG_USER_VIDEO_RATE_EDITED', str_replace('%', '*', (int) $video_id));
		}

		$template->assign_vars(array(
			'S_RATED_SUCCESSFUL'	=> true,
		));
	}
}

// This will check whether the user has already voted.
$s_hidden_fields = '';

$sql_array = array(
	'SELECT'	=> 'g.video_votetotal, g.video_votesum, r.*',
	'FROM'		=> array(
		DM_VIDEO_TABLE	=> 'g',
	),
	'LEFT_JOIN'	=> array(
		array(
			'FROM'	=> array(DM_VIDEO_RATE_TABLE => 'r'),
			'ON'	=> 'g.video_id = r.video_id'
		)
	),
	'WHERE'		=> 'g.video_id = ' . (int) $video_id . '
		AND r.user_id = ' . (int) $user->data['user_id'],
);

$sql = $db->sql_build_query('SELECT', $sql_array);
$result = $db->sql_query($sql);

$row = $db->sql_fetchrow($result);
$db->sql_freeresult($result);

$user_rating = ((int) $row['video_rating'] == '') ? 0 : (int) $row['video_rating'];
$video_rating_select = get_rating_select($user_rating);

// Send the variables to the template
$template->assign_vars(array(
	'S_HAS_PERM_RATE'		=> $auth->acl_get('u_dm_video_rate'),

	'VIDEO_RATING_SELECT'	=> $video_rating_select,

	'U_BACK_CAT'			=> append_sid("{$phpbb_root_path}{$dm_video_path}viewcat.$phpEx", "c=" . (int) $cat_id),

	'S_ACTION' 				=> append_sid("{$phpbb_root_path}{$dm_video_path}showvideo.$phpEx", "v=" . (int) $video_id . "&amp;c=" . (int) $cat_id),
	'S_HIDDEN_FIELDS' 		=> $s_hidden_fields,
	)
);
$db->sql_freeresult($result);

/**
* Show the first xx comments for the video
**/

// Selecting the needed fields from the comment and users table
// and send the values as the comment rows with pagination
$sql_array = array(
    'SELECT'    => 'vc.*, u.username, u.user_id, u.user_colour',

    'FROM'      => array(
        DM_VIDEO_COMMENT_TABLE  => 'vc'
    ),

    'LEFT_JOIN' => array(
        array(
            'FROM'  => array(USERS_TABLE => 'u'),
            'ON'    => 'vc.video_user_id = u.user_id'
        )
    ),

    'WHERE'     => 'vc.video_id = ' . (int) $video_id,
	
	'ORDER_BY'	=> 'vc.comment_id DESC'
);

$sql = $db->sql_build_query('SELECT', $sql_array);

// Following limit is need to set the pagination!!
$result = $db->sql_query_limit($sql, $number, $start);

while ($row = $db->sql_fetchrow($result))
{
	$video_comment = generate_text_for_display($row['video_comment'], $row['comment_bbcode_uid'], $row['comment_bbcode_bitfield'], $row['comment_bbcode_options']);
	$video_comment_id	= (int) $row['comment_id'];
	$video_username		= (string) $row['video_username'];
	$video_user_colour	= (string) $row['video_user_colour'];
	$video_time			= (string) $row['video_time'];

	$template->assign_block_vars('comments',array(
		'VIDEO_COMMENT'			=> $video_comment,

		'S_EDIT_COMMENT'		=> (($auth->acl_get('u_dm_video_comment_edit') && $row['video_user_id'] == $user->data['user_id']) || $auth->acl_get('a_dm_video_edit')) ? true : false,
		'S_DELETE_COMMENT'		=> (($auth->acl_get('u_dm_video_comment_del') && $row['video_user_id'] == $user->data['user_id']) || $auth->acl_get('a_dm_video_edit')) ? true : false,

		'COMMENT_AUTHOR_FULL'	=> get_username_string('full', $row['user_id'], $row['username'], $row['user_colour'], $row['username']),
		'COMMENT_POST_TIME'		=> $user->format_date($row['video_time']),

		'U_EDIT_COMMENT'		=> append_sid("{$phpbb_root_path}{$dm_video_path}postcomment.$phpEx", "mode=edit&amp;p=" . (int) $video_comment_id),
		'U_DELETE_COMMENT'		=> append_sid("{$phpbb_root_path}{$dm_video_path}postcomment.$phpEx", "mode=delete&amp;p=" . (int) $video_comment_id),
	));
}

// Check enabled shared buttons
// Read out config values to get them
$facebook_like = '';
$sql = 'SELECT *
	FROM ' . DM_VIDEO_CONFIG_TABLE;
$result = $db->sql_query($sql);
while ($row = $db->sql_fetchrow($result))
{
	$video_config[$row['config_name']] = $row['config_value'];
}
$db->sql_freeresult($result);

// Main template stuff
$template->assign_vars(array(
	'EDIT_IMG' 				=> $user->img('icon_post_edit', 'EDIT_POST'),
	'DELETE_IMG' 			=> $user->img('icon_post_delete', 'DELETE_POST'),

	'S_ADD_COMMENT'			=> $auth->acl_get('u_dm_video_comment_add'),

	'U_ADD_COMMENT'			=> append_sid("{$phpbb_root_path}{$dm_video_path}postcomment.$phpEx", "mode=new&amp;c=" . (int) $cat_id . "&amp;v=" . (int) $video_id),
	'U_BACK_CAT'			=> append_sid("{$phpbb_root_path}{$dm_video_path}viewcat.$phpEx", "c=" . (int) $cat_id),
	'U_SHOW_ALL_COMMENTS'	=> append_sid("{$phpbb_root_path}{$dm_video_path}showcomments.$phpEx", "v=" . (int)$video_id . "&amp;c=" . (int) $cat_id),
));

// Select the needed data to show the video
$sql_array = array(
    'SELECT'    => 'v.*, u.username, u.user_id, u.user_colour',

    'FROM'      => array(
        DM_VIDEO_TABLE  => 'v'
    ),

    'LEFT_JOIN' => array(
        array(
            'FROM'  => array(USERS_TABLE => 'u'),
            'ON'    => 'v.video_user_id = u.user_id'
        )
    ),

    'WHERE'     => 'v.video_id = ' . (int) $video_id,
);

$sql = $db->sql_build_query('SELECT', $sql_array);
$result = $db->sql_query($sql);
$row = $db->sql_fetchrow($result);

$video_user_id 		= (int) $row['video_user_id'];
$video_username 	= (string) $row['video_username'];
$video_usercolour 	= (string) $row['user_colour'];
$video_img 			= (string) $row['video_image'];
$songtext 			= generate_text_for_display($row['video_songtext'], $row['bbcode_uid'], $row['bbcode_bitfield'], $row['bbcode_options']);
$video = '<img src="' . $phpbb_root_path . 'images/dm_video/icon_charts_video.png" alt="" title="' . sprintf($user->lang['DMV_SHOW_POPUP'], $row['video_title']) . '" />';

// Select the category name
$sql_array = array(
    'SELECT'    => 'cat_name',

    'FROM'      => array(
        DM_VIDEO_CATS_TABLE => 'c'
    ),

    'WHERE'     =>  'cat_id = ' . (int) $cat_id,
);

$sql2 = $db->sql_build_query('SELECT', $sql_array);
$result = $db->sql_query($sql2);
$row2 = $db->sql_fetchrow($result);

// Count the number of comments
$sql_array = array(
    'SELECT'    => 'COUNT(comment_id) AS comment_counter',

    'FROM'      => array(
        DM_VIDEO_COMMENT_TABLE => 'vc'
    ),

    'WHERE'     =>  'video_id = ' . (int) $video_id,
);

$sql3 = $db->sql_build_query('SELECT', $sql_array);
$result = $db->sql_query($sql3);

$comment_counter = (int) $db->sql_fetchfield('comment_counter');

if ( $comment_counter == 0 )
{
	$template->assign_vars(array(
		'CC' => $user->lang['DMV_COMMENT_NO_CC'],
	));
}
elseif ( $comment_counter == 1 )
{
	$template->assign_vars(array(
		'CC' => $user->lang['DMV_COMMENT_SINGLE'],
	));
}
else
{
	$template->assign_vars(array(
		'CC' => sprintf($user->lang['DMV_COMMENT_MULTI'], $comment_counter),
	));
}

// Check, if Highslide JS is installed
if (file_exists($phpbb_root_path . 'highslide/highslide-full.js'))
{
	$template->assign_vars(array(
		'S_HIGHSLIDE'	=> true,
	));
}

// Send the variables to the template
$template->assign_vars(array(
	'EDIT_IMG' 				=> $user->img('icon_post_edit', 'EDIT_POST'),
	'DELETE_IMG' 			=> $user->img('icon_post_delete', 'DELETE_POST'),
	'REPORT_IMG'			=> $user->img('icon_post_report', 'REPORT_POST'),

	'S_HAS_PERM_RATE'		=> $auth->acl_get('u_dm_video_rate'),
	'S_ADD_VIDEO'			=> $auth->acl_get('u_dm_video_add'),
	'S_EDIT_VIDEO'			=> (($auth->acl_get('u_dm_video_edit') && $row['video_user_id'] == $user->data['user_id']) || $auth->acl_get('a_dm_video_edit')),
	'S_REPORT_VIDEO'		=> $auth->acl_get('u_dm_video_report') || $auth->acl_get('a_dm_video_edit'),
	'S_DELETE_VIDEO'		=> (($auth->acl_get('u_dm_video_del') && $row['video_user_id'] == $user->data['user_id']) || $auth->acl_get('a_dm_video_edit')),
	'S_SHOW_COMMENTS'		=> $auth->acl_get('u_dm_video_comment_view'),

	'S_SHARE_ENABLE'		=> $video_config['share_enable'],
	'S_SHARE_FACEBOOK'		=> $video_config['facebook_enable'],
	'S_SHARE_DIGG'			=> $video_config['digg_enable'],
	'S_SHARE_DELICIOUS' 	=> $video_config['delicious_enable'],
	'S_SHARE_TECHNORATI'	=> $video_config['technorati_enable'],

	'U_COMMENT_VIDEO'		=> append_sid("{$phpbb_root_path}{$dm_video_path}showcomments.$phpEx", "v=" . (int) $video_id . "&amp;c=" . (int) $cat_id),
	'U_EDIT_VIDEO'			=> append_sid("{$phpbb_root_path}{$dm_video_path}postvideo.$phpEx", "mode=edit&amp;v=" . (int) $video_id . "&amp;c=" . (int) $cat_id . '&amp;sk=' . $sort_key1 . '&amp;sd=' . $sort_dir1 . '&amp;start=' . $start1),
	'U_DELETE_VIDEO'		=> append_sid("{$phpbb_root_path}{$dm_video_path}postvideo.$phpEx", "mode=delete&amp;v=" . (int) $video_id . "&amp;c=" . (int) $cat_id),
	'U_REPORT_VIDEO'		=> append_sid("{$phpbb_root_path}{$dm_video_path}reportvideo.$phpEx", "v=" . (int) $video_id . "&amp;c=" . (int) $cat_id),
	'U_RATE_VIDEO'			=> append_sid("{$phpbb_root_path}{$dm_video_path}ratevideo.$phpEx", "v=" . (int) $video_id . "&amp;c=" . (int) $cat_id),
	'U_SHOW_POPUP'			=> append_sid("{$phpbb_root_path}{$dm_video_path}showpopup.$phpEx", "v=" . (int) $video_id),
	'U_FACEBOOK'			=> "http://www.facebook.com/share.php?u=" . generate_board_url() . "/showvideo.$phpEx?v=" . $video_id . "&amp;c=" . $cat_id,
	'U_DIGG'				=> "http://digg.com/submit?phase=2&amp;url=" . generate_board_url() . "/showvideo.$phpEx?v=" . $video_id . "&amp;c=" . $cat_id . '&amp;title=' . $row['video_title'],
	'U_MYSPACE'				=> "http://www.myspace.com/Modules/PostTo/Pages/?u=" . generate_board_url() . "/showvideo.$phpEx?v=" . $video_id . "&amp;c=" . $cat_id . '&amp;t=' . $row['video_title'],
	'U_DELICIOUS' 			=> "http://delicious.com/post?url=" . generate_board_url() . "/showvideo.$phpEx?v=" . $video_id . "&amp;c=" . $cat_id . '&amp;title=' . $row['video_title'],		
	'U_TECHNORATI'			=> "http://technorati.com/faves?add=" . generate_board_url() . "/showvideo.$phpEx?v=" . $video_id . "&amp;c=" . $cat_id,
	
	'VIDEO_RATING_IMG' 		=> set_rating_image($votes, $sum, $avg_score),	
	
	'VIDEO_CLICKS'			=> ($row['video_counter'] == 1) ? $user->lang['DMV_SINGLE_VIEW'] : sprintf($user->lang['DMV_MULTI_VIEW'], $row['video_counter']),

	'VIDEO_ADDED_BY'		=> get_username_string('full', $video_user_id, $video_username,$video_usercolour),
	'VIDEO_TITLE' 			=> (string) $row['video_title'],
	'VIDEO_SONGTEXT'		=> (string) $songtext,
	'VIDEO_URL' 			=> htmlspecialchars_decode($row['video_url']),
	'VIDEO_SINGER'			=> (string) $row['video_singer'],
	'VIDEO_DURATION'		=> (string) $row['video_duration'],
	'VIDEO_CAT_NAME'		=> (string) $row2['cat_name'],
	'VIDEO_TIME'			=> $user->format_date($row['video_time']),
	'VIDEO_IMAGE'			=> htmlspecialchars_decode($video_img),
	'U_BACK_CAT'			=> append_sid("{$phpbb_root_path}{$dm_video_path}viewcat.$phpEx", "c=" . $cat_id),
	'VIDEO'					=> $video,
	'U_BACK_PAGE'			=> append_sid("{$phpbb_root_path}{$dm_video_path}viewcat.$phpEx", 'c=' . (int) $cat_id . '&amp;sk=' . $sort_key1 . '&amp;sd=' . $sort_dir1 . '&amp;start=' . $start1),
));

$db->sql_freeresult($result);

// Update the view counter
if (isset($user->data['session_page']) && !$user->data['is_bot'] && strpos($user->data['session_page'], '&amp;c=' . $cat_id) === false)
{
	$db->sql_query('UPDATE ' . DM_VIDEO_TABLE . ' SET video_counter = video_counter + 1 WHERE video_id = ' . (int) $video_id);
}

// Copyright notice
$template->assign_vars(array(
	'DMV_COPY_NOTE'		=> sprintf($user->lang['DMV_COPY_NOTE'], (string) $dmv_config['copyright_email'], $dmv_config['copyright_show']),
));

// Header information
page_header($user->lang['DMV_VIDEO'] . ' &bull; ' . censor_text($row['video_title']));

// Template file to use
$template->set_filenames(array(
   'body' => $dm_video_path . 'showvideo_body.html'
));

// Footer information
page_footer();

?>