<?php
/**
*
* @package version check
* @version $Id: dm_video_check_version.php 251 2010-12-29 17:14:51Z femu $
* @copyright (c) 2008, 2009 femu - http://die-muellers.org
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @package mod_version_check
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

class dm_video_check_version
{
	function version()
	{
		global $config;

		return array(
			'author'	=> 'femu',
			'title'		=> 'DM Video',
			'tag'		=> 'dm_video',
			'version'	=> '1.0.6',
			'file'		=> array('die-muellers.org', 'updatecheck', 'dm_video.xml'),
		);
	}
}

?>