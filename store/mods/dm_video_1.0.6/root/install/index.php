<?php
/** 
*
* @package DM Video
* @version $Id: index.php 259 2011-01-01 06:16:22Z femu $
* @copyright (c) 2009 wuerzi & femu
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
define('UMIL_AUTO', true);
define('IN_PHPBB', true);
define('IN_INSTALL', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();

if (!file_exists($phpbb_root_path . 'umil/umil_auto.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}

// The name of the mod to be displayed during installation.
$mod_name = 'UMIL_DMV_NAME';

/*
* The name of the config variable which will hold the currently installed version
* You do not need to set this yourself, UMIL will handle setting and updating the version itself.
*/

$version_config_name = 'dm_video_version';

/*
* The language file which will be included when installing
* Language entries that should exist in the language file for UMIL (replace $mod_name with the mod's name you set to $mod_name above)
*/
$language_file = 'mods/dm_video';

/*
* Optionally we may specify our own logo image to show in the upper corner instead of the default logo.
* $phpbb_root_path will get prepended to the path specified
* Image height should be 50px to prevent cut-off or stretching.
*/
$logo_img = 'install/dm_logo.png';

/*
* The array of versions and actions within each.
* You do not need to order it a specific way (it will be sorted automatically), however, you must enter every version, even if no actions are done for it.
*
* You must use correct version numbering.  Unless you know exactly what you can use, only use X.X.X (replacing X with an integer).
* The version numbering must otherwise be compatible with the version_compare function - http://php.net/manual/en/function.version-compare.php
*/

/**
* Define the basic structure
* The format:
*		array('{TABLE_NAME}' => {TABLE_DATA})
*		{TABLE_DATA}:
*			COLUMNS = array({column_name} = array({column_type}, {default}, {auto_increment}))
*			PRIMARY_KEY = {column_name(s)}
*			KEYS = array({key_name} = array({key_type}, {column_name(s)})),
*
*	Column Types:
*	INT:x		=> SIGNED int(x)
*	BINT		=> BIGINT
*	UINT		=> mediumint(8) UNSIGNED
*	UINT:x		=> int(x) UNSIGNED
*	TINT:x		=> tinyint(x)
*	USINT		=> smallint(4) UNSIGNED (for _order columns)
*	BOOL		=> tinyint(1) UNSIGNED
*	VCHAR		=> varchar(255)
*	CHAR:x		=> char(x)
*	XSTEXT_UNI	=> text for storing 100 characters (topic_title for example)
*	STEXT_UNI	=> text for storing 255 characters (normal input field with a max of 255 single-byte chars) - same as VCHAR_UNI
*	TEXT_UNI	=> text for storing 3000 characters (short text, descriptions, comments, etc.)
*	MTEXT_UNI	=> mediumtext (post text, large text)
*	VCHAR:x		=> varchar(x)
*	TIMESTAMP	=> int(11) UNSIGNED
*	DECIMAL		=> decimal number (5,2)
*	DECIMAL:		=> decimal number (x,2)
*	PDECIMAL		=> precision decimal number (6,3)
*	PDECIMAL:	=> precision decimal number (x,3)
*	VCHAR_UNI	=> varchar(255) BINARY
*	VCHAR_CI		=> varchar_ci for postgresql, others VCHAR
*/

$versions = array(
	// Version 1.0.0 - this is the first version using UMIL
	'1.0.0' => array(
		// Now to add some permission settings
		'permission_add' => array(
			array('u_dm_video_view', true),
			array('u_dm_video_rate', true),
			array('a_dm_video_view', true),
			array('a_dm_video_rate', true),
		),

		// Now to add the tables (this uses the layout from develop/create_schema_files.php and from phpbb_db_tools)
		'table_add' => array(
			array($table_prefix . 'dm_video', array(
					'COLUMNS'		=> array(
						'video_id'				=> array('UINT:10', NULL, 'auto_increment'),
						'video_title'			=> array('VCHAR', ''),
						'video_url'				=> array('MTEXT_UNI', ''),
						'video_songtext'		=> array('MTEXT_UNI', ''),
						'video_singer'			=> array('VCHAR', ''),
						'video_duration'		=> array('VCHAR', ''),
						'video_user_id'			=> array('UINT', 0),
						'video_username'		=> array('VCHAR:32', ''),
						'video_time'			=> array('VCHAR:15', ''),
						'video_cat_id'			=> array('UINT', 0),
						'video_approval'		=> array('TINT:3', 0),
						'video_counter'			=> array('UINT:11', 0),
						'video_votetotal'		=> array('UINT:8', 0),
						'video_votesum'			=> array('UINT:8', 0),
						'last_poster_id'		=> array('UINT', 0),
						'last_poster_name'		=> array('VCHAR', ''),
						'last_poster_colour'	=> array('VCHAR:6', ''),
						'last_poster_time'		=> array('UINT:11', 0),
						'last_poster_cat_id'	=> array('UINT', 0),
					),
					'PRIMARY_KEY'	=> 'video_id',
					'KEYS'			=> array(
						'video_cat_id'				=> array('INDEX', 'video_cat_id'),
						'video_user_id'				=> array('INDEX', 'video_user_id'),
						'video_time'				=> array('INDEX', 'video_time'),
					),
				),
			),

			array($table_prefix . 'dm_video_cat', array(
					'COLUMNS'		=> array(
						'cat_id'					=> array('UINT', NULL, 'auto_increment'),
						'parent_id'					=> array('UINT', 0),
						'left_id'					=> array('UINT', 1),
						'right_id'					=> array('UINT', 2),
						'cat_parents'				=> array('MTEXT_UNI', ''),
						'cat_name'					=> array('VCHAR', ''),
						'cat_desc'					=> array('MTEXT_UNI', ''),
						'cat_desc_uid'				=> array('VCHAR:8', ''),
						'cat_desc_bitfield'			=> array('VCHAR', ''),
						'cat_desc_options'			=> array('UINT', 0),
						'video_last_poster_title'	=> array('VCHAR', ''),
						'video_last_poster_id'		=> array('UINT', 0),
						'video_last_poster_name'	=> array('VCHAR', ''),
						'video_last_poster_colour'	=> array('VCHAR:6', ''),
						'video_last_video_time'		=> array('UINT:11', 0),
					),
					'PRIMARY_KEY'	=> 'cat_id',
				),
			),

			array($table_prefix . 'dm_video_rating', array(
					'COLUMNS'		=> array(
						'video_id'		=> array('UINT:8', 0),
						'user_id'		=> array('UINT:8', 0),
						'video_rating'	=> array('UINT:8', 0),
						'rating_date'	=> array('UINT:11', 0),
						'user_ip'		=> array('VCHAR:11', ''),
					),
					'PRIMARY_KEY'	=> array('video_id', 'user_id'),
				),
			),
		),

		// Clear the general cache as well as the templates, imagesets and themes cache
		'cache_purge' => array(
			array(),
			array('imageset'),
			array('template'),
			array('theme'),
		),

		// Alright, now lets add some modules to the ACP
		'module_add' => array(
			// First, lets add a new category named ACP_DMV_DM_VIDEO to ACP_CAT_DOT_MODS
			array('acp', 'ACP_CAT_DOT_MODS', 'ACP_DMV_DM_VIDEO'),

			// Now we will add the category modul
			array('acp', 'ACP_DMV_DM_VIDEO', array(
					'module_basename'	=> 'dm_video',
					'module_langname'	=> 'ACP_DMV_MANAGE_CATEGORIES',
					'module_mode'		=> 'manage_categories',
					'module_auth'		=> 'acl_a_dm_video_cats',
				),
			),

			// Now we will add the edit modul
			array('acp', 'ACP_DMV_DM_VIDEO', array(
					'module_basename'	=> 'dm_video',
					'module_langname'	=> 'ACP_DMV_EDIT',
					'module_mode'		=> 'edit_videos',
					'module_auth'		=> 'acl_a_dm_video_edit',
				),
			),

			// Now we will add the release modul
			array('acp', 'ACP_DMV_DM_VIDEO', array(
					'module_basename'	=> 'dm_video',
					'module_langname'	=> 'ACP_DMV_RELEASE',
					'module_mode'		=> 'release_videos',
					'module_auth'		=> 'acl_a_dm_video_release',
				),
			),

			// Now we will add the reported modul
			array('acp', 'ACP_DMV_DM_VIDEO', array(
					'module_basename'	=> 'dm_video',
					'module_langname'	=> 'ACP_DMV_REPORTED',
					'module_mode'		=> 'reported_videos',
					'module_auth'		=> 'acl_a_dm_video_report',
				),
			),
		),

		/*
		* Now we need to insert some data.  The easiest way to do that is through a custom function
		* Enter 'custom' for the array key and the name of the function for the value.
		*/
		'custom' => 'first_fill_1_0_0',
	),

	// Version 1.0.2
	'1.0.2' => array(
		// Custom fill for version 1.0.2 - version 1.0.1 was skipped
		'custom' => 'fill_1_0_2',
	),

	// Version 1.0.3
	'1.0.3' => array(
		// Now to add the tables (this uses the layout from develop/create_schema_files.php and from phpbb_db_tools)
		'table_add' => array(
			array($table_prefix . 'dm_video_comment', array(
					'COLUMNS'		=> array(
						'comment_id'				=> array('UINT:11', NULL, 'auto_increment'),
						'video_id'					=> array('UINT:11', 0),
						'video_user_id'				=> array('UINT:8', 0),
						'video_username'			=> array('VCHAR', ''),
						'video_user_colour'			=> array('VCHAR:6', ''),
						'video_time'				=> array('UINT:11', 0),
						'video_comment'				=> array('MTEXT_UNI', ''),
						'comment_bbcode_bitfield'	=> array('VCHAR', ''),
						'comment_bbcode_options'	=> array('UINT:4', 0),
						'comment_bbcode_uid'		=> array('VCHAR:8', ''),
						'enable_magic_url'			=> array('TINT:1', 1),
						'enable_smilies'			=> array('TINT:1', 1),
						'enable_bbcode'				=> array('TINT:1', 1),
					),
					'PRIMARY_KEY'	=> 'comment_id',
				),
			),

			array($table_prefix . 'dm_video_config', array(
					'COLUMNS'		=> array(
						'config_name'	=> array('VCHAR', ''),
						'config_value'	=> array('VCHAR_UNI', ''),
					),
					'PRIMARY_KEY'	=> 'config_name',
				),
			),
		),

		// Alright, now lets add the config module to the ACP
		'module_add' => array(
			// Now we will add the config modul
			array('acp', 'ACP_DMV_DM_VIDEO', array(
					'module_basename'	=> 'dm_video',
					'module_langname'	=> 'ACP_DMV_CONFIG',
					'module_mode'		=> 'video_config',
					'module_auth'		=> 'acl_a_dm_video_config',
					'before'			=> 'ACP_DMV_MANAGE_CATEGORIES',
				),
			),
		),
		// Custom fill for version 1.0.3
		'custom' => 'fill_1_0_3',
	),

	// Version 1.0.4
	'1.0.4' => array(
		// Now to add some permission settings
		'permission_add' => array(
			array('u_dm_video_add', true),
			array('u_dm_video_edit', true), 
			array('u_dm_video_del', true),
			array('u_dm_video_report', true),
			array('u_dm_video_comment_view', true),
			array('u_dm_video_comment_add', true),
			array('u_dm_video_comment_edit', true),
			array('u_dm_video_comment_del', true),
			array('a_dm_video_add', true),
			array('a_dm_video_edit', true), 
			array('a_dm_video_del', true),
			array('a_dm_video_report', true),
			array('a_dm_video_comment_view', true),
			array('a_dm_video_comment_add', true),
			array('a_dm_video_comment_edit', true),
			array('a_dm_video_comment_del', true),
		),

		// Clear the general cache as well as the templates, imagesets and themes cache
		'cache_purge' => array(
			array(),
			array('imageset'),
			array('template'),
			array('theme'),
		),
		// Custom fill for version 1.0.4
		'custom' => 'fill_1_0_4',
	),

	// Version 1.0.5
	'1.0.5'	=> array(
		// Lets remove some of those permission settings we added before
		'permission_remove' => array(
			array('a_dm_video_view', true),
			array('a_dm_video_add', true),
			array('a_dm_video_edit', true), 
			array('a_dm_video_del', true),
			array('a_dm_video_rate', true),
			array('a_dm_video_report', true),
			array('a_dm_video_comment_view', true),
			array('a_dm_video_comment_add', true),
			array('a_dm_video_comment_edit', true),
			array('a_dm_video_comment_del', true),
		),

		// Add new permission settings
		'permission_add' => array(
			array('a_dm_video', true),
			array('a_dm_video_auto_announce', true),
			array('a_dm_video_config', true),
			array('a_dm_video_cats', true),
			array('a_dm_video_edit', true),
			array('a_dm_video_release', true),
			array('a_dm_video_report', true),
		),

		// Giving default permissions
		'permission_set' => array(
			array('REGISTERED', 'u_dm_video_view', 'group'),
			array('REGISTERED', 'u_dm_video_add', 'group'),
			array('REGISTERED', 'u_dm_video_edit', 'group'),
			array('REGISTERED', 'u_dm_video_del', 'group'),
			array('REGISTERED', 'u_dm_video_rate', 'group'),
			array('REGISTERED', 'u_dm_video_report', 'group'),
			array('REGISTERED', 'u_dm_video_comment_view', 'group'),
			array('REGISTERED', 'u_dm_video_comment_add', 'group'),
			array('REGISTERED', 'u_dm_video_comment_edit', 'group'),
			array('REGISTERED', 'u_dm_video_comment_del', 'group'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video', 'role'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video_auto_announce', 'role'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video_config', 'role'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video_cats', 'role'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video_edit', 'role'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video_release', 'role'),
			array('ROLE_ADMIN_STANDARD', 'a_dm_video_report', 'role'),
		),

		// Remove no longer needed columns in dm_video_cat table
		'table_column_remove' => array(
			array($table_prefix . 'dm_video_cat', 'video_last_poster_title'),
			array($table_prefix . 'dm_video_cat', 'video_last_poster_id'),
			array($table_prefix . 'dm_video_cat', 'video_last_poster_name'),
			array($table_prefix . 'dm_video_cat', 'video_last_poster_colour'),
			array($table_prefix . 'dm_video_cat', 'video_last_video_time'),
		),
		
		// Clear the general cache as well as the templates, imagesets and themes cache
		'cache_purge' => array(
			array(),
			array('imageset'),
			array('template'),
			array('theme'),
		),
	),

	// Version 1.0.6
	'1.0.6'	=> array(
		// Custom fill for version 1.0.6
		'custom' => 'fill_1_0_6',
	),
);

// Include the UMIF Auto file and everything else will be handled automatically.
include($phpbb_root_path . 'umil/umil_auto.' . $phpEx);

/*
* Here is our custom function that will be called for version 1.0.0
*
* @param string $action The action (install|update|uninstall) will be sent through this.
* @param string $version The version this is being run for will be sent through this.
*/
function first_fill_1_0_0($action, $version)
{
	global $db, $table_prefix, $umil;

	switch ($action)
	{
		case 'install' :    
			// Run this when installing the first time
			if ($umil->table_exists($table_prefix . 'config'))
			{
				$sql_ary = array();

				$sql_ary[] = array('config_name' => 'dm_video_version',	'config_value' => '1.0.0',);
				$db->sql_multi_insert($table_prefix . 'config ', $sql_ary);
			}

			// Send the message, that the command was successful
			return 'UMIL_DMV_INSERT_FIRST_FILL';
		break;

		case 'update' :
		break;

 		case 'uninstall' :
		break;
	}
}

/*
* Here is our custom function that will be called for version 1.0.2
*
* @param string $action The action (install|update|uninstall) will be sent through this.
* @param string $version The version this is being run for will be sent through this.
*/
function fill_1_0_2($action, $version)
{
	global $db, $table_prefix, $umil;

	switch ($action)
	{
		case 'install' :
		case 'update' :
			// Run this when installing/updating
			if ($umil->table_exists($table_prefix . 'dm_video'))
			{
				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD video_image TINYTEXT NOT NULL";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD bbcode_bitfield varchar(255) NOT NULL DEFAULT ''";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD bbcode_uid varchar(8) NOT NULL DEFAULT ''";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD bbcode_options mediumint(4) NOT NULL DEFAULT '0'";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD video_reported tinyint(1) NOT NULL DEFAULT '1'";
				$db->sql_query($sql);
			}
			
			// Method 1 of displaying the command (and Success for the result)
			return 'UMIL_DMV_UPDATE_SUCCESFUL';
		break;

		case 'uninstall' :
		break;
	}
}

/*
* Here is our custom function that will be called for version 1.0.3
*
* @param string $action The action (install|update|uninstall) will be sent through this.
* @param string $version The version this is being run for will be sent through this.
*/
function fill_1_0_3($action, $version)
{
	global $db, $table_prefix, $umil;

	switch ($action)
	{
		case 'install' :
		case 'update' :
			// Run this when installing/updating
			if ($umil->table_exists($table_prefix . 'dm_video'))
			{
				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD enable_magic_url TINYINT(1) NOT NULL DEFAULT '0'";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD enable_smilies TINYINT(1) NOT NULL DEFAULT '0'";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD enable_bbcode TINYINT(1) NOT NULL DEFAULT '0'";
				$db->sql_query($sql);
			}

			if ($umil->table_exists($table_prefix . 'dm_video_config'))
			{
				$sql_ary = array();

				$sql_ary[] = array('config_name' => 'video_page_user', 'config_value' => '15');
				$sql_ary[] = array('config_name' => 'video_page_acp', 'config_value' => '15');
				$sql_ary[] = array('config_name' => 'top_views', 'config_value' => '10');
				$sql_ary[] = array('config_name' => 'top_ratings', 'config_value' => '10');
				$sql_ary[] = array('config_name' => 'newest_videos', 'config_value' => '5');
				$sql_ary[] = array('config_name' => 'video_page_comment', 'config_value' => '15');
				
				$db->sql_multi_insert($table_prefix . 'dm_video_config ', $sql_ary);
			}

			// Method 1 of displaying the command (and Success for the result)
			return 'UMIL_DMV_UPDATE_SUCCESFUL';
		break;

		case 'uninstall' :
		break;
	}
}

/*
* Here is our custom function that will be called for version 1.0.4
*
* @param string $action The action (install|update|uninstall) will be sent through this.
* @param string $version The version this is being run for will be sent through this.
*/
function fill_1_0_4($action, $version)
{
	global $db, $table_prefix, $umil;

	switch ($action)
	{
		case 'install' :
		case 'update' :
			// Run this when installing/updating
			if ($umil->table_exists($table_prefix . 'dm_video'))
			{
				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD video_announced TINYINT(1) NOT NULL DEFAULT '0'";
				$db->sql_query($sql);

				$sql = "ALTER TABLE " . $table_prefix . "dm_video
					ADD video_points INT(4) NOT NULL DEFAULT '0'";
				$db->sql_query($sql);

			}

			if ($umil->table_exists($table_prefix . 'dm_video_config'))
			{
				$sql_ary = array();

				$sql_ary[] = array('config_name' => 'copyright_email', 'config_value' => 'sample@yourdomain.com');
				$sql_ary[] = array('config_name' => 'copyright_show', 'config_value' => 'Sample At Yourdomain Dot Com');
				$sql_ary[] = array('config_name' => 'video_announce_forum_id', 'config_value' => '0');
				$sql_ary[] = array('config_name' => 'video_announce_enable', 'config_value' => '0');
				$sql_ary[] = array('config_name' => 'new_video_pm_from', 'config_value' => '2');
				$sql_ary[] = array('config_name' => 'new_video_pm_to', 'config_value' => '2');
				$sql_ary[] = array('config_name' => 'video_points_enable', 'config_value' => '0');
				$sql_ary[] = array('config_name' => 'video_points_value', 'config_value' => '0');
				
				$db->sql_multi_insert($table_prefix . 'dm_video_config ', $sql_ary);
			}

			// Method 1 of displaying the command (and Success for the result)
			return 'UMIL_DMV_UPDATE_SUCCESFUL';
		break;

		case 'uninstall' :
		break;
	}
}

/*
* Here is our custom function that will be called for version 1.0.6
*
* @param string $action The action (install|update|uninstall) will be sent through this.
* @param string $version The version this is being run for will be sent through this.
*/
function fill_1_0_6($action, $version)
{
	global $db, $table_prefix, $umil;

	switch ($action)
	{
		case 'install' :
		case 'update' :
			// Run this when installing/updating
			if ($umil->table_exists($table_prefix . 'dm_video_rating'))
			{
				$sql = "ALTER TABLE " . $table_prefix . "dm_video_rating
					CHANGE user_ip user_ip VARCHAR(40)";
				$db->sql_query($sql);
			}

			if ($umil->table_exists($table_prefix . 'dm_video_config'))
			{
				$sql_ary = array();

				$sql_ary[] = array('config_name' => 'video_comments_page1',	'config_value' => '5',);
				$sql_ary[] = array('config_name' => 'share_enable',	'config_value' => '1',);
				$sql_ary[] = array('config_name' => 'facebook_enable',	'config_value' => '1',);
				$sql_ary[] = array('config_name' => 'facebook_path',	'config_value' => '',);
				$sql_ary[] = array('config_name' => 'myspace_enable',	'config_value' => '1',);
				$sql_ary[] = array('config_name' => 'digg_enable',	'config_value' => '1',);
				$sql_ary[] = array('config_name' => 'delicious_enable',	'config_value' => '1',);
				$sql_ary[] = array('config_name' => 'technorati_enable',	'config_value' => '1',);

				$db->sql_multi_insert($table_prefix . 'dm_video_config ', $sql_ary);
			}

			// Method 1 of displaying the command (and Success for the result)
			return 'UMIL_DMV_UPDATE_SUCCESFUL';
		break;

		case 'uninstall' :
		break;
	}
}

?>