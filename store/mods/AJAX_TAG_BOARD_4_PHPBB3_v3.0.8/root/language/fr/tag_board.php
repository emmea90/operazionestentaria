<?php
/**
*
* @package	language
* @version	3.0.7
* @license	GNU Public License
* @author	Lyan53
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'TB_TB'					=> 'Tag Board',
	'TB_ACP'				=> 'Paramètres de la Tag Board',
	'TB_INSTALL'			=> 'La Tag Board est installée avec succès<br />Merci de supprimer le fichier tb_install.php',
	'TB_ACP_EXPLAIN'		=> 'Ici vous pouvez déterminer les options de base pour votre Tag Board.',
	'TB_CVERSION' 			=> 'Version actuelle',
	'TB_LVERSION' 			=> 'Dernière version',
	'TB_CHECK' 				=> 'Cliquez ici',
	'TB_AUTH' 				=> 'Permissions',
	'TB_GROUPS' 			=> 'Groupes alloués',
	'TB_GROUPS_EXPLAIN' 	=> 'Id des groupes pouvant voir la Tag Board. Plusieurs peuvent être utilisés, séparer chaque id par une virgule.',
	'TB_DENIED' 			=> 'Utilisateurs refusés',
	'TB_DENIED_EXPLAIN' 	=> 'Id des utilisateurs ne pouvant pas voir la Tag Board. Plusieurs peuvent être utilisés, séparer chaque id par une virgule.',
	'TB_STANDARD' 			=> 'Tous les utilisateurs peuvent LIRE mais seuls ceux enregistrés peuvent POSTER.<br />',
	'TB_FULL' 				=> 'Tous les utilisateurs peuvent LIRE et POSTER.<br />',
	'TB_LIMITED' 			=> 'Seuls les utilisateurs enregistrés peuvent LIRE et POSTER.',
	'TB_GUEST' 				=> 'Nom de l’invité',
	'TB_GUEST_EXPLAIN' 		=> 'Laisser vide pour l’utilisation de l’IP en tant que Nom de l’invité.',
	'TB_LIMIT' 				=> 'Voir les derniers',
	'TB_POSTS' 				=> 'posts',
	'TB_HEIGTH' 			=> 'Hauteur de la Tag Board',
	'TB_PX' 				=> 'px',
	'TB_MAXLENGTH' 			=> 'Maximum de caractères par post',
	'TB_MAXLENGTH_EXPLAIN' 	=> 'Nombre de caractères alloués par post.',
	'TB_CHARS' 				=> 'caractères',
	'TB_BBCODE' 			=> 'Autoriser le BBCode',
	'TB_CUSTOM' 			=> 'Voir le bouton Custom BBCode',
	'TB_FSIZE' 				=> 'Voir le bouton Taille du texte',
	'TB_IMG' 				=> 'Autoriser l’utilisation du BBCode <code>[IMG]</code>',
	'TB_FLASH' 				=> 'Autoriser l’utilisation du BBCode <code>[FLASH]</code>',
	'TB_SMILIES' 			=> 'Autoriser les smileys',
	'TB_URLS' 				=> 'Autoriser les liens',
	'TB_URLS_EXPLAIN' 		=> 'Si non autorisé, les liens URL automatiques seront désactivés.',
	'TB_BUTTONS' 			=> 'Afficher les boutons',
	'TB_DELETE' 			=> 'Autoriser aux utilisateurs la suppression de leurs anciens posts',
	'TB_EDIT' 				=> 'Autoriser aux utilisateurs l’édition de leurs anciens posts',
	'TB_EDIT_TIME' 			=> 'Limite de temps pour l’édition',
	'TB_EDIT_TIME_EXPLAIN' 	=> 'Limite de temps disponible pour pouvoir éditer un nouveau post. Mettre la valeur à 0 pour désactiver cette fonction.',
	'TB_MINUTES' 			=> 'minutes',
	'TB_FLOOD' 				=> 'Intervalle de flood',
	'TB_FLOOD_EXPLAIN' 		=> 'Nombre de secondes qu’un utilisateur doit attendre pour pouvoir poster un nouveau message. Mettre la valeur à 0 pour désactiver cette fonction.',
	'TB_SECONDS' 			=> 'secondes',
	'TB_PURGE' 				=> 'Auto délestage des anciens posts',
	'TB_PURGE_EXPLAIN' 		=> 'Nombre de jours alloué aux anciens posts avant leur auto suppression. Mettre la valeur à 0 pour désactiver cette fonction.',
	'TB_DAYS' 				=> 'jours',
	'TB_REFRESH' 			=> 'Auto rafraîchissement',
	'TB_REFRESH_EXPLAIN' 	=> 'Nombre de secondes qui s’écoule entre chaque auto rafraîchissement de la Tag Board. Mettre la valeur à 0 pour désactiver cette fonction.',
	'TB_HISTORY' 			=> 'Maximum de posts qui seront stockés dans la base de données',
	'TB_HISTORY_EXPLAIN' 	=> 'Mettre la valeur à 0 pour stocker l’intégralité des posts.',
	'TB_DELETEALL' 			=> 'Supprimer l’intégralité des messages de la Tag Board',
	'TB_DELETEALL_EXPLAIN' 	=> 'Note importante: La suppression des messages est définitive, aucune récupération n’est possible.',
	'TB_FLOOD_ERROR' 		=> 'Vous ne pouvez pas poster si peu de temps après votre dernier message.',
	'TB_OFFLINE' 			=> 'Hors ligne',
	'TB_INACTIVE' 			=> 'Inactif',
	'TB_ONLINE' 			=> 'En ligne',
	'TB_GUESTNAME' 			=> 'Invité',
	'TB_MORE_SMILIES' 		=> 'Voir plus de smileys',
));

?>