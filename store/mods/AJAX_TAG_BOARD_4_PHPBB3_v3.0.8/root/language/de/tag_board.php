<?php
/**
*
* @package	language
* @version	3.0.7
* @license	GNU Public License
* @author	Highway311
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'TB_TB'					=> 'Tag Board',
	'TB_ACP'				=> 'Tag Board Einstellungen',
	'TB_INSTALL'			=> 'Das Tag Board  wurde vollständig installiert<br />Lösche nun die Datei tb_install.php',
	'TB_ACP_EXPLAIN'		=> 'Hier können Sie die grundlegenden Funktionen Ihres Tag Board bestimmen.',
	'TB_CVERSION' 			=> 'Aktuelle Version',
	'TB_LVERSION' 			=> 'Neuest Version',
	'TB_CHECK' 				=> 'Maqnuell Checken',
	'TB_AUTH' 				=> 'Befugnis',
	'TB_GROUPS' 			=> 'Gruppen bestimmen',
	'TB_GROUPS_EXPLAIN' 	=> 'Benenne Gruppen, die die Shoutbox sehen können. Mehrere Gruppen durch ein Koma getrennt. ',
	'TB_DENIED' 			=> 'User ausschliessen',
	'TB_DENIED_EXPLAIN' 	=> 'Benenne User, die die Shoutbox <font color="red"><strong>NICHT</strong></font> sehen können. Mehrere User durch ein Koma getrennt.',
	'TB_STANDARD' 			=> 'L 4 alle / S 4 Reg<br />',
	'TB_FULL' 				=> 'L & S 4 Alle<br />',
	'TB_LIMITED' 			=> 'L & S 4 Reg',
	'TB_GUEST' 				=> 'Gästename',
	'TB_GUEST_EXPLAIN' 		=> 'Leer lassen, wenn du statt eines namen die IP sehen willst.',
	'TB_LIMIT' 				=> 'Zeige die letzten',
	'TB_POSTS' 				=> 'Beiträge',
	'TB_HEIGTH' 			=> 'Höhe der angeteigten Shoutbox',
	'TB_PX' 				=> 'px',
	'TB_MAXLENGTH' 			=> 'Maximale Anzahl an Zeichen pro Beitrag',
	'TB_MAXLENGTH_EXPLAIN' 	=> 'Die Anzahl an Zeichen, die pro Beitrag angezeigt werden sollen.',
	'TB_CHARS' 				=> 'Zeichen',
	'TB_BBCODE' 			=> 'Erlaube BBCode',
	'TB_CUSTOM' 			=> 'Soll der BBCode-Button angezeigt werden?',
	'TB_FSIZE' 				=> 'Zeige eine Auswahl an Schriftgrössen an?',
	'TB_IMG' 				=> 'Den BB-Code <code>[IMG]</code> erlauben?',
	'TB_FLASH' 				=> 'Den BB-Code <code>[FLASH]</code> erlauben',
	'TB_SMILIES' 			=> 'Smilies erlauben',
	'TB_URLS' 				=> 'Links erlauben',
	'TB_URLS_EXPLAIN' 		=> 'Bei Nein werden die angegebene Links nicht automatisch umgewandelt.',
	'TB_BUTTONS' 			=> 'Die Buttons anzeigen?',
	'TB_DELETE' 			=> 'Dürfen die User ihre Beiträge löschen?',
	'TB_EDIT' 				=> 'Dürfen die User ihre Beiträge bearbeiten?',
	'TB_EDIT_TIME' 			=> 'Erlaubte Zeit zum bearbeiten des Eintrag',
	'TB_EDIT_TIME_EXPLAIN' 	=> 'Hier kannst du angeben, wie lange ein User seinen Beitrag bearbeiten kann, wenn du dies oben erlaubt hast. Eine 0 (NULL) schaltet diese Einstellung aus.',
	'TB_MINUTES' 			=> 'Minuten',
	'TB_FLOOD' 				=> 'Wartezeit für weitere Beiträge',
	'TB_FLOOD_EXPLAIN' 		=> 'Wie viele Sekunden muss ein User warten, um einen weiteren Beitrag abzusenden? Eine 0 (NULL) schaltet diese Begrenzung aus.',
	'TB_SECONDS' 			=> 'Sekunden',
	'TB_PURGE' 				=> 'Automatische löschen von alten Beiträgen',
	'TB_PURGE_EXPLAIN' 		=> 'Die Anzahl an Tagen, nach denen alte Beiträge gelöscht werden sollen.  Se0 (Null) ergibt keine automatische Löschung.',
	'TB_DAYS' 				=> 'Tage',
	'TB_REFRESH' 			=> 'Automatisches aktualisieren',
	'TB_REFRESH_EXPLAIN' 	=> 'Nach wie vielen Sekunden soll die Shoutbox automatisch aktualisiert werden? 0 (Null) ergibt keine automatische Aktualisierung.',
	'TB_HISTORY' 			=> 'Wieviele Beiträge sollen in der Datenbank gespeichert werden?',
	'TB_HISTORY_EXPLAIN' 	=> 'Bei einer 0 (NULL) werden alle Beiträge gespeichert.',
	'TB_DELETEALL' 			=> '<font color="red"><strong>Lösche alle Einträge</strong></font>',
	'TB_DELETEALL_EXPLAIN' 	=> '<br \><font color="red"><strong>Bitte beachte, dass dieses Löschen endgültig ist. Das Löschen ist unwiederruflich!</strong></font>',
	'TB_FLOOD_ERROR' 		=> 'Du musst bis zu deinem nächsten Beitrag eine kleine Pause einlegen.',
	'TB_OFFLINE' 			=> 'Offline',
	'TB_INACTIVE' 			=> 'Nicht aktiv',
	'TB_ONLINE' 			=> 'Online',
	'TB_GUESTNAME' 			=> 'Gast',
	'TB_MORE_SMILIES' 		=> 'Weitere Smilies',
));

?>