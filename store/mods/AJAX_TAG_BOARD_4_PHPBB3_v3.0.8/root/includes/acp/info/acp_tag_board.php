<?php
/**
*
* @package	acp
* @version	3.0.7
* @license	GNU Public License
* @author	draghetto
*
*/

/**
* @package module_install
*/
class acp_tag_board_info
{
	
	function module()
	{
		global $user;
		$user->add_lang('tag_board');
		return array(
			'filename'	=> 'acp_tag_board',
			'title'		=> 'TB_TB',
			'version'	=> '1.0.0',
			'modes'		=> array(
				'settings'	=> array('title'=>'TB_TB',	'auth'=>'acl_a_board',	'cat'=>array('')),
			),
		);
	}
}

?>