<?php
/**
*
* @package	acp
* @version	3.0.7
* @license	GNU Public License
* @author	draghetto
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package acp
*/
class acp_tag_board
{
	function main($id, $mode)
	{
		global $user, $config, $template, $tb_lversion, $update_link;
		
		$update_link = 'http://forum.blogantropo.it/viewtopic.php?t=2480';
		if (function_exists('curl_init'))
		{
			$link = 'http://forum.blogantropo.it/tb_version.php';
			$ch = curl_init($link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$str = curl_exec($ch);
			curl_close($ch);
			if(preg_match('/(<version>)(.+)(<\/version>)/', $str, $latest))
			{
				$tb_lversion = $latest[2];
			}
		}

		$user->add_lang('tag_board');

		$submit = (isset($_POST['submit'])) ? true : false;

		$form_key = 'acp_tag_board';
		add_form_key($form_key);

		switch ($mode)
		{
			case 'settings':
				$display_vars = array(
					'title'	=> 'TB_ACP',
					'vars'	=> array(
						'legend1'		=> 'TB_ACP',
						'tb_version'	=> array('lang' => 'TB_CVERSION',	'validate' => 'string',	'type' => 'custom', 		'explain' => true, 'method' => 'select_cversion'),
						'tb_null'		=> array('lang' => 'TB_LVERSION',	'validate' => 'string',	'type' => 'custom', 		'explain' => true, 'method' => 'select_lversion'),
						'tb_auth'		=> array('lang' => 'TB_AUTH',		'validate' => 'string',	'type' => 'custom', 		'explain' => true, 'method' => 'select_auth'),
						'tb_groups'		=> array('lang' => 'TB_GROUPS',		'validate' => 'string',	'type' => 'text:20:255', 	'explain' => true),
						'tb_denied'		=> array('lang' => 'TB_DENIED',		'validate' => 'string',	'type' => 'text:20:255', 	'explain' => true),
						'tb_guest'		=> array('lang' => 'TB_GUEST',		'validate' => 'string',	'type' => 'text:15:255', 	'explain' => true),
						'tb_limit'		=> array('lang' => 'TB_LIMIT',		'validate' => 'int',	'type' => 'text:3:3', 		'explain' => true, 'append' => ' ' . $user->lang['TB_POSTS']),
						'tb_heigth'		=> array('lang' => 'TB_HEIGTH',		'validate' => 'int',	'type' => 'text:3:3', 		'explain' => true, 'append' => ' ' . $user->lang['TB_PX']),
						'tb_maxlength'	=> array('lang' => 'TB_MAXLENGTH',	'validate' => 'int',	'type' => 'text:3:3', 		'explain' => true, 'append' => ' ' . $user->lang['TB_CHARS']),
						'tb_buttons'	=> array('lang' => 'TB_BUTTONS',	'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_bbcode'		=> array('lang' => 'TB_BBCODE',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_custom'		=> array('lang' => 'TB_CUSTOM',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_img'		=> array('lang' => 'TB_IMG',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_flash'		=> array('lang' => 'TB_FLASH',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_fsize'		=> array('lang' => 'TB_FSIZE',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_smilies'	=> array('lang' => 'TB_SMILIES',	'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_urls'		=> array('lang' => 'TB_URLS',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_delete'		=> array('lang' => 'TB_DELETE',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_edit'		=> array('lang' => 'TB_EDIT',		'validate' => 'bool',	'type' => 'radio:yes_no', 	'explain' => true),
						'tb_edit_time'	=> array('lang' => 'TB_EDIT_TIME',	'validate' => 'int',	'type' => 'text:3:3', 		'explain' => true, 'append' => ' ' . $user->lang['TB_MINUTES']),
						'tb_purge'		=> array('lang' => 'TB_PURGE',		'validate' => 'int',	'type' => 'text:3:3', 		'explain' => true, 'append' => ' ' . $user->lang['TB_DAYS']),
						'tb_flood'		=> array('lang' => 'TB_FLOOD',		'validate' => 'int',	'type' => 'text:4:5', 		'explain' => true, 'append' => ' ' . $user->lang['TB_SECONDS']),
						'tb_refresh'	=> array('lang' => 'TB_REFRESH',	'validate' => 'int',	'type' => 'text:4:5', 		'explain' => true, 'append' => ' ' . $user->lang['TB_SECONDS']),
						'tb_history'	=> array('lang' => 'TB_HISTORY',	'validate' => 'int',	'type' => 'text:4:255', 	'explain' => true, 'append' => ' ' . $user->lang['TB_POSTS']),
						'tb_deleteall'	=> array('lang' => 'TB_DELETEALL',	'validate' => 'int',	'type' => 'custom', 		'explain' => true, 'method' => 'select_delete'),
					)
				);
			break;

			default:
				trigger_error('NO_MODE', E_USER_ERROR);
			break;
		}

		if (isset($display_vars['lang']))
		{
			$user->add_lang($display_vars['lang']);
		}

		$this->new_config = $config;
		$cfg_array = (isset($_REQUEST['config'])) ? utf8_normalize_nfc(request_var('config', array('' => ''), true)) : $this->new_config;
		$error = array();

		// We validate the complete config if whished
		validate_config_vars($display_vars['vars'], $cfg_array, $error);

		if ($submit && !check_form_key($form_key))
		{
			$error[] = $user->lang['FORM_INVALID'];
		}
		// Do not write values if there is an error
		if (sizeof($error))
		{
			$submit = false;
		}

		// We go through the display_vars to make sure no one is trying to set variables he/she is not allowed to...
		foreach ($display_vars['vars'] as $config_name => $null)
		{
			if (!isset($cfg_array[$config_name]) || strpos($config_name, 'legend') !== false)
			{
				continue;
			}

			$this->new_config[$config_name] = $config_value = $cfg_array[$config_name];

			if ($submit)
			{
				set_config($config_name, $config_value);
			}
		}

		if ($submit)
		{
			add_log('admin', 'LOG_CONFIG_' . strtoupper($mode));

			trigger_error($user->lang['CONFIG_UPDATED'] . adm_back_link($this->u_action));
		}

		$this->tpl_name = 'acp_board';
		$this->page_title = $display_vars['title'];

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang[$display_vars['title']],
			'L_TITLE_EXPLAIN'	=> $user->lang[$display_vars['title'] . '_EXPLAIN'],

			'S_ERROR'			=> (sizeof($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'			=> $this->u_action)
		);

		// Output relevant page
		foreach ($display_vars['vars'] as $config_key => $vars)
		{
			if (!is_array($vars) && strpos($config_key, 'legend') === false)
			{
				continue;
			}

			if (strpos($config_key, 'legend') !== false)
			{
				$template->assign_block_vars('options', array(
					'S_LEGEND'		=> true,
					'LEGEND'		=> (isset($user->lang[$vars])) ? $user->lang[$vars] : $vars)
				);

				continue;
			}

			$type = explode(':', $vars['type']);

			$l_explain = '';
			if ($vars['explain'] && isset($vars['lang_explain']))
			{
				$l_explain = (isset($user->lang[$vars['lang_explain']])) ? $user->lang[$vars['lang_explain']] : $vars['lang_explain'];
			}
			else if ($vars['explain'])
			{
				$l_explain = (isset($user->lang[$vars['lang'] . '_EXPLAIN'])) ? $user->lang[$vars['lang'] . '_EXPLAIN'] : '';
			}

			$content = build_cfg_template($type, $config_key, $this->new_config, $config_key, $vars);

			if (empty($content))
			{
				continue;
			}

			$template->assign_block_vars('options', array(
				'KEY'			=> $config_key,
				'TITLE'			=> (isset($user->lang[$vars['lang']])) ? $user->lang[$vars['lang']] : $vars['lang'],
				'S_EXPLAIN'		=> $vars['explain'],
				'TITLE_EXPLAIN'	=> $l_explain,
				'CONTENT'		=> $content,
				)
			);

			unset($display_vars['vars'][$config_key]);
		}
	}

	function select_cversion()
	{
		global $config, $tb_lversion, $update_link;
			if(!$tb_lversion)
			{
				return '<span style="color:#666666;font-weight:bold">' . $config['tb_version'] . '</span>';
			}
			else if($config['tb_version'] == $tb_lversion)
			{
				return '<span style="color:#009900;font-weight:bold">' . $config['tb_version'] . '</span>';
			}
			else
			{
				return '<a href="' . $update_link . '" style="color:#FF0000"><strong>' . $config['tb_version'] . '</strong></a>';
			}
	}

	function select_lversion()
	{
		global $user, $tb_lversion, $update_link;
			if($tb_lversion)
			{
				return '<em>' . $tb_lversion . '</em>';
			}
			else
			{
				return '<a href="' . $update_link . '" style="color:#FF6600">' . $user->lang['TB_CHECK'] . '</a>';
				
			}
	}

	function select_auth($value, $key = '')
	{
		$radio_ary = array(
			'STANDARD'	=> 'TB_STANDARD',
			'FULL'		=> 'TB_FULL',
			'LIMITED'	=> 'TB_LIMITED'
		);
		return h_radio('config[tb_auth]', $radio_ary, $value, $key);
	}
	
	function select_delete()
	{
		return '<input id="tb_deleteall" type="checkbox" name="config[tb_deleteall]" value="1" />';
	}
}

?>