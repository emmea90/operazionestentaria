<?php

/**
*
* help_skype [Greek]
*
* @package language
* @version $Id: help_skype.php,v 1.1 2009/01/02 22:56:58 rmcgirr83 Exp $
* @copyright (c) Richard McGirr
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$help = array(
	array(
		0 => '--',
		1 => 'Skype'
	),
	array(
		0 => 'Τι ειναι το Skype?',
		1 => '<a href="http://www.skype.com">Skype</a> επιτρεπει να κανετε δωρεαν κλησεις μεσω Ιντερνετ σε οποιον εχει επισης αυτη την υπηρεσια. Ειναι Δωρεαν κι ευκολο στη χρηση του, και λειτουργει σχεδον σε ολους τους υπολογιστες. Το Skype προσφερει δωρεαν διεθνη τηλεφωνια και επικοινωνια με το λογισμικο του.',
	),
	array(
		0 => 'Πως μπορω να προσθεσω χρηστες στον λογαριασμο μου Skype?',
		1 => 'Μπορειτε να προσθεσετε χρηστες στο λογαριασμο σας Skype απλα κανοντας κλικ στις συντομευσεις που θα βρειτε βλεποντας το προφιλ του χρηστη. Το εικονιδιο αλλαζει αναλογα με την κατασταση του χρηστη στο Skype.'
	),
	array(
		0 => 'Ενταξει, συμπληρωσα τη διευθυνση μου Skype αλλα το εικονιδιο δεν δειχνει την κατασταση μου!',
		1 => 'Μολις συμπληρωσετε τη διευθυνση Skype στο προφιλ σας, πρεπει να αλλαξετε και τις ρυθμισεις Ιδιωτικοτητας του Skype (Skype->Εργαλεια->Ρυθμισεις->Ιδιωτικοτητα->Εμφανιση επιλογων για προχωρημενους->Να φαινεται η κατασταση συνδεσης μου).  Ετσι θα φαινεται η κατασταση Skype σε ολους τους εγγεγραμενους χρηστες.'
	),
	array(
		0 => 'Τι κανει το πληκτρο στο προφίλ μέσα στα θέματα?',
		1 => 'Εαν εχετε εγκατεστημενο το Skype, θα γίνει "κληση" ή "συνομιλία" αναλογα με τη ρυθμιση που εχει επιλεξει ο χρηστης.you will place either a <b><i>call</i></b> or send a <b><i>chat</i></b> message to that user depending on that users preferences.'
	),
	array(
		0 => 'Τι κάνει το πλήκτρο/δεσμός μέσα στο προφίλ χρήστη?',
		1 => 'Επιτρεπει σε εναν χρηστη να να εκτελεσει καποιες λειτουργιες με εσας μεσω Skype.  Ο χρήστης θα μπορεσει να σας προσθεσει σαν επαφη, να σας στειλει ενα αρχειο, να αφησει ενα μηνυμα, να ελεγξει τις πληροφοριες σας στο skype, να κανει μια κληση η μια συνομιλια.'
	),
);
?>
