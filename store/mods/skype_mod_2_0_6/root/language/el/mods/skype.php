<?php
/**
*
* skype [Greek]
*
* @package language
* @version $Id: skype.php,v 1.1 2009/01/02 22:56:58 rmcgirr83 Exp $
* @copyright (c) 2008 Richard McGirr 
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
// do not translate these next two lines!!  Else skype will not be able to connect
	'SKYPE_CHAT'			=> 'Chat',
	'SKYPE_CALL'			=> 'Call',
// translate following
	'IM_SKYPE'				=> 'ΣΗΜΕΙΩΣΗ: Οι χρήστες μπορεί να επέλεξαν να μη δέχονται μηνύματα skype.',
	
	'SEND_SKYPE_MESSAGE'	=> 'Στείλε μήνυμα Skype',
	'SKYPE'					=> 'Skype',
	'SKYPE_ADD'   			=> 'Προσθήκη στο Skype',
	'SKYPE_EXPLAIN'       	=> 'Δείτε στο %sFAQ%s για λεπτομέρειες.',
	'SKYPE_FAQ'             => 'Skype FAQ',
	'SEND_SKYPE_MESSAGE'	=> 'Στείλε μήνυμα Skype',
	'SKYPE_SENDFILE'    	=> 'Στείλε αρχείο σε',
	'SKYPE_USERINFO'    	=> 'Προβολή λεπτομέρειες χρήστη Skype',
	'SKYPE_VOICEMAIL'   	=> 'Τηλεφωνητής Skype',
	'SKYPE_TYPE'			=> 'Τύπος μηνύματος Skype',
	'SKYPE_CALL_TRANS'		=> 'Κλήση',
	'SKYPE_CHAT_TRANS'		=> 'Συνομιλία',
	'TOO_LONG_SKYPE'		=> 'Το όνομα Skype που έδωσες είναι πολύ μακρύ.',
	'TOO_SHORT_SKYPE'		=> 'Το όνομα Skype που έδωσες είναι πολύ κοντό.',
	'UCP_SKYPE'				=> 'Ονομα Skype',
));

?>