<?php

/**
*
* help_skype [German]
*
* @package language
* @version $Id: help_skype.php,v 1.1 2009/01/02 22:56:58 rmcgirr83 Exp $
* @copyright (c) Richard McGirr
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @translation by Fleshgrinder
* @copyright (c) 2008 Richard Fussenegger
* @conatact admin@nervenhammer.com
* @website http://www.nervenhammer.com/
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$help = array(
	array(
		0 => '--',
		1 => 'Skype'
	),
	array(
		0 => 'Was ist Skype?',
		1 => '<a href="http://www.skype.com/intl/de/" title="Skype Deutsch">Skype</a> ermöglicht es dir völlig kostenlose Telefonanrufe über das Internet zu tätigen, mit jeder Person die auch Skype nutzt. Die Software ist kostenlos, einfach herunterzuladen, zu installieren und auch zu nutzen. Des Weiteren gibt es Skype für nahezu jedes Betriebssystem. Skype offeriert kostenlose globale Telefonie und unlimitierte Verwendung des Anrufbeantworters mit ihrer Peer-to-Peer Software der nächsten Generation.',
	),
	array(
		0 => 'Wie kann ich andere Benutzer zu meinen Skype Kontakten hinzufügen?',
		1 => 'Du kannst andere Benutzer durch einen simplen Klick auf den Link innerhalb eines Themas oder beim Benutzerprofil hinzufügen. Das Statusicon welches du dort siehst wird sich dem aktuellen Status des Mitgliedes anpassen.'
	),
	array(
		0 => 'Okay, ich habe meinen Skype Benutzernamen angegeben, das Statusicon zeigt jedoch nicht meinen aktuellen Status an!',
		1 => 'Sobald du deinen Skype Benutzernamen in deinem Profil angegeben hast musst du noch die passenden Einstellungen in deinem Skype Programm vornehmen. Öffne hierzu dein Skype Programm und gehe vor wie folgt: Aktionen &raquo; Optionen &raquo; Privatsphäre &raquo; Erweiterte Optionen und setzte ein Häckchen bei "Mein Status darf im Netz veröffentlicht werden." klicke jetzt auf Speichern. Dein aktueller Skypestatus wird ab sofort allen registrierten Mitgliedern angezeigt werden.'
	),
	array(
		0 => 'Was kann ich mit den Buttons innerhalb eines Themas anstellen?',
		1 => 'Nachdem du Skype installiert hast, kannst du entweder durch klicken auf den entsprechenden Button einen anderen Benutzer <strong><em>Anrufen</em></strong> oder eine <strong><em>Sofortnachricht senden</em></strong>, je nachdem was dir oder dem anderen Benutzer lieber ist.'
	),
	array(
		0 => 'Was kann ich mit den Buttons innerhalb eines Benutzerprofiles anstellen?',
		1 => 'Mit den Buttons auf einer Benutzerprofilseite kannst viele verschiedene Skype Funktionen direkt ausführen. Du kannst einen anderen Benutzer zu deinen Kontakten hinzufügen, ihm eine Datei senden, eine Nachricht auf dem Anrufbeantworter hinterlassen, sein Skype Profil ansehen und natürlich einen Anruf tätigen oder eine Sofortnachricht senden.'
	),
);
?>
