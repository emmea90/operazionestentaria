<?php
/**
*
* skype [English]
*
* @package language
* @version $Id: skype.php,v 1.1 2008/12/11 14:41:29 rmcgirr83 Exp $
* @copyright (c) 2008 Richard McGirr 
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
// do not translate these next two lines!!  Else skype will not be able to connect
	'SKYPE_CHAT'			=> 'Chat',
	'SKYPE_CALL'			=> 'Call',
// translate following
	'IM_SKYPE'				=> 'Please note that users may have selected to not receive unsolicited skype messages.',
	
	'SEND_SKYPE_MESSAGE'	=> 'Send Skype message',
	'SKYPE'					=> 'Skype',
	'SKYPE_ADD'   			=> 'Add to Skype',
	'SKYPE_EXPLAIN'       	=> 'Please visit the %sFAQ%s to learn more.',
	'SKYPE_FAQ'             => 'Skype FAQ',
	'SEND_SKYPE_MESSAGE'	=> 'Send a Skype message',
	'SKYPE_SENDFILE'    	=> 'Send a file to',
	'SKYPE_USERINFO'    	=> 'Look up Skype User Info',
	'SKYPE_VOICEMAIL'   	=> 'Skype Voicemail',
	'SKYPE_TYPE'			=> 'Type of Skype Message',
	'SKYPE_CALL_TRANS'		=> 'Call',
	'SKYPE_CHAT_TRANS'		=> 'Chat',
	'TOO_LONG_SKYPE'		=> 'The Skype name you entered is too long.',
	'TOO_SHORT_SKYPE'		=> 'The Skype name you entered is too short.',
	'UCP_SKYPE'				=> 'Skype Name',
));

?>