<?php
/**
*
* skype [Spanish]
*
* @package language
* @version $Id: skype.php,v 1.1 2009/01/02 22:56:58 rmcgirr83 Exp $
* @copyright (c) 2008 Richard McGirr 
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
// do not translate these next two lines!!  Else skype will not be able to connect
	'SKYPE_CHAT'			=> 'Chat',
	'SKYPE_CALL'			=> 'Call',
// translate following
	'IM_SKYPE'				=> 'Tenga en cuenta que los usuarios pueden tener seleccionado no recibir mensajes de Skype no solicitados.',
	
	'SEND_SKYPE_MESSAGE'	=> 'Enviar un mensaje Skype',
	'SKYPE'					=> 'Skype',
	'SKYPE_ADD'   			=> 'A&ntilde;adir a Skype a',
	'SKYPE_EXPLAIN'       	=> 'Por favor, visite las %sFAQ%s para aprender m&aacute;s.',
	'SKYPE_FAQ'             => 'FAQ de Skype',
	'SEND_SKYPE_MESSAGE'	=> 'Enviar un mensaje Skype',
	'SKYPE_SENDFILE'    	=> 'Enviar un archivo a',
	'SKYPE_USERINFO'    	=> 'Buscar informaci&oacute;n del usuario de Skype',
	'SKYPE_VOICEMAIL'   	=> 'Skype Voicemail',
	'SKYPE_TYPE'			=> 'Tipo de Mensaje Skype',
	'SKYPE_CALL_TRANS'		=> 'Llamada',
	'SKYPE_CHAT_TRANS'		=> 'Chat',
	'TOO_LONG_SKYPE'		=> 'El nombre de Skype que ha introducido es demasiado largo.',
	'TOO_SHORT_SKYPE'		=> 'El nombre de Skype que ha introducido es demasiado corto.',
	'UCP_SKYPE'				=> 'Usuario Skype',
));

?>