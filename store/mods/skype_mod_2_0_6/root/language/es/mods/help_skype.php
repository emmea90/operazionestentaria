<?php

/**
*
* help_skype [Spanish]
*
* @package language
* @version $Id: help_skype.php,v 1.1 2009/01/02 22:56:58 rmcgirr83 Exp $
* @copyright (c) Richard McGirr
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$help = array(
	array(
		0 => '--',
		1 => 'Skype'
	),
	array(
		0 => '&iquest;Qu&eacute; es Skype?',
		1 => '<a href="http://www.skype.com">Skype</a> le permite realizar llamadas gratuitas a trav&eacute;s de Internet a cualquier otra persona que tambi&eacute;n tenga el servicio. Es gratis, f&aacute;cil de descargar y utilizar y funciona con la mayor&iacute;a de los equipos. Skype ofrece un servicio gratuito de telefon&iacute;a global e ilimitadas llamadas de voz con su pr&oacute;xima generaci&oacute;n de programas P2P (peer-to-peer).',
	),
	array(
		0 => '&iquest;C&oacute;mo puedo a&ntilde;adir usuarios a mi cuenta de Skype?',
		1 => 'Puede a&ntilde;adir usuarios a su cuenta Skype con s&oacute;lo hacer clic en los enlaces encontrados cuando se visualiza un tema o se visualiza un perfil de los usuarios registrados. El icono mostrado cambiar&aacute; dependiendo del estado de los usuarios en Skype.'
	),
	array(
		0 => '&iexcl;Bueno, ya he introducido mi direcci&oacute;n de Skype pero el icono no est&aacute; mostrando mi estado!',
		1 => 'Una vez que haya introducido su direcci&oacute;n de Skype en su perfil, usted necesitar&aacute; cambiar entonces su configuraci&oacute;n de privacidad del programa Skype (Skype-> Privacidad...-> Mostrar Opciones avanzadas-> Mostrar mi estado de conexi&oacute;n en la web.). Su estado en Skype ser&aacute; mostrado entonces a todos los usuarios registrados.'
	),
	array(
		0 => '&iquest;Qu&eacute; hace el bot&oacute;n del tema?',
		1 => 'Si tiene Skype instalado, usted puede realizar una llamada <b><i>call</i></b> a cualquier lugar o enviar un mensaje <b><i>chat</i></b> a ese usuario (dependiendo de las preferencias de los usuarios).'
	),
	array(
		0 => '&iquest;Qu&eacute; hace el bot&oacute;n/enlace del perfil?',
		1 => 'Le permite a un usuario realizar una multitud de funciones contigo v&iacute;a Skype. El usuario podr&aacute; a&ntilde;adirle como un contacto, enviarle un archivo, enviarle un mensaje de voz, comprobar su informaci&oacute;n de skype, hacerle una llamada o invitarle a un chat.'
	),
);
?>
