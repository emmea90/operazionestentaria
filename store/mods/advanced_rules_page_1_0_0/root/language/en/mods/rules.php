<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @package Advanced Rules Page
* @version $Id$
* @copyright (c) 2012 Dugi ( http://imgit.org )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'RULES'					=> 'Rules',
	'BOARD_RULES'			=> 'Board rules',
	'RULES_SECTIONS'		=> 'Rules sections',
	'RULES_CLEAR_HIGHLIGHT'	=> 'Clear highlighting',
	'RULES_BACK_TO_TOP'		=> 'Top',
	'RULES_HIGHLIGHT'		=> 'Highlight',
	'NO_CATS'				=> 'There are no sections',
	'BOARD_RULES_EXPLAIN'	=> 'This page is designed to explain your responsibilities as a member of our forum. By following these rules, we ensure that our board runs smoothly and without problems. If you have any question regarding these rules, do not hesitate to contact a staff member with your question.',
	'BOARD_RULES_DISABLED'	=> 'This page is currently undergoing an update. Please check again in a few minutes.',
));
?>