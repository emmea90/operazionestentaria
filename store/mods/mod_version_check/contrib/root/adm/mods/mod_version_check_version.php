<?php
/**
*
* @package acp
* @version $Id: mod_version_check_version.php 48 2007-09-23 20:23:14Z Handyman $
* @copyright (c) 2005 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @package mod_version_check
*/
class mod_version_check_version
{
	function version()
	{
		return array(
			'author'	=> 'Handyman`',
			'title'		=> 'MOD Version Check',
			'tag'		=> 'mod_version_check',
			'version'	=> '1.0.0',
			'file'		=> array('startrekguide.com', 'updatecheck', 'mods.xml'),
		);
	}
}

?>