For Mod Authors
To use this mod, follow this checklist of instructions

1) Upload an XML file to your website, use the mods.xml as a template
	note the xml structure -> <yourboardname><mods><mod_title>
	the mod_title goes in the tag section of the version file found in root/adm/mods/

2) Create your version file.
	Use the root/adm/mods/mod_version_check_version.php as an example
	
	//your phpbb.com username only (you must be a registered member on phpbb.com)
	'author'	=> 'Handyman`',
	
	//Title of your mod (as you want it displayed)
	'title'		=> 'MOD Version Check',
	
	//the tag name� the one you are using in the XML� it's critical that the xml tag name matches this
	'tag'		=> 'mod_version_check',
	
	//your current installed mod version
	'version'	=> '1.0.0',

	//file location where you uploaded your xml update check file
	//1) domain
	//2) path
	//3) filename (i.e. http://startrekguide.com/updatecheck/mods.xml)
	'file'		=> array('startrekguide.com', 'updatecheck', 'mods.xml'),

3) Make sure to include the adm/mods/your_mod_version.php with your mod in the installation instructions
4) Make a note in your mod for the user to download the mod version check so they will be able to put this to good use.
Mod is located at http://startrekguide.com/forum/viewtopic.php?f=87&amp;t=3584