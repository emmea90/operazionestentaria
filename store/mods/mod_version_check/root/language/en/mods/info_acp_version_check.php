<?php
/**
*
* acp [English]
*
* @package language
* @version $Id: info_acp_version_check.php 51 2007-10-30 04:40:42Z Handyman $
* @copyright (c) 2007 StarTrekGuide
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_MOD_VERSION_CHECK'	=> 'Check for MOD updates',
	'ANNOUNCEMENT_TOPIC'	=> 'Release Announcement',

	'CURRENT_VERSION'		=> 'Current Version',

	'DOWNLOAD_LATEST'		=> 'Download Latest Version',

	'LATEST_VERSION'		=> 'Latest Version',

	'NO_ACCESS_MODS_DIRECTORY'	=> 'Unable to open adm/mods, check to make sure that directory exists and you have read permission on that directory',
	'NO_INFO'					=> 'Version server could not be contacted',
	'NOT_UP_TO_DATE'			=> '%s is not up to date',

	'RELEASE_ANNOUNCEMENT'	=> 'Annoucement Topic',
	'UP_TO_DATE'			=> '%s is up to date',

	'VERSION_CHECK'			=> 'MOD Version Check',
	'VERSION_CHECK_EXPLAIN'	=> 'Checks to see if your mods are up to date',
));

?>