<?php
/** 
*
* @package phpBB3
* @version 1.1.0
* @copyright (c) 2012 Geolim4
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

// define constants..used in multiple files
// so do a if !defined first

// everyone can view..well at least registered users
if (!defined('POSTER_IP_DISABLED'))
{
	define('POSTER_IP_DISABLED', 0);
}
// only mods and admins can view
if (!defined('POSTER_IP_INPROFIL'))
{
	define('POSTER_IP_INPROFIL', 1);
}
// only admins
if (!defined('POSTER_IP_INPOST'))
{
	define('POSTER_IP_INPOST', 2);
}

?>