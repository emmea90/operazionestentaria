<?php
//
//	file: adm/mods/piv_version.php
//	author: Geolim4
//	begin: 30/06/2012
//	version: 1.1.1 - 07/06/2012
//	licence: http://opensource.org/licenses/gpl-license.php GNU Public License
//

// ignore
if ( !defined('IN_PHPBB') )
{
	exit;
}

class piv_version
{
	function version()
	{
		return array(
			'author' => 'Geolim4',
			'title' => 'Poster IP in viewtopic',
			'tag' => 'aucm',
			'version' => '1.1.1',
			'file' => array('geolim4.com', 'buildversion', 'piv_version.xml'), //Direct link: http://geolim4.com/buildversion/piv_version.xml
		);
	}
}
