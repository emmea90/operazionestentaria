<?php
/**
*
* @package - Sequential number on memberlist MOD [Polish]
* @version $Id$
* @copyright (c) 2012 all4phone (http://phpbbmods.cba.pl/)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang)) 
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM
//


$lang = array_merge($lang, array(
		'INSTALL_SNOM'				=> 'Sequential Number on Memberlist MOD',
		'INSTALL_SNOM_CONFIRM'		=> 'Czy jesteś gotowy do instalacji Sequential Number on Memberlist MOD?',
		'SNOM_TITLE'				=> 'Sequential Number on Memberlist MOD',
		'SNOM_EXPLAIN'				=> 'Instalacja Sequential Number on Memberlist, zmiany w bazie danych metodą auto UMIL.',
		'UNINSTALL_SNOM'			=> 'Deinstalacja Sequential Number on Memberlist MOD',
		'UNINSTALL_SNOM_CONFIRM'	=> 'Czy jesteś gotowy do deinstalacji Sequential Number on Memberlist MOD ? Wszystkie ustawienia i dane zapisane przez tego moda zostaną usunięte!',
		'UPDATE_SNOM'				=> 'Aktualizacja Sequential Number on Memberlist MOD',
		'UPDATE_SNOM_CONFIRM'		=> 'Czy jesteś gotowy do aktualizacji Sequential Number on Memberlist MOD ?',
));

?>