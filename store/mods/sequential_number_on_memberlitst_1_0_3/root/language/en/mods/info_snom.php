<?php
/**
*
* @package - Sequential number on memberlist MOD [English]
* @version $Id$
* @copyright (c) 2012 all4phone (http://phpbbmods.cba.pl/)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang)) 
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM
//


$lang = array_merge($lang, array(
		'INSTALL_SNOM'				=> 'Sequential Number on Memberlist MOD',
		'INSTALL_SNOM_CONFIRM'		=> 'Are you ready to install the Sequential Number on Memberlist MOD?',
		'SNOM_TITLE'				=> 'Sequential Number on Memberlist MOD',
		'SNOM_EXPLAIN'				=> 'Install Sequential Number on Memberlist database changes with UMIL auto method.',
		'UNINSTALL_SNOM'			=> 'Uninstall Sequential Number on Memberlist MOD',
		'UNINSTALL_SNOM_CONFIRM'	=> 'Are you ready to uninstall the Sequential Number on Memberlist MOD? All settings and data saved by this mod will be removed!',
		'UPDATE_SNOM'				=> 'Update Sequential Number on Memberlist MOD',
		'UPDATE_SNOM_CONFIRM'		=> 'Are you ready to update the Sequential Number on Memberlist MOD?',
));

?>