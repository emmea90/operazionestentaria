##############################################################
## MOD Title: 		phpBB SEO No Duplicate MAJ 0.4.4 => 0.6.0
## MOD Author: 		dcz <n/a> http://www.phpbb-seo.com/
## MOD Description: 	Ceci est la procédure de mise à jour du MOD phpBB SEO No Duplicate pour la transition version 0.4.4 => 0.6.0.
## 			Rendez-vous sur http://www.phpbb-seo.com/fr/toolkit-phpbb-seo/no-duplicate-phpbb3-seo-t1506.html
## 			pour toujours obtenir la dernière version ou de l'aide pour ce MOD.
##
## MOD Version: 	1.1
##
## Installation Level: 	Easy
## Installation Time: 	1 Minutes
## Files To Edit: 	1
##			viewforum.php.
##
## Included Files:	0
##
##############################################################
## Author Notes:
## _____________
##
## Voici les instructions de mise à jour : phpBB SEO No Duplicate 0.4.4 => 0.6.0
##
## LICENSE: http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
##
##############################################################
## MOD History:
##
##	2009-06-28 - 1.1
##		- Added a missing find / replace (the first one in this file)
##	2009-06-10 - 1.0
##		- First version released
##
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################

#
#-----[ OPEN ]------------------------------------------
#

includes/functions_display.php

#
#-----[ FIND ]------------------------------------------
#
	// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
	if ($phpbb_seo->seo_opt['no_dupe']['on']) {
		$sql_array['SELECT'] .= ', t.topic_id, t.topic_title, t.topic_replies, t.topic_replies_real, t.topic_status, t.topic_type, t.topic_moved_id';
		$sql_array['LEFT_JOIN'][] = array(
			'FROM'	=> array(TOPICS_TABLE => 't'),
			'ON'	=> "t.topic_last_post_id = f.forum_last_post_id" 
		);
	}
	// www.phpBB-SEO.com SEO TOOLKIT END -> no dupe

#
#-----[ REPLACE WITH ]------------------------------------------
#

	// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
	if ($phpbb_seo->seo_opt['no_dupe']['on']) {
		$sql_array['SELECT'] .= ', t.topic_id, t.topic_title, t.topic_replies, t.topic_replies_real, t.topic_status, t.topic_type, t.topic_moved_id' . (!empty($phpbb_seo->seo_opt['sql_rewrite']) ? ', t.topic_url ' : ' ');
		$sql_array['LEFT_JOIN'][] = array(
			'FROM'	=> array(TOPICS_TABLE => 't'),
			'ON'	=> "t.topic_last_post_id = f.forum_last_post_id" 
		);
	}
	// www.phpBB-SEO.com SEO TOOLKIT END -> no dupe

#
#-----[ FIND ]------------------------------------------
#

			if ( empty($phpbb_seo->seo_url['topic'][$row['topic_id']]) ) {
				if ($row['topic_type'] == POST_GLOBAL) {
					$phpbb_seo->seo_opt['topic_type'][$row['topic_id']] = POST_GLOBAL;
				}
				$phpbb_seo->seo_censored[$row['topic_id']] = censor_text($row['topic_title']);
				$phpbb_seo->seo_url['topic'][$row['topic_id']] = $phpbb_seo->format_url($phpbb_seo->seo_censored[$row['topic_id']]);
				$phpbb_seo->seo_opt['topic_forum_name'][$row['topic_id']] = $row['forum_name']; 

			}

#
#-----[ REPLACE WITH ]------------------------------------------
#

			$phpbb_seo->prepare_iurl($row, 'topic', $row['topic_type'] == POST_GLOBAL ? $phpbb_seo->seo_static['global_announce'] : $phpbb_seo->seo_url['forum'][$forum_id]);
			$phpbb_seo->seo_opt['topic_forum_name'][$row['topic_id']] = $row['forum_name'];

#
#-----[ FIND ]------------------------------------------
#

				// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
				if (@$phpbb_seo->seo_opt['no_dupe']['on']) {
					$forum_rows[$parent_id]['topic_id'] = $row['topic_id'];

#
#-----[ AFTER, ADD ]------------------------------------------
#

					$forum_rows[$parent_id]['topic_title'] = $row['topic_title'];

#
#-----[ FIND ]------------------------------------------
#

				$last_post_url =  append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . $row['forum_id_last_post'] . '&amp;t=' . $row['topic_id'] . '&amp;start=' . @intval($phpbb_seo->seo_opt['topic_last_page'][$row['topic_id']]) ) . '#p' . $row['forum_last_post_id'];
				// Limit in chars for the last post link text.
				$char_limit = 25;
				// Limit topic text link to $char_limit, without breacking words
				$topic_text_lilnk = $char_limit > 0 && ( ( $length = utf8_strlen($phpbb_seo->seo_censored[$row['topic_id']]) ) > $char_limit ) ? utf8_strlen($fragment = utf8_substr($phpbb_seo->seo_censored[$row['topic_id']], 0, $char_limit + 1 - 4)) < $length + 1 ? preg_replace('`\s*\S*$`', '', $fragment) . ' ...' : $phpbb_seo->seo_censored[$row['topic_id']] : $phpbb_seo->seo_censored[$row['topic_id']];
				$last_post_link = '<a href="' . append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . $row['forum_id_last_post'] . '&amp;t=' . $row['topic_id']) . '" title="' . $phpbb_seo->seo_censored[$row['topic_id']] . ' : ' . $phpbb_seo->seo_opt['topic_forum_name'][$row['topic_id']] . '">' . $topic_text_lilnk . '</a>';

#
#-----[ REPLACE WITH ]------------------------------------------
#

				$last_post_url =  append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . $row['forum_id_last_post'] . '&amp;t=' . $row['topic_id'] . '&amp;start=' . @intval($phpbb_seo->seo_opt['topic_last_page'][$row['topic_id']]) ) . '#p' . $row['forum_last_post_id'];
				$topic_title = censor_text($row['topic_title']);
				// Limit in chars for the last post link text.
				$char_limit = 25;
				// Limit topic text link to $char_limit, without breacking words
				$topic_text_lilnk = $char_limit > 0 && ( ( $length = utf8_strlen($topic_title) ) > $char_limit ) ? ( utf8_strlen($fragment = utf8_substr($topic_title, 0, $char_limit + 1 - 4)) < $length + 1 ? preg_replace('`\s*\S*$`', '', $fragment) . ' ...' : $topic_title ) : $topic_title;
				$last_post_link = '<a href="' . append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . $row['forum_id_last_post'] . '&amp;t=' . $row['topic_id']) . '" title="' . $topic_title . ' : ' . $phpbb_seo->seo_opt['topic_forum_name'][$row['topic_id']] . '">' . $topic_text_lilnk . '</a>';

#
#-----[ OPEN ]------------------------------------------
# NOTE : This may not be there is you updated to the SEO Utlimate URL mod already

phpbb_seo/phpbb_seo_class.php

#
#-----[ FIND ]------------------------------------------
# NOTE : This may not be there is you updated to the SEO Utlimate URL mod already

		// --> No Dupe
		$this->seo_opt['no_dupe']['on'] = $this->cache_config['dynamic_options']['no_dupe']['on'] = false;
		// <-- No Dupe


#
#-----[ REPLACE WITH ]------------------------------------------
# EG Delete

#
#-----[ OPEN ]------------------------------------------
#

phpbb_seo/includes/setup_phpbb_seo.php

#
#-----[ FIND ]------------------------------------------
#

	function init_phpbb_seo() {
		global $phpEx, $config, $phpbb_root_path;

#
#-----[ AFTER ADD ]------------------------------------------
#

		// --> No Dupe
		$this->seo_opt['no_dupe']['on'] = $this->cache_config['dynamic_options']['no_dupe']['on'] = false;
		// <-- No Dupe

#
#-----[ OPEN ]------------------------------------------
#

viewforum.php

#
#-----[ FIND ]------------------------------------------
#

			// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
			'U_LAST_POST' => @$phpbb_seo->seo_opt['no_dupe']['on'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . (($row['forum_id']) ? $row['forum_id'] : $forum_id) . '&amp;t=' . $topic_id . '&amp;start=' . @intval($phpbb_seo->seo_opt['topic_last_page'][$topic_id])) . '#p' . $row['topic_last_post_id'] : append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . (($row['forum_id']) ? $row['forum_id'] : $forum_id) . '&amp;t=' . $topic_id . '&amp;p=' . $row['topic_last_post_id']) . '#p' . $row['topic_last_post_id'],
			// www.phpBB-SEO.com SEO TOOLKIT END -> no dupe

#
#-----[ REPLACE WITH ]------------------------------------------
#

			// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
			'U_LAST_POST' => @$phpbb_seo->seo_opt['no_dupe']['on'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . (($row['forum_id']) ? $row['forum_id'] : $forum_id) . '&amp;t=' . $topic_id . '&amp;start=' . @intval($phpbb_seo->seo_opt['topic_last_page'][$topic_id])) . '#p' . $row['topic_last_post_id'] : append_sid("{$phpbb_root_path}viewtopic.$phpEx", $view_topic_url_params . '&amp;p=' . $row['topic_last_post_id']) . '#p' . $row['topic_last_post_id'],
			// www.phpBB-SEO.com SEO TOOLKIT END -> no dupe

#
#-----[ OPEN ]------------------------------------------
#

viewtopic.php

#
#-----[ FIND ]------------------------------------------
#

		// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
		'U_MINI_POST'		=> @$phpbb_seo->seo_opt['no_dupe']['on'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", 't=' . $topic_id . '&amp;f=' . $forum_id . '&amp;start=' . $start ) . '#p' . $row['post_id'] : append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'p=' . $row['post_id'] . (($topic_data['topic_type'] == POST_GLOBAL) ? '&amp;f=' . $forum_id : '')) . '#p' . $row['post_id'],
		// www.phpBB-SEO.com SEO TOOLKIT END -> no dupe

#
#-----[ REPLACE WITH ]------------------------------------------
#

		// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
		'U_MINI_POST' => @$phpbb_seo->seo_opt['no_dupe']['on'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", 't=' . $topic_id . '&amp;f=' . $forum_id . '&amp;start=' . $start ) . '#p' . $row['post_id'] : append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'p=' . $row['post_id'] . (($topic_data['topic_type'] == POST_GLOBAL) ? '&amp;f=' . $forum_id : '')) . '#p' . $row['post_id'],
		// www.phpBB-SEO.com SEO TOOLKIT END -> no dupe

#
#-----[ SAVE/CLOSE ALL FILES ]------------------------------------------
#
# EoM
