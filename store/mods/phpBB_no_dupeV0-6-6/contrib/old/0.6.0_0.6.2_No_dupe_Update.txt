##############################################################
## MOD Title: 		phpBB SEO No Duplicate 0.6.0 => 0.6.2 update
## MOD Author: 		dcz <n/a> http://www.phpbb-seo.com/
## MOD Description: 	This are the update steps for the phpBB SEO No Duplicate 0.6.0 => 0.6.2 update.
## 			Check http://www.phpbb-seo.com/en/phpbb-seo-toolkit/phpbb3-seo-no-duplicate-t1225.html
## 			for the latest version or to get help with this MOD.
##
## MOD Version: 	1.0
##
## Installation Level: 	Easy
## Installation Time: 	2 Minutes
## Files To Edit: 	1
##			includes/functions_display.php
##
## Included Files:	0
##
##############################################################
## Author Notes:
## _____________
##
## This are the update steps for the phpBB SEO No Duplicate 0.6.0 => 0.6.2 update.
##
## LICENSE: http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
##
##############################################################
## MOD History:
##
##	2009-11-24 - 1.0
##		- First version released
##
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################

#
#-----[ OPEN ]------------------------------------------
#

includes/functions_display.php

#
#-----[ FIND ]------------------------------------------
#
			if ($row['topic_status'] == ITEM_MOVED) {
				$row['topic_id'] = $row['topic_moved_id'];
			}
			$phpbb_seo->prepare_iurl($row, 'topic', $row['topic_type'] == POST_GLOBAL ? $phpbb_seo->seo_static['global_announce'] : $phpbb_seo->seo_url['forum'][$forum_id]);


#
#-----[ REPLACE WITH ]------------------------------------------
#

			if ($row['topic_status'] == ITEM_MOVED) {
				$row['topic_id'] = $row['topic_moved_id'];
			}

#
#-----[ FIND ]------------------------------------------
#
					$forum_rows[$parent_id]['topic_id'] = $row['topic_id'];
					$forum_rows[$parent_id]['topic_title'] = $row['topic_title'];

#
#-----[ AFTER, ADD ]------------------------------------------
#

					$forum_rows[$parent_id]['topic_type'] = $row['topic_type'];


#
#-----[ FIND ]------------------------------------------
#

			// www.phpBB-SEO.com SEO TOOLKIT BEGIN -> no dupe
			if (@$phpbb_seo->seo_opt['no_dupe']['on'] && !$row['forum_password'] && $auth->acl_get('f_read', $row['forum_id_last_post'])) {


#
#-----[ AFTER, ADD ]------------------------------------------
#

				$phpbb_seo->prepare_iurl($row, 'topic', $row['topic_type'] == POST_GLOBAL ? $phpbb_seo->seo_static['global_announce'] : $phpbb_seo->seo_url['forum'][$forum_id]);


#
#-----[ SAVE/CLOSE ALL FILES ]------------------------------------------
#
# EoM
