/**
 * OverGT for GamerCards & SACS scripts
 *
 * @version 1.0
 * @license GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html
 * @author (c) Rich McGirr http://rmcgirr83.org
 * @created 2010
 */
 
 var cancelImage = false, loadingImage = './safegt/loading.gif', ol_text = '';
function overlibImage(imagePath)
{
  bgImage=new Image();
  bgImage.src=imagePath;
  
  if(!bgImage.complete)
  {
    overlib(BACKGROUND,loadingImage,FGCOLOR,'',WIDTH,16,HEIGHT,16,VAUTO,HAUTO,OFFSETX,30);
    cancelImage=false;
    bgImage.onload=function()
    {   
      if(!cancelImage)
      {
        var substringpos=bgImage.src.length-imagePath.length;
        if(bgImage.src.substring(substringpos)==imagePath)
        {
          overlib(BACKGROUND,imagePath,FGCOLOR,'',WIDTH,bgImage.width,HEIGHT,bgImage.height,VAUTO,HAUTO,OFFSETX,30);
        }
      }
    }
  }
  else
  {
    overlib(BACKGROUND,imagePath,FGCOLOR,'',WIDTH,bgImage.width,HEIGHT,bgImage.height,VAUTO,HAUTO,OFFSETX,30);
  }
}

function overlibMouseout()
{
  cancelImage=true;
  return nd();
}


/**
 * OverGT for XboX Live GamerCards
 *
 * @version 1.0
 * @license GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html
 * @author (c) Soshen http://nipponart.org
 * @created 2010
 */
function getXboxCard(card)
{
	overlib('<iframe src="'+card+'\" width="200" height="136" frameborder="0" scrolling="no"></iframe>',WRAP,WIDTH,200,HEIGHT,136,BELOW,OFFSETX,50,FGBACKGROUND,'./safegt/xboxloading.gif',FGCOLOR,'',BGCOLOR,'',DELAY,0);
}