<?php
/**
*
* @package acp SafeGT
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

define('PLAYSTATION3', 0);
define('VITA', 1);

/**
* @package acp
*/
class acp_safegt
{
	var $u_action;

	function main($id, $mode)
	{
		global $db, $user, $auth, $template;
		global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx;
		
		// Setup the language.
		$user->add_lang('mods/lang_safegt');

		switch($mode)
		{
			case 'main':
			
				$this->tpl_name = 'acp_safegt_cfg';
				$this->page_title = 'ACP_SAFEGT_CFG';
		
				$submit = (isset($_POST['submit'])) ? true : false;
				
				// Check if the umil file are installed.
				if (empty($config['safegt_version']))
				{
					$redirect_url = append_sid("{$phpbb_admin_path}index.$phpEx");
					meta_refresh(4, $redirect_url);
					trigger_error(sprintf($user->lang['SAFEGT_UMIL_NOT_ACTIVE'], '<a href="' . $redirect_url . '">', '</a>'));
				}
				
				// VERSION CHECK
				$errstr		= '';
				$errno		= 0;
				$remotefile	= get_remote_file("nipponart.org", '', 'modversion.txt', $errstr, $errno);
				
				if ($remotefile !== false)
				{
					preg_match('/safegt=(.*?)__/msu', $remotefile, $sgtversion);
					if ($config['safegt_version'] != $sgtversion[1])
					{
						$template->assign_vars(array(
							'TUAV'		=> $config['safegt_version'],
							'LATESTV'	=> $sgtversion[1],
							'S_ERROR' 	=> true,
						));
					}
				}
				
				// Template definitions
				$template->assign_vars(array(
					'U_ACTION'				=> $this->u_action,
					'CK_XBOX'				=> !empty($config['safegt_xbox']) ? ' checked="checked"' : '',
					'CK_PSN'				=> !empty($config['safegt_psn']) ? ' checked="checked"' : '',
					'CK_WII'				=> !empty($config['safegt_wii']) ? ' checked="checked"' : '',
					'CK_STEAM'				=> !empty($config['safegt_steam']) ? ' checked="checked"' : '',
					'CK_XFIRE'				=> !empty($config['safegt_xfire']) ? ' checked="checked"' : '',
					'CK_ORIGIN'				=> !empty($config['safegt_origin']) ? ' checked="checked"' : '',
					'CK_XBOX_VALIDATION'	=> !empty($config['safegt_xboxcheck']) ? ' checked="checked"' : '',
					'CK_PSN_VALIDATION'		=> !empty($config['safegt_psncheck']) ? ' checked="checked"' : '',
					'CREDIT'				=> sprintf($user->lang['CREDIT1'], '<a href="http://www.nipponart.org">', '</a>')
				));
				
				// Actions
				if ($submit)
				{
					// Yes, handle the form.
					$xbox	= request_var('xbox', 0);
					$psn	= request_var('psn', 0);
					$wii	= request_var('wii', 0);
					$steam	= request_var('steam', 0);
					$xfire	= request_var('xfire', 0);
					$origin	= request_var('origin', 0);
					$ckxbox	= request_var('xbox_check', 0);
					$ckpsn	= request_var('psn_check', 0);

					// Update options.
					set_config('safegt_xbox', (int) $xbox);
					set_config('safegt_psn', (int) $psn);
					set_config('safegt_wii', (int) $wii);
					set_config('safegt_steam', (int) $steam);
					set_config('safegt_xfire', (int) $xfire);
					set_config('safegt_origin', (int) $origin);
					set_config('safegt_xboxcheck', (int) $ckxbox);
					set_config('safegt_psncheck', (int) $ckpsn);
					
					add_log('admin', 'LOG_SAFEGT_CONFIG_EDIT', $user->data['username']);
					trigger_error($user->lang['SAFEGT_CONFIG_EDITED'] . adm_back_link($this->u_action));
				}
			break;
			
			case 'psn':
				$this->tpl_name = 'safegt/acp_psngame';
				$this->page_title = 'ACP_PSN_GAME';

				// Include the file for this mode.
				include($phpbb_root_path . 'includes/acp/safegt/acp_psngame.' . $phpEx);
				acp_safegt_psn($id, $mode, $this->u_action);
			break;
			
			case 'psn_trophy':
				$this->tpl_name = 'safegt/acp_psntrophy';
				$this->page_title = 'ACP_PSN_TROPHY';

				// Include the file for this mode.
				include($phpbb_root_path . 'includes/acp/safegt/acp_psntrophy.' . $phpEx);
				acp_safegt_psntrophy($id, $mode, $this->u_action);
			break;
			
			case 'psn_user':
				$this->tpl_name = 'safegt/acp_psnuser';
				$this->page_title = 'ACP_PSN_USER';

				// Include the file for this mode.
				include($phpbb_root_path . 'includes/acp/safegt/acp_psnuser.' . $phpEx);
				acp_safegt_user($id, $mode, $this->u_action);
			break;
			
			case 'coop':
				$this->tpl_name = 'safegt/acp_coop';
				$this->page_title = 'ACP_COOP_ADD_GAME';

				// Include the file for this mode.
				include($phpbb_root_path . 'includes/acp/safegt/acp_coop.' . $phpEx);
				acp_safegt_coop($id, $mode, $this->u_action);
			break;
			
			case 'xbox':
				$this->tpl_name = 'safegt/acp_xbox';
				$this->page_title = 'ACP_XBOX';

				// Include the file for this mode.
				include($phpbb_root_path . 'includes/acp/safegt/acp_xbox.' . $phpEx);
				acp_safegt_xbox($id, $mode, $this->u_action);
			break;
		}
	}
}
?>