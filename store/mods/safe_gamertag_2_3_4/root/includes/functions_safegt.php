<?php
/**
*
* @package Safe GamerTag
* @version $Id: functions_safegt.php
* @copyright (c) 2012, Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * Check if a gamercard is valid
 * @param $platform string (XBOX|WII|STEAM|XFIRE|PSN)
 * @param $gamercard string
 * @param $error array
 * return bolean, true if the gamercard exist
 */
function gt_validate($platform = 'XBOX', $gamercard = '', &$error)
{
	global $user;
	
	if (!empty($gamercard))
	{
		switch ($platform)
		{
			case 'WII':
				$url = $gamercard;
				if (!preg_match('#^(http|https|ftp)://(?:(.*?\.)*?[a-z0-9\-]+?\.[a-z]{2,4}|(?:\d{1,3}\.){3,5}\d{1,3}):?([0-9]*?).*?\.(gif|jpg|jpeg|png)$#i', $url))
				{
					$error[] = $user->lang['WII_URL_NO_CORRECT_URL'];
					return false;
				}
				
				if (($data = @getimagesize($url)) === false)
				{
					$error[] = $user->lang['WII_URL_NO_IMAGE'];
					return false;
				}
				
				if ($data[0] > 411 || $data[1] > 201)
				{
					$error[] = $user->lang['IMMAGINE_TROPPO_GRANDE'];
					return false;
				}
				
				if (!sizeof($error))
				{
					$S_result = true;
				}
				else
				{
					$S_result = false;
				}
			break;
			case 'PSN':
				if (function_exists('curl_version') == 'Enabled')
				{
					$url = $gamercard;
					
					$curl = curl_init($url);
					curl_setopt($curl, CURLOPT_NOBODY, true);
					$result = curl_exec($curl);
					$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
					
					if ($result === false || $statusCode != 200)
					{
						$error[] = $user->lang['PSN_URL_NO_IMAGE'];
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					$error[] = $user->lang['CURL_UNACTIVE'];
					return false;
				}
			break;
			case 'STEAM':
				$url = "http://badges.steamprofile.com/profile/extended/steam/{$gamercard}.png";
				if (@getimagesize($url) === false)
				{
					$error[] = $user->lang['STEAM_URL_NO_IMAGE'];
					return false;
				}
				else
				{
					return true;
				}
			break;
			case 'XFIRE':
				$url = "http://miniprofile.xfire.com/bg/sh/type/0/{$gamercard}.png";
				if (@getimagesize($url) === false)
				{
					$error[] = $user->lang['XFIRE_URL_NO_IMAGE'];
					return false;
				}
				else
				{
					return true;
				}
			break;
			case 'XBOX':
			default:
				if (function_exists('curl_version') == 'Enabled')
				{
					$url = 'http://gamercard.xbox.com/en-US/'. str_replace(' ','%20',$gamercard) . '.card';
					$ch = curl_init();
					if (version_compare(PHP_VERSION, '5.1.3', '<'))
					{
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
					}
					else
					{
						curl_setopt_array($ch, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true, CURLOPT_CONNECTTIMEOUT => 10));
					}
					$res = curl_exec($ch);
					curl_close($ch);
					
					preg_match('/<div id="Gamerscore">(.*?)<\/div>/msu',$res,$match);
					$thematch = (!empty($match)) ? $match[1] : '--';
					if ($thematch == '--')
					{
						$error[] = $user->lang['XBOX_GT_INACTIVE'];
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					$error[] = $user->lang['CURL_UNACTIVE'];
					return false;
				}
			break;
		}
	}
}

function gt_unique($platform = 'XBOX', $gamertag)
{
	global $db;
	
	if (empty($gamertag))
	{
		return true;
	}
	
	switch ($platform)
	{
		case 'PSN':
			$sql = 'SELECT user_psngt
					FROM ' . USERS_TABLE . "
					WHERE LOWER(user_psngt) = '" . $db->sql_escape(utf8_strtolower($gamertag)) . "'";
		break;
		case 'WII':
			$sql = 'SELECT user_wiigt
					FROM ' . USERS_TABLE . "
					WHERE LOWER(user_wiigt) = '" . $db->sql_escape(utf8_strtolower($gamertag)) . "'";
		break;
		case 'STEAM':
			$sql = 'SELECT user_steamid
					FROM ' . USERS_TABLE . "
					WHERE LOWER(user_steamid) = '" . $db->sql_escape(utf8_strtolower($gamertag)) . "'";
		break;
		case 'XFIRE':
			$sql = 'SELECT user_xfiregt
					FROM ' . USERS_TABLE . "
					WHERE LOWER(user_xfiregt) = '" . $db->sql_escape(utf8_strtolower($gamertag)) . "'";
		break;
		case 'ORIGIN':
			$sql = 'SELECT user_originid
					FROM ' . USERS_TABLE . "
					WHERE LOWER(user_originid) = '" . $db->sql_escape(utf8_strtolower($gamertag)) . "'";
		break;
		case 'XBOX':
		default:
			$sql = 'SELECT user_xboxgt
					FROM ' . USERS_TABLE . "
					WHERE LOWER(user_xboxgt) = '" . $db->sql_escape(utf8_strtolower($gamertag)) . "'";
		break;
	}
	
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	if ($row)
	{
		return false;
	}
	
	return true;
}
?>