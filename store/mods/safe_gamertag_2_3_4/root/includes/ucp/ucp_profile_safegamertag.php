<?php
/**
*
* @package ucp
* @version $Id ucp_profile_safegamertag.php
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function ucp_profile_safegamertag($id, $mode, $u_action)
{
	global $user, $template, $auth, $db;
	global $phpbb_root_path, $config, $phpEx;
	
	include($phpbb_root_path . 'includes/functions_safegt.' . $phpEx);
	$gtmode = request_var('mode', '');
	$user->add_lang('mods/lang_safegt');
	$error = array();
	
	// Check if the umil file are installed or if at least one gt system are actived.
	if (empty($config['safegt_xbox']) && empty($config['safegt_psn']) && empty($config['safegt_wii']) && empty($config['safegt_steam']) && empty($config['safegt_xfire']) && empty($config['safegt_origin']))
	{
		$redirect_url = append_sid("{$phpbb_root_path}ucp.$phpEx");
		meta_refresh(4, $redirect_url);
		trigger_error(sprintf($user->lang['SAFEGT_UMIL_NOT_ACTIVE'], '<a href="' . $redirect_url . '">', '</a>'));
	}
	
	$template->assign_vars(array(
		'XBOX' 			=> $user->data['user_xboxgt'],
		'XBOXIMG' 		=> 'http://gamercard.xbox.com/en-US/'. $user->data['user_xboxgt'] .'.card',
		'S_XBOXANTE'	=> (!empty($user->data['user_xboxgt'])) ? true : false,
		'ULANG' 		=> $user->data['user_lang'],
		'PSN' 			=> $user->data['user_psngt'],
		'PSNIMG' 		=> ($user->data['user_psnzone'] == 0) ? "http://mypsn.eu.playstation.com/psn/profile/{$user->data['user_psngt']}.png" : "http://fp.profiles.us.playstation.com/playstation/psn/pid/{$user->data['user_psngt']}.png",
		'PSNZONE1' 		=> ($user->data['user_psnzone'] == 0) ? 'selected="selected"' : '',
		'PSNZONE2' 		=> ($user->data['user_psnzone'] == 1) ? 'selected="selected"' : '',
		'S_PSNANTE' 	=> (!empty($user->data['user_psngt'])) ? true : false,
		'WII'			=> $user->data['user_wiigt'],
		'WIIURL' 		=> $user->data['user_wiiurl'],
		'S_WIIANTE' 	=> (!empty($user->data['user_wiiurl'])) ? true : false,
		'XFIRE' 		=> $user->data['user_xfiregt'],
		'XFIRESKIN1' 	=> ($user->data['user_xfireskin'] == 'sh') ? 'selected="selected"' : '',
		'XFIRESKIN2' 	=> ($user->data['user_xfireskin'] == 'co') ? 'selected="selected"' : '',
		'XFIRESKIN3' 	=> ($user->data['user_xfireskin'] == 'sf') ? 'selected="selected"' : '',
		'XFIRESKIN4' 	=> ($user->data['user_xfireskin'] == 'os') ? 'selected="selected"' : '',
		'XFIRESKIN5' 	=> ($user->data['user_xfireskin'] == 'bg') ? 'selected="selected"' : '',
		'S_XFIREANTE' 	=> (!empty($user->data['user_xfiregt'])) ? true : false,
		'XFIREIMG' 		=> "http://miniprofile.xfire.com/bg/{$user->data['user_xfireskin']}/type/0/{$user->data['user_xfiregt']}.png",
		'STEAM' 		=> $user->data['user_steamgt'],
		'S_STEAMANTE' 	=> (!empty($user->data['user_steamid'])) ? true : false,
		'STEAMIMG' 		=> "http://badges.steamprofile.com/profile/extended/steam/{$user->data['user_steamid']}.png",
		'STEAMID' 		=> $user->data['user_steamid'],
		'ORIGINID'		=> $user->data['user_originid'],
		'S_XBOX'		=> !empty($config['safegt_xbox']) ? true : false,
		'S_PSN'			=> !empty($config['safegt_psn']) ? true : false,
		'S_WII'			=> !empty($config['safegt_wii']) ? true : false,
		'S_STEAM'		=> !empty($config['safegt_steam']) ? true : false,
		'S_XFIRE'		=> !empty($config['safegt_xfire']) ? true : false,
		'S_ORIGIN'		=> !empty($config['safegt_origin']) ? true : false
	));	
	
	$editusergt	= (!empty($_POST['editusergt'])) ? true : false;
	if ($editusergt)
	{
		$xbox 		= utf8_normalize_nfc(request_var('xbox', '', true));
		$psn 		= utf8_normalize_nfc(request_var('psn', '', true));
		$psnzone 	= request_var('psnzone', 0);
		$wii 		= utf8_normalize_nfc(request_var('wii', '', true));
		$wiiurl 	= utf8_normalize_nfc(request_var('wiiurl', '', true));
		$steam 		= utf8_normalize_nfc(request_var('steam', '', true));
		$steamid 	= request_var('steamid', '');
		$xfire 		= utf8_normalize_nfc(request_var('xfire', '', true));
		$xfireskin 	= utf8_normalize_nfc(request_var('xfireskin', '', true));
		$origin 	= utf8_normalize_nfc(request_var('origin', '', true));
		
		// Refix some values
		$psnzone	= (int) $psnzone;
		$steamid 	= (string) $steamid;
		
		if (!empty($xbox) && (strpos($xbox, '&quot;') !== false || strpos($xbox, '"') !== false || !preg_match('#^[a-zA-Z0-9\ ]+$#u', $xbox) || strlen($xbox) < 3 || strlen($xbox) > 15))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_XBOX'];
		}
	
		if (!empty($psn) && (strpos($psn, '&quot;') !== false || strpos($psn, '"') !== false || !preg_match('#^[a-zA-Z0-9-_]+$#u', $psn) || strlen($psn) < 4 || strlen($psn) > 16))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_PSN'];
		}

		if (!empty($wii) && (strpos($wii, '&quot;') !== false || strpos($wii, '"') !== false || !preg_match('#^[0-9- ]+$#u', $wii) || strlen(str_replace("-", "", str_replace(" ", "", $wii))) != 16))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_WII'];
		}
		
		if ((!empty($wii) && empty($wiiurl)) || (empty($wii) && !empty($wiiurl)))
		{
			$error[]	= $user->lang['WII_NOT_FILLED_ALL'];
		}

		if (!empty($steam) && (strpos($steam , '&quot;') !== false || strpos($steam , '"') !== false || !preg_match('#^[a-zA-Z0-9-_\ ]+$#u', $steam) || strlen($steam) < 2 || strlen($steam) > 30))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_STEAM_NOME'];
		}

		if (!empty($steamid) && (strpos($steamid , '&quot;') !== false || strpos($steamid , '"') !== false || !preg_match('#^[0-9]+$#u', $steamid) || strlen($steamid) != 17))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_STEAM'];
		}
		
		if ((!empty($steam) && empty($steamid)) || (empty($steam) && !empty($steamid)))
		{
			$error[]	= $user->lang['STEAM_NOT_FILLED_ALL'];
		}
				
		if (!empty($xfire) && (strpos($xfire , '&quot;') !== false || strpos($xfire , '"') !== false || !preg_match('#^[a-zA-Z0-9\ ]+$#u', $xfire) || strlen($xfire) < 1 || strlen($xfire) > 25))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_XFIRE'];
		}
		
		if (!empty($origin) && (strpos($origin, '&quot;') !== false || strpos($origin, '"') !== false || !preg_match('#^[a-zA-Z0-9-_]+$#u', $origin) || strlen($origin) < 4 || strlen($origin) > 16))
		{
			$error[]	= $user->lang['NON_ALFANUMERICO_ORIGIN'];
		}
		
		// Check for Unique gamertag
		if (!empty($xbox) && $xbox != $user->data['user_xboxgt'])
		{
			if (!gt_unique('XBOX', $xbox))
			{
				$error[] = sprintf($user->lang['GT_ALREADY_USED'], $user->lang['XBOXGT']);
			}
		}
		
		if (!empty($psn) && $psn != $user->data['user_psngt'])
		{			
			if (!gt_unique('PSN', $psn))
			{
				$error[] = sprintf($user->lang['GT_ALREADY_USED'], $user->lang['PSNGT']);
			}
		}
		
		if (!empty($wii) && $wii != $user->data['user_wiigt'])
		{			
			if (!gt_unique('WII', $wii))
			{
				$error[] = sprintf($user->lang['GT_ALREADY_USED'], $user->lang['WIIGT']);
			}
		}
		
		if (!empty($steamid) && $steamid != $user->data['user_steamid'])
		{
			if (!gt_unique('STEAM', $steamid))
			{
				$error[] = sprintf($user->lang['GT_ALREADY_USED'], $user->lang['STEAMID']);
			}
		}
		
		if (!empty($xfire) && $xfire != $user->data['user_xfiregt'])
		{			
			if (!gt_unique('XFIRE', $xfire))
			{
				$error[] = sprintf($user->lang['GT_ALREADY_USED'], $user->lang['XFIRE_NAME']);
			}
		}
		
		if (!empty($origin) && $origin != $user->data['user_originid'])
		{			
			if (!gt_unique('ORIGIN', $origin))
			{
				$error[] = sprintf($user->lang['GT_ALREADY_USED'], $user->lang['ORIGIN']);
			}
		}
		
		// Advanced check
		if (!empty($config['safegt_xboxcheck']) && !empty($xbox) && $xbox != $user->data['user_xboxgt'])
		{
			gt_validate('XBOX', $xbox, $error);
		}
		
		if (!empty($config['safegt_psncheck']) && !empty($psn) && $psn != $user->data['user_psngt'])
		{
			$psn4check = ($psnzone == 0) ? "http://mypsn.eu.playstation.com/psn/profile/{$psn}.png" : "http://fp.profiles.us.playstation.com/playstation/psn/pid/{$psn}.png";
			gt_validate('PSN', $psn4check, $error);
		}
		
		if (!empty($wiiurl) && $wiiurl != $user->data['user_wiiurl'])
		{
			gt_validate('WII', $wiiurl, $error);
		}

		
		// if error send temp data to template
		if (sizeof($error))
		{
			$template->assign_vars(array(
				'XBOX' 			=> $xbox,
				'PSN' 			=> $psn,
				'PSNZONE1' 		=> ($psnzone == 0) ? 'selected="selected"' : '',
				'PSNZONE2' 		=> ($psnzone == 1) ? 'selected="selected"' : '',
				'WII'			=> $wii,
				'WIIURL' 		=> $wiiurl,
				'XFIRE' 		=> $xfire,
				'XFIRESKIN1' 	=> ($xfireskin == 'sh') ? 'selected="selected"' : '',
				'XFIRESKIN2' 	=> ($xfireskin == 'co') ? 'selected="selected"' : '',
				'XFIRESKIN3' 	=> ($xfireskin == 'sf') ? 'selected="selected"' : '',
				'XFIRESKIN4' 	=> ($xfireskin == 'os') ? 'selected="selected"' : '',
				'XFIRESKIN5' 	=> ($xfireskin == 'bg') ? 'selected="selected"' : '',
				'STEAM' 		=> $steam,
				'STEAMID' 		=> $steamid,
				'ORIGIN' 		=> $origin
			));
		}
					
		if (!sizeof($error))
		{
			$sql_array	= array(
				'user_xboxgt' 		=> $xbox,
				'user_psngt' 		=> $psn,
				'user_psnzone' 		=> $psnzone,
				'user_wiigt' 		=> str_replace(" ","-",$wii),
				'user_wiiurl'		=> $wiiurl,
				'user_steamgt' 		=> $steam,
				'user_steamid' 		=> $steamid,
				'user_xfiregt' 		=> $xfire,
				'user_xfireskin' 	=> $xfireskin,
				'user_originid' 	=> $origin,
			);
				
			$sql = "UPDATE " . USERS_TABLE . " SET " . $db->sql_build_array('UPDATE', $sql_array) . " WHERE user_id =" . (int) $user->data['user_id'];
			$db->sql_query($sql);
	
			$redirect_url = append_sid("{$phpbb_root_path}ucp.$phpEx", "i=profile&amp;mode=safegt");
			meta_refresh(2, $redirect_url);
			trigger_error('AGGIORNATE_INFO_GIOCATORE_GT');
		}	
	}

	$template->assign_vars(array(
	   'S_SAFEGT_UCP'	=> ($gtmode == 'safegt') ? true : false,
	   'ERROR'			=> (sizeof($error)) ? implode('<br />', $error) : '',
  	   'U_ACTION' 		=> $u_action)
	);
}
?>