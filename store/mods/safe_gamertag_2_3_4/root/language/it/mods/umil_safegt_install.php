<?php
/**
*
* safegamertag install [Italian]
*
* @package language
* @version $Id: umil_safegt_install.php 4933 2011-01-09 14:40:11Z Soshen $
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_SAFE_GAMERTAG'                     => 'Safe GamerTag',
	'INSTALL_ACP_SAFE_GAMERTAG'             => 'Installa Safe GamerTag Mod',
	'INSTALL_ACP_SAFE_GAMERTAG_CONFIRM'     => 'Sei pronto a installare la Safe GamerTag mod?',
	'UNINSTALL_ACP_SAFE_GAMERTAG'			=> 'Disinstalla Safe GamerTag Mod',
	'UNINSTALL_ACP_SAFE_GAMERTAG_CONFIRM'	=> 'Sei pronto a disinstallare Safe GamerTag Mod?  Tutti i settaggi e i record salvati da questa mod saranno cancellati!',
	'UPDATE_ACP_SAFE_GAMERTAG'				=> 'Aggiorna Safe GamerTag Mod',
	'UPDATE_ACP_SAFE_GAMERTAG_CONFIRM'		=> 'Sei pronto ad aggiornare Safe GamerTag Mod?',
));

?>