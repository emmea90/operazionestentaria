##############################################################
## MOD Title: 		META TAGS gallery v 1.0.3
## MOD Author: 		dcz http://www.phpbb-seo.com/
## MOD Description: 	ADD on to the  to the phpBB SEO Dynamic meta tag mod for the gallery mod.
##			phpBB SEO Dynamic meta tag mod adds dynamic meta tags to phpBB ( http://www.phpbb-seo.com/en/phpbb-seo-toolkit/seo-dynamic-meta-tags-t1308.html )
##			The gallery mods adds a groovy photo album to phpBB ( http://www.flying-bits.org/download.php?mod_id=6 )
##
## MOD Version: 	1.0.3
##
## Installation Level: 	(Eazy)
## Installation Time: 	2 Minutes
## Files To Edit: 	(2)
##			gallery/album.php,
##			gallery/image_page.php,
##
## Included Files: n/a
##############################################################
## Author Notes:
##
## You will need the phpBB SEO Dynamic Meta tags mod installed to install this add on :
##	http://www.phpbb-seo.com/en/phpbb-seo-toolkit/seo-dynamic-meta-tags-t1308.html
##
## LICENSE: http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
##
##############################################################
## MOD History:
##
##	2009-05-09 - 1.0.3
##		- updated to phpBB gallery 1.0.3
##	2009-19-07 - 1.0.1RC1
##		- first release
##
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################

#
#-----[ OPEN ]------------------------------------------
#

gallery/album.php

#
#-----[ FIND ]------------------------------------------
# Note the line may be longer, but tabs are preserved!

if (version_compare($config['version'], '3.0.5', '>'))
{
	page_header(


#
#-----[ BEFORE, ADD ]------------------------------------------
#

// www.phpBB-SEO.com SEO TOOLKIT BEGIN - META
$seo_meta->meta['meta_desc'] = $seo_meta->meta_filter_txt($album_data['album_name'] . ' : ' . (!empty($album_data['album_desc']) ? $album_data['album_desc'] : $config['site_desc']));
$seo_meta->meta['keywords'] = $seo_meta->make_keywords($seo_meta->meta['meta_desc']);
// www.phpBB-SEO.com SEO TOOLKIT END - META

#
#-----[ OPEN ]------------------------------------------
#

gallery/image_page.php

#
#-----[ FIND ]------------------------------------------
# Note the line may be longer.

page_header(

#
#-----[ BEFORE, ADD ]------------------------------------------
#

// www.phpBB-SEO.com SEO TOOLKIT BEGIN - META
$seo_meta->meta['meta_desc'] = $seo_meta->meta_filter_txt($image_data['image_name'] . ' : ' . (!empty($image_data['image_desc']) ? $image_data['image_desc'] : (!empty($album_data['album_desc']) ? $album_data['album_desc'] : $config['site_desc'])) . ' - ' . $album_data['album_name']);
$seo_meta->meta['keywords'] = $seo_meta->make_keywords($seo_meta->meta['meta_desc']);
// www.phpBB-SEO.com SEO TOOLKIT END - META

#
#-----[ SAVE/CLOSE ALL FILES ]------------------------------------------
#
# EoM
