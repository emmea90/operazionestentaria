<?php
/**
*
* prime_trash_bin_b [Arabic]
*
* @package language
* @version $Id: prime_trash_bin_b.php,v 1.0.5 2008/08/23 17:01:00 primehalo Exp $
* @copyright (c) 2007 Ken F. Innes IV
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
* @Translated by: http://www.elibrary4arab.com
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

//Logs
$lang = array_merge($lang, array(
	// Overwrite
	'LOG_TOPIC_DELETED'		=> '<strong>موضوع محذوف بشكل دائم</strong><br />» %s',
	'LOG_DELETE_TOPIC'		=> '<strong>موضوع محذوف بشكل دائم</strong><br />» %s',
	'LOG_DELETE_POST'		=> '<strong>مشاركة محذوفة بشكل دائم</strong><br />» %s',

	// New
	'LOG_TOPIC_STIFLED'		=> '<strong>موضوع محذوف</strong><br />» %1$s',
	'LOG_TOPIC_TRASHED'		=> '<strong>موضوع محذوف إلى سلة المحذوفات</strong><br />» %1$s',
	'LOG_TOPIC_UNSTIFLED'	=> '<strong>استرجاع الموضوع</strong><br />» %1$s',

	'LOG_POST_STIFLED'		=> '<strong>مشاركة محذوفة</strong><br />» %1$s',
	'LOG_POST_TRASHED'		=> '<strong>مشاركة محذوفة إلى سلة المحذوفات</strong><br />» %1$s',
	'LOG_POST_UNSTIFLED'	=> '<strong>استرجاع المشاركة</strong><br />» %1$s',
));

$lang = array_merge($lang, array(
	'LOG_TOPIC_STIFLED_2'	=> $lang['LOG_TOPIC_STIFLED'] . '<br />» » <strong>السبب:</strong> %2$s',
	'LOG_TOPIC_TRASHED_2'	=> $lang['LOG_TOPIC_TRASHED'] . '<br />» » <strong>السبب:</strong> %2$s',
	'LOG_TOPIC_UNSTIFLED_2'	=> $lang['LOG_TOPIC_UNSTIFLED'] . '<br />» » <strong>السبب:</strong> %2$s',

	'LOG_POST_STIFLED_2'	=> $lang['LOG_POST_STIFLED'] . '<br />» » <strong>السبب:</strong> %2$s',
	'LOG_POST_TRASHED_2'	=> $lang['LOG_POST_TRASHED'] . '<br />» » <strong>السبب:</strong> %2$s',
	'LOG_POST_UNSTIFLED_2'	=> $lang['LOG_POST_UNSTIFLED'] . '<br />» » <strong>السبب:</strong> %2$s',
));


// Administration
$lang = array_merge($lang, array(
	'PRIME_FAKE_DELETE'					=> 'حذف المواضيع',
	'PRIME_FAKE_DELETE_EXPLAIN'			=> 'قرّر كيف تريد أن تُحذف المواضيع.',
	'PRIME_FAKE_DELETE_DISABLE'			=> 'حذف الموضوع بشكل دائم',
	'PRIME_FAKE_DELETE_ENABLE'			=> 'احتفظ بالموضوع وعلّمه كموضوع محذوف',
	'PRIME_FAKE_DELETE_AUTO_TRASH'		=> 'نقل الموضوع إلى سلة المحذوفات',
	'PRIME_FAKE_DELETE_SHADOW_ON'		=> 'نقل الموضوع إلى سلة المحذوفات مع بقاء ظل الموضوع',
	'PRIME_FAKE_DELETE_SHADOW_OFF'		=> 'نقل الموضوع إلى سلة المحذوفات بدون ترك ظل الموضوع',

	'PRIME_TRASH_FORUM'					=> 'منتدى سلة المحذوفات',
	'PRIME_TRASH_FORUM_EXPLAIN'			=> 'إذا تم اختيار حذف الموضوع سيتم نقله إلى هذا المنتدى. وحذف موضوع موجود في سلة المحذوفات سوف يزيله بشكل دائم.',
	'PRIME_TRASH_FORUM_DISABLE'			=> 'لا تستخدم سلة المحذوفات',
	'PRIME_TRASH_FORUM_DIVIDER'			=> '---------------------------',
	'PRIME_NO_TRASH_FORUM_ERROR'		=> 'عليك أن تختار منتدى لسلة المحذوفات عند اختيارك لـ "%s"',

	'PRIME_FOREVER_WHEN_DELETE_USER'	=> 'حذف المشاركات بشكل دائم',
));

// Moderation
$lang = array_merge($lang, array(

	// Topics - Deleting
	'PRIME_DELETE_TOPIC_REASON'			=> 'الرجاء إدخال سبب الحذف',
	'PRIME_DELETE_TOPIC_FOREVER'		=> 'حذف هذا الموضوع بشكل دائم',
	'PRIME_DELETE_TOPICS_FOREVER'		=> 'حذف هذه المواضيع بشكل دائم',
	'PRIME_DELETE_TO_TRASH_BIN'			=> 'نقل الموضوع إلى منتدى سلة المحذوفات',
	'PRIME_DELETE_TOPIC_FOREVER_DENIED'	=> 'لا تستطيع حذف المواضيع بشكل دائم في هذا المنتدى.',
	'PRIME_DELETE_TOPIC_MIX_NOTICE'		=> 'ملاحظة: أي موضوع معلَّم مسبقا كمحذوف لن يتأثر.',

	// Topics - Deleted
	'PRIME_DELETED_TOPIC_SUCCESS'		=> 'تم تحديد الموضوع المختار كمحذوف بنجاح.',
	'PRIME_DELETED_TOPICS_SUCCESS'		=> 'تم تحديد المواضيع المختارة كمحذوفة بنجاح.',
	'PRIME_DELETED_TOPIC_FAILURE'		=> 'لم يتم تعليم الموضوع المحدد كمحذوف.',
	'PRIME_DELETED_TOPICS_FAILURE'		=> 'لم يتم تعليم المواضيع المحددة كمحذوفة.',

	// Topics - Deleted to Trash Bin
	'PRIME_TRASHED_TOPIC_SUCCESS'		=> 'تم نقل الموضوع المحدد إلى منتدى سلة المحذوفات بنجاح.',
	'PRIME_TRASHED_TOPICS_SUCCESS'		=> 'تم نقل المواضيع المحددة إلى منتدى سلة المحذوفات بنجاح.',
	'PRIME_TRASHED_TOPIC_FAILURE'		=> 'لم يتم نقل الموضوع المحدد إلى منتدى سلة المحذوفات.',
	'PRIME_TRASHED_TOPICS_FAILURE'		=> 'لم يتم نقل المواضيع المحددة إلى منتدى سلة المحذوفات.',
	'PRIME_GO_TO_TRASH_BIN'				=> '%sالذهاب إلى منتدى سلة المحذوفات%s',

	// Topics - Undeleting
	'PRIME_UNDELETE_TOPICS'				=> 'استرجاع',
	'PRIME_UNDELETE_TOPIC_REASON'		=> 'الرجاء إدخال سبب لإلغاء الحذف',
	'PRIME_UNDELETE_TOPIC_CONFIRM'		=> 'هل أنت متأكد أنك تريد استرجاع هذا الموضوع؟',
	'PRIME_UNDELETE_TOPICS_CONFIRM'		=> 'هل أنت متأكد أنك تريد استرجاع هذه المواضيع؟',
	'PRIME_UNDELETE_TOPICS_UNNEEDED'	=> 'لا يمكن استرجاع المواضيع المحددة.',


	// Topics - Undeleted
	'PRIME_UNDELETED_TOPIC_SUCCESS'		=> 'تم استرجاع الموضوع المحدد بنجاح.',
	'PRIME_UNDELETED_TOPICS_SUCCESS'	=> 'تم استرجاع المواضيع المحددة بنجاح.',
	'PRIME_UNDELETED_TOPIC_FAILURE'		=> 'لم يتم استرجاع الموضوع المحدد.',
	'PRIME_UNDELETED_TOPICS_FAILURE'	=> 'لم يتم استرجاع المواضيع المحددة.',

	// Posts - Deleting
	'PRIME_DELETE_POST_REASON'			=> 'الرجاء إدخال سبب للحذف',
	'PRIME_DELETE_POST_FOREVER'			=> 'حذف هذه المشاركة بشكل دائم',
	'PRIME_DELETE_POSTS_FOREVER'		=> 'حذف هذه المشاركات بشكل دائم',
	'PRIME_DELETE_POST_FOREVER_DENIED'	=> 'لا تستطيع حذف المشاركات بشكل دائم في هذا المنتدى.',
	'PRIME_DELETE_POST_MIX_NOTICE'		=> 'ملاحظة: أي مشاركة معلَّمة مسبقا كمحذوفة لن تتأثر.',

	// Posts - Deleted
	'PRIME_DELETED_POST_SUCCESS'		=> 'تم تحديد المشاركة المختارة كمحذوفة بنجاح.',
	'PRIME_DELETED_POSTS_SUCCESS'		=> 'تم تحديد المشاركات المختارة كمحذوفة بنجاح.',
	'PRIME_DELETED_POST_FAILURE'		=> 'لم يتم تعليم المشاركة المحددة كمحذوفة.',
	'PRIME_DELETED_POSTS_FAILURE'		=> 'لم يتم تعليم المشاركات المحددة كمحذوفة.',

	// Posts - Undeleting
	'PRIME_UNDELETE_POST'				=> 'استرجاع المشاركة',
	'PRIME_UNDELETE_POSTS'				=> 'استرجاع المشاركات',
	'PRIME_UNDELETE_POST_REASON'		=> 'الرجاء إدخال سبب لإلغاء الحذف',
	'PRIME_UNDELETE_POST_CONFIRM'		=> 'هل أنت متأكد أنك تريد استرجاع هذه المشاركة؟',
	'PRIME_UNDELETE_POSTS_CONFIRM'		=> 'هل أنت متأكد أنك تريد استرجاع هذه المشاركات',
	'PRIME_UNDELETE_POSTS_UNNEEDED'		=> 'لا يمكن استرجاع المشاركاتالمحددة.',

	// Posts - Undeleted
	'PRIME_UNDELETED_POST_SUCCESS'		=> 'تم استرجاع المشاركة المحددة بنجاح.',
	'PRIME_UNDELETED_POSTS_SUCCESS'		=> 'تم استرجاع المشاركات المحددة بنجاح.',
	'PRIME_UNDELETED_POST_FAILURE'		=> 'لم يتم استرجاع المشاركة المحددة.',
	'PRIME_UNDELETED_POSTS_FAILURE'		=> 'لم يتم استرجاع المشاركات المحددة.',

));

?>