<?php
/**
*
* prime_trash_bin_b [French]
*
* @package language
* @version $Id: prime_trash_bin_b.php,v 0.0.0 2007/07/30 22:30:00 primehalo Exp $
* @copyright (c) 2007 Ken F. Innes IV
* @translation 2007 French by cotp (Baptiste Caraux)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// � � � � �
//

//Logs
$lang = array_merge($lang, array(
	// Ecrire par dessus
	'LOG_TOPIC_DELETED'		=> '<strong>Sujet supprim� totalement</strong><br />� %s',
	'LOG_DELETE_TOPIC'		=> '<strong>Sujet effac�</strong><br />� %s',
	'LOG_DELETE_POST'		=> '<strong>Post effac�</strong><br />� %s',

	// Nouveaux
	'LOG_TOPIC_STIFLED'		=> '<strong>Sujet effac�</strong><br />� %1$s',
	'LOG_TOPIC_TRASHED'		=> '<strong>Sujet effac� vers la Poubelle</strong><br />� %1$s',
	'LOG_TOPIC_UNSTIFLED'	=> '<strong>Sujet restaur�</strong><br />� %1$s',

	'LOG_POST_STIFLED'		=> '<strong>Post effac�</strong><br />� %1$s',
	'LOG_POST_TRASHED'		=> '<strong>Post effac� vers la Poubelle</strong><br />� %1$s',
	'LOG_POST_UNSTIFLED'	=> '<strong>Post restaur�</strong><br />� %1$s',
));

$lang = array_merge($lang, array(
	'LOG_TOPIC_STIFLED_2'	=> $lang['LOG_TOPIC_STIFLED'] . '<br />� � <strong>Raison:</strong> %2$s',
	'LOG_TOPIC_TRASHED_2'	=> $lang['LOG_TOPIC_TRASHED'] . '<br />� � <strong>Raison:</strong> %2$s',
	'LOG_TOPIC_UNSTIFLED_2'	=> $lang['LOG_TOPIC_UNSTIFLED'] . '<br />� � <strong>Raison:</strong> %2$s',

	'LOG_POST_STIFLED_2'	=> $lang['LOG_POST_STIFLED'] . '<br />� � <strong>Raison:</strong> %2$s',
	'LOG_POST_TRASHED_2'	=> $lang['LOG_POST_TRASHED'] . '<br />� � <strong>Raison:</strong> %2$s',
	'LOG_POST_UNSTIFLED_2'	=> $lang['LOG_POST_UNSTIFLED'] . '<br />� � <strong>Raison:</strong> %2$s',
));


// Administration
$lang = array_merge($lang, array(
	'PRIME_FAKE_DELETE'					=> 'Suppression des sujets',
	'PRIME_FAKE_DELETE_EXPLAIN'			=> 'Comment les sujets seront-ils effac�s?',
	'PRIME_FAKE_DELETE_DISABLE'			=> 'Supprimer totalement les sujets du forum',
	'PRIME_FAKE_DELETE_ENABLE'			=> 'Conserver les sujets et les marquer effac�s',
	'PRIME_FAKE_DELETE_AUTO_TRASH'		=> 'D�placer le sujet � la Poubelle',
	'PRIME_FAKE_DELETE_SHADOW_ON'		=> 'D�placer le sujet � la Poubelle avec un traceur',
	'PRIME_FAKE_DELETE_SHADOW_OFF'		=> 'D�placer le sujet � la Poubelle sans traceur',

	'PRIME_TRASH_FORUM'					=> 'Forum de la Poubelle',
	'PRIME_TRASH_FORUM_EXPLAIN'			=> 'Si choisi, effacer un sujet le d�placera dans ce forum. Effacer un sujet de la Poubelle le supprimera compl�tement.',
	'PRIME_TRASH_FORUM_DISABLE'			=> 'Ne pas utiliser de poubelle',
	'PRIME_TRASH_FORUM_DIVIDER'			=> '---------------------------',
	'PRIME_NO_TRASH_FORUM_ERROR'		=> 'Vous devez choisir une Poubelle quand vous s�l�ctionnez l\'option "%s" ',

	'PRIME_FOREVER_WHEN_DELETE_USER'	=> 'Supprimer totalement les posts',
));

// Moderation
$lang = array_merge($lang, array(
	// Sujets - Suppression totale
	'PRIME_DELETE_TOPIC_REASON'			=> 'Merci d\'ecrire pourquoi vous effacez ce sujet',
	'PRIME_DELETE_TOPIC_FOREVER'		=> 'Supprimer totalement ce sujet du forum',
	'PRIME_DELETE_TOPICS_FOREVER'		=> 'Supprimer totalement ces sujets du forum',
	'PRIME_DELETE_TO_TRASH_BIN'			=> 'D�placer le sujet � la Poubelle',
	'PRIME_DELETE_TOPIC_FOREVER_DENIED'	=> 'Vous n\'avez pas les autorisations requises pour supprimer des sujets de ce forum.',
	'PRIME_DELETE_TOPIC_MIX_NOTICE'		=> 'NB: Tous les sujets d�j� marqu�s comme effac�s ne seront pas affect�s.',

	// Sujets - Effac�s
	'PRIME_DELETED_TOPIC_SUCCESS'		=> 'Le sujet s�l�ctionn� a bien �t� marqu� effac�.',
	'PRIME_DELETED_TOPICS_SUCCESS'		=> 'Les sujets s�l�ctionn�s ont bien �t� marqu�s effac�s.',
	'PRIME_DELETED_TOPIC_FAILURE'		=> 'Le sujet s�l�ctionn� N\'A PAS �t� marqu� effac�.',
	'PRIME_DELETED_TOPICS_FAILURE'		=> 'Les sujets s�l�ctionn�s N\'ONT pas �t� marqu�s effac�s.',

	// Sujets - Effac�s vers la poubelle
	'PRIME_TRASHED_TOPIC_SUCCESS'		=> 'Le sujet s�l�ctionn� a bien �t� d�plac� dans la Poubelle.',
	'PRIME_TRASHED_TOPICS_SUCCESS'		=> 'Les sujets s�l�ctionn�s ont bien �t� d�plac�s dans la Poubelle.',
	'PRIME_TRASHED_TOPIC_FAILURE'		=> 'The selected topic was NOT moved to the Trash Bin forum.',
	'PRIME_TRASHED_TOPICS_FAILURE'		=> 'The selected topics were NOT moved to the Trash Bin forum.',
	'PRIME_GO_TO_TRASH_BIN'				=> '%sAller au forum Poubelle%s',

	// Sujets - Restauration
	'PRIME_UNDELETE_TOPICS'				=> 'Restaurer',
	'PRIME_UNDELETE_TOPIC_REASON'		=> 'Merci d\'ecrire pourquoi vous restaurez ce sujet',
	'PRIME_UNDELETE_TOPIC_CONFIRM'		=> 'Etes vous s�r de vouloir restaurer ce sujet?',
	'PRIME_UNDELETE_TOPICS_CONFIRM'		=> 'Etes vous s�r de vouloir restaurer ces sujets?',
	'PRIME_UNDELETE_TOPICS_UNNEEDED'	=> 'Les sujets s�l�ctionn�s ne peuvent pas �tre restaur�s.',


	// Sujets - Restaur�s
	'PRIME_UNDELETED_TOPIC_SUCCESS'		=> 'Le sujet s�l�ctionn� a bien �t� restaur�.',
	'PRIME_UNDELETED_TOPICS_SUCCESS'	=> 'Les sujets s�l�ctionn�s ont bien �t� restaur�s.',
	'PRIME_UNDELETED_TOPIC_FAILURE'		=> 'Le sujet s�l�ctionn� N\'A PAS �t� restaur�.',
	'PRIME_UNDELETED_TOPICS_FAILURE'	=> 'Les sujets s�l�ctionn�s N\'ONT pas �t� restaur�s.',

	// Posts - Effacement
	'PRIME_DELETE_POST_REASON'			=> 'Merci d\'ecrire pourquoi vous effacez ce post',
	'PRIME_DELETE_POST_FOREVER'			=> 'Supprimer totalement ce post du forum',
	'PRIME_DELETE_POSTS_FOREVER'		=> 'Supprimer totalement ced posts du forum',
	'PRIME_DELETE_POST_FOREVER_DENIED'	=> 'Vous n\'avez pas les autorisations requises pour supprimer des posts de ce forum.',
	'PRIME_DELETE_POST_MIX_NOTICE'		=> 'NB: Tous les posts d�j� marqu�s comme effac�s ne seront pas affect�s.',

	// Posts - Effac�s
	'PRIME_DELETED_POST_SUCCESS'		=> 'Le post s�l�ctionn� a bien �t� marqu� effac�.',
	'PRIME_DELETED_POSTS_SUCCESS'		=> 'Les posts s�l�ctionn�s ont bien �t� marqu�s effac�s.',
	'PRIME_DELETED_POST_FAILURE'		=> 'Le post s�l�ctionn� N\'A PAS �t� marqu� effac�.',
	'PRIME_DELETED_POSTS_FAILURE'		=> 'Les posts s�l�ctionn�s N\'ONT pas �t� marqu�s effac�s.',

	// Posts - Restauration
	'PRIME_UNDELETE_POST'				=> 'Restaurer ce post',
	'PRIME_UNDELETE_POSTS'				=> 'Restaurer ces post',
	'PRIME_UNDELETE_POST_REASON'		=> 'Merci d\'ecrire pourquoi vous restaurez ce post',
	'PRIME_UNDELETE_POST_CONFIRM'		=> 'Etes vous s�r de vouloir restaurer ce post?',
	'PRIME_UNDELETE_POSTS_CONFIRM'		=> 'Etes vous s�r de vouloir restaurer ces posts?',
	'PRIME_UNDELETE_POSTS_UNNEEDED'		=> 'Les posts s�l�ctionn�s ne peuvent pas �tre restaur�s.',

	// Posts - Restaur�s
	'PRIME_UNDELETED_POST_SUCCESS'		=> 'Le post s�l�ctionn� a bien �t� restaur�.',
	'PRIME_UNDELETED_POSTS_SUCCESS'		=> 'Les posts s�l�ctionn�s ont bien �t� restaur�s.',
	'PRIME_UNDELETED_POST_FAILURE'		=> 'Le post s�l�ctionn� N\'A PAS �t� restaur�.',
	'PRIME_UNDELETED_POSTS_FAILURE'		=> 'Les posts s�l�ctionn�s N\'ONT pas �t� restaur�s.',
));

?>