<?php
/**
*
* prime_trash_bin_b [German]
*
* @package language
* @version $Id: prime_trash_bin_b.php,v 0.0.0 2007/07/30 22:30:00 primehalo Exp $
* @copyright (c) 2007 Ken F. Innes IV
* @translation 2007 German by Thilak
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

//Logs
$lang = array_merge($lang, array(
	// Overwrite
	'LOG_TOPIC_DELETED'		=> '<strong>Endgültig gelöschtes Thema</strong><br />» %s',
	'LOG_DELETE_TOPIC'		=> '<strong>Endgültig gelöschter Beitrag</strong><br />» %s',
	'LOG_DELETE_POST'		=> '<strong>Endgültig gelöschter Beitrag</strong><br />» %s',
	
	// New
	'LOG_TOPIC_STIFLED'		=> '<strong>Gelöschtes Thema</strong><br />» %1$s',
	'LOG_TOPIC_TRASHED'		=> '<strong>In den Papierkorb gelöschtes Thema</strong><br />» %1$s',
	'LOG_TOPIC_UNSTIFLED'	=> '<strong>Wiederhergestelltes Thema</strong><br />» %1$s',
	
	'LOG_POST_STIFLED'		=> '<strong>Gelöschter Beitrag</strong><br />» %1$s',
	'LOG_POST_TRASHED'		=> '<strong>In den Papierkorb gelöschter Beitrag</strong><br />» %1$s',
	'LOG_POST_UNSTIFLED'	=> '<strong>Wiederhergestellter Beitrag</strong><br />» %1$s',
));

$lang = array_merge($lang, array(
	'LOG_TOPIC_STIFLED_2'	=> $lang['LOG_TOPIC_STIFLED'] . '<br />» » <strong>Grund:</strong> %2$s',
	'LOG_TOPIC_TRASHED_2'	=> $lang['LOG_TOPIC_TRASHED'] . '<br />» » <strong>Grund:</strong> %2$s',
	'LOG_TOPIC_UNSTIFLED_2'	=> $lang['LOG_TOPIC_UNSTIFLED'] . '<br />» » <strong>Grund:</strong> %2$s',
	
	'LOG_POST_STIFLED_2'	=> $lang['LOG_POST_STIFLED'] . '<br />» » <strong>Grund:</strong> %2$s',
	'LOG_POST_TRASHED_2'	=> $lang['LOG_POST_TRASHED'] . '<br />» » <strong>Grund:</strong> %2$s',
	'LOG_POST_UNSTIFLED_2'	=> $lang['LOG_POST_UNSTIFLED'] . '<br />» » <strong>Grund:</strong> %2$s',
));


// Administration
$lang = array_merge($lang, array(
	'PRIME_FAKE_DELETE'					=> 'Themen Löschen',
	'PRIME_FAKE_DELETE_EXPLAIN'			=> 'Legt fest, wie Themen gelöscht werden.',
	'PRIME_FAKE_DELETE_DISABLE'			=> 'Thema endgültig löschen',
	'PRIME_FAKE_DELETE_ENABLE'			=> 'Thema behalten und als gelöscht markieren',
	'PRIME_FAKE_DELETE_AUTO_TRASH'		=> 'In Papierkorb verschieben',
	'PRIME_FAKE_DELETE_SHADOW_ON'		=> 'In Papierkorb verschieben und einen Link im Forum behalten',
	'PRIME_FAKE_DELETE_SHADOW_OFF'		=> 'In Papierkorb verschieben ohne Link im Forum',

	'PRIME_TRASH_FORUM'					=> 'Papierkorb',
	'PRIME_TRASH_FORUM_EXPLAIN'			=> 'Falls markiert, werden gelöschte Themen in dieses Forum verschoben. Löschen von Themen im Papierkorb entfernt diese endgültig.',
	'PRIME_TRASH_FORUM_DISABLE'			=> 'Keinen Papierkorb verwenden',
	'PRIME_TRASH_FORUM_DIVIDER'			=> '---------------------------',
	'PRIME_NO_TRASH_FORUM_ERROR'		=> 'Du musst ein Papierkorb-Forum festlegen, um die "%s" Option zu verwenden',

	'PRIME_FOREVER_WHEN_DELETE_USER'	=> 'Dauerhaft löschen',
));

// Moderation
$lang = array_merge($lang, array(

	// Topics - Deleting
	'PRIME_DELETE_TOPIC_REASON'			=> 'Bitte einen Grund für die Löschung angeben',
	'PRIME_DELETE_TOPIC_FOREVER'		=> 'Dieses Thema endgültig löschen',
	'PRIME_DELETE_TOPICS_FOREVER'		=> 'Diese Themen endgültig löschen',
	'PRIME_DELETE_TO_TRASH_BIN'			=> 'Dieses Thema ins Papierkorb-Forum verschieben',
	'PRIME_DELETE_TOPIC_FOREVER_DENIED'	=> 'In diesem Forum kannst du keine Themen endgültig löschen.',
	'PRIME_DELETE_TOPIC_MIX_NOTICE'		=> 'Achtung: Das hat keine Auswirkung auf Themen, die schon als gelöscht angezeigt werden.',

	// Topics - Deleted
	'PRIME_DELETED_TOPIC_SUCCESS'		=> 'Das ausgewählte Thema wurde erfolgreich als gelöscht gekennzeichnet.',
	'PRIME_DELETED_TOPICS_SUCCESS'		=> 'Die ausgewählten Themen wurden erfolgreich als gelöscht gekennzeichnet.',
	'PRIME_DELETED_TOPIC_FAILURE'		=> 'Das ausgewählte Thema wurde erfolgreich als gelöscht gekennzeichnet.',
	'PRIME_DELETED_TOPICS_FAILURE'		=> 'Die ausgewählten Themen wurden erfolgreich als gelöscht gekennzeichnet.',

	// Topics - Deleted to Trash Bin
	'PRIME_TRASHED_TOPIC_SUCCESS'		=> 'Das ausgewählte Thema wurde erfolgreich ins Papierkorb-Forum verschoben.',
	'PRIME_TRASHED_TOPICS_SUCCESS'		=> 'Die ausgewählten Themen wurden erfolgreich in den Papierkorb-Forum verschoben.',
	'PRIME_TRASHED_TOPIC_FAILURE'		=> 'Das ausgewählte Thema wurde NICHT ins Papierkorb-Forum verschoben.',
	'PRIME_TRASHED_TOPICS_FAILURE'		=> 'Die ausgewählten Themen wurde NICHT ins Papierkorb-Forum verschoben.',
	'PRIME_GO_TO_TRASH_BIN'				=> '%sIns Papierkorb-Forum%s',

	// Topics - Undeleting
	'PRIME_UNDELETE_TOPICS'				=> 'Wiederherstellen',
	'PRIME_UNDELETE_TOPIC_REASON'		=> 'Bitte einen Grund für die Wiederherstellung angeben',
	'PRIME_UNDELETE_TOPIC_CONFIRM'		=> 'Sicher? Das Thema wiederherstellen ?',
	'PRIME_UNDELETE_TOPICS_CONFIRM'		=> 'Sicher? Die Themen wiederherstellen ??',
	'PRIME_UNDELETE_TOPICS_UNNEEDED'	=> 'Die ausgewählten Themen können nicht wiederhergestellt werden.',


	// Topics - Undeleted
	'PRIME_UNDELETED_TOPIC_SUCCESS'		=> 'Das ausgewählte Thema wurde erfolgreich wiederhergestellt.',
	'PRIME_UNDELETED_TOPICS_SUCCESS'	=> 'Die ausgewählte Themen wurde erfolgreich wiederhergestellt.',
	'PRIME_UNDELETED_TOPIC_FAILURE'		=> 'Das Thema wurde NICHT wiederhergestellt.',
	'PRIME_UNDELETED_TOPICS_FAILURE'	=> 'Die Themen wurden NICHT wiederhergestellt.',

	// Posts - Deleting
	'PRIME_DELETE_POST_REASON'			=> 'Bitte einen Grund für das Löschen angeben',
	'PRIME_DELETE_POST_FOREVER'			=> 'Beitrag endgültig löschen',
	'PRIME_DELETE_POSTS_FOREVER'		=> 'Beiträge endgültig löschen',
	'PRIME_DELETE_POST_FOREVER_DENIED'	=> 'Du kannst keine Beiträge in diesem Forum endgültig löschen.',
	'PRIME_DELETE_POST_MIX_NOTICE'		=> 'Achtung ! Das hat keine Auswirkung auf Beiträge, die schon als gelöscht angezeigt werden.',

	// Posts - Deleted
	'PRIME_DELETED_POST_SUCCESS'		=> 'Der ausgewählte Beitrag wurde erfolgreich als gelöscht gekennzeichnet.',
	'PRIME_DELETED_POSTS_SUCCESS'		=> 'Die ausgewählten Beiträge wurde erfolgreich als gelöscht gekennzeichnet.',
	'PRIME_DELETED_POST_FAILURE'		=> 'Der ausgewählte Beitrag wurde NICHT als gelöscht gekennzeichnet.',
	'PRIME_DELETED_POSTS_FAILURE'		=> 'Die ausgewählten Beiträge wurde NICHT als gelöscht gekennzeichnet.',

	// Posts - Undeleting
	'PRIME_UNDELETE_POST'				=> 'Beitrag wiederherstellen',
	'PRIME_UNDELETE_POSTS'				=> 'Beiträge wiederherstellen',
	'PRIME_UNDELETE_POST_REASON'		=> 'Bitte einen Grund für die Wiederherstellung angeben',
	'PRIME_UNDELETE_POST_CONFIRM'		=> 'Sicher ? Diesen Beitrag wiederherstellen ?',
	'PRIME_UNDELETE_POSTS_CONFIRM'		=> 'Sicher ? Diese Beiträge wiederherstellen ?',
	'PRIME_UNDELETE_POSTS_UNNEEDED'		=> 'Der ausgewählte Beitrag kann nicht wiederhergestellt werden.',

	// Posts - Undeleted
	'PRIME_UNDELETED_POST_SUCCESS'		=> 'Der ausgewählte Beitrag wurde erfolgreich wiederhergestellt.',
	'PRIME_UNDELETED_POSTS_SUCCESS'		=> 'Die ausgewählten Beiträge wurden erfolgreich wiederhergestellt.',
	'PRIME_UNDELETED_POST_FAILURE'		=> 'Der ausgewählte Beitrag wurde NICHT wiederhergestellt.',
	'PRIME_UNDELETED_POSTS_FAILURE'		=> 'Die ausgewählten Beiträge wurden NICHT wiederhergestellt.',
));

?>