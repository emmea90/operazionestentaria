<?php
/**
*
* prime_trash_bin_b [Ukrainian]
*
* @package language
* @version $Id: prime_trash_bin_b.php,v 0.0.0 2007/07/30 22:30:00 primehalo Exp $
* @copyright (c) 2007 Ken F. Innes IV
* @translation 2009 Ukrainian by Ridna (http://ridna.com)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

//Logs
$lang = array_merge($lang, array(
	// Overwrite
	'LOG_TOPIC_DELETED'		=> '<strong>Видалена тема</strong><br />» %s',
	'LOG_DELETE_TOPIC'		=> '<strong>Видалена тема</strong><br />» %s',
	'LOG_DELETE_POST'		=> '<strong>Видалено допис</strong><br />» %s',

	// New
	'LOG_TOPIC_STIFLED'		=> '<strong>Видалена тема</strong><br />» %1$s',
	'LOG_TOPIC_TRASHED'		=> '<strong>Тема переміщена у смітник</strong><br />» %1$s',
	'LOG_TOPIC_UNSTIFLED'	=> '<strong>Тему відновлено</strong><br />» %1$s',

	'LOG_POST_STIFLED'		=> '<strong>Допис видалений</strong><br />» %1$s',
	'LOG_POST_TRASHED'		=> '<strong>Допис був переміщений у смітник</strong><br />» %1$s',
	'LOG_POST_UNSTIFLED'	=> '<strong>Допис відновлено</strong><br />» %1$s',
));

$lang = array_merge($lang, array(
	'LOG_TOPIC_STIFLED_2'	=> $lang['LOG_TOPIC_STIFLED'] . '<br />» » <strong>Причина:</strong> %2$s',
	'LOG_TOPIC_TRASHED_2'	=> $lang['LOG_TOPIC_TRASHED'] . '<br />» » <strong>Причина:</strong> %2$s',
	'LOG_TOPIC_UNSTIFLED_2'	=> $lang['LOG_TOPIC_UNSTIFLED'] . '<br />» » <strong>Причина:</strong> %2$s',

	'LOG_POST_STIFLED_2'	=> $lang['LOG_POST_STIFLED'] . '<br />» » <strong>Причина:</strong> %2$s',
	'LOG_POST_TRASHED_2'	=> $lang['LOG_POST_TRASHED'] . '<br />» » <strong>Причина:</strong> %2$s',
	'LOG_POST_UNSTIFLED_2'	=> $lang['LOG_POST_UNSTIFLED'] . '<br />» » <strong>Причина:</strong> %2$s',
));


// Administration
$lang = array_merge($lang, array(
	'PRIME_FAKE_DELETE'					=> 'Видалення теми',
	'PRIME_FAKE_DELETE_EXPLAIN'			=> 'Визначає принцип видалення тем на форумі.',
	'PRIME_FAKE_DELETE_DISABLE'			=> 'Безповоротно знищувати теми',
	'PRIME_FAKE_DELETE_ENABLE'			=> 'Помічати теми як видалені',
	'PRIME_FAKE_DELETE_AUTO_TRASH'		=> 'Переміщати теми у смітник',
	'PRIME_FAKE_DELETE_SHADOW_ON'		=> 'Переміщати тему у смітник з можливістю залишити ланку на неї',
	'PRIME_FAKE_DELETE_SHADOW_OFF'		=> 'Переміщати тему у смітник без можливості залишити ланку на неї',

	'PRIME_TRASH_FORUM'					=> 'Форум смітника',
	'PRIME_TRASH_FORUM_EXPLAIN'			=> 'Якщо вибрано, то видалена гілка буде переміщена у смітник, а дописи будуть приховані від перегляду. Видалення гілки зі смітника призведе до повного знищення без можливості відновлення.',
	'PRIME_TRASH_FORUM_DISABLE'			=> 'Не використовувати смітник',
	'PRIME_TRASH_FORUM_DIVIDER'			=> '---------------------------',
	'PRIME_NO_TRASH_FORUM_ERROR'		=> 'Ви повинні налаштувати смітник перш ніж вибрати "%s" опцію',

	'PRIME_FOREVER_WHEN_DELETE_USER'	=> 'Назавжди знищити дописи (якщо знищити учасника)',
));

// Moderation
$lang = array_merge($lang, array(

	// Topics - Deleting
	'PRIME_DELETE_TOPIC_REASON'			=> 'Вкажіть причину видалення',
	'PRIME_DELETE_TOPIC_FOREVER'		=> 'Знищити тему без можливості відновлення',
	'PRIME_DELETE_TOPICS_FOREVER'		=> 'Всі теми знищити назовсім!?',
	'PRIME_DELETE_TO_TRASH_BIN'			=> 'Перекласти тему у смітник',
	'PRIME_DELETE_TOPIC_FOREVER_DENIED'	=> 'Ви не можете знищувати теми в цьому форумі.',
	'PRIME_DELETE_TOPIC_MIX_NOTICE'		=> 'Інформація: Теми, котрі позначені як видалені, не будуть змінені.',

	// Topics - Deleted
	'PRIME_DELETED_TOPIC_SUCCESS'		=> 'Вибрана тема була успішно позначена як видалена.',
	'PRIME_DELETED_TOPICS_SUCCESS'		=> 'Вибрані теми були успішно позначені як видалені.',
	'PRIME_DELETED_TOPIC_FAILURE'		=> 'Вибрана тема НЕ була позначена як видалена.',
	'PRIME_DELETED_TOPICS_FAILURE'		=> 'Вибрані теми НЕ були позначені як видалені.',

	// Topics - Deleted to Trash Bin
	'PRIME_TRASHED_TOPIC_SUCCESS'		=> 'Вибрана тема була успішно переміщена у смітник.',
	'PRIME_TRASHED_TOPICS_SUCCESS'		=> 'Вибрані теми були успішно переміщені у смітник.',
	'PRIME_TRASHED_TOPIC_FAILURE'		=> 'Вибрана тема НЕ була переміщена у смітник.',
	'PRIME_TRASHED_TOPICS_FAILURE'		=> 'Вибрані теми НЕ були переміщені у смітник.',
	'PRIME_GO_TO_TRASH_BIN'				=> '%sПерейти у смітник%s',

	// Topics - Undeleting
	'PRIME_UNDELETE_TOPICS'				=> 'Відновити',
	'PRIME_UNDELETE_TOPIC_REASON'		=> 'Вкажіть причину відновлення',
	'PRIME_UNDELETE_TOPIC_CONFIRM'		=> 'Ви впевнені, що вибрану тему слід відновити?',
	'PRIME_UNDELETE_TOPICS_CONFIRM'		=> 'Ви впевнені, цо вибрані теми слід відновити?',
	'PRIME_UNDELETE_TOPICS_UNNEEDED'	=> 'Вибрані теми НЕ були відновлені',


	// Topics - Undeleted
	'PRIME_UNDELETED_TOPIC_SUCCESS'		=> 'Вибрана тема була успішно відновлена.',
	'PRIME_UNDELETED_TOPICS_SUCCESS'	=> 'Вибрані теми були успішно відновлені.',
	'PRIME_UNDELETED_TOPIC_FAILURE'		=> 'Вибрана тема НЕ була відновлена.',
	'PRIME_UNDELETED_TOPICS_FAILURE'	=> 'Вибрані теми НЕ були відновлені.',

	// Posts - Deleting
	'PRIME_DELETE_POST_REASON'			=> 'Вкажіть причину видалення',
	'PRIME_DELETE_POST_FOREVER'			=> 'Допис зовсім знищити',
	'PRIME_DELETE_POSTS_FOREVER'		=> 'Дописи знищити зовсім',
	'PRIME_DELETE_POST_FOREVER_DENIED'	=> 'Ви не можете видаляти дописи в цьому форумі.',
	'PRIME_DELETE_POST_MIX_NOTICE'		=> 'Інформація: Всі дописи, котрі позначені як видалені, не будуть змінені.',

	// Posts - Deleted
	'PRIME_DELETED_POST_SUCCESS'		=> 'Допис був успішно позначений як видалений.',
	'PRIME_DELETED_POSTS_SUCCESS'		=> 'Дописи були успішно позначені як видалені.',
	'PRIME_DELETED_POST_FAILURE'		=> 'Допис НЕ був позначений як видалений.',
	'PRIME_DELETED_POSTS_FAILURE'		=> 'Дописи НЕ були позначені як видалені.',

	// Posts - Undeleting
	'PRIME_UNDELETE_POST'				=> 'Відновити допис',
	'PRIME_UNDELETE_POSTS'				=> 'Відновити дописи',
	'PRIME_UNDELETE_POST_REASON'		=> 'Будь ласка, вкажіть причину відновлення',
	'PRIME_UNDELETE_POST_CONFIRM'		=> 'Ви впевнені, що слід відновити допис?',
	'PRIME_UNDELETE_POSTS_CONFIRM'		=> 'Ви впевнені, що слід відновити дописи?',
	'PRIME_UNDELETE_POSTS_UNNEEDED'		=> 'Допис НЕ може бути відновленим.',

	// Posts - Undeleted
	'PRIME_UNDELETED_POST_SUCCESS'		=> 'Допис був успішно відновлений.',
	'PRIME_UNDELETED_POSTS_SUCCESS'		=> 'Дописи були успішно відновлені.',
	'PRIME_UNDELETED_POST_FAILURE'		=> 'Допис НЕ був відновлений.',
	'PRIME_UNDELETED_POSTS_FAILURE'		=> 'Дописи НЕ були відновлені.',

));

?>