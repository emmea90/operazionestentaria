<?php
/**
*
* prime_trash_bin_b [Hebrew]
*
* @package language
* @version $Id: prime_trash_bin_b.php,v 1.0.5 2008/08/23 17:01:00 primehalo Exp $
* @copyright (c) 2007 Ken F. Innes IV
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

//Logs
$lang = array_merge($lang, array(
	// Overwrite
	'LOG_TOPIC_DELETED'		=> '<strong>הסיר נושא לצמיתות</strong><br />» %s',
	'LOG_DELETE_TOPIC'		=> '<strong>הנושא הוסר לצמיתות</strong><br />» %s',
	'LOG_DELETE_POST'		=> '<strong>הסיר הודעה לצמיתות</strong><br />» %s',

	// New
	'LOG_TOPIC_STIFLED'		=> '<strong>הסיר נושא</strong><br />» %1$s',
	'LOG_TOPIC_TRASHED'		=> '<strong>הסיר נושא לתיבת האשפה</strong><br />» %1$s',
	'LOG_TOPIC_UNSTIFLED'	=> '<strong>ביטל מחיקת נושא</strong><br />» %1$s',

	'LOG_POST_STIFLED'		=> '<strong>הסיר הודעה זו</strong><br />» %1$s',
	'LOG_POST_TRASHED'		=> '<strong>הסיר הודעה לתיבת האשפה</strong><br />» %1$s',
	'LOG_POST_UNSTIFLED'	=> '<strong>ביטל מחיקת הודעה</strong><br />» %1$s',
));

$lang = array_merge($lang, array(
	'LOG_TOPIC_STIFLED_2'	=> $lang['LOG_TOPIC_STIFLED'] . '<br />» » <strong>סיבה:</strong> %2$s',
	'LOG_TOPIC_TRASHED_2'	=> $lang['LOG_TOPIC_TRASHED'] . '<br />» » <strong>סיבה:</strong> %2$s',
	'LOG_TOPIC_UNSTIFLED_2'	=> $lang['LOG_TOPIC_UNSTIFLED'] . '<br />» » <strong>סיבה:</strong> %2$s',

	'LOG_POST_STIFLED_2'	=> $lang['LOG_POST_STIFLED'] . '<br />» » <strong>סיבה:</strong> %2$s',
	'LOG_POST_TRASHED_2'	=> $lang['LOG_POST_TRASHED'] . '<br />» » <strong>סיבה:</strong> %2$s',
	'LOG_POST_UNSTIFLED_2'	=> $lang['LOG_POST_UNSTIFLED'] . '<br />» » <strong>סיבה:</strong> %2$s',
));


// Administration
$lang = array_merge($lang, array(
	'PRIME_FAKE_DELETE'					=> 'מחיקת נושא',
	'PRIME_FAKE_DELETE_EXPLAIN'			=> 'קבע איך הנושא ימחק.',
	'PRIME_FAKE_DELETE_DISABLE'			=> 'מחיקת נושא לצמיתות',
	'PRIME_FAKE_DELETE_ENABLE'			=> 'שמור זה נושא וסמן אותו כמחוק',
	'PRIME_FAKE_DELETE_AUTO_TRASH'		=> 'העבר נושא לתיבת האשפה',
	'PRIME_FAKE_DELETE_SHADOW_ON'		=> 'העבר נושא עם עקבות',
	'PRIME_FAKE_DELETE_SHADOW_OFF'		=> 'העבר נושא ללא עקיבות',

	'PRIME_TRASH_FORUM'					=> 'פורום תיבת אשפה',
	'PRIME_TRASH_FORUM_EXPLAIN'			=> 'אם תסמן אפשרות זו, נושאים מחוקים יועברו לפורום זה. מחיקת נושא מתיבת האשפה יוסרו לצמיתות.',
	'PRIME_TRASH_FORUM_DISABLE'			=> 'אל תשתמש בתיבת האשפה',
	'PRIME_TRASH_FORUM_DIVIDER'			=> '---------------------------',
	'PRIME_NO_TRASH_FORUM_ERROR'		=> 'אתה חייב לבחור פורום תיבת אשפה כאשר אתה מסמן את האפשרות "%s"',

	'PRIME_FOREVER_WHEN_DELETE_USER'	=> 'הסר הודעות לצמיתות',
));

// Moderation
$lang = array_merge($lang, array(

	// Topics - Deleting
	'PRIME_DELETE_TOPIC_REASON'			=> 'אנא ציין סיבה למחיקה',
	'PRIME_DELETE_TOPIC_FOREVER'		=> 'הסר נושא זה לצמיתות',
	'PRIME_DELETE_TOPICS_FOREVER'		=> 'הסר נושאים אלה לצמיתות',
	'PRIME_DELETE_TO_TRASH_BIN'			=> 'העבר נושא לפורום תיבת האשפה',
	'PRIME_DELETE_TOPIC_FOREVER_DENIED'	=> 'אינך רשאי להסיר נושאים לצמיתות בפורום זה.',
	'PRIME_DELETE_TOPIC_MIX_NOTICE'		=> 'הערה: כל הנושאים שכבר מסומנים לא יושפעו.',

	// Topics - Deleted
	'PRIME_DELETED_TOPIC_SUCCESS'		=> 'הנושא שנבחר הוסר בהצלחה.',
	'PRIME_DELETED_TOPICS_SUCCESS'		=> 'הנושאים שנבחרו הוסרו בהצלחה.',
	'PRIME_DELETED_TOPIC_FAILURE'		=> 'הנושא שסומן לא הוסרו.',
	'PRIME_DELETED_TOPICS_FAILURE'		=> 'הנושאים שסומנו לא הוסרו.',

	// Topics - Deleted to Trash Bin
	'PRIME_TRASHED_TOPIC_SUCCESS'		=> 'הנושא שנבחר הועבר בהצלחה לפורום תיבת האשפה.',
	'PRIME_TRASHED_TOPICS_SUCCESS'		=> 'הנושאים שנבחרו הועברו בהצלחה לפורום תיבת האשפה.',
	'PRIME_TRASHED_TOPIC_FAILURE'		=> 'הנושא שנבחר לא הועבר לפורום תיבת האשפה.',
	'PRIME_TRASHED_TOPICS_FAILURE'		=> 'הנושאים שנבחרו לא הועברו לפורום תיבת האשפה.',
	'PRIME_GO_TO_TRASH_BIN'				=> '%sעבור לפורום תיבת האשפה%s',

	// Topics - Undeleting
	'PRIME_UNDELETE_TOPICS'				=> 'בטל מחיקה',
	'PRIME_UNDELETE_TOPIC_REASON'		=> 'אנא ציין סיבה לביטול המחיקה',
	'PRIME_UNDELETE_TOPIC_CONFIRM'		=> 'אתה בטוח שברצונך לבטל מחיקה לנושא זה?',
	'PRIME_UNDELETE_TOPICS_CONFIRM'		=> 'אתה בטוח שברצונך לבטל מחיקת הנושאים האלה?',
	'PRIME_UNDELETE_TOPICS_UNNEEDED'	=> 'אי אפשר לבטל את מחיקת הנושאים שנבחרו.',


	// Topics - Undeleted
	'PRIME_UNDELETED_TOPIC_SUCCESS'		=> 'הנושא שנבחר שוחזר בהצלחה.',
	'PRIME_UNDELETED_TOPICS_SUCCESS'	=> 'הנושאים שנבחרו שוחזרו בהצלחה.',
	'PRIME_UNDELETED_TOPIC_FAILURE'		=> 'הנושא שנבחר לא שוחזר.',
	'PRIME_UNDELETED_TOPICS_FAILURE'	=> 'הנושאים שנבחרו לא שוחזרו.',

	// Posts - Deleting
	'PRIME_DELETE_POST_REASON'			=> 'אנא ציין סיבה למחיקה',
	'PRIME_DELETE_POST_FOREVER'			=> 'הסר הודעה זו לצמיתות',
	'PRIME_DELETE_POSTS_FOREVER'		=> 'הסר את ההודעות האלה לצמיתות',
	'PRIME_DELETE_POST_FOREVER_DENIED'	=> 'אינך רשאי להסיר הודעות לצמיתות בפורום זה.',
	'PRIME_DELETE_POST_MIX_NOTICE'		=> 'הערה: ההודעות שסומנו כבר לא יושפעו.',

	// Posts - Deleted
	'PRIME_DELETED_POST_SUCCESS'		=> 'ההודעה שנבחרה הוסרה בהצלחה.',
	'PRIME_DELETED_POSTS_SUCCESS'		=> 'ההודעות שנבחרו הוסרו בהצלחה.',
	'PRIME_DELETED_POST_FAILURE'		=> 'ההודעה שסומנה לא נמחקה.',
	'PRIME_DELETED_POSTS_FAILURE'		=> 'ההודעות שסומנו לא נמחקו.',

	// Posts - Undeleting
	'PRIME_UNDELETE_POST'				=> 'בטל מחיקת הודעה',
	'PRIME_UNDELETE_POSTS'				=> 'בטל מחיקות הודעות',
	'PRIME_UNDELETE_POST_REASON'		=> 'אנא ציין סיבה לביטול המחיקה',
	'PRIME_UNDELETE_POST_CONFIRM'		=> 'אתה בטוח שברצונך לבטל מחיקה להודעה זו?',
	'PRIME_UNDELETE_POSTS_CONFIRM'		=> 'אתה בטוח שברצונך לבטל מחיקה להודעות האלה?',
	'PRIME_UNDELETE_POSTS_UNNEEDED'		=> 'אי אפשר לבטל מחיקת ההודעות שנבחרו.',

	// Posts - Undeleted
	'PRIME_UNDELETED_POST_SUCCESS'		=> 'ההודעה שנבחרה שוחזרה בהצלחה.',
	'PRIME_UNDELETED_POSTS_SUCCESS'		=> 'ההודעות שנבחרו שוחזרו בהצלחה.',
	'PRIME_UNDELETED_POST_FAILURE'		=> 'ההודעה שנבחרה לא שוחזר.',
	'PRIME_UNDELETED_POSTS_FAILURE'		=> 'ההודעות שנבחרו לא שוחזרו.',

));

?>