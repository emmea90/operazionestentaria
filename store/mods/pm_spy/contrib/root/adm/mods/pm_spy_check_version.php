<?php
/**
*
* @package acp_pm_spy
* @version $Id: 1.0.0
* @copyright (c) 2008 david63
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package pm_spy_check_version
*/
class pm_spy_check_version
{
	function version()
	{
		return array(
			'author'	=> 'david63',
			'title'		=> 'PM Spy',
			'tag'		=> 'pm_spy_check',
			'version'	=> '0.0.1',
			'file'		=> array('mywebworld.org.uk', 'updatecheck', 'pm_spy.xml'),
		);
	}
}

?>