<?php
/** 
*
* @package acp_pm_spy
* @version $Id: 1.0.0
* @copyright (c) 2008 david63
* @license http://opensource.org/licenses/gpl-license.php GNU Public License  
*
*/

/**
* @ignore
*/

define('IN_PHPBB', true);

$phpbb_root_path = './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/info_acp_pm_spy');

if ($user->data['user_type'] != USER_FOUNDER)
{
	trigger_error($user->lang['NO_FOUNDER']);
}

$page_title		= $user->lang['INSTALL_PM_SPY'];
$template_html	= 'message_body.html';

//Setup $auth_admin class so we can add permission options
include($phpbb_root_path . 'includes/acp/auth.' . $phpEx);
$auth_admin = new auth_admin();

// Add the new permissions
$permissions = array(
	'local'		=> array(),
	'global'	=> array('a_pm_spy')
);

$auth_admin->acl_add_option($permissions);

// Add Full Admin role
$role = get_role_by_name('ROLE_ADMIN_FULL');

if ($role)
{
	acl_update_role($role['role_id'], array('a_pm_spy'));
}

// Automatically add the module
require($phpbb_root_path . 'includes/acp/acp_modules.' . $phpEx);
$modules = new acp_modules();
$module_data = array();

$cat_module = 'ACP_CAT_DOT_MODS';

$acp_settings_parent = get_module_id('acp', $cat_module);

if (!$acp_settings_parent)
{
	trigger_error(sprintf($user->lang['CAT_ERROR'], $cat_module), E_USER_ERROR);
}

// Define ACP module category in .MODs
$module_data[] = array('module_enabled' => '1', 'module_display' => '1', 'module_basename' => '', 'parent_id' => $acp_settings_parent, 'module_class' => 'acp', 'module_langname' => 'ACP_PM_SPY', 'module_mode' => '', 'module_auth' => '' );

create_modules($module_data);

$acp_settings_parent = get_module_id('acp', 'ACP_PM_SPY');

//Define ACP Module
$module_data[] = array('module_enabled' => '1', 'module_display' => '1', 'module_basename' => 'pm_spy',  'module_class' => 'acp', 'parent_id' => $acp_settings_parent, 'module_langname' => 'ACP_PM_SPY', 'module_mode' => 'list', 'module_auth' => 'acl_a_pm_spy');

create_modules($module_data);

$cache->purge();

$template->assign_vars(array(
	'MESSAGE_TITLE'	=> $user->lang['INSTALL_PM_SPY'],
	'MESSAGE_TEXT'	=> $user->lang['COMPLETE'],
	)
);

// Output the page
page_header($page_title);

$template->set_filenames(array(
	'body' => $template_html)
);

page_footer();

// The following functions are by poyntesm
// (modified to comply with mod db)
function create_modules($module_data)
{
	global $modules;

	for ($i = 0, $count = sizeof($module_data); $i < $count; $i++)
	{
		$errors = $modules->update_module_data($module_data[$i]);
		if (!sizeof($errors))
		{
			$modules->remove_cache_file();
		}
	}
}

function get_module_id($type, $module)
{
	global $db;

	$sql = 'SELECT module_id
		FROM ' . MODULES_TABLE . '
		WHERE ' . $db->sql_in_set('module_class', $type) . '
			AND ' . $db->sql_in_set('module_langname', $module);
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);

	return $row['module_id'];
}

function get_role_by_name($name)
{
	global $db;

	$sql = 'SELECT *
		FROM ' . ACL_ROLES_TABLE . '
		WHERE ' . $db->sql_in_set('role_name', $name);
	$result = $db->sql_query($sql);
	$data = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	return $data;
}

function acl_update_user($user_id, $auth_options, $auth_setting = ACL_YES)
{
	global $db, $cache, $auth;

	$acl_options_ids = get_acl_option_ids($auth_options);

	$role_options = array();

	$sql = 'SELECT auth_option_id
		FROM ' . ACL_USERS_TABLE . '
		WHERE ' . $db->sql_in_set('user_id', (int) $user_id) . '
		GROUP BY auth_option_id';
	$result = $db->sql_query($sql);

	while ($row = $db->sql_fetchrow($result))
	{
		$user_options[] = $row;
	}
	$db->sql_freeresult($result);

	$sql_ary = array();

	for ($i = 0, $count = sizeof($acl_options_ids); $i < $count; $i++)
	{
		if (in_array($acl_options_ids[$i]['auth_option_id'], $user_options))
		{
			continue;
		}

		$sql_ary[] = array(
			'user_id'			=> (int) $user_id,
			'auth_option_id'	=> (int) $acl_options_ids[$i]['auth_option_id'],
			'auth_setting'      => $auth_setting,
		);
	}

	$db->sql_multi_insert(ACL_USERS_TABLE, $sql_ary);

	$cache->destroy('acl_options');
	$auth->acl_clear_prefetch();
}

function get_acl_option_ids($auth_options)
{
	global $db;

	$data = array();

	$sql = 'SELECT auth_option_id
		FROM ' . ACL_OPTIONS_TABLE . '
		WHERE ' . $db->sql_in_set('auth_option', $auth_options) . '
		GROUP BY auth_option_id';
	$result = $db->sql_query($sql);

	while ($row = $db->sql_fetchrow($result))
	{
		$data[] = $row;
	}
	$db->sql_freeresult($result);

	return $data;
}

function acl_update_role($role_id, $auth_options, $auth_setting = ACL_YES)
{
	global $db, $cache, $auth;

	$acl_options_ids = get_acl_option_ids($auth_options);

	$role_options = array();

	$sql = 'SELECT auth_option_id
		FROM ' . ACL_ROLES_DATA_TABLE . '
		WHERE ' . $db->sql_in_set('role_id', (int) $role_id) . '
		GROUP BY auth_option_id';
	$result = $db->sql_query($sql);

	while ($row = $db->sql_fetchrow($result))
	{
		$role_options[] = $row;
	}
	$db->sql_freeresult($result);

	$sql_ary = array();

	foreach($acl_options_ids as $option)
	{
		if (!in_array($option, $role_options))
		{
			$sql_ary[] = array(
				'role_id'			=> (int) $role_id,
				'auth_option_id'	=> (int) $option['auth_option_id'],
				'auth_setting'      => $auth_setting
			);   
		}
	}

	$db->sql_multi_insert(ACL_ROLES_DATA_TABLE, $sql_ary);

	$cache->destroy('acl_options');
	$auth->acl_clear_prefetch();
}

?>