<?php
/**
*
* acp [English]
*
* @package disclaimer
* @version 1.0.0
* @copyright (c) 2008 david63
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_PM_SPY'			=> 'PM Spy',
	'AUTHOR_IP'				=> 'IP del autor',
	'DATE'					=> 'Fecha',
	'DELETE_PMS'			=> 'Borrar Mensajes Privados',
	'NO_PM_SELECTED'		=> 'No has seleccionado ningún Mensaje Privado',
	'PM_BOX'				=> 'Bandeja de Mensajes Privados',
	'PM_SPY_READ'			=> 'Lista de Mensajes Privados',
	'PM_SPY_READ_EXPLAIN'	=> 'Este es un listado de todos los Mensajes privados de tu foro.',
	'TO'					=> 'A',
	'TOTAL_USERS'			=> 'Usuarios totales',
	'PM_COUNT'				=> 'Contador de Mensajes',

	'INSTALL_NOT_DELETED'	=> 'El archivo de instalación de este mod no ha sido borrado.',

	'PM_HOLDBOX'			=> 'Retenido',	
	'PM_INBOX'				=> 'Bande de entrada',
	'PM_NOBOX'				=> 'Sin bandeja',
	'PM_OUTBOX'				=> 'Bandeja de salida',
	'PM_SAVED'				=> 'Guardado',
	'PM_SENTBOX'			=> 'Enviado',

	'SORT_FROM'				=> 'De',
	'SORT_TO'				=> 'Para',
	'SORT_BCC'				=> 'BCC',
	'SORT_PM_BOX'			=> 'Bandeja de Mensajes Privados',
	
	'LOG_PM_SPY'			=> '<strong>Mensajes Privados han sido borrados por PM Spy</strong><br />',
));

// Install
$lang = array_merge($lang, array(
	'NO_FOUNDER'				=> 'No estás autorizado para realizar esta instalación - Debes de tener rango de Fundador.',
	'INSTALL_PM_SPY'			=> 'Instalando PM Spy Mod',
	'COMPLETE'					=> 'Instalación completa.',
));

?>