<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @package Advanced Rules Page
* @version $Id$
* @copyright (c) 2012 Dugi ( http://imgit.org )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('IN_PHPBB', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);
include($phpbb_root_path . 'includes/functions_user.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/rules');
// End session management

if (!isset($config['enable_board_rules']) || !$config['enable_board_rules'] || !isset($config['advanced_rules_page_version']))
{
	trigger_error($user->lang['BOARD_RULES_DISABLED'] . '<br /><br />' . sprintf($user->lang['RETURN_INDEX'], '<a href="' . append_sid("{$phpbb_root_path}index.$phpEx") . '">', '</a>'));	
}

// Prepare query
$columns = 'c.rule_id, c.rule_title, r.rule_description AS rule_desc, ';
$columns .= 'r.parse_bbcode AS bbcode, ';
$columns .= 'r.parse_links AS links, ';
$columns .= 'r.parse_smilies AS smilies, ';
$columns .= 'r.group_ids AS groups';

$sql_array = array(
	'SELECT'    => $columns,
	'FROM'      => array(RULES_TABLE => 'c'),
	'LEFT_JOIN' => array(
		array(
			'FROM'  => array(RULES_TABLE => 'r'),
			'ON'    => 'r.parent_id = c.rule_id',
		),
	),
	'WHERE'     => 'c.parent_id = 0 AND r.public = 1',
	'ORDER_BY'  => 'c.cat_position, r.rule_position',
);

$sql = $db->sql_build_query('SELECT', $sql_array);
$result = $db->sql_query($sql);

$counter 		= 0;
$cat_count 		= 1;
$alpha_count	= 'abcdefghijklmnopqrstuvwxyz';
$rule_id 		= null;
$public_cats	= array();

while ($row = $db->sql_fetchrow($result))
{
	if ($rule_id != $row['rule_id']) 
	{		
		$group_ids 	= array_map('intval', explode(' ', $row['groups']));
		$is_grouped = false;
		
		// Check if user can see a specific category if he is not an admin
		if (!$auth->acl_get('a_'))
		{
			$is_grouped = (group_memberships($group_ids, $user->data['user_id'], true)) ? true : false;
		}
		else
		{
			$is_grouped = true;	
		}
		
		// Fill $public_cats with boolean values
		if ($is_grouped !== false)
		{
			$public_cats[] = $is_grouped;
		}
				
		$rule_id = $row['rule_id'];
		
		$template->assign_block_vars('rules', array(
			'RULE_CATEGORY' => $row['rule_title'],
			'ROW_COUNT'     => $cat_count,
			'CAN_SEE_CAT'	=> $is_grouped
		));
		
		$cat_count++;
		$counter = 0;
	}
	
	// Reset the counter once we reach 26. 
	// This should never occur, but in case someone changes something in the acp file.
	$counter = ($counter == 26) ? 0 : $counter;
	
	// Prepare rules for displaying
	$uid = $bitfield = $options = '';
	generate_text_for_storage($row['rule_desc'], $uid, $bitfield, $options, $row['bbcode'], $row['links'], $row['smilies']);

	$template->assign_block_vars('rules.rule', array(
		'RULE_DESC'     => generate_text_for_display($row['rule_desc'], $uid, $bitfield, $options),
		'ALPHA_COUNT'   => $alpha_count{$counter}
	));

	$counter++;
}
$db->sql_freeresult($result);

// Assign a template variable
$template->assign_var('S_PUBLIC_CATS', sizeof($public_cats) + ((sizeof($public_cats) % 2 != 0) ? -1 : 0));

// Output the page
page_header($user->lang['BOARD_RULES'], false);
$template->set_filenames(array(
	'body' => 'rules_body.html')
);
page_footer();
?>