<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

//Standard page header
page_header($user->lang['MAPS']);

if(!$auth->acl_get('u_can_access_maps')) {
	trigger_error('NOT_AUTHORISED');
}
		
//template
$template->set_filenames(array(
 'body' => 'maps/maps_home.html'
));

$totaltracks = getTotalTracks();
$totaltours = getTotalTours();
$totalkms = getTotalKms();

$template->assign_vars(array(
	'TOTALTRACKS' => $totaltracks,
	'TOTALTOURS' => $totaltours,
	'TOTALKMS' => $totalkms,
));	

// Last tracks 
if($auth->acl_get('u_can_view_all_tracks')) {
	$result = get_last_tracks();
} else {
	$result = get_last_public_tracks();
}
while ($row = $db->sql_fetchrow($result))
{
	$trackid = $row['trackid'];
	$trackname = $row['trackname'];
	$tracktype = $row['tracktype'];
	$trackdistance = $row['trackdistance'];
	$trackowner = $row['trackowner'];
	$trackdatecreation = $row['trackdatecreation'];
	$trackdatelastedit = $row['trackdatelastedit'];
	$trackdepart = $row['trackdepart'];
	$trackarrive = $row['trackarrive'];
	$trackpublic = $row['trackpublic'];	
	$trackdatecreation = date('d/m/Y - G.i', strtotime($trackdatecreation));	
	$trackdatelastedit = date('d/m/Y - G.i', strtotime($trackdatelastedit));
	
	$userinfo = fetch_user_info($trackowner);
	$userrow = $db->sql_fetchrow($userinfo);
	$usercolour = $userrow['user_colour'];
	$username = $userrow['username'];	

	$typequery = get_type_info($tracktype);
	$typerow = $db->sql_fetchrow($typequery);
	$typelogo = $typerow['typelogo'];			
		
	switch ($tracktype) {
		case '1':
		$typedescr = $user->lang['FLAT'];
		break;
		case '2':
		$typedescr = $user->lang['HILL'];
		break;			
		case '3':
		$typedescr = $user->lang['MOUNTAIN'];
		break;			
		case '4':
		$typedescr = $user->lang['ITT'];
		break;			
		case '5':
		$typedescr = $user->lang['TTT'];			
		break;									
	}
			
	$template->assign_block_vars('othertracks', array(
		'TRACKID' => $trackid,
		'TRACKNAME' => $trackname,
		'TRACKTYPE' => $tracktype,
		'TRACKTYPEDESCR' => $typedescr,
		'TRACKTYPELOGO' => $typelogo,
		'TRACKOWNER' => get_username_string('full', $trackowner, $username, $usercolour, "Ospite"),
		'TRACKDISTANCE' => $trackdistance,
		'TRACKDATECREATION' => $trackdatecreation,
		'TRACKDATELASTEDIT' => $trackdatelastedit,
		'TRACKDEPART' => $trackdepart,
		'TRACKARRIVE' => $trackarrive,
		'TRACKPUBLIC' => $trackpublic,	
		'TRACKVIEWLINK'	=> append_sid($phpbb_root_path.'maps/viewtrack.'.$phpEx, array('trackid' => $trackid)),
		'TRACKEDITLINK'	=> append_sid($phpbb_root_path.'maps/maps.'.$phpEx, array('action' => 'edit', 'track' => $trackid)),		
	));		
}

$template->assign_vars(array(
	'ARROWLOGO' => "./../images/logo/Arrow.gif",
	'CANVIEWALLTRACKS' => $auth->acl_get('u_can_view_all_tracks'),
));	

generatebar();
page_footer();

?>