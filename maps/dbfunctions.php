<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Db Functions
function get_users() {
	global $db;
	$sql = 'SELECT * FROM ' . USERS_TABLE;
	$sql = $sql.' ORDER BY  `username_clean` ASC';
	$result = $db->sql_query($sql);
	return $result;	
}

function get_tours($loadprivate, $page) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;	
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_TABLE;
	if(!$loadprivate) {
		$sql = $sql.' WHERE `tourpublic` = 1';		
	}
	$sql = $sql.' ORDER BY  `tourdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;
	$result = $db->sql_query($sql);
	return $result;	
}

function get_all_tours($loadprivate) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_TABLE;
	if(!$loadprivate) {
		$sql = $sql.' WHERE `tourpublic` = 1';		
	}
	$sql = $sql.' ORDER BY  `tourdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;	
}

function get_all_author_tours($author) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_TABLE;
	if(!$loadprivate) {
		$sql = $sql.' WHERE `tourowner` = '.$author;		
	}
	$sql = $sql.' ORDER BY  `tourdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;	
}

function get_author_tours($author, $page) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;		
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_TABLE;
	if(!$loadprivate) {
		$sql = $sql.' WHERE `tourowner` = '.$author;		
	}
	$sql = $sql.' ORDER BY  `tourdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;
	$result = $db->sql_query($sql);
	return $result;	
}

function get_stage_info($trackid) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackid` = '.$trackid;		
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	return $row;	
}

function get_tour_info($tourid) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_TABLE . ' WHERE `tourid` = '.$tourid;		
	$result = $db->sql_query($sql);
	return $result;	
}

function insert_tour_stage($tourid, $order, $stageid) {
	global $db;
	$sql_arr = array(
		'tourid'    => $tourid,
		'trackid'        => $stageid,
		'tracktourorder'    => $order,
	);
	$sql = 'INSERT INTO ' . TRACKS_TOURS_ORDER_TABLE . ' ' . $db->sql_build_array('INSERT', $sql_arr);
	$db->sql_query($sql);
}

function update_tour_stage($tourid, $order, $stageid) {
	global $db;
	$sql_arr = array(
		'trackid'        => $stageid,
	);
	$sql_where_arr = array(
		'tourid'    => $tourid,
		'tracktourorder'        => $order,
	);	
	$sql = 'UPDATE ' . TRACKS_TOURS_ORDER_TABLE . ' SET ' . $db->sql_build_array('UPDATE', $sql_arr) . ' WHERE ' . $db->sql_build_array('SELECT', $sql_where_arr) ;
	$db->sql_query($sql);
}

function get_previous_stage($tourid, $trackid) {
	global $db;
	$number = get_tour_stage_number($tourid, $trackid);
	if($number > 1) {
		$result = get_tour_stage($tourid, $number-1);
		$row = $db->sql_fetchrow($result);
		return $row['trackid'];
	} else {
		return 0;
	}
}

function get_next_stage($tourid, $trackid, $tourstages) {
	global $db;
	$number = get_tour_stage_number($tourid, $trackid);
	if($number < $tourstages) {
		$result = get_tour_stage($tourid, $number+1);
		$row = $db->sql_fetchrow($result);
		return $row['trackid'];
	} else {
		return 0;
	}
}

function get_tours_of_stage($trackid) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_ORDER_TABLE . ' WHERE `trackid` = '.$trackid;		
	$result = $db->sql_query($sql);
	return $result;	
}

function get_tour_stage_number($tourid, $trackid) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_ORDER_TABLE . ' WHERE `tourid` = '.$tourid.' AND `trackid` = '.$trackid;		
	$db->sql_query($sql);
	$number = $db->sql_fetchfield('tracktourorder');
	return $number;	
}

function get_tour_stage($tourid, $stage) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_ORDER_TABLE . ' WHERE `tourid` = '.$tourid.' AND `tracktourorder` = '.$stage;		
	$result = $db->sql_query($sql);
	return $result;	
}

function get_tour_stages($tourid) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TOURS_ORDER_TABLE . ' WHERE `tourid` = '.$tourid.' ORDER BY `tracktourorder`';		
	$result = $db->sql_query($sql);
	return $result;	
}

function get_type_info($type) {
	global $db;
	$sql = 'SELECT * FROM  ' . STAGE_TYPES_TABLE . ' WHERE typeid='.$type;
	$result = $db->sql_query($sql);
	return $result;		
}

function get_stages_type() {
	global $db;
	$sql = 'SELECT * FROM  ' . STAGE_TYPES_TABLE;
	$result = $db->sql_query($sql);
	return $result;		
}

function get_races_type() {
	global $db;
	$sql = 'SELECT * FROM  ' . TRACKS_RACES_TABLE;
	$result = $db->sql_query($sql);
	return $result;		
}

function get_user_paginated_tracks($owner, $page) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;	
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackowner` ='.$owner. ' ORDER BY  `trackdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;
	$result = $db->sql_query($sql);
	return $result;
}

function get_user_tracks($owner) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackowner` ='.$owner. ' ORDER BY  `trackdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;
}

function get_all_tracks() {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' ORDER BY  `trackdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;
}

function get_all_public_tracks() {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackpublic` = 1 ORDER BY  `trackdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;
}

function get_paginated_public_tracks($page) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackpublic` = 1 ORDER BY  `trackdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;
	$result = $db->sql_query($sql);
	return $result;
}

function get_paginated_tracks($page) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' ORDER BY  `trackdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;
	$result = $db->sql_query($sql);
	return $result;
}

function get_all_author_tracks_by_id($author) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' 
	WHERE trackowner = '.$author.' ORDER BY `trackowner` , `trackdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;
}

function get_all_author_tracks($author) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' JOIN ' . USERS_TABLE . ' ON trackowner = user_id 
	WHERE username_clean LIKE "%'.$author.'%" ORDER BY `trackowner` , `trackdatecreation` DESC';
	$result = $db->sql_query($sql);
	return $result;
}

function get_all_author_public_tracks($author) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' JOIN ' . USERS_TABLE . ' ON trackowner = user_id 
	WHERE username_clean LIKE "%'.$author.'%" AND `trackpublic` = 1 ORDER BY `trackowner` , `trackdatecreation` DESC';	
	$result = $db->sql_query($sql);
	return $result;
}

function get_author_tracks($page, $author) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' JOIN ' . USERS_TABLE . ' ON trackowner = user_id 
	WHERE username_clean LIKE "%'.$author.'%" ORDER BY `trackowner` , `trackdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;
	$result = $db->sql_query($sql);
	return $result;
}

function get_author_public_tracks($page, $author) {
	global $db;
	$minlimit = ($page - 1) * RESULT_PER_PAGE;
	$pageresults = RESULT_PER_PAGE;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' JOIN ' . USERS_TABLE . ' ON trackowner = user_id 
	WHERE username_clean LIKE "%'.$author.'%" AND `trackpublic` = 1 ORDER BY `trackowner` , `trackdatecreation` DESC LIMIT '.$minlimit.' ,  '.$pageresults;	
	$result = $db->sql_query($sql);
	return $result;
}

function fetch_user_info($owner) {
	global $db;
	$sql = 'SELECT *
	FROM ' . USERS_TABLE . '
	WHERE `user_id` =  "'.$owner.'"';
	$result = $db->sql_query($sql);
	return $result;
}

function get_track_owner($track) {
	global $db;
	$sql = 'SELECT * FROM  ' . TRACKS_TABLE .' WHERE `trackid` ='.$track;
	$db->sql_query($sql);	
	$owner = $db->sql_fetchfield('trackowner');
	return $owner;	
}

function get_tour_owner($tour) {
	global $db;
	$sql = 'SELECT * FROM  ' . TRACKS_TOURS_TABLE . ' WHERE `tourid` ='.$tour;
	$db->sql_query($sql);	
	$owner = $db->sql_fetchfield('tourowner');
	return $owner;	
}

function get_tour_stages_number($tour) {
	global $db;
	$sql = 'SELECT * FROM  ' . TRACKS_TOURS_TABLE . ' WHERE `tourid` ='.$tour;
	$db->sql_query($sql);	
	$stagenumber = $db->sql_fetchfield('tourstages');
	return $stagenumber;	
}

function ispublic($track) {
	global $db;
	$sql = 'SELECT * FROM  ' . TRACKS_TABLE .' WHERE `trackid` ='.$track;
	$db->sql_query($sql);	
	$ispublic = $db->sql_fetchfield('trackpublic');
	return $ispublic;	
}

function isTourpublic($tour) {
	global $db;
	$sql = 'SELECT * FROM  ' .  TRACKS_TOURS_TABLE . ' WHERE `tourid` ='.$tour;
	$db->sql_query($sql);	
	$ispublic = $db->sql_fetchfield('tourpublic');
	return $ispublic;	
}

function get_track_info($track) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackid` ='.$track;
	$result = $db->sql_query($sql);
	return $result;	
}

function deleteStage($stageid) {
	global $db;
	$sql_ary = array(	
		'trackid'      => $stageid,											
	);
	$sql = 'DELETE
	FROM ' .  TRACKS_TABLE . '
	WHERE ' . $db->sql_build_array('SELECT', $sql_ary);	
	$db->sql_query($sql);	
}

function deleteTour($tourid) {
	global $db;
	$sql_ary = array(	
		'tourid'      => $tourid,											
	);
	$sql = 'DELETE
	FROM ' .  TRACKS_TOURS_TABLE . '
	WHERE ' . $db->sql_build_array('SELECT', $sql_ary);	
	$db->sql_query($sql);	
}

function insertStage($stageid, $name, $type, $distance, $userid, $date, $depart, $arrive, $public, $descr, $trackracetype) {
	global $db;
	$sql_ary = array(	
		'trackowner'      => $userid,		
		'trackname'      => $name,		
		'tracktype'      => $type,		
		'trackdistance'      => $distance,	
		'trackdatecreation'      => $date,	
		'trackdatelastedit'      => $date,	
		'trackid'      => $stageid,			
		'trackdepart'      => $depart,	
		'trackarrive'      => $arrive,	
		'trackpublic'      => $public,	
		'trackdescr'      => $descr,		
		'trackcompetition'      => $trackracetype,											
	);		
	$sql = 'INSERT INTO ' . TRACKS_TABLE . ' 
		' . $db->sql_build_array('INSERT', $sql_ary);
	$db->sql_query($sql);		
}

function updateStage($stageid, $name, $type, $distance, $userid, $date, $depart, $arrive, $public, $descr, $trackracetype) {
	global $db;
	$sql_ary = array(	
		'trackowner'      => $userid,	
		'trackname'      => $name,		
		'tracktype'      => $type,		
		'trackdistance'      => $distance,	
		'trackdatelastedit'      => $date,	
		'trackdepart'      => $depart,	
		'trackarrive'      => $arrive,	
		'trackpublic'      => $public,	
		'trackdescr'      => $descr,		
		'trackcompetition'      => $trackracetype,											
	);		
	$sql = 'UPDATE ' . TRACKS_TABLE . ' 
		SET ' . $db->sql_build_array('UPDATE', $sql_ary) . '
		WHERE trackid = ' . (int) $stageid;
	$db->sql_query($sql);	
}

function updateStageProperties($trackid, $trackname, $trackdepart, $trackarrive, $tracktype, $racetype, $trackowner, $trackpublic, $trackdescription) {
	global $db;
	$sql_ary = array(	
		'trackowner'      => $trackowner,	
		'trackname'      => $trackname,		
		'tracktype'      => $tracktype,		
		'trackdepart'      => $trackdepart,	
		'trackarrive'      => $trackarrive,	
		'trackpublic'      => $trackpublic,	
		'trackdescr'      => $trackdescription,		
		'trackcompetition'      => $racetype,											
	);		
	$sql = 'UPDATE ' . TRACKS_TABLE . ' 
		SET ' . $db->sql_build_array('UPDATE', $sql_ary) . '
		WHERE trackid = ' . (int) $trackid;
	$db->sql_query($sql);	
}

function make_public($stageid) {
	global $db;
	$sql_ary = array(	
		'trackpublic'      => 1,											
	);		
	$sql = 'UPDATE ' . TRACKS_TABLE . ' 
		SET ' . $db->sql_build_array('UPDATE', $sql_ary) . '
		WHERE trackid = ' . (int) $stageid;
	$db->sql_query($sql);	
}

function make_private($stageid) {
	global $db;
	$sql_ary = array(	
		'trackpublic'      => 0,											
	);		
	$sql = 'UPDATE ' . TRACKS_TABLE . ' 
		SET ' . $db->sql_build_array('UPDATE', $sql_ary) . '
		WHERE trackid = ' . (int) $stageid;
	$db->sql_query($sql);	
}

function insertTour($tourid, $name, $description, $stages, $public, $date, $userid) {
	global $db;
	$sql_ary = array(	
		'tourid'      => $tourid,		
		'tourowner'      => $userid,		
		'tourname'      => $name,		
		'tourdescr'      => $description,		
		'tourpublic'      => $public,	
		'tourdatecreation'      => $date,	
		'tourdatelastedit'      => $date,		
		'tourstages'      => $stages,											
	);		
	$sql = 'INSERT INTO ' . TRACKS_TOURS_TABLE . ' 
		' . $db->sql_build_array('INSERT', $sql_ary);
	$db->sql_query($sql);		
	deleteTourStages($tourid, $stages);
}

function editTour($tourid, $name, $description, $stages, $public, $date, $userid) {
	global $db;
	$sql_ary = array(		
		'tourowner'      => $userid,		
		'tourname'      => $name,		
		'tourdescr'      => $description,		
		'tourpublic'      => $public,	
		'tourdatelastedit'      => $date,		
		'tourstages'      => $stages,											
	);		
	$sql = 'UPDATE ' . TRACKS_TOURS_TABLE . ' 
		SET ' . $db->sql_build_array('UPDATE', $sql_ary) . '
		WHERE tourid = ' . (int) $tourid;
	$db->sql_query($sql);		
	deleteTourStages($tourid, $stages);
}

function deleteTourStages ($tourid, $laststagetokeep) {
	global $db;
	$sql = 'DELETE FROM ' . TRACKS_TOURS_ORDER_TABLE  . ' WHERE `tourid`='.$tourid.' AND `tracktourorder`>'.$laststagetokeep;
	$db->sql_query($sql);
}

function isPresent($stageid) {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackid` ='.$stageid;
	$result = $db->sql_query($sql);
	if(mysql_num_rows($result)!=0) {
		return 1;
	} else {
		return 0;
	}	
}

function getNewId() {
	global $db;
	$sql = 'SELECT MAX(trackid) as `trackmax` FROM ' . TRACKS_TABLE;
	$db->sql_query($sql);
	$maxid = (int) $db->sql_fetchfield('trackmax');
	return $maxid+1;
}

function get_new_tour_id() {
	global $db;
	$sql = 'SELECT MAX(tourid) as `tourmax` FROM ' . TRACKS_TOURS_TABLE;
	$db->sql_query($sql);
	$maxid = (int) $db->sql_fetchfield('tourmax');
	return $maxid+1;
}

function canViewTrack ($trackid) {
	global $user, $auth;
	$owner = get_track_owner($trackid);
	$userid = $user->data["user_id"];	
	if(ispublic($trackid)) {
		return 1;
	}
	if($auth->acl_get('u_can_view_all_tracks')) {
		return 1;
	}
	if($owner == $userid) {
		return 1;
	}
	return 0;
}

function canEditTrack ($trackid) {
	global $user, $auth;
	$owner = get_track_owner($trackid);
	$userid = $user->data["user_id"];	
	if(ispublic($trackid)) {
		return 1;
	}
	if($auth->acl_get('m_can_edit_tracks')) {
		return 1;
	}
	if($owner == $userid) {
		return 1;
	}
	return 0;
}

function canViewTour ($tourid) {
	global $user, $auth;
	$owner = get_tour_owner($tourid);
	$userid = $user->data["user_id"];	
	if(isTourpublic($tourid)) {
		return 1;
	}
	if($auth->acl_get('u_can_view_all_tours')) {
		return 1;
	}
	if($owner == $userid) {
		return 1;
	}
	return 0;
}

function canEditTour ($tourid) {
	global $user, $auth;
	$owner = get_tour_owner($tourid);
	$userid = $user->data["user_id"];	
	if(isTourpublic($tourid)) {
		return 1;
	}
	if($auth->acl_get('m_can_edit_tours')) {
		return 1;
	}
	if($owner == $userid) {
		return 1;
	}
	return 0;
}

function getTotalTracks () {
	global $db;
	$sql = 'SELECT COUNT(*) as `totaltracks` FROM ' . TRACKS_TABLE;
	$db->sql_query($sql);
	$tracknumber = (int) $db->sql_fetchfield('totaltracks');
	return $tracknumber;
}

function getTotalKms () {
	global $db;
	$sql = 'SELECT SUM(`trackdistance`) as `totalkms` FROM ' . TRACKS_TABLE;
	$db->sql_query($sql);
	$totalkms = (int) $db->sql_fetchfield('totalkms');
	return $totalkms;
}

function getTotalTours () {
	global $db;
	$sql = 'SELECT COUNT(*) as `totaltours` FROM ' . TRACKS_TOURS_TABLE;
	$db->sql_query($sql);
	$tournumber = (int) $db->sql_fetchfield('totaltours');
	return $tournumber;
}

function get_last_tracks() {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' ORDER BY  `trackdatecreation` DESC LIMIT 0, 10';
	$result = $db->sql_query($sql);
	return $result;
}

function get_last_public_tracks() {
	global $db;
	$sql = 'SELECT * FROM ' . TRACKS_TABLE . ' WHERE `trackpublic` = 1 ORDER BY  `trackdatecreation` DESC LIMIT 0, 10';
	$result = $db->sql_query($sql);
	return $result;
}

?>