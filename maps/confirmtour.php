<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

$name = htmlspecialchars($_POST["tourname"]);
$description = htmlspecialchars($_POST["tourdescription"]);
$public = htmlspecialchars($_POST["tourpublic"]);
$stages = htmlspecialchars($_POST["tourstages"]);
$mode = htmlspecialchars($_POST["mode"]);
$date = date('Y-m-d H:i:s');
$userid = $user->data["user_id"];

if($mode == 'edit') {
	$tourid = htmlspecialchars($_POST["tourid"]);
	if(canViewTour($tourid)) {
		editTour($tourid, $name, $description, $stages, $public, $date, $userid);		
	}
} else if ($mode == 'add') {
	$tourid = get_new_tour_id();	
	insertTour($tourid, $name, $description, $stages, $public, $date, $userid);
}

?>