<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
define('RESULT_PER_PAGE', 30);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

if(!$auth->acl_get('u_can_view_tours')) {
	trigger_error('NOT_AUTHORISED');
}


//template
$template->set_filenames(array(
 'body' => 'maps/maps_viewtour.html'
));

$tourid = htmlspecialchars($_GET["tour"]);
$totaldistance = 0;

if(canViewTour($tourid)) {	
	$result = get_tour_info($tourid);
	$row = $db->sql_fetchrow($result);
	
	$tourid = $row['tourid'];
	$tourowner = $row['tourowner'];
	$owner = $user->data["user_id"];
	if($tourowner==$owner) {
		$mine = 1;
	} else {
		$mine = 0;
	}
	$tourname = $row['tourname'];
	page_header($tourname);
	$tourdescr = $row['tourdescr'];
	$tourdatecreation = $row['tourdatecreation'];
	$tourdatelastedit = $row['tourdatelastedit'];
	$tourpublic = $row['tourpublic'];
	$tourdatecreation = date('d/m/Y - G.i', strtotime($tourdatecreation));	
	$tourdatelastedit = date('d/m/Y - G.i', strtotime($tourdatelastedit));
	$userinfo = fetch_user_info($tourowner);
	$userrow = $db->sql_fetchrow($userinfo);
	$usercolour = $userrow['user_colour'];
	$username = $userrow['username'];	
	$logofolder = "./../images/logo/";		
	$template->assign_vars(array(
		'LOGOFOLDER' => $logofolder,	
		'TOURID' => $tourid,
		'TOURNAME' => $tourname,
		'TOURDESCR' => nl2br($tourdescr),
		'TOUROWNER' => get_username_string('full', $tourowner, $username, $usercolour, "Ospite"),
		'TOURDATECREATION' => $tourdatecreation,
		'TOURDATELASTEDIT' => $tourdatelastedit,
		'TOURPUBLIC' => $tourpublic,	
		'TOURVIEWLINK'	=> append_sid($phpbb_root_path.'maps/viewtour.'.$phpEx, array('tour' => $tourid)),
		'TOUREDITLINK'	=> append_sid($phpbb_root_path.'maps/newtour.'.$phpEx, array('action' => 'edit', 'tour' => $tourid)),	
		'TOURSTAGEEDITLINK'	=> append_sid($phpbb_root_path.'maps/tourstageedit.'.$phpEx, array('tour' => $tourid)),	
	));			

	// Extract stages
	$stagenumber = get_tour_stages_number($tourid);
	if($stagenumber) {
		for($stage = 1; $stage <= $stagenumber; $stage++) {
			$result = get_tour_stage($tourid, $stage);
			if(!mysql_num_rows($result)) {
				$stageid = 0;
				$stagename = '';
				$stagetype = '';
				$stagedepart = '';
				$stagearrive = '';
				$typedescr = '';
				$typelogo = '';
				$stagedistance = '';
			} else {
				$row = $db->sql_fetchrow($result);
				$stageid = $row['trackid'];
				$stageinfo = get_stage_info($stageid);
				$stagename = $stageinfo['trackname'];
				$stagetype = $stageinfo['tracktype'];
				$stagedistance = $stageinfo['trackdistance'];
				$stagedepart = $stageinfo['trackdepart'];
				$stagearrive = $stageinfo['trackarrive'];
				$trackdatecreation = $stageinfo['trackdatecreation'];
				$trackdatelastedit = $stageinfo['trackdatelastedit'];			
				$trackdatecreation = date('d/m/Y - G.i', strtotime($trackdatecreation));	
				$trackdatelastedit = date('d/m/Y - G.i', strtotime($trackdatelastedit));						
			}
				
			if($stagetype) {
				$typequery = get_type_info($stagetype);
				$typerow = $db->sql_fetchrow($typequery);
				$typelogo = $typerow['typelogo'];		
				switch ($stagetype) {
					case '1':
					$typedescr = $user->lang['FLAT'];
					break;
					case '2':
					$typedescr = $user->lang['HILL'];
					break;			
					case '3':
					$typedescr = $user->lang['MOUNTAIN'];
					break;			
					case '4':
					$typedescr = $user->lang['ITT'];
					break;			
					case '5':
					$typedescr = $user->lang['TTT'];			
					break;				
			}
			
			$totaldistance = $totaldistance + $stagedistance;
			
			if (file_exists('tracks/Track'.$stageid.'.json')) {
				$trackroute = gzuncompress(file_get_contents('tracks/Track'.$stageid.'.json', true));
			} else {
				$trackroute = 0;
			}
				
			$template->assign_block_vars('stages', array(
				'STAGENUMBER' => $stage,
				'STAGEID' => $stageid,
				'STAGENAME' => $stagename,
				'STAGEROUTE' => $trackroute,
				'STAGETYPE' => $typedescr,
				'STAGEDEPART' => $stagedepart,
				'STAGEARRIVE' => $stagearrive,
				'STAGETYPELOGO' => $typelogo,
				'STAGEDISTANCE' => $stagedistance,
				'STAGEDATECREATION' => $trackdatecreation,
				'STAGEDATELASTEDIT' => $trackdatelastedit,
				'STAGEVIEWLINK' => append_sid($phpbb_root_path.'maps/viewtrack.'.$phpEx, array('trackid' => $stageid)),
				'STAGEEDITLINK' => append_sid($phpbb_root_path.'maps/maps.'.$phpEx, array('action' => 'edit', 'track' => $stageid)),
				'CANVIEWSTAGE' => canViewTrack($stageid)
			));	
			}				
		}
						
	}
} else {
	trigger_error('NOT_AUTHORISED');	
}

$template->assign_vars(array(	
	'TOTALDISTANCE'			=> $totaldistance,
	'ISMINE' => $mine,
	'ARROWLOGO' => "./../images/logo/Arrow.gif",	
	'U_STAGEUPDATE'			=> append_sid("{$phpbb_root_path}maps/tourstageupdate.$phpEx"),
	'TOURSTAGEEDITLINK'	=> append_sid($phpbb_root_path.'maps/tourstageedit.'.$phpEx, array('tour' => $tourid)),		
	'CANVIEWALLTRACKS' => $auth->acl_get('u_can_view_all_tracks'),
));

generatebar();
page_footer();

?>