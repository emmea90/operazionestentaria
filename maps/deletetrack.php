<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

$stageid = htmlspecialchars($_POST["track"]);
$userid = $user->data["user_id"];

// Get Track Owner
$owner = get_track_owner($stageid);
if($owner==$userid || $auth->acl_get('m_can_edit_tracks')) {
	deleteStage($stageid);
	unlink("tracks/Track".$stageid."alt.json");
	unlink("tracks/Track".$stageid.".json");	
	unlink("tracks/Track".$stageid."sprints.json");
} else {
	trigger_error('NOT_AUTHORISED');
}

?>