<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

// Get all tracks
$trackresults = get_all_tracks();
while ($row = $db->sql_fetchrow($trackresults))
{
	$trackid = $row['trackid'];	
	if (file_exists('tracks/Track'.$trackid.'.json')) {
		$fileroute = file_get_contents('tracks/Track'.$trackid.'.json', true);
		$fileroutealt = file_get_contents('tracks/Track'.$trackid.'alt.json', true);
		$fileroutesprints = file_get_contents('tracks/Track'.$trackid.'sprints.json', true);
		$trackroute = $fileroute;
		$trackroutealt = $fileroutealt;			
		$tracksprints = $fileroutesprints;			
	} else {
		$trackroute = $row['trackroute'];
		$trackroutealt = $row['trackroutealt'];		
		$tracksprints = $row['tracksprints'];		
	}
	$trackname = $row['trackname'];
	$trackdistance = $row['trackdistance'];
	$trackdepart = $row['trackdepart'];
	$trackarrive = $row['trackarrive'];
	$trackpublic = $row['trackpublic'];
	$tracktype = $row['tracktype'];	
	$trackdescr = $row['trackdescr'];	
	$trackracetype = $row['trackcompetition'];	
	$trackowner = $row['trackowner'];
	file_put_contents("tracks/Track".$trackid."alt.json", $trackroutealt);
	file_put_contents("tracks/Track".$trackid."sprints.json", $tracksprints);
	file_put_contents("tracks/Track".$trackid.".json", $trackroute);	
}


?>