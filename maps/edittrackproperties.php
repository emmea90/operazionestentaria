<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');
$userid = $user->data["user_id"];

//template
$template->set_filenames(array(
 'body' => 'maps/maps_edittrackproperties.html'
));

$trackid = htmlspecialchars($_GET["trackid"]) ? htmlspecialchars($_GET["trackid"]) : 0;

$userid = $user->data["user_id"];
$ismine = (get_track_owner($trackid)==$userid) ? 1 : 0;
// am i editing?
$editing = htmlspecialchars($_POST["editing"]) ? htmlspecialchars($_POST["editing"]) : 0;
if($editing) {
	if(canEditTrack($trackid)) {
		$trackname = htmlspecialchars($_POST["trackname"]) ? htmlspecialchars($_POST["trackname"]) : 0;	
		$trackdepart = htmlspecialchars($_POST["trackdepart"]) ? htmlspecialchars($_POST["trackdepart"]) : 0;	
		$trackarrive = htmlspecialchars($_POST["trackarrive"]) ? htmlspecialchars($_POST["trackarrive"]) : 0;		
		$tracktype = htmlspecialchars($_POST["tracktype"]) ? htmlspecialchars($_POST["tracktype"]) : 0;		
		$racetype = htmlspecialchars($_POST["racetype"]) ? htmlspecialchars($_POST["racetype"]) : 0;		
		$trackowner = htmlspecialchars($_POST["trackowner"]) ? htmlspecialchars($_POST["trackowner"]) : $userid;
		if($trackowner!=$userid && !$auth->acl_get('m_can_change_track_authors')) {
			$trackowner = $userid;				
		}
		$trackpublic = htmlspecialchars($_POST["trackpublic"]) ? htmlspecialchars($_POST["trackpublic"]) : 0;		
		$trackdescription = htmlspecialchars($_POST["trackdescription"]) ? htmlspecialchars($_POST["trackdescription"]) : "";	
	updateStageProperties($trackid, $trackname, $trackdepart, $trackarrive, $tracktype, $racetype, $trackowner, $trackpublic, $trackdescription);	
	} else {
		trigger_error('NOT_AUTHORISED');		
	}
} else {
	
	if(canEditTrack($trackid)) {			
		$result = get_track_info($trackid);
		$row = $db->sql_fetchrow($result);
		if (file_exists('tracks/Track'.$trackid.'.json')) {
			$trackroute = gzuncompress(file_get_contents('tracks/Track'.$trackid.'.json', true));
			$trackroutealt = gzuncompress(file_get_contents('tracks/Track'.$trackid.'alt.json', true));	
			$tracksprints = gzuncompress(file_get_contents('tracks/Track'.$trackid.'sprints.json', true));	
		}
		$trackname = $row['trackname'];
		$trackdistance = $row['trackdistance'];
		$trackdepart = $row['trackdepart'];
		$trackowner = $row['trackowner'];
		$trackarrive = $row['trackarrive'];
		$trackpublic = $row['trackpublic'];
		$tracktype = $row['tracktype'];	
		$trackdescr = $row['trackdescr'];	
		$trackracetype = $row['trackcompetition'];	
		$ismine = (get_track_owner($trackid)==$userid) ? 1 : 0;
		
		//Standard page header
		page_header($trackname);
		
		$template->assign_vars(array(
			'LOGOFOLDER' => "./../images/logo/",
			'TRACKID' => $trackid,
			'TRACKROUTE' => $trackroute,
			'TRACKROUTEALT' => $trackroutealt,
			'TRACKNAME' => $trackname,
			'TRACKDEPART' => $trackdepart,
			'TRACKARRIVE' => $trackarrive,
			'TRACKTYPE' => $tracktype,
			'TRACKOWNER' => $trackowner,
			'TRACKDISTANCE' => $trackdistance,
			'TRACKDESCRIPTION' => $trackdescr,
			'TRACKPUBLIC' => $trackpublic,
			'TRACKSPRINTS' => $tracksprints,
			'TRACKRACETYPE' => $trackracetype,
			'ISMINE' => $ismine,
			'CANEDITALL' => $auth->acl_get('m_can_edit_tracks'),
			'CANCHANGEAUTHOR' => $auth->acl_get('m_can_change_track_authors'),
			'EDITRACKLINK' => append_sid($phpbb_root_path.'maps/edittrackproperties.'.$phpEx, array('trackid' => $trackid)),	
		));		
	} else {
		trigger_error('NOT_AUTHORISED');
	}
	
	// Get all the users
	$result = get_users();
	while ($row = $db->sql_fetchrow($result))
	{
		$group = $row['group_id'];
		if($group!=1 && $group!=6) {
			$template->assign_block_vars('users', array(
				'USERID' => $row['user_id'],
				'USERNAME' => $row['username'],
			));			
		}			
	}	
	
	// Get all the races type
	$result = get_races_type();
	while ($row = $db->sql_fetchrow($result))
	{
		$template->assign_block_vars('racetypes', array(
			'RACETYPEID' => $row['raceid'],
			'RACENAME' => $row['racename'],
		));				
	}	
	
	// Get all the types
	$result = get_stages_type();
	while ($row = $db->sql_fetchrow($result))
	{
		$template->assign_block_vars('stagetypes', array(
			'STAGETYPEID' => $row['typeid'],
			'STAGETYPEDESCR' => $row['typedescription'],
			'STAGETYPELOGO' => $row['typelogo'],
		));				
	}	
	
	generatebar();
	page_footer();

}

?>