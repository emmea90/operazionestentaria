<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

$user->session_begin();
$auth->acl($user->data);
$user->setup();

include($phpbb_root_path . 'includes/acp/auth.' . $phpEx);
$auth_admin = new auth_admin();

// Add foo permissions as local permissions
// (you could instead make them global permissions by making the obvious changes below)
$auth_admin->acl_add_option(array(
    'local'     => array(),
	'global'    => array('u_can_access_maps',
						'u_can_view_tracks',
						'u_can_view_tours',
						'u_can_view_all_tracks',
						'u_can_view_all_tours',
						'u_can_view_debug_stuff',
						'm_can_edit_tracks',
						'm_can_edit_tours',
						'm_can_change_track_authors',
						'a_can_manage_maps'),
));

?>