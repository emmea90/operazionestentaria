<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
define('RESULT_PER_PAGE', 30);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

//Standard page header
page_header($user->lang['NEWTOUR']);

if(!$auth->acl_get('u_can_view_tours')) {
	trigger_error('NOT_AUTHORISED');
}

//template
$template->set_filenames(array(
 'body' => 'maps/maps_addtour.html'
));

$action = htmlspecialchars($_GET["action"]);
$tourid = htmlspecialchars($_GET["tour"]);

if($action=='edit') {	
	if(canViewTour($tourid)) {
		$result = get_tour_info($tourid);
		$row = $db->sql_fetchrow($result);
		$tourname = $row['tourname'];
		$tourdescr = $row['tourdescr'];
		$tourpublic = $row['tourpublic'];
		$tourstages = $row['tourstages'];
		$template->assign_vars(array(
			'TOURID' => $tourid,
			'TOURNAME' => $tourname,
			'TOURDESCR' => $tourdescr,
			'TOURPUBLIC' => $tourpublic,
			'TOURSTAGES' => $tourstages,
			'EDITING' => true,
		));			
	} else {
		trigger_error('NOT_AUTHORISED');
	}			
} 

$template->assign_vars(array(	
	'U_CONFIRMTOUR'			=> append_sid("{$phpbb_root_path}maps/confirmtour.$phpEx"),
	'U_NEWTOUR'				=> append_sid("{$phpbb_root_path}maps/newtour.$phpEx"),
	'U_TOURS'				=> append_sid("{$phpbb_root_path}maps/tours.$phpEx"),
));

generatebar();
page_footer();

?>