<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

$tourid = htmlspecialchars($_POST["tourid"]);
$order = htmlspecialchars($_POST["order"]);
$stageid = htmlspecialchars($_POST["stageid"]);

// Check if present
$result = get_tour_stage($tourid, $order);
if(!mysql_num_rows($result)) {
	insert_tour_stage($tourid, $order, $stageid);	
} else {
	update_tour_stage($tourid, $order, $stageid);	
}
?>