<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
define('RESULT_PER_PAGE', 30);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

//Standard page header
page_header($user->lang['NEWTOUR']);

if(!$auth->acl_get('u_can_view_tours')) {
	trigger_error('NOT_AUTHORISED');
}


//template
$template->set_filenames(array(
 'body' => 'maps/maps_tourstageedit.html'
));

$tourid = htmlspecialchars($_POST["tourid"]);

if(canEditTour($tourid)) {	
	// Who is owner
	$tourowner = get_tour_owner($tourid);	
	
	// Extract stages
	$stagenumber = get_tour_stages_number($tourid);
	for($stage = 1; $stage <= $stagenumber; $stage++) {
		$result = get_tour_stage($tourid, $stage);
		if(mysql_num_rows($result)) {
			$row = $db->sql_fetchrow($result);
			$stageid = $row['trackid'];
			make_private($stageid);
		} 
	}
} else {
	trigger_error('NOT_AUTHORISED');	
}

?>