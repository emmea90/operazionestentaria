<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
define('RESULT_PER_PAGE', 30);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

//Standard page header
page_header($user->lang['TRACKTOURS']);

if(!$auth->acl_get('u_can_view_tours')) {
	trigger_error('NOT_AUTHORISED');
}

//template
$template->set_filenames(array(
 'body' => 'maps/maps_mytours.html'
));

$loadprivate = $auth->acl_get('u_can_view_all_tours');
$owner = $user->data["user_id"];
$page = htmlspecialchars($_GET["page"]) ? htmlspecialchars($_GET["page"]) : 1;
$result = get_author_tours($owner, $page);

while ($row = $db->sql_fetchrow($result))
{
	$tourid = $row['tourid'];
	$tourowner = $row['tourowner'];
	$tourname = $row['tourname'];
	$tourdescr = $row['tourdescr'];
	$tourdatecreation = $row['tourdatecreation'];
	$tourdatelastedit = $row['tourdatelastedit'];
	$tourpublic = $row['tourpublic'];
	$tourdatecreation = date('d/m/Y - G.i', strtotime($tourdatecreation));	
	$tourdatelastedit = date('d/m/Y - G.i', strtotime($tourdatelastedit));
	$userinfo = fetch_user_info($tourowner);
	$userrow = $db->sql_fetchrow($userinfo);
	$usercolour = $userrow['user_colour'];
	$username = $userrow['username'];	

	$template->assign_block_vars('tours', array(
		'TOURID' => $tourid,
		'TOURNAME' => $tourname,
		'TOURDESCR' => nl2br($tourdescr),
		'TOUROWNER' => get_username_string('full', $tourowner, $username, $usercolour, "Ospite"),
		// 'TOURDISTANCE' => todo,
		'TOURDATECREATION' => $tourdatecreation,
		'TOURDATELASTEDIT' => $tourdatelastedit,
		'TOURPUBLIC' => $tourpublic,	
		'TOURVIEWLINK'	=> append_sid($phpbb_root_path.'maps/viewtour.'.$phpEx, array('tour' => $tourid)),
		'TOUREDITLINK'	=> append_sid($phpbb_root_path.'maps/newtour.'.$phpEx, array('action' => 'edit', 'tour' => $tourid)),	
		'TOURSTAGEEDITLINK'	=> append_sid($phpbb_root_path.'maps/tourstageedit.'.$phpEx, array('tour' => $tourid)),	
	));		
}

$result = get_all_author_tours($owner);
$totalpages = ceil(mysql_num_rows($result) / RESULT_PER_PAGE);

for($i=1; $i<=$totalpages; $i++) {
	$template->assign_block_vars('pages', array(
		'PAGENUMBER' => $i,		
	));			
}


$template->assign_vars(array(	
	'TOTALPAGES' => $totalpages,
	'CURRENTPAGE' => $page,
	'U_NEWTOUR'			=> append_sid("{$phpbb_root_path}maps/newtour.$phpEx"),
	'ARROWLOGO' => "./../images/logo/Arrow.gif",
	'CANVIEWALLTOURS' => $auth->acl_get('u_can_view_all_tours'),
));

generatebar();
page_footer();

?>