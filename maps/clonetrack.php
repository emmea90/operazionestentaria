<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

$stageid = htmlspecialchars($_POST["track"]);
$userid = $user->data["user_id"];

// May i clone track?
if(canViewTrack($stageid)) {
	cloneTrack($stageid);
	echo($user->lang['TRACKCLONED']);
} else {
	echo($user->lang['NOTPUBLIC']);
}

function cloneTrack($trackid) {
	global $user, $db;
	$result = get_track_info($trackid);
	$row = $db->sql_fetchrow($result);
	$newtrackid = getNewId();
	if (file_exists('tracks/Track'.$trackid.'.json')) {
		$trackroute = gzuncompress(file_get_contents('tracks/Track'.$trackid.'.json', true));
		$trackroutealt = gzuncompress(file_get_contents('tracks/Track'.$trackid.'alt.json', true));
		$tracksprints = gzuncompress(file_get_contents('tracks/Track'.$trackid.'sprints.json', true));	
	}	
	$trackname = $row['trackname'];
	$tracktype = $row['tracktype'];
	$trackdistance = $row['trackdistance'];
	$trackdepart = $row['trackdepart'];
	$trackarrive = $row['trackarrive'];
	$trackpublic = $row['trackpublic'];
	$trackdescr = $row['trackdescr'];	
	$trackracetype = $row['trackcompetition'];
	$userid = $user->data["user_id"];
	$date = date('Y-m-d H:i:s');
	insertStage($newtrackid, $trackname, $tracktype, $trackdistance, $userid, $date, $trackdepart, $trackarrive, $trackpublic, $trackdescr, $trackracetype);	
	file_put_contents("tracks/Track".$newtrackid."alt.json", gzcompress($trackroutealt, 9));
	file_put_contents("tracks/Track".$newtrackid.".json", gzcompress($trackroute, 9));	
	file_put_contents("tracks/Track".$newtrackid."sprints.json", gzcompress($tracksprints, 9));
}

?>