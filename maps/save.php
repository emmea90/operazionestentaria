<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

$folderfile = $phpbb_root_path . '/tracks';
$route = $_POST["route"];
$routealt = $_POST["routewithalt"];
$name = htmlspecialchars($_POST["name"]);
$type = htmlspecialchars($_POST["type"]);
$distance = htmlspecialchars($_POST["distance"]);
$stageid = htmlspecialchars($_POST["stageid"]);
$depart = htmlspecialchars($_POST["depart"]);
$arrive = htmlspecialchars($_POST["arrive"]);
$public = htmlspecialchars($_POST["ispublic"]);
$sprints = $_POST["sprints"];
$trackdescr = htmlspecialchars($_POST["trackdescription"]);
$trackracetype = htmlspecialchars($_POST["trackracetype"]);
$date = date('Y-m-d H:i:s');
$userid = $user->data["user_id"];

if($userid==1) {
	echo("Error, please login");
} else {
	
	if($stageid == 0) {
		// I am creating a new track
		$stageid = getNewId();
	} else {
		// I am saving over existing track
		$userid = get_track_owner($stageid);
	}
	
	
	echo($stageid);
	
	if(isPresent($stageid)) {
		updateStage($stageid, $name, $type, $distance, $userid, $date, $depart, $arrive, $public, $trackdescr, $trackracetype);
	} else {
		insertStage($stageid, $name, $type, $distance, $userid, $date, $depart, $arrive, $public, $trackdescr, $trackracetype);
	}
	
	file_put_contents("tracks/Track".$stageid."alt.json", gzcompress($routealt,9));
	file_put_contents("tracks/Track".$stageid.".json", gzcompress($route, 9));
	file_put_contents("tracks/Track".$stageid."sprints.json", gzcompress($sprints, 9));
}
?>