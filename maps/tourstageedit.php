<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
define('RESULT_PER_PAGE', 30);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

//Standard page header
page_header($user->lang['NEWTOUR']);

if(!$auth->acl_get('u_can_view_tours')) {
	trigger_error('NOT_AUTHORISED');
}


//template
$template->set_filenames(array(
 'body' => 'maps/maps_tourstageedit.html'
));

$tourid = htmlspecialchars($_GET["tour"]);
$totaldistance = 0;

if(canEditTour($tourid)) {	
	// Who is owner
	$tourowner = get_tour_owner($tourid);	
	

	// Extract stages
	$stagenumber = get_tour_stages_number($tourid);
	for($stage = 1; $stage <= $stagenumber; $stage++) {
		$result = get_tour_stage($tourid, $stage);
		if(!mysql_num_rows($result)) {
			$stageid = 0;
			$stagename = '';
			$stagetype = '';
			$stagedepart = '';
			$stagearrive = '';
			$typedescr = '';
			$typelogo = '';
			$stagedistance = '';
		} else {
			$row = $db->sql_fetchrow($result);
			$stageid = $row['trackid'];
			$stageinfo = get_stage_info($stageid);
			$stagename = $stageinfo['trackname'];
			$stagetype = $stageinfo['tracktype'];
			$stagedistance = $stageinfo['trackdistance'];
			$stagedepart = $stageinfo['trackdepart'];
			$stagearrive = $stageinfo['trackarrive'];
			
		}
		
		if($stagetype) {
			$typequery = get_type_info($stagetype);
			$typerow = $db->sql_fetchrow($typequery);
			$typelogo = $typerow['typelogo'];		
			switch ($stagetype) {
				case '1':
				$typedescr = $user->lang['FLAT'];
				break;
				case '2':
				$typedescr = $user->lang['HILL'];
				break;			
				case '3':
				$typedescr = $user->lang['MOUNTAIN'];
				break;			
				case '4':
				$typedescr = $user->lang['ITT'];
				break;			
				case '5':
				$typedescr = $user->lang['TTT'];			
				break;							
			}
		}
		
		$totaldistance = $totaldistance + $stagedistance;
		
		$template->assign_block_vars('stages', array(
			'STAGENUMBER' => $stage,
			'STAGEID' => $stageid,
			'STAGENAME' => $stagename,
			'STAGETYPE' => $typedescr,
			'STAGEDEPART' => $stagedepart,
			'STAGEARRIVE' => $stagearrive,
			'STAGETYPELOGO' => $typelogo,
			'STAGEDISTANCE' => $stagedistance,
			'TRACKVIEWLINK'	=> append_sid($phpbb_root_path.'maps/viewtrack.'.$phpEx, array('trackid' => $stage)),
			'TRACKEDITLINK'	=> append_sid($phpbb_root_path.'maps/maps.'.$phpEx, array('action' => 'edit', 'track' => $stage)),				
		));		
		
		// Extract his stages
		$result = get_all_author_tracks_by_id($tourowner);
		while ($row = $db->sql_fetchrow($result))
		{
			$trackid = $row['trackid'];
			$trackname = $row['trackname'];	
			$template->assign_block_vars('stages.tracks', array(
				'TRACKID' => $trackid,
				'TRACKNAME' => $trackname,
			));			
		}			
	}
} else {
	trigger_error('NOT_AUTHORISED');	
}

$template->assign_vars(array(	
	'TOURID'			=> $tourid,
	'TOTALDISTANCE'			=> $totaldistance,
	'ARROWLOGO' => "./../images/logo/Arrow.gif",
	'U_STAGEUPDATE'			=> append_sid("{$phpbb_root_path}maps/tourstageupdate.$phpEx"),
	'U_MAKESTAGEPUBLIC'		=> append_sid("{$phpbb_root_path}maps/makestagepublic.$phpEx"),
	'U_MAKESTAGEPRIVATE'	=> append_sid("{$phpbb_root_path}maps/makestageprivate.$phpEx"),
	'TOURVIEWLINK'			=> append_sid($phpbb_root_path.'maps/viewtour.'.$phpEx, array('tour' => $tourid)),	
));

generatebar();
page_footer();

?>