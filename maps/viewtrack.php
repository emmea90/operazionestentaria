<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');
$userid = $user->data["user_id"];

//template
$template->set_filenames(array(
 'body' => 'maps/maps_viewtrack.html'
));

$trackid = htmlspecialchars($_GET["trackid"]) ? htmlspecialchars($_GET["trackid"]) : 0;
$tourid = htmlspecialchars($_GET["tour"]) ? htmlspecialchars($_GET["tour"]) : 0;

$userid = $user->data["user_id"];
if(canViewTrack($trackid)) {	
	$result = get_track_info($trackid);
	$row = $db->sql_fetchrow($result);
	if (file_exists('tracks/Track'.$trackid.'.json')) {
		$trackroute = gzuncompress(file_get_contents('tracks/Track'.$trackid.'.json', true));
		$trackroutealt = gzuncompress(file_get_contents('tracks/Track'.$trackid.'alt.json', true));	
		$tracksprints = gzuncompress(file_get_contents('tracks/Track'.$trackid.'sprints.json', true));	
	}
	$trackname = $row['trackname'];
	$trackdistance = $row['trackdistance'];
	$trackdepart = $row['trackdepart'];
	$trackarrive = $row['trackarrive'];
	$trackpublic = $row['trackpublic'];
	$tracktype = $row['tracktype'];	
	$trackdescr = $row['trackdescr'];	
	$trackracetype = $row['trackcompetition'];	
	$ismine = (get_track_owner($trackid)==$userid) ? 1 : 0;
	
	//Standard page header
	page_header($trackname);
	
	$template->assign_vars(array(
		'LOGOFOLDER' => "./../images/logo/",
		'TRACKID' => $trackid,
		'TRACKROUTE' => $trackroute,
		'TRACKROUTEALT' => $trackroutealt,
		'TRACKNAME' => $trackname,
		'TRACKDEPART' => $trackdepart,
		'TRACKARRIVE' => $trackarrive,
		'TRACKTYPE' => $tracktype,
		'TRACKDISTANCE' => $trackdistance,
		'TRACKDESCRIPTION' => nl2br($trackdescr),
		'TRACKPUBLIC' => $trackpublic,
		'TRACKSPRINTS' => $tracksprints,
		'TRACKRACETYPE' => $trackracetype,
		'ISMINE' => $ismine,
		'CANEDITALL' => $auth->acl_get('m_can_edit_tracks'),
	));		
} else {
	trigger_error('NOT_AUTHORISED');
}

// Get all the races type
$result = get_races_type();
while ($row = $db->sql_fetchrow($result))
{
	$template->assign_block_vars('racetypes', array(
		'RACETYPEID' => $row['raceid'],
		'RACENAME' => $row['racename'],
	));				
}	

// If i am in a tour, get the tour info
$isinatour = 0;
$result = get_tours_of_stage($trackid);
while ($row = $db->sql_fetchrow($result)) {
	$isinatour = 1;
	$tourid = $row['tourid'];
	if(canViewTour($tourid)) {		
		$result = get_tour_info($tourid);
		$row = $db->sql_fetchrow($result);
			
		$tourid = $row['tourid'];
		$tourowner = $row['tourowner'];
		$tourname = $row['tourname'];
		$tourstages = $row['tourstages'];
		$tourdescr = $row['tourdescr'];
	
		// Get previous stage
		$previousstageid = get_previous_stage($tourid, $trackid);
		// Get next stage
		$nextstageid = get_next_stage($tourid, $trackid, $tourstages);
		
		$template->assign_block_vars('stagetours', array(
			'TOURID' => $tourid,
			'TOURNAME' => $tourname,
			'TOURDESCR' => nl2br($tourdescr),
			'TOURVIEWLINK'	=> append_sid($phpbb_root_path.'maps/viewtour.'.$phpEx, array('tour' => $tourid)),
			'PREVIOUSTRACKLINK'	=> append_sid($phpbb_root_path.'maps/viewtrack.'.$phpEx, array('trackid' => $previousstageid)),
			'NEXTTRACKLINK'	=> append_sid($phpbb_root_path.'maps/viewtrack.'.$phpEx, array('trackid' => $nextstageid)),			
			'PREVIOUSID' => $previousstageid,
			'NEXTID' => $nextstageid,
		));		
			
	} else {
		trigger_error('NOT_AUTHORISED');	
	}
}

$template->assign_vars(array(
	'ISINATOUR' => $isinatour,
));		

generatebar();
page_footer();

?>