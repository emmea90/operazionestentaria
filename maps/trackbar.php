<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function generatebar() {
	global $template, $phpbb_root_path, $phpEx;
	$template->assign_vars(array(	
		'MAPSEDITOR'			=> append_sid("{$phpbb_root_path}maps/maps.$phpEx"),
		'MYTRACKS'			=> append_sid("{$phpbb_root_path}maps/mytracks.$phpEx"),
		'ALLTRACKS'			=> append_sid("{$phpbb_root_path}maps/tracks.$phpEx"),
		'TRACKTOURS'			=> append_sid("{$phpbb_root_path}maps/tours.$phpEx"),
		'MYTOURS'			=> append_sid("{$phpbb_root_path}maps/mytours.$phpEx"),
		'MAPSHOME'			=> append_sid("{$phpbb_root_path}maps/editor.$phpEx"),
		'TRACKHELP'			=> append_sid("{$phpbb_root_path}maps/help.$phpEx"),
	));
}
?>