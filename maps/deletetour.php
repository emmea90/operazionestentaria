<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

$tourid = htmlspecialchars($_POST["tour"]);
$userid = $user->data["user_id"];

// Get Track Owner
$owner = get_tour_owner($tourid);
if($owner==$userid || $auth->acl_get('m_can_edit_tours')) {
	deleteTour($tourid);
} else {
	trigger_error('NOT_AUTHORISED');
}

?>