<?php
/**
*
* @package Maps
* @version $Id: 1.00
* @copyright (c) 2007 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'maps/dbfunctions.' . $phpEx);
include($phpbb_root_path . 'maps/trackbar.' . $phpEx);

// Start session
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/maps');

if(!$auth->acl_get('u_can_access_maps')) {
	trigger_error('NOT_AUTHORISED');
}

	$logofolder = "./../images/logo/";		
	$template->assign_vars(array(
		'LOGOFOLDER' => $logofolder,
		'CANSEEDEBUGSTUFF' => $auth->acl_get('u_can_view_debug_stuff'),
		'STAGEID' => 0,
	));	
		
//template
$template->set_filenames(array(
 'body' => 'maps/maps_index.html'
));

// Get all the types
$result = get_stages_type();
while ($row = $db->sql_fetchrow($result))
{
	$template->assign_block_vars('stagetypes', array(
		'STAGETYPEID' => $row['typeid'],
		'STAGETYPEDESCR' => $row['typedescription'],
		'STAGETYPELOGO' => $row['typelogo'],
	));				
}	

// Get all the races type
$result = get_races_type();
while ($row = $db->sql_fetchrow($result))
{
	$template->assign_block_vars('racetypes', array(
		'RACETYPEID' => $row['raceid'],
		'RACENAME' => $row['racename'],
	));				
}	

$action = htmlspecialchars($_GET["action"]);
if($action=="edit") {
	$track = htmlspecialchars($_GET["track"]);
	$owner = get_track_owner($track);
	if($owner!=$user->data["user_id"]) {
		if(!$auth->acl_get('m_can_edit_tracks')) {
			trigger_error('NOT_AUTHORISED');
		} 
	}
	$result = get_track_info($track);
	$row = $db->sql_fetchrow($result);
	$trackname = $row['trackname'];
	$tracktype = $row['tracktype'];
	if (file_exists('tracks/Track'.$track.'.json')) {
		$trackroute = gzuncompress(file_get_contents('tracks/Track'.$track.'.json', true));
		$trackroutealt = gzuncompress(file_get_contents('tracks/Track'.$track.'alt.json', true));
		$tracksprints = gzuncompress(file_get_contents('tracks/Track'.$track.'sprints.json', true));				
	}
	$trackdepart = $row['trackdepart'];
	$trackdistance = $row['trackdistance'];
	$trackarrive = $row['trackarrive'];	
	$trackpublic = $row['trackpublic'];	
	$trackdescr = $row['trackdescr'];		
	$trackracetype = $row['trackcompetition'];	
	$template->assign_vars(array(
		'ISLOADEDTRACK' => 1,
		'TRACKLINK'	=> append_sid($phpbb_root_path.'maps/viewtrack.'.$phpEx, array('trackid' => $track)),
		'TRACKROUTE' => $trackroute,
		'TRACKNAME' => $trackname,
		'TRACKTYPE' => $tracktype,
		'TRACKROUTEALT' => $trackroutealt,
		'TRACKDISTANCE' => $trackdistance,
		'TRACKDEPART' => $trackdepart,
		'TRACKARRIVE' => $trackarrive,
		'TRACKPUBLIC' => $trackpublic,
		'TRACKDESCRIPTION' => $trackdescr,
		'TRACKSPRINTS' => $tracksprints,
		'TRACKRACETYPE' => $trackracetype,
		'STAGEID' => $track,
	));		
	//Standard page header
	page_header($trackname);	
	
	// If i am in a tour, get the tour info
	$isinatour = 0;
	$result = get_tours_of_stage($track);
	while ($row = $db->sql_fetchrow($result)) {
		$isinatour = 1;
		$tourid = $row['tourid'];
		if(canViewTour($tourid)) {		
			$result = get_tour_info($tourid);
			$row = $db->sql_fetchrow($result);
				
			$tourid = $row['tourid'];
			$tourowner = $row['tourowner'];
			$tourname = $row['tourname'];
			$tourstages = $row['tourstages'];
			$tourdescr = $row['tourdescr'];
		
			// Get previous stage
			$previousstageid = get_previous_stage($tourid, $track);
			// Get next stage
			$nextstageid = get_next_stage($tourid, $track, $tourstages);
			
			$template->assign_block_vars('stagetours', array(
				'TOURID' => $tourid,
				'TOURNAME' => $tourname,
				'TOURDESCR' => nl2br($tourdescr),
				'TOURVIEWLINK'	=> append_sid($phpbb_root_path.'maps/viewtour.'.$phpEx, array('tour' => $tourid)),
				'PREVIOUSTRACKLINK'	=> append_sid($phpbb_root_path.'maps/maps.'.$phpEx, array('track' => $previousstageid, 'action' => 'edit')),
				'NEXTTRACKLINK'	=> append_sid($phpbb_root_path.'maps/maps.'.$phpEx, array('track' => $nextstageid, 'action' => 'edit')),			
				'PREVIOUSID' => $previousstageid,
				'NEXTID' => $nextstageid,
			));		
				
		} else {
			trigger_error('NOT_AUTHORISED');	
		}
	}
	
	$template->assign_vars(array(
		'ISINATOUR' => $isinatour,
	));			
} else {
	//Standard page header
	page_header($user->lang['NEWTRACK']);
}

generatebar();
page_footer();

?>