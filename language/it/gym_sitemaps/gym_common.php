<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-27
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_common [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'RSS_AUTH_SOME_USER' => '<b>Attenzione:</b> Questo elemento presente nella lista è personalizzato in base alle relative autorizzazioni <b>%s</b>.<br />Alcuni elementi potrebbero non essere visualizzabili se non si è effettuato il login.',
	'RSS_AUTH_THIS_USER' => '<b>Attenzione:</b> Questo elemento presente nella lista è personalizzato in base alle relative autorizzazioni <b>%s</b>.<br />Non sarà visibile senza prima aver effettuato il login.',
	'RSS_AUTH_SOME' => '<b>Attenzione:</b> Questa lista di elementi non è pubblica.<br />Alcuni degli elementi elencati, potrebbero non essere visualizzati se non hai prima effettuato il login.',
	'RSS_AUTH_THIS' => '<b>Attenzione:</b> Questo elemento non è pubblico.<br />Non sarà visibile se non prima hai effettuato il login.',
	'RSS_CHAN_LIST_TITLE' => 'Lista dei Canali',
	'RSS_CHAN_LIST_DESC' => 'Questa lista dei Canali è disponibile nell’elenco dei Feed RSS.',
	'RSS_CHAN_LIST_DESC_MODULE' => 'Questa lista dei Canali è disponibile nell’elenco dei Feed RSS disponibili per: %s.',
	'RSS_ANNOUCES_DESC' => 'In questi Feed sono elencati tutti gli Annunci globali di: %s',
	'RSS_ANNOUNCES_TITLE' => 'Annunci di: %s',
	'GYM_LAST_POST_BY' => 'Ultimo messaggio di ',
	'GYM_FIRST_POST_BY' => 'Messaggio di ',
	'GYM_LINK' => 'Link',
	'GYM_SOURCE' => 'Sorgente',
	'GYM_RSS_SOURCE' => 'Sorgente',
	'RSS_MORE' => 'Continua a leggere',
	'RSS_CHANNELS' => 'Canali',
	'RSS_CONTENT' => 'Anteprima',
	'RSS_SHORT' => 'lista breve',
	'RSS_LONG' => 'lista lunga',
	'RSS_NEWS' => 'News',
	'RSS_NEWS_DESC' => 'Ultime News da',
	'RSS_REPORTED_UNAPPROVED' => 'Questo elemento è in attesa di approvazione.',

	'GYM_HOME' => 'Home',
	'GYM_FORUM_INDEX' => 'Indice',
	'GYM_LASTMOD_DATE' => 'Ultimo aggiornamento',
	'GYM_SEO' => 'Ottimizzazione per i motori di ricerca',
	'GYM_MINUTES' => 'minuto(i)',
	'GYM_SQLEXPLAIN' => 'Esplicazione rapporto SQL',
	'GYM_SQLEXPLAIN_MSG' => 'Effettua il login come amministratore per controllare questa pagina: %s.',
	'GYM_BOOKMARK_THIS' => 'Segnalibri',
	// Errors
	'GYM_ERROR_404' => 'Questa pagina non esiste o non è stata attivata',
	'GYM_ERROR_404_EXPLAIN' => 'Il server non ha trovato nessuna pagina corrispondente all’URL usato.',
	'GYM_ERROR_401' => 'Non sei autorizzato a visualizzare questa pagina.',
	'GYM_ERROR_401_EXPLAIN' => 'Questa pagina è accessibile solo agli utenti registrati che hanno le autorizzazioni necessarie.',
	'GYM_LOGIN' => 'Non sei autorizzato a visualizzare questa pagina.',
	'GYM_LOGIN_EXPLAIN' => 'Devi essere iscritto e connesso per visualizzare questa pagina.',
	'GYM_TOO_FEW_ITEMS' => 'Pagina non disponibile',
	'GYM_TOO_FEW_ITEMS_EXPLAIN' => 'Questa pagina non contiene abbastanza elementi da visualizzare.',
	'GYM_TOO_FEW_ITEMS_EXPLAIN_ADMIN' => 'Questa pagina è vuota o non contiene elementi sufficienti (opzione configurabile dal PCA) per essere visualizzata.<br />È stato inviato un Errore 404 in modo che i motori di ricerca escludano questo link.',

	'GOOGLE_SITEMAP' => 'Sitemap',
	'GOOGLE_SITEMAP_OF' => 'Sitemap di',
	'GOOGLE_MAP_OF' => 'Sitemap di %1$s',
	'GOOGLE_SITEMAPINDEX' => 'Indice della Sitemap',
	'GOOGLE_NUMBER_OF_SITEMAP' => 'Numero di Sitemap in quest’Indice',
	'GOOGLE_NUMBER_OF_URL' => 'Numero di URL in questa Sitemap',
	'GOOGLE_SITEMAP_URL' => 'URL Sitemap',
	'GOOGLE_CHANGEFREQ' => 'Cambia freq.',
	'GOOGLE_PRIORITY' => 'priorità',

	'RSS_FEED' => 'Feed RSS',
	'RSS_FEED_OF' => 'Feed RSS di %1$s',
	'RSS_2_LINK' => 'Link Feed RSS 2.0',
	'RSS_UPDATE' => 'Aggiornamento',
	'RSS_LAST_UPDATE' => 'Ultimo aggiornamento',
	'RSS_SUBSCRIBE_POD' => '<h2>Sottoscrivi questo Feed ora!</h2>Con il tuo servizio preferito.',
	'RSS_SUBSCRIBE' => 'Per iscriverti a questo Feed RSS manualmente, utilizza il seguente URL:',
	'RSS_ITEM_LISTED' => 'Uno degli elementi elencati.',
	'RSS_ITEMS_LISTED' => 'elementi elencati.',
	'RSS_VALID' => 'Feed RSS 2.0 valido',

	// Old URL handling
	'RSS_1XREDIR' => 'Questo Feed RSS è stato spostato',
	'RSS_1XREDIR_MSG' => 'Questo feed RSS è stato spostato e puoi trovarlo qui ',
	// HTML sitemaps
	'HTML_MAP' => 'Sitemap',
	'HTML_MAP_OF' => 'Sitemap di %1$s',
	'HTML_MAP_NONE' => 'Nessuna Sitemap',
	'HTML_NO_ITEMS' => 'Nessun elemento',
	'HTML_NEWS' => 'News',
	'HTML_NEWS_OF' => 'News di %1$s',
	'HTML_NEWS_NONE' => 'Nessuna News',
	'HTML_PAGE' => 'Pagina',
	'HTML_MORE' => 'Leggi il testo completo',
	// Forum
	'HTML_FORUM_MAP' => 'Sitemap Forum',
	'HTML_FORUM_NEWS' => 'Forum News',
	'HTML_FORUM_GLOBAL_MAP' => 'Lista Annunci globali',
	'HTML_FORUM_GLOBAL_NEWS' => 'Annunci globali',
	'HTML_FORUM_ANNOUNCE_MAP' => 'Lista Annunci',
	'HTML_FORUM_ANNOUNCE_NEWS' => 'Annunci',
	'HTML_FORUM_STICKY_MAP' => 'Lista Argomenti importanti',
	'HTML_FORUM_STICKY_NEWS' => 'Argomenti importanti',
	'HTML_LASTX_TOPICS_TITLE' => 'Ultimi %1$s argomenti attivi',
));
?>