<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-16
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*
*/
/**
*
* google_forum [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_FORUM' => 'Sitemap forum',
	'GOOGLE_FORUM_EXPLAIN' => 'Queste sono le impostazioni per il modulo di Google Sitemap forum.<br /> Alcune sono sovrascritte a seconda delle impostazioni di Google Sitemap e a seconda delle impostazioni principali di sovrascrittura.',
	'GOOGLE_FORUM_SETTINGS' => 'Impostazioni Sitemap forum',
	'GOOGLE_FORUM_SETTINGS_EXPLAIN' => 'Le seguenti impostazioni sono specifiche del modulo Sitemap forum di Google.',
	'GOOGLE_FORUM_STICKY_PRIORITY' => 'Priorità argomenti importanti',
	'GOOGLE_FORUM_STICKY_PRIORITY_EXPLAIN' => 'Priorità argomenti importanti (deve essere un numero compreso tra 0,0 e 1,0 incluso).',
	'GOOGLE_FORUM_ANNOUCE_PRIORITY' => 'Priorità annuncio',
	'GOOGLE_FORUM_ANNOUCE_PRIORITY_EXPLAIN' => 'Priorità annuncio (deve essere un numero compreso tra 0,0 e 1,0 incluso).',
	'GOOGLE_FORUM_GLOBAL_PRIORITY' => 'Priorità annuncio a livello globale',
	'GOOGLE_FORUM_GLOBAL_PRIORITY_EXPLAIN' => 'Priorità annuncio globale (deve essere un numero compreso tra 0,0 e 1,0 incluso).',
	'GOOGLE_FORUM_EXCLUDE' => 'Esclusione forum',
	'GOOGLE_FORUM_EXCLUDE_EXPLAIN' => 'Da qui è possibile escludere uno o diversi forum dall’elenco delle Sitemap.<br /><b>Nota:</b> Se questo campo viene lasciato vuoto, tutti i forum pubblici saranno elencati.',


	// Reset settings
	'GOOGLE_FORUM_RESET' => 'Modulo Sitemap forum',
	'GOOGLE_FORUM_RESET_EXPLAIN' => 'Ripristina tutte le opzioni modulo Sitemap forum ai valori predefiniti.',
	'GOOGLE_FORUM_MAIN_RESET' => 'Principali Sitemap forum',
	'GOOGLE_FORUM_MAIN_RESET_EXPLAIN' => 'Ripristina tutte le opzioni nella scheda  (principale)  “Sitemap forum” del modulo Sitemap forum.',
	'GOOGLE_FORUM_CACHE_RESET' => 'Cache Sitemap forum',
	'GOOGLE_FORUM_CACHE_RESET_EXPLAIN' => 'Ripristina tutte le opzioni di cache del modulo Sitemap forum.',
	'GOOGLE_FORUM_MODREWRITE_RESET' => 'Forum Sitemap riscrittura URL',
	'GOOGLE_FORUM_MODREWRITE_RESET_EXPLAIN' => 'Ripristina tutte le opzioni di riscrittura URL del modulo Sitemap forum.',
	'GOOGLE_FORUM_GZIP_RESET' => 'Sitemap forum gunzip',
	'GOOGLE_FORUM_GZIP_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni gunzip del modulo Sitemap forum.',
	'GOOGLE_FORUM_LIMIT_RESET' => 'Limiti Sitemap forum',
	'GOOGLE_FORUM_LIMIT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni limite del modulo Sitemap forum.',
	'GOOGLE_FORUM_SORT_RESET' => 'Ordinamento Sitemap forum',
	'GOOGLE_FORUM_SORT_RESET_EXPLAIN' => 'Ripristina tutte le opzioni di ordinamento del modulo Sitemap forum.',
	'GOOGLE_FORUM_PAGINATION_RESET' => 'Impaginazione Sitemap forum',
	'GOOGLE_FORUM_PAGINATION_RESET_EXPLAIN' => 'Ripristina tutte le opzioni di impaginazione del modulo Sitemap forum.',
));
?>