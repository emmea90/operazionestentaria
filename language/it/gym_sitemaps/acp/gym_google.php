<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-19
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_google [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_MAIN' => 'Impostazioni Google Sitemap',
	'GOOGLE_MAIN_EXPLAIN' => 'Impostazioni principali per il modulo Google Sitemap.<br />Saranno applicate le impostazioni predefinite a tutti i moduli Google Sitemap.',
	// Setup linking
	'GOOGLE_LINKS_ACTIVATION' => 'Link forum',
	'GOOGLE_LINKS_MAIN' => 'Link principali',
	'GOOGLE_LINKS_MAIN_EXPLAIN' => 'Visualizza, o no, il link nella parte bassa della pagina della Sitemap.<br />Questa funzione richiede che "Link principali" sia attivata nelle impostazioni principali.',
	'GOOGLE_LINKS_INDEX' => 'Link sull’Indice',
	'GOOGLE_LINKS_INDEX_EXPLAIN' => 'Visualizza, o no, i link sulle Sitemap disponibili per ogni forum nell’Indice del Forum. Questi link sono aggiunti sotto la descrizione del forum.<br />Questa funzione richiede che l’opzione "Link sull’Indice" sia attivata nelle impostazioni principali.',
	'GOOGLE_LINKS_CAT' => 'Link nella pagina del forum',
	'GOOGLE_LINKS_CAT_EXPLAIN' => 'Visualizza, o no, i link della Sitemap nel forum corrente. Questi link sono aggiunti sotto il nome del forum.<br />Questa funzione richiede che l’opzione "Link nella pagina del forum" sia attivata nelle impostazioni principali.',
	// Ripristino impostazioni
	'GOOGLE_ALL_RESET' => '<b>Tutti</b> Moduli Google Sitemap',
	'GOOGLE_URL_EXPLAIN' => 'Inserisci l’URL completo dell’Indice nella tua Sitemap. Per esempio http://www.miosito.com/cartella_forum/ se Sitemap.php è installata su http://www.miosito.com/cartella_forum/.<br />Questa opzione è utile quando phpBB non è installato nella root del dominio e si vogliono mettere in lista nella Google Sitemap URL posizionati nella root del dominio.',
	'GOOGLE_PING' => 'Avvisa Google',
	'GOOGLE_PING_EXPLAIN' => 'Avvisa Google ogni volta che la Sitemap è aggiornata.',
	'GOOGLE_THRESHOLD' => 'Soglia Sitemap',
	'GOOGLE_THRESHOLD_EXPLAIN' => 'Numero minimo di argomenti visualizzati nella Sitemap. Per il forum, questo significa che solo i forum con un numero di argomenti che superano la soglia avranno una Sitemap.',
	'GOOGLE_PRIORITIES' => 'Priorità delle impostazioni',
	'GOOGLE_DEFAULT_PRIORITY' => 'Priorità predefinita',
	'GOOGLE_DEFAULT_PRIORITY_EXPLAIN' => 'La priorità predefinita per gli URL nella lista della Sitemap; sarà utilizzata se le opzioni dei moduli lo permettono (deve essere un numero compreso tra 0.0 e 1.0)',
	'GOOGLE_XSLT' => 'Stile XSLT',
	'GOOGLE_XSLT_EXPLAIN' => 'Attiva il foglio di stile XSL per avere in output una Google Sitemap per gli utenti con link cliccabili e altro. Questa sarà effettiva solo dopo che si sarà svuotata la cache dalla Google Sitemap utilizzando il link di manutenzione qui sopra.',
	'GOOGLE_LOAD_PHPBB_CSS' => 'Carica il CSS di phpBB',
	'GOOGLE_LOAD_PHPBB_CSS_EXPLAIN' => 'Il modulo di GYM Sitempap utilizza il sistema di template del phpBB3. I fogli di stile XSL utilizzati per costruire l’HTML in output sono compatibili con gli stili del phpBB3.<br />Così puoi applicare il CSS del phpBB3 sui fogli di stile XSL invece di quello predefinito. In questo modo tutte le personalizzazioni dei temi come gli sfondi e i colori dei font o le immagini, saranno usati come stile in output della Google Sitemap.<br />Questo avrà effetto solo dopo aver svuotato la cache RSS nel menù "Manutenzione".<br />Se i file di stile della Google Sitemap non sono presenti nello stile corrente, sarà utilizzato lo stile predefinito (sempre disponibile, basato sul prosilver).<br /> Non provare ad utilizzare il template del prosilver con un altro stile perché il CSS, molto probabilmente, non corrisponderà.',
	// Impostazioni autorizzazione
	'GOOGLE_AUTH_SETTINGS' => 'Impostazioni autorizzazione',
	'GOOGLE_ALLOW_AUTH' => 'Autorizzazioni',
	'GOOGLE_ALLOW_AUTH_EXPLAIN' => 'Attiva le autorizzazioni per la Sitemap. Se attive, gli utenti loggati e i Bot potranno visualizzare nella Sitemap anche i forum privati, purché abbiano le necessarie autorizzazioni.',
	'GOOGLE_CACHE_AUTH' => 'Cache Sitemap private',
	'GOOGLE_CACHE_AUTH_EXPLAIN' => 'Puoi disabilitare la cache per le Sitemap non pubbliche dove è permesso.<br /> Inserire nella cache le Sitemap private, aumenta il numero dei file processati. Non dovrebbe essere un problema, ma puoi decidere qui di inserire nella cache solo le Sitemap pubbliche.',
));
?>