<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-16
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* rss_forum [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'RSS_FORUM' => 'Modulo RSS Forum',
	'RSS_FORUM_EXPLAIN' => 'Queste sono le impostazioni per il modulo Feed RSS del Forum.<br /> Alcune di queste possono essere sovrascritte a seconda delle impostazioni principali di sovrascrittura degli RSS.',
	'RSS_FORUM_ALTERNATE' => 'Link alternativi RSS',
	'RSS_FORUM_ALTERNATE_EXPLAIN' => 'Visualizza o no i link RSS alternativi nella barra di navigazione del browser.',
	'RSS_FORUM_EXCLUDE' => 'Forum esclusi',
	'RSS_FORUM_EXCLUDE_EXPLAIN' => 'Da qui puoi escludere uno o più forum dalla lista RSS.<br /><b>Nota:</b> Se il campo è vuoto tutti i forum leggibili saranno aggiunti alla lista.',
	// Contenuto
	'RSS_FORUM_CONTENT' => 'Impostazioni contenuto forum',
	'RSS_FORUM_FIRST' => 'Primo messaggio',
	'RSS_FORUM_FIRST_EXPLAIN' => 'Visualizza o no l’URL del primo messaggio per tutti gli argomenti nella lista Feed RSS.<br /> Per impostazione predefinita, solo l’ultimo messaggio di ogni argomento viene inserito nella lista. Mostrare il primo messaggio significa aggiungere un po’ di lavoro al server.',
	'RSS_FORUM_LAST' => 'Ultimo messaggio',
	'RSS_FORUM_LAST_EXPLAIN' => 'Visualizza o no l’ultimo messaggio per tutti gli argomenti nella lista Feed RSS.<br /> Per impostazione predefinita, solo l’ultimo messaggio di ogni argomento è inserito nella lista. Questa opzione è utile se vuoi inserire solamente l’URL del primo messaggio nella lista Feed RSS.<br />Nota: impostare il primo messaggio su SI e l’ultimo messaggio su NO equivale a creare nuovi Feed.',
	'RSS_FORUM_RULES' => 'Visualizza le regole del forum',
	'RSS_FORUM_RULES_EXPLAIN' => 'Visualizza o no le regole del Forum nei Feed RSS.',
	// Ripristino impostazioni
	'RSS_FORUM_RESET' => 'Modulo RSS Forum',
	'RSS_FORUM_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni del Modulo RSS Forum.',
	'RSS_FORUM_MAIN_RESET' => 'Principale forum RSS',
	'RSS_FORUM_MAIN_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della scheda "Impostazioni Feed RSS" (principale) del modulo RSS Forum.',
	'RSS_FORUM_CONTENT_RESET' => 'Contenuto RSS Forum',
	'RSS_FORUM_CONTENT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni del contenuto del modulo RSS Forum.',
	'RSS_FORUM_CACHE_RESET' => 'Cache RSS Forum',
	'RSS_FORUM_CACHE_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della cache del modulo RSS Forum.',
	'RSS_FORUM_MODREWRITE_RESET' => 'Riscrittura RSS URL Forum',
	'RSS_FORUM_MODREWRITE_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di riscrittura RSS URL del modulo RSS Forum.',
	'RSS_FORUM_GZIP_RESET' => 'Gunzip RSS Forum',
	'RSS_FORUM_GZIP_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di Gunzip del modulo RSS Forum.',
	'RSS_FORUM_LIMIT_RESET' => 'Limiti RSS Forum',
	'RSS_FORUM_LIMIT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni del modulo limiti RSS Forum.',
	'RSS_FORUM_SORT_RESET' => 'Ordinamento RSS Forum',
	'RSS_FORUM_SORT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di ordinamento del modulo RSS Forum.',
	'RSS_FORUM_PAGINATION_RESET' => 'Impaginazione RSS Forum',
	'RSS_FORUM_PAGINATION_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di impaginazione del modulo RSS Forum.',
));
?>