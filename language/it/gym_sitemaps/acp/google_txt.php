<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-16
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* google_txt [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_TXT' => 'Sitemap TXT',
	'GOOGLE_TXT_EXPLAIN' => 'Questi sono i parametri per il modulo Sitemap TXT di Google. Si può integrare completamente l’elenco degli URL da un file di testo (un URL per riga) in GYM Sitemap e sfruttare tutte le funzionalità del modulo come stile XSLT e cache.<br /> Alcune impostazioni possono essere sovrascritte a seconda delle principali impostazioni di sovrascrittura di Google Sitemap.<br /> Ogni file di testo aggiunto nella cartella gym_sitemaps/sources/ sarà preso in considerazione una volta che hai eliminato la cache del modulo dal PCA, utilizzando il link di manutenzione qui sopra.<br /> Ogni file di testo che elenca gli URL deve essere composto da un URL completo per riga e dovrà seguire un modello di base per la denominazione del file: <b>google_</b>txt_file_name<b>.txt</b>.<br /> Una voce sarà creata nella sitemapindex con un URL <b>esempio.com/sitemap.php?txt=txt_file_name</b> e <b>esempio.com/txt-txt_file_name.xml</b> quando l’URL è riscritto.<br /> Il nome del file sorgente deve necessariamente utilizzare caratteri alfanumerici (0-9 a-z) più i separatori “_” e “-”.<br /><b style="color:red;">Nota:</b><br /> Si consiglia di utilizzare la cache della Sitemap di questo modulo per evitare inutili analisi di file di testo potenzialmente grandi.',
	// Main
	'GOOGLE_TXT_CONFIG' => 'Impostazioni Sitemap TXT',
	'GOOGLE_TXT_CONFIG_EXPLAIN' => 'Alcune impostazioni possono essere sovrascritte a seconda del Google Sitemap e le impostazioni principali di sovrascrittura.',
	'GOOGLE_TXT_RANDOMIZE' => 'Casuale',
	'GOOGLE_TXT_RANDOMIZE_EXPLAIN' => 'È possibile mettere in ordine causale gli URL estratti dal file di testo. Modificare l’ordine su base regolare può aiutare la scansione. Questa opzione è anche utile per esempio quando si vogliano limitare gli URL a 1000 per questo modulo e utilizzare file di testo di origine con 5000 URL; in questo caso tutti i 5000 URL saranno regolarmente visualizzati sulla corrispondente Sitemap.',
	'GOOGLE_TXT_UNIQUE' => 'Controlla duplicati',
	'GOOGLE_TXT_UNIQUE_EXPLAIN' => 'Attiva per garantire se qualche URL appare più di una volta nel testo sorgente, si visualizzerà solo una volta nella Sitemap.',
	'GOOGLE_TXT_FORCE_LASTMOD' => 'Ultima modifica',
	'GOOGLE_TXT_FORCE_LASTMOD_EXPLAIN' => 'Puoi forzare un momento dell’ultima modifica in base al ciclo di durata della cache (anche se la cache non è attivata) per tutti gli URL della Sitemap. Il modulo calcolerà anche le priorità e le frequenze di cambiamento basate su questa ultima modifica. Per impostazione predefinita, nessun tag ultima modifica viene aggiunto.',
	// Reset settings
	'GOOGLE_TXT_RESET' => 'Modulo Sitemap TXT',
	'GOOGLE_TXT_RESET_EXPLAIN' => 'Ripristina alle impostazioni predefinite tutte le opzioni di ordinamento del modulo Sitemap TXT.',
	'GOOGLE_TXT_MAIN_RESET' => 'Impostazioni Sitemap TXT',
	'GOOGLE_TXT_MAIN_RESET_EXPLAIN' => 'Ripristina alle impostazioni predefinite tutte le opzioni della (principale) scheda “Impostazioni Sitemap TXT ” del modulo Sitemap TXT.',
	'GOOGLE_TXT_CACHE_RESET' => 'Cache Sitemap TXT',
	'GOOGLE_TXT_CACHE_RESET_EXPLAIN' => 'Ripristina alle impostazioni predefinite tutte le opzioni di cache del modulo Sitemap TXT.',
	'GOOGLE_TXT_GZIP_RESET' => 'Sitemap TXT Gunzip',
	'GOOGLE_TXT_GZIP_RESET_EXPLAIN' => 'Ripristina alle impostazioni predefinite tutte le opzioni Gunzip del modulo Sitemap TXT.',
	'GOOGLE_TXT_LIMIT_RESET' => 'Limiti Sitemap TXT',
	'GOOGLE_TXT_LIMIT_RESET_EXPLAIN' => 'Ripristina alle impostazioni predefinite tutte le opzioni limiti del modulo Sitemap TXT.',
));
?>