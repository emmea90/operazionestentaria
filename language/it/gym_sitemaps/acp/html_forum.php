<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-16
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* html_forum [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'HTML_FORUM' => 'Modulo HTML Forum',
	'HTML_FORUM_EXPLAIN' => 'Queste sono le impostazioni per il modulo HTML del Forum.<br /> Alcune di queste possono essere sovrascritte a seconda delle impostazioni di sovrascrittura di HTML.',
	'HTML_FORUM_EXCLUDE' => 'Forum esclusi',
	'HTML_FORUM_EXCLUDE_EXPLAIN' => 'Da qui puoi escludere uno o più forum dalla lista RSS.<br /><b>Nota:</b> Se questo campo è vuoto, tutti i forum leggibili saranno aggiunti alla lista.',
	'HTML_FORUM_ALLOW_NEWS' => 'Forum news',
	'HTML_FORUM_ALLOW_NEWS_EXPLAIN' => ' La pagina Forum news è una pagina che mostra il primo messaggio di uno o più argomenti, uniti o no, e che appartengono ad uno o più forum che puoi selezionare sotto.',
	'HTML_FORUM_ALLOW_CAT_NEWS' => 'Categorie Forum news',
	'HTML_FORUM_ALLOW_CAT_NEWS_EXPLAIN' => 'Attiva o disattiva la pagina Forum news. Se attivata, ogni forum non escluso avrà una pagina di News per i suoi argomenti.',
	'HTML_FORUM_NEWS_IDS' => 'Sorgente Forum news',
	'HTML_FORUM_NEWS_IDS_EXPLAIN' => 'Puoi selezionare uno o più forum, anche privati, come sorgente per la tua pagina Forum news principale.<br /><b>Nota</b>:<br />Se lasciato vuoto, tutti i forum autorizzati saranno presi come sorgente per la pagina Forum news.',
	'HTML_FORUM_LTOPIC' => 'Lista ultimi argomenti attivi opzionale',
	'HTML_FORUM_INDEX_LTOPIC' => 'Visualizza nella forum Sitemap',
	'HTML_FORUM_INDEX_LTOPIC_EXPLAIN' => 'Visualizza, o no, la lista degli ultimi argomenti attivi nella forum Sitemap.<br />Inserisci il numero di argomenti da visualizzare; 0 per disattivare.',
	'HTML_FORUM_CAT_LTOPIC' => 'Visualizza nelle Sitemap le categorie del forum',
	'HTML_FORUM_CAT_LTOPIC_EXPLAIN' => 'Visualizza, o no, la lista degli ultimi argomenti attivi in ogni forum Sitemap.<br />Inserisci il numero di argomenti da visualizzare; 0 per disattivare.',
	'HTML_FORUM_NEWS_LTOPIC' => 'Visualizza sulla pagina i forum news',
	'HTML_FORUM_NEWS_LTOPIC_EXPLAIN' => 'Visualizza, o no, la lista degli ultimi argomenti attivi nella pagina forum news.<br />Inserisci il numero di argomenti da visualizzare; 0 per disattivare.',
	'HTML_FORUM_CAT_NEWS_LTOPIC' => 'Visualizza nelle categorie la pagina forum news',
	'HTML_FORUM_CAT_NEWS_LTOPIC_EXPLAIN' => 'Visualizza, o no, la lista degli ultimi argomenti attivi in ogni pagina forum news.<br />Inserisci il numero degli argomenti da visualizzare; 0 per disattivare.',
	'HTML_FORUM_LTOPIC_PAGINATION' => 'Impaginazione degli ultimi argomenti attivi',
	'HTML_FORUM_LTOPIC_PAGINATION_EXPLAIN' => 'Visualizza, o no, l’impaginazione nella lista degli ultimi argomenti attivi.',
	'HTML_FORUM_LTOPIC_EXCLUDE' => 'Escludi dalla lista gli ultimi argomenti attivi',
	'HTML_FORUM_LTOPIC_EXCLUDE_EXPLAIN' => ' Da qui puoi escludere uno o più forum dalla lista degli ultimi argomenti attivi.<br /><b>Nota:</b> Se questo campo è vuoto, tutti i forum leggibili saranno inseriti nella lista.',
	// Impaginazione
	'HTML_FORUM_PAGINATION' => 'Impaginazione Sitemap forum',
	'HTML_FORUM_PAGINATION_EXPLAIN' => 'Attiva, o no, l’impaginazione della forum Sitemap. Attiva questa opzione se vuoi visualizzare più di una pagina ed elencare tutti gli argomenti in ogni forum Sitemap.',
	'HTML_FORUM_PAGINATION_LIMIT' => 'Argomenti per pagina',
	'HTML_FORUM_PAGINATION_LIMIT_EXPLAIN' => 'Quando l’impaginazione della forum Sitemap è attivata, da qui puoi definire il numero di argomenti visualizzati per pagina.',
	// Contenuti
	'HTML_FORUM_CONTENT' => 'Impostazioni contenuto forum',
	'HTML_FORUM_FIRST' => 'Ordinamento Sitemap',
	'HTML_FORUM_FIRST_EXPLAIN' => 'La forum Sitemap può essere ordinata per data del primo messaggio dell’argomento o per data dell’ultimo messaggio dell’argomento. Questo vuol dire che puoi usare come criterio di ordinamento sia la data di creazione dell’argomento che quella di ultima risposta all’argomento.',
	'HTML_FORUM_NEWS_FIRST' => 'Ordinamento news',
	'HTML_FORUM_NEWS_FIRST_EXPLAIN' => 'La pagina forum news può essere ordinata in base alla data del primo messaggio dell’argomento o in base alla data dell’ultimo messaggio dell’argomento. Questo vuol dire che puoi usare come criterio di ordinamento sia la data di creazione dell’argomento che quella di ultima risposta all’argomento.',
	'HTML_FORUM_LAST_POST' => 'Visualizza l’ultimo messaggio',
	'HTML_FORUM_LAST_POST_EXPLAIN' => 'Visualizza, o no, le informazioni dell’ultimo messaggio degli argomenti in lista.',
	'HTML_FORUM_POST_BUTTONS' => 'Visualizza i pulsanti del messaggio',
	'HTML_FORUM_POST_BUTTONS_EXPLAIN' => 'Visualizza, o no, i pulsanti del messaggio: rispondi, modifica, ecc ...',
	'HTML_FORUM_RULES' => 'Visualizza le regole del forum',
	'HTML_FORUM_RULES_EXPLAIN' => 'Visualizza, o no, le regole del forum nella pagina News e nella Sitemap.',
	'HTML_FORUM_DESC' => 'Visualizza la descrizione delle regole del forum',
	'HTML_FORUM_DESC_EXPLAIN' => 'Visualizza, o no, la descrizione del forum nella pagina forum news e nella Sitemap.',
	// Ripristino impostazioni
	'HTML_FORUM_RESET' => 'Modulo HTML Forum',
	'HTML_FORUM_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni del modulo HTML Forum.',
	'HTML_FORUM_MAIN_RESET' => 'Principale HTML forum',
	'HTML_FORUM_MAIN_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della scheda "Impostazioni HTML" (principale) del modulo HTML Forum.',
	'HTML_FORUM_CONTENT_RESET' => 'HTML Forum news',
	'HTML_FORUM_CONTENT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni News del modulo HTML Forum.',
	'HTML_FORUM_CACHE_RESET' => 'Cache HTML Forum',
	'HTML_FORUM_CACHE_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della cache del modulo HTML Forum.',
	'HTML_FORUM_MODREWRITE_RESET' => 'Riscrittura URL HTML forum',
	'HTML_FORUM_MODREWRITE_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di riscrittura URL del modulo HTML Forum.',
	'HTML_FORUM_GZIP_RESET' => 'Gunzip HTML Forum',
	'HTML_FORUM_GZIP_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di Gunzip del modulo HTML Forum.',
	'HTML_FORUM_LIMIT_RESET' => 'Limiti HTML Forum',
	'HTML_FORUM_LIMIT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni limiti del modulo HTML Forum.',
	'HTML_FORUM_SORT_RESET' => 'Ordinamento HTML Forum',
	'HTML_FORUM_SORT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di ordinamento del modulo HTML Forum.',
	'HTML_FORUM_PAGINATION_RESET' => 'Impaginazione HTML Forum',
	'HTML_FORUM_PAGINATION_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni di impaginazione del modulo HTML Forum.',
));
?>