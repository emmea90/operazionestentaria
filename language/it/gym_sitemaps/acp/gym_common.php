<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-19
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_common [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	// Main
	'ALL' => 'Tutto',
	'MAIN' => 'GYM Sitemap',
	'MAIN_MAIN_RESET' => 'Opzioni principali di GYM Sitemap',
	'MAIN_MAIN_RESET_EXPLAIN' => 'Ripristina tutte le opzioni predefinite di GYM Sitemap.',
	// Linking setup
	'GYM_LINKS_ACTIVATION' => 'Forum con link',
	'GYM_LINKS_MAIN' => 'Link principali',
	'GYM_LINKS_MAIN_EXPLAIN' => 'Visualizza o no i link nella parte bassa della pagina principale di GYM Sitemap: SitemapIndex, i principali Feed RSS e la pagina relativa alla lista dei Feed, la Sitemap principale e la nuova pagina.',
	'GYM_LINKS_INDEX' => 'Link sull’Indice',
	'GYM_LINKS_INDEX_EXPLAIN' => 'Visualizza o no i link delle pagine GYM Sitemap disponibili per ogni forum sull’Indice del Forum. Questi link vengono aggiunti sotto le descrizioni del forum.',
	'GYM_LINKS_CAT' => 'Link nella pagina forum',
	'GYM_LINKS_CAT_EXPLAIN' => 'Visualizza o no i link delle pagine GYM Sitemap disponibili su una pagina del forum. Questi link vengono inseriti sotto il titolo del forum.',
	// Google sitemaps
	'GOOGLE' => 'Google',
	'GOOGLE_URL' => 'Google URL Sitemap',
	// Reset settings
	'GOOGLE_MAIN_RESET' => 'Google Sitemap opzioni principali',
	'GOOGLE_MAIN_RESET_EXPLAIN' => 'Ripristina tutte le opzioni di Google Sitemap ai valori predefiniti.',
	// RSS feeds
	'RSS' => 'RSS',
	'RSS_URL' => 'URL Feed RSS',
	'RSS_ALTERNATE' => 'Link RSS alternativo',
	'RSS_ALTERNATE_EXPLAIN' => 'Visualizza o no i link RSS alternativi nella barra di navigazione dei browser',
	'RSS_LINKING_TYPE' => 'Tipo di link RSS',
	'RSS_LINKING_TYPE_EXPLAIN' => 'Il tipo di Feed da visualizzare tra le pagine del forum.<br />Può essere configurato su:<br /><b>&bull; Feed news con o senza contenuto</b><br />Gli elementi vengono visualizzati in base alla data di creazione, con o senza contenuto.<br /><b>&bull; Feed regolare con o senza contenuto</b><br />Gli elementi vengono visualizzati in base alla data dell’ultima attività, con o senza contenuto.<br />Questo riguarda solo il link visualizzato, non i Feed effettivamente disponibili.',
	'RSS_LINKING_NEWS' => 'Feed news',
	'RSS_LINKING_NEWS_DIGEST' => 'News Feed con contenuto',
	'RSS_LINKING_REGULAR' => 'Feed regolari',
	'RSS_LINKING_REGULAR_DIGEST' => 'Feed regolari con contenuto',
	// Reset settings
	'RSS_MAIN_RESET' => 'Opzioni principali RSS',
	'RSS_MAIN_RESET_EXPLAIN' => 'Ripristina tutte le opzioni RSS ai valori predefiniti.',
	'YAHOO' => 'Yahoo',
	// HTML
	'HTML_MAIN_RESET' => 'Opzioni HTML globali',
	'HTML_MAIN_RESET_EXPLAIN' => 'Ripristina tutte opzioni delle Sitemap HTML ai valori predefiniti.',
	'HTML' => 'HTML',
	'HTML_URL' => 'HTML URL',

	// GYM authorisation array
	'GYM_AUTH_ADMIN' => 'Amministratore',
	'GYM_AUTH_GLOBALMOD' => 'Moderatori globali',
	'GYM_AUTH_REG' => 'Connesso come',
	'GYM_AUTH_GUEST' => 'Ospiti',
	'GYM_AUTH_ALL' => 'Tutto',
	'GYM_AUTH_NONE' => 'Nessuno',
	// XSLT
	'GYM_STYLE' => 'Stile',

	// Cache status
	'SEO_CACHE_FILE_TITLE' => 'Stato cache',
	'SEO_CACHE_STATUS' => 'La cache è configurata su: <b>%s</b>',
	'SEO_CACHE_FOUND' => 'La cache è stata trovata.',
	'SEO_CACHE_NOT_FOUND' => 'La cache non è stata trovata.',
	'SEO_CACHE_WRITABLE' => 'La cache è scrivibile.',
	'SEO_CACHE_UNWRITABLE' => 'La cache <b>non</b> è scrivibile. Imposta i permessi CHMOD della cartella cache a 0777.',

	// Mod Rewrite type
	'ACP_SEO_SIMPLE' => 'Semplice',
	'ACP_SEO_MIXED' => 'Mista',
	'ACP_SEO_ADVANCED' => 'Avanzata',
	'ACP_PHPBB_SEO_VERSION' => 'Versione',
	'ACP_SEO_SUPPORT_FORUM' => 'Forum di supporto',
	'ACP_SEO_RELEASE_THREAD' => 'Tematica disponibile',
	'ACP_SEO_REGISTER_TITLE' => 'Registrati',
	'ACP_SEO_REGISTER_UPDATE' => 'notifiche sugli aggiornamenti',
	'ACP_SEO_REGISTER_MSG' => '%1$s per ricevere %2$s',

	// Maintenance
	'GYM_MAINTENANCE' => 'Manutenzione',
	'GYM_MODULE_MAINTENANCE' => '%1$s manutenzione',
	'GYM_MODULE_MAINTENANCE_EXPLAIN' => 'Qui è possibile gestire i file della cache utilizzati dai moduli %1$s<br />Ci sono due tipi: quello utilizzato per memorizzare i dati in output delle pagine pubbliche, e quello usato per costruire il relativo modulo nel PCA. È possibile eliminare la cache del modulo del PCA, selezionando lo svuotamento della cache; con le impostazioni predefinite si può svuotare la cache dei contenuti per i moduli selezionati.',
	'GYM_CLEAR_CACHE' => 'Svuota la cache %1$s',
	'GYM_CLEAR_CACHE_EXPLAIN' => 'Puoi svuotare la cache attraverso il modulo %1$s. I file memorizzati nella cache contengono i dati utilizzati per costruire il %1$s.<br />Può essere utile se si desidera forzare l’aggiornamento della cache.',
	'GYM_CLEAR_ACP_CACHE' => 'Svuota la cache %1$s dal PCA',
	'GYM_CLEAR_ACP_CACHE_EXPLAIN' => 'Puoi scegliere di cancellare la cache di installazione %1$s dal PCA. I file memorizzati nella cache contengono i dati utilizzati per costruire il %1$s nel PCA.<br />Può essere utile per attivare nuove opzioni che possono essere state aggiunte per i moduli di questo tipo.',
	'GYM_CACHE_CLEARED' => 'Svuota la cache con successo su:',
	'GYM_CACHE_NOT_CLEARED' => 'Si è verificato un errore durante lo svuotamento della cache; controlla i permessi della cartella (CHMOD 0666 o 0777).<br />La cartella attualmente configurata per la cache è la seguente:',
	'GYM_FILE_CLEARED' => 'File eliminato/i:',
	'GYM_CACHE_ACCESSED' => 'La cartella cache è correttamente accessibile, ma i file non sono stati cancellati:',
	'MODULE_CACHE_CLEARED' => 'Il modulo del PCA presente nella cartella cache è stato eliminato con successo; se si è appena caricato un modulo, adesso sarà visualizzato nel PCA.',

	// set defaults
	'GYM_SETTINGS' => 'Impostazioni',
	'GYM_RESET_ALL' => 'Ripristina tutto',
	'GYM_RESET_ALL_EXPLAIN' => 'Se si seleziona questa opzione, tutti i parametri sopra scritti verranno riportati ai valori predefiniti.',
	'GYM_RESET' => 'Ripristina la configurazione %1$s',
	'GYM_RESET_EXPLAIN' => 'Qui di seguito è possibile reimpostare i moduli di configurazione %1$s, o un intero modulo in una sola volta o solo un dato set di configurazione del modulo.',

	'GYM_INSTALL' => 'Installa',
	'GYM_MODULE_INSTALL' => 'Installa il modulo %1$s',
	'GYM_MODULE_INSTALL_EXPLAIN' => 'Qui di seguito puoi attivare / disattivare il modulo %1$s.<br />Se hai appena caricato un modulo, è necessario attivarlo prima di poterlo utilizzare.<br />Se non riesci a vedere il nuovo modulo, prova a svuotare la cache sul PCA nella pagina di manutenzione.',

	// Titles
	'GYM_MAIN' => 'Impostazioni GYM Sitemap',
	'GYM_MAIN_EXPLAIN' => 'Queste sono le impostazioni comuni per tutti i tipi di output e per tutti i moduli.<br />Possono essere applicati a tutti i tipi di output (HTML, RSS, Google Sitemap, Lista URL Yahoo!) e/o per tutti i moduli a seconda delle impostazioni di sovrascrittura.',
	'MAIN_MAIN' => 'Panoramica GYM Sitemap',
	'MAIN_MAIN_EXPLAIN' => 'GYM Sitemap è un modulo molto flessibile per ottimizzare phpBB nei motori di ricerca. Essa ti permetterà di costruire la Google Sitemap, i Feed RSS 2.0, gli elenchi degli URL di Yahoo e la Sitemap HTML per il tuo forum così come per qualsiasi parte del tuo sito web, grazie alla sua struttura modulare.<br /><br /> Ogni tipo (Google, RSS, HTML e Yahoo) può utilizzare oggetti nella lista delle diverse applicazioni installate nel tuo sito (forum, album, ecc...) utilizzando un modulo dedicato.<br />È possibile attivare / disattivare i moduli utilizzando i link per installarli nel PCA; ogni modulo ha le proprie pagine di configurazione.<br /><br />Assicurati di controllare il supporto %1$s che viene fornito sul %2$s.<br />Il Supporto generale SEO e le relative discussioni li puoi trovare su	%3$s.<br />%4$s.<br />',

	'GYM_GOOGLE' => 'Google Sitemap',
	'GYM_GOOGLE_EXPLAIN' => 'Queste sono le impostazioni comuni per tutti i moduli di Google Sitemap (forum, personalizzazioni, ecc. ...). <br />Possono essere applicati a tutti i moduli di Google Sitemap a seconda delle principali impostazioni di sovrascrittura per questo tipo di output.',
	'GYM_RSS' => 'Feed RSS',
	'GYM_RSS_EXPLAIN' => 'Queste sono le impostazioni comuni per tutti i  moduli Feed RSS (forum, personalizzazioni, ecc. ...).<br />Possono essere applicati a tutti i moduli Feed RSS a seconda delle principali impostazioni di sovrascrittura per questo tipo di output.',
	'GYM_HTML' => 'Pagine HTML',
	'GYM_HTML_EXPLAIN' => 'Queste sono le impostazioni comuni per tutti i moduli HTML (forum, personalizzazioni, ecc. ...).<br />Possono essere applicati a tutti i moduli HTML a seconda delle principali impostazioni di sovrascrittuta per questo tipo di output.',
	'GYM_MODULES_INSTALLED' => 'Moduli attivi',
	'GYM_MODULES_UNINSTALLED' => 'Moduli non attivi',

	// Overrides
	'GYM_OVERRIDE_GLOBAL' => 'Globale',
	'GYM_OVERRIDE_OTYPE' => 'Tipo di output',
	'GYM_OVERRIDE_MODULE' => 'Modulo',

	// override messages
	'GYM_OVERRIDED_GLOBAL' => 'Questa opzione è attualmente sottoposta a sovrascrittura al livello superiore (configurazione principale)',
	'GYM_OVERRIDED_OTYPE' => 'Questa opzione è attualmente sottoposta a sovrascrittura in base al tipo di livello.',
	'GYM_OVERRIDED_MODULE' => 'Questa opzione è attualmente sottoposta a sovrascrittura a livello di modulo',
	'GYM_OVERRIDED_VALUE' => 'Il valore attualmente preso in considerazione è il seguente: ',
	'GYM_OVERRIDED_VALUE_NOTHING' => 'Nessuno',
	'GYM_COULD_OVERRIDE' => 'Questa opzione potrebbe essere sovrascritta, ma attualmente non lo è.',

	// Overridable / common options
	'GYM_CACHE' => 'Cache',
	'GYM_CACHE_EXPLAIN' => 'Qui è possibile impostare diverse opzioni relative alla cache. Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.',
	'GYM_MOD_SINCE' => 'Attiva modificato dal',
	'GYM_MOD_SINCE_EXPLAIN' => 'Il modulo chiederà al browser se ha già una versione aggiornata della pagina nella cache prima di reinviare il contenuto.<br /><b>NOTA:</b> Questa opzione riguarderà tutti i tipi di output.',
	'GYM_CACHE_ON' => 'Attiva la cache',
	'GYM_CACHE_ON_EXPLAIN' => 'Tu puoi attivare / disattivare la memorizzazione nella cache per questo modulo.',
	'GYM_CACHE_FORCE_GZIP' => 'Forza la compressione della cache',
	'GYM_CACHE_FORCE_GZIP_EXPLAIN' => 'Ti permette di forzare la compressione gzip per i file memorizzati nella cache indipendentemente dalla configurazione di gunzip. Questo può aiutare un po’ se ti manca spazio web, ma comporterà un po’ più di lavoro per il server per decomprimere i file prima di inviarli al browser.',
	'GYM_CACHE_MAX_AGE' => 'Durata della cache',
	'GYM_CACHE_MAX_AGE_EXPLAIN' => 'Numero massimo di ore che indica quanto un file di cache sarà utilizzato prima che possa essere aggiornato. Ogni file di cache verrà aggiornato ogni volta che qualcuno lo visualizza se la durata stabilita è stata superata attraverso la rigenerazione automatica della funzione. In caso contrario, la cache verrà aggiornata solo su richiesta avviata dal PCA.',
	'GYM_CACHE_AUTO_REGEN' => 'Aggiornamento automatico della cache',
	'GYM_CACHE_AUTO_REGEN_EXPLAIN' => 'Se si attiva l’aggiornamento automatico della cache, gli elenchi in output saranno aggiornati una volta che la cache è scaduta; in caso contrario, si dovranno cancellare manualmente i file memorizzati nella cache attraverso il link di manutenzione qui sopra per vedere i nuovi URL nei vostri elenchi.',
	'GYM_SHOWSTATS' => 'Statistiche della cache',
	'GYM_SHOWSTATS_EXPLAIN' => 'Output o non generazione delle statistiche nel codice sorgente.<br /><b>NOTA:</b> La durata è il tempo necessario per creare la pagina. Questo passaggio non è ripetuto quando si scrive dalla cache.',
	'GYM_CRITP_CACHE' => 'Codifica i nomi dei file di cache',
	'GYM_CRITP_CACHE_EXPLAIN' => 'Crittografa i nomi dei file della cache o no. È più sicuro per conservare la cache utilizzare i nomi dei file crittografati, ma può essere utile per verificare i nomi dei file in chiaro per il debug.<br /><b>NOTA:</b> Questa opzione riguarderà tutti i tipi di file memorizzati nella cache.',

	'GYM_MODREWRITE' => 'Riscrittura URL',
	'GYM_MODREWRITE_EXPLAIN' => 'Qui è possibile impostare le varie riscritture degli URL. Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.',
	'GYM_MODREWRITE_ON' => 'Attiva la riscrittura degli URL',
	'GYM_MODREWRITE_ON_EXPLAIN' => 'Questo attiva la riscrittura degli URL per i link del modulo.<br /><b>NOTA:</b> Tu DEVI utilizzare un server Apache con il modulo mod_rewrite attivo o un server IIS che esegue il modulo isapi_rewrite E inoltre impostare correttamente le regole di riscrittura del modulo nel file .htaccess (o httpd.ini con IIS).',
	'GYM_ZERO_DUPE_ON' => 'Attiva Zero duplicati',
	'GYM_ZERO_DUPE_ON_EXPLAIN' => 'Questo attiva Zero duplicati nel modulo dei link.<br /><b>NOTA:</b> I reindirizzamenti possono avvenire soltanto quando la cache viene (ri)generata.',
	'GYM_MODRTYPE' => 'Tipo di riscrittura degli URL',
	'GYM_MODRTYPE_EXPLAIN' => 'Queste opzioni vengono sovrascritte dall’utilizzo della MOD phpBB SEO rewrite (rilevazione automatica).<br />Quattro tipi di riscrittura degli URL possono essere impostati: Nessuna, Semplice, Mista e Avanzata:<br /><ul><li><b>Nessuna:</b> Nessuna riscrittura degli URL;<br /></li><li><b>Semplice:</b> Riscrittura degli URL statici per tutti i link, con nessun riferimento al titolo;<br /></li><li><b>Mista:</b> I titolo dei forum e delle categorie vengono inseriti negli URL, ma i titoli degli argomenti rimangono scritti in modo statico;<br /></li><li><b>Avanzata:</b> Tutti i titoli sono inseriti in tutti gli URL.</li></ul>',

	'GYM_GZIP' => 'GUNZIP',
	'GYM_GZIP_EXPLAIN' => 'Qui puoi impostare varie opzioni di gunzip. Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.%1$s',
	'GYM_GZIP_FORCED' => '<br /><b style="color:red;">NOTA:</b> Le compressioni Gun-Zip sono state attivate configurando il phpBB. Possono dunque essere modificate nel modulo apposito.',
	'GYM_GZIP_CONFIGURABLE' => '<br /><b style="color:red;">NOTA:</b> Le compressioni Gun-Zip non sono state attivate configurando il phpBB. Puoi impostare le opzioni che seguono come ritieni.',
	'GYM_GZIP_ON' => 'Attiva gunzip',
	'GYM_GZIP_ON_EXPLAIN' => 'Questo attiva la compressione gunzip in output. Questo può ridurre significativamente la quantità di dati trasmessi al browser, e quindi il tempo necessario per trasmettere il contenuto.',
	'GYM_GZIP_EXT' => 'Suffisso Gunzip',
	'GYM_GZIP_EXT_EXPLAIN' => 'Da qui è possibile scegliere di utilizzare o no il suffisso .gz nel modulo degli URL. Questo vale solo quando gunzip e la riscrittura degli URL vengono attivati.',
	'GYM_GZIP_LEVEL' => 'Livello di compressione Gunzip',
	'GYM_GZIP_LEVEL_EXPLAIN' => 'Il numero intero compreso tra 1 e 9, dove 9, indica maggiore compressione. Di solito non è il caso di andare oltre 6.<br /><b>NOTA:</b> Questa opzione riguarderà tutti i tipi di output.',

	'GYM_LIMIT' => 'Limiti',
	'GYM_LIMIT_EXPLAIN' => 'Qui puoi impostare il limite da applicare quando si costruisce l’output: il numero degli URL generati, il ciclo SQL (numero di query) e l’età degli elementi elencati.<br />Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.',
	'GYM_URL_LIMIT' => 'Limite elementi',
	'GYM_URL_LIMIT_EXPLAIN' => 'Il numero massimo di elementi per l’output.',
	'GYM_SQL_LIMIT' => 'Ciclo SQL',
	'GYM_SQL_LIMIT_EXPLAIN' => 'Per tutti i tipi di output, ad eccezione dell’HTML, le query SQL vengono suddivise in più parti per essere in grado di elencare grandi quantità di elementi senza dovere così eseguire query troppo pesanti.<br />Definisci qui il numero della voce per eseguire la query una sola volta. Il numero di query SQL sarà il numero di elementi elencati divisi da questo ciclo.',
	'GYM_TIME_LIMIT' => 'Limiti di tempo',
	'GYM_TIME_LIMIT_EXPLAIN' => 'Limite di giorni. L’età massima degli elementi presi in considerazione durante la creazione delle liste. Può essere molto utile per ridurre il carico del server su grandi banche di dati. Inserisci 0 per non avere nessun limite.',

	'GYM_SORT' => 'Ordinamento',
	'GYM_SORT_EXPLAIN' => 'Qui è possibile scegliere come ordinare l’output.<br />Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.',
	'GYM_SORT_TYPE' => 'Ordinamento predefinito',
	'GYM_SORT_TYPE_EXPLAIN' => 'Tutti i link in output sono ordinati in modo predefinito in base all’ultima attività (Decrescente).<br />Qui puoi impostare questo valore in modo Crescente, ad esempio se vuoi rendere più facile per i motori di ricerca trovare i link relativi a contenuti vecchi.<br />Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.',

	'GYM_PAGINATION' => 'Impaginazione',
	'GYM_PAGINATION_EXPLAIN' => 'Qui è possibile impostare diverse opzioni di impaginazione. Ricorda che queste impostazioni possono essere sovrascritte a seconda delle impostazioni di sovrascrittura.',
	'GYM_PAGINATION_ON' => 'Attiva l’impaginazione',
	'GYM_PAGINATION_ON_EXPLAIN' => 'Da qui puoi decidere l’impaginazione dei link di output (ove disponibili) per gli elementi elencati. Per esempio, il modulo può determinare i link delle pagine in output relativi agli argomenti dei forum.',
	'GYM_LIMITDOWN' => 'Limite minimo dell’impaginazione',
	'GYM_LIMITDOWN_EXPLAIN' => 'Indica il numero delle pagine impaginate, a partire dalla prima pagina, in output.',
	'GYM_LIMITUP' => 'Limite massimo dell’impaginazione',
	'GYM_LIMITUP_EXPLAIN' => 'Indica il numero delle pagine impaginate, a partire dall’ultima pagina, in output.',

	'GYM_OVERRIDE' => 'Sovrascrittura',
	'GYM_OVERRIDE_EXPLAIN' => 'GYM Sitemap è completamente modulare. Ogni tipo di output (Google, RSS ...) utilizza la produzione di moduli corrispondenti al tipo di elemento della lista. Per esempio, il primo modulo per ogni tipo di output è il modulo forum che elenca gli elementi dal forum.<br />Molte opzioni, come la riscrittura degli URL, la cache, la compressione gunzip, ecc. ... sono ripetute su più livelli della GYM Sitemap nel PCA. Questo consente di utilizzare impostazioni diverse per la stessa opzione a seconda del tipo di output e a seconda del tipo di modulo di output. Ma può accadere che si preferisce, ad esempio, attivare la riscrittura degli URL su tutto il modulo di GYM Sitemap con una sola azione (tutti i tipi di output su tutti i moduli).<br />Questo è ciò che l’impostazione "Sovrascrittura" ti permetterà di fare per molti tipi di impostazioni.<br />Il processo di successione va dal livello più alto di impostazioni (Configurazione principale) al tipo di livello di output (Google, RSS ...) e termina al livello più basso: i moduli di output (forum, album ...)<br />Le impostazioni primarie possono assumere tre valori:<br /><ul><li><b>Globale:</b> Saranno utilizzate le impostazioni principali;<br /></li><li><b>Tipo di output:</b> Le impostazioni del tipo di output verranno utilizzate per i suoi moduli;<br /></li><li><b>Modulo:</b> Sarà utilizzato il valore più basso a disposizione, ad esempio, il primo modulo, e se non impostato, sarà utilizzato il tipo di output e così via fino all’impostazione globale, se disponibile.</li></ul>',
	'GYM_OVERRIDE_ON' => 'Attiva la riscrittura principale',
	'GYM_OVERRIDE_ON_EXPLAIN' => 'Tu puoi attivare / disattivare la riscrittura principale. La disattivazione è la stessa di tutte le sovrascritture a "modulo", che ignora le impostazioni del tipo di output per impostare la riscrittura del modulo.',
	'GYM_OVERRIDE_MAIN' => 'Sovrascrittura predefinita',
	'GYM_OVERRIDE_MAIN_EXPLAIN' => 'Impostare il livello di sovrascrittura per gli altri tipi di impostazioni che potrebbero utilizzare un modulo.',
	'GYM_OVERRIDE_CACHE' => 'Sovrascrittura cache',
	'GYM_OVERRIDE_CACHE_EXPLAIN' => 'Livello di sovrascrittura per impostare le opzioni di memorizzazione nella cache.',
	'GYM_OVERRIDE_GZIP' => 'Sovrascrittura Gunzip',
	'GYM_OVERRIDE_GZIP_EXPLAIN' => 'Livello di sovrascrittura per  impostare le opzioni di gunzip.',
	'GYM_OVERRIDE_MODREWRITE' => 'Sostituzione di riscrittura degli URL',
	'GYM_OVERRIDE_MODREWRITE_EXPLAIN' => 'Livello di sovrascrittura per impostare le opzioni di riscrittura degli URL.',
	'GYM_OVERRIDE_LIMIT' => 'Limite di sovrascrittura',
	'GYM_OVERRIDE_LIMIT_EXPLAIN' => 'Livello di sovrascrittura per impostare le opzioni relative al limite.',
	'GYM_OVERRIDE_PAGINATION' => 'Impaginazione sovrascrittura',
	'GYM_OVERRIDE_PAGINATION_EXPLAIN' => 'Livello di sovrascrittura per per impostare le opzioni per l’impaginazione.',
	'GYM_OVERRIDE_SORT' => 'Ordinamento sovrascrittura',
	'GYM_OVERRIDE_SORT_EXPLAIN' => 'Livello di sovrascrittura per impostare le opzioni di ordinamento.',

	// Mod rewrite
	'GYM_MODREWRITE_ADVANCED' => 'Avanzata',
	'GYM_MODREWRITE_MIXED' => 'Mista',
	'GYM_MODREWRITE_SIMPLE' => 'Semplice',
	'GYM_MODREWRITE_NONE' => 'Nessuna',

	// Sorting
	'GYM_ASC' => 'Crescente',
	'GYM_DESC' => 'Decrescente',

	// Other
	// robots.txt
	'GYM_CHECK_ROBOTS' => 'Controlla i disallow nel robots.txt',
	'GYM_CHECK_ROBOTS_EXPLAIN' => 'Controlla e applica le regole del robots.txt (se presente) all’elenco degli URL. Il modulo riconoscerà automaticamente gli aggiornamenti del robots.txt.<br />Questa opzione è molto utile per l’importazione XML e TXT, quando non si può essere sicuri della consistenza della lista degli URL.<br /><br /><b>Nota</b>:<br />Questa opzione implica più lavoro sul file sorgente, si raccomanda di usarlo quando è attivata la memorizzazione nella cache.',
	// summarize method
	'GYM_METHOD_CHARS' => 'Da caratteri',
	'GYM_METHOD_WORDS' => 'Da parole',
	'GYM_METHOD_LINES' => 'Da righe',

	// script location checking
	'GYM_WRONG_PHPBB_URL' => 'Le impostazioni del server di phpBB non sono corrette. Devi configurare correttamente il tuo <a href="%1$s"><b>Impostazioni del server URL</b></a>.<br /><a href="http://www.phpbb-seo.com/en/phpbb-forum/server-and-cookie-settings-t4451.html" onclick ="window.open(this.href); return false;">Ulteriori informazioni</a>',
	'GYM_WRONG_SCRIPT_URL' => 'L’URL configurato per la modalità <b>%1$s</b> è scorretto. Si deve portare dove si trova il file <b>%2$s</b> sul server.',
	'GYM_WRONG_SCRIPT_DOMAIN' => 'L’URL configurato per la modalità <b>%1$s</b> non è coerente con il phpBB. L’URL deve utilizzare lo stesso dominio o sotto-dominio e lo stesso protocollo (http:// or https://) come phpBB. Esso deve contenere <b>%2$s</b> e portare alla cartella contenente <b>%3$s</b> sul server.',
	'GYM_WRONG_SCRIPT2_PHPBB' => 'Anche se l’URL configurato per la modalità <b>%1$s</b> è corretto, il percorso di phpBB ($phpbb_root_path) situato su <b>%2$s%3$s</b> è scorretto. Questo può avvenire solo quando $phpbb_root_path è stato modificato manualmente e / o il file è stato spostato.<br />Nota che $phpbb_root_path deve essere in un percorso relativo valido per la cartella <b>%3$s</b> dove è installato phpBB, e che deve iniziare con "./".<br />In base alle impostazioni, $phpbb_root_path deve essere impostato su <b>%4$s</b> su <b>%3$s</b>.',
	'GYM_WRONG_SITEMAP_LOCATION' => 'Il tuo file <b>sitemap.php</b> si trova in una posizione da cui non può essere utilizzato per elencare gli URL dei forum.<br />Esso deve essere posto <b>all’interno o all’esterno</b> della cartella di phpBB.<br />Posizione configurata: <b>%1$s</b><br />Ubicazione di phpBB: <b>%2$s</b>',
	'GYM_GO_CONFIG_SCRIPT_URL' => 'Vai a correggere la configurazione: <a href="%2$s"><b>%1$s</b></a>',
));
?>
