<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-20
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-20
*
*/
/**
*
* gym_rss [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'RSS_MAIN' => 'Impostazioni Feed RSS',
	'RSS_MAIN_EXPLAIN' => 'Queste sono le impostazioni principali per il modulo Feed RSS.<br />Possono essere applicate a tutti i moduli RSS 
	a seconda delle impostazioni di sovrascrittura degli RSS.',
	// Linking setup
	'RSS_LINKS_ACTIVATION' => 'Forum link',
	'RSS_LINKS_MAIN' => 'Link principali',
	'RSS_LINKS_MAIN_EXPLAIN' => 'Visualizza o no la lista dei link RSS nella parte bassa della pagina.<br />Questa funzione richiede che la visualizzazione dei link principali sia attivata nella configurazione principale.',
	'RSS_LINKS_INDEX' => 'Link nell’Indice',
	'RSS_LINKS_INDEX_EXPLAIN' => 'Visualizza o no i link dei Feed RSS disponibili per ogni forum sull’Indice del forum. Questi link vengono aggiunti sotto le descrizioni dei forum.<br />Questa funzionalità richiede che i link nell’Indice siano attivati nella configurazione principale.', 
	'RSS_LINKS_CAT' => 'Link sulla pagina del forum',
	'RSS_LINKS_CAT_EXPLAIN' => 'Visualizza o no i link dei Feed RSS del forum corrente. Questi link sono inseriti sotto il titolo del forum. <br />Questa funzionalità richiede che i link della pagina del forum siano attivati nella configurazione principale.',
	// Reset settings
	'RSS_ALL_RESET' => 'Tutti i moduli RSS',
	// Limits
	'RSS_LIMIT_GEN' => 'Limiti principali',
	'RSS_LIMIT_SPEC' => 'Limiti RSS',
	'RSS_URL_LIMIT_LONG' => 'Limite di lunghezza dei Feed',
	'RSS_URL_LIMIT_LONG_EXPLAIN' => 'Il numero degli elementi visualizzati in un Feed lungo senza contenuto, richiede che l’opzione Abilita i Feed lunghi sia attivata.',
	'RSS_SQL_LIMIT_LONG' => 'Ciclo di SQL lungo',
	'RSS_SQL_LIMIT_LONG_EXPLAIN' => 'Numero di elementi richiesti in un momento per un Feed lungo senza contenuto.',
	'RSS_URL_LIMIT_SHORT' => 'Limite Feed corti',
	'RSS_URL_LIMIT_SHORT_EXPLAIN' => 'Il numero degli elementi visualizzati in un Feed corto senza contenuto, richiede che l’opzione Abilita i Feed corti sia attivata.',
	'RSS_SQL_LIMIT_SHORT' => 'Ciclo di SQL corto',
	'RSS_SQL_LIMIT_SHORT_EXPLAIN' => 'Numero degli elementi richiesti in un momento per un Feed corto senza contenuto.',
	'RSS_URL_LIMIT_MSG' => 'Limite predefinito con il contenuto',
	'RSS_URL_LIMIT_MSG_EXPLAIN' => 'Numero degli elementi visualizzati per impostazione predefinita nei Feed con contenuti, richiede l’opzione Abilita l’elemento contenuto sia attiva.',
	'RSS_SQL_LIMIT_MSG' => 'Ciclo di SQL con il contenuto',
	'RSS_SQL_LIMIT_MSG_EXPLAIN' => 'Numero di elementi richiesti in un momento per un Feed con contenuto.',
	// Basic settings
	'RSS_SETTINGS' => 'Impostazioni di base',
	'RSS_C_INFO' => 'Informazioni copyright',
	'RSS_C_INFO_EXPLAIN' => 'Le informazioni di copyright da visualizzare nel tag di copyright dei Feed RSS. Il valore predefinito è il nome del sito phpBB.',
	'RSS_SITENAME' => 'Nome del sito',
	'RSS_SITENAME_EXPLAIN' => 'Il nome del sito da mostrare nei Feed RSS. Il valore predefinito è il nome del sito phpBB.',
	'RSS_SITE_DESC' => 'Descrizione del sito',
	'RSS_SITE_DESC_EXPLAIN' => 'La descrizione del sito da mostrare nei Feed RSS. Il valore predefinito è la descrizione del sito phpBB.',
	'RSS_LOGO_URL' => 'Logo del sito',
	'RSS_LOGO_URL_EXPLAIN' => 'Il file di immagine da utilizzare come logo del sito nei Feed RSS, nella cartella gym_sitemaps/images/.',
	'RSS_IMAGE_URL' => 'Logo RSS',
	'RSS_IMAGE_URL_EXPLAIN' => 'Il file di immagine da utilizzare come logo RSS nei Feed RSS, nella cartella gym_sitemaps/images/.',
	'RSS_LANG' => 'Lingua RSS',
	'RSS_LANG_EXPLAIN' => 'La lingua da dichiarare come lingua principale nei Feed RSS. La lingua predefinita è quella del phpBB.',
	'RSS_URL_EXPLAIN' => 'Inserisci l’URL completo del file gymrss.php, ad esempio http://www.esempio.it/cartella_forum/ se gymrss.php è installato su http://www.esempio.it/cartella_forum/...<br />Questa opzione è utile quando phpBB non è installato nella cartella principale del dominio e si desidera inserire il file gymrss.php nella root principale.',
	// Auth settings
	'RSS_AUTH_SETTINGS' => 'Autorizzazione impostazioni',
	'RSS_ALLOW_AUTH' => 'Autorizzazioni',
	'RSS_ALLOW_AUTH_EXPLAIN' => 'Attiva le autorizzazioni per i Feed RSS. Se attivate, gli utenti registrati saranno in grado di navigare nei Feed privati e di visualizzare gli argomenti dei forum privati nei Feed generali se hanno le corrette autorizzazioni.', 
	'RSS_CACHE_AUTH' => 'Cache Feed privati',
	'RSS_CACHE_AUTH_EXPLAIN' => 'È possibile disattivare la cache per i Feed non pubblici quando consentito.<br />La cache dei Feed privati aumenterà il numero di file nella cache; non dovrebbe essere un problema, ma qui si può regolare solo la cache dei Feed pubblici.',
	'RSS_NEWS_UPDATE' => 'Aggiornamento Feed news',
	'RSS_NEWS_UPDATE_EXPLAIN' => 'Quando vengono attivati i Feed news, è possibile impostare un tempo personalizzato di durata in ore per tutti i Feed news. Utilizza 0 o lascia vuoto per disattivare e usare invece tutta la durata regolare dell’aggiornamento.',
	'RSS_ALLOW_NEWS' => 'Abilita i Feed news',
	'RSS_ALLOW_NEWS_EXPLAIN' => 'I Feed news sono così chiamati a motivo della personalizzazione del primo elemento che viene elencato senza considerare le successive risposte. Si tratta di Feed supplementari che non interferiscono con gli altri. Sono utili se, ad esempio, vuoi condividere i Feed del tuo Forum su Google News.',
	'RSS_ALLOW_SHORT' => 'Abilita i Feed corti',
	'RSS_ALLOW_SHORT_EXPLAIN' => 'Abilita o no l’utilizzo di Feed RSS corti.',
	'RSS_ALLOW_LONG' => 'Abilita i Feed lunghi',
	'RSS_ALLOW_LONG_EXPLAIN' => 'Abilita o no i Feed RSS lunghi',
	// Notifications
	'RSS_NOTIFY' => 'Notifiche',
	'RSS_YAHOO_NOTIFY' => 'Notifiche Yahoo!',
	'RSS_YAHOO_NOTIFY_EXPLAIN' => 'Attiva le Notifiche Yahoo! per i Feed RSS.<br />Questo non riguarda i Feed generali (RSS.xml).<br />Ogni volta che la cache dei Feed viene aggiornata, una notifica sarà inviata su Yahoo!<br /><b>NOTA:</b> Tu DEVI inserire il tuo Yahoo! AppID di seguito per inviare la notifica.',
	'RSS_YAHOO_APPID' => 'Yahoo! AppID',
	'RSS_YAHOO_APPID_EXPLAIN' => 'Inserisci il tuo AppID Yahoo!. Se non ne hai ancora uno, visita <a href="http://api.search.yahoo.com/webservices/register_application">questa pagina</a>.<br /><b>NOTA:</b> Devi registrarti su Yahoo! per poter ottenere un AppID Yahoo!.',
	// Styling
	'RSS_STYLE' => 'Stile RSS',
	'RSS_XSLT' => 'Stile XSLT',
	'RSS_XSLT_EXPLAIN' => 'I Feed RSS possono essere generati utilizzando il foglio di stile <a href="http://www.w3schools.com/xsl/xsl_transformation.asp">Trasformazione XSL</a>.',
	'RSS_FORCE_XSLT' => 'Forza la stilizzazione',
	'RSS_FORCE_XSLT_EXPLAIN' => 'Paradossalmente dobbiamo alterare la lettura del browser per consentire l’utilizzo di XLST. Lo facciamo aggiungendo alcuni caratteri di spazio all’inizio del codice XML. <br />FF 2 e IE7 leggono solo i primi 500 caratteri per decidere se è un RSS o no e imporre così la loro gestione interna',
	'RSS_LOAD_PHPBB_CSS' => 'Carica il CSS del phpBB',
	'RSS_LOAD_PHPBB_CSS_EXPLAIN' => 'Il modulo GYM Sitemap utilizza pienamente il potente sistema di template del phpBB3. I fogli di stile XSL utilizzati per costruire l’output HTML sono compatibili con gli stili del phpBB3.<br />Con questa opzione, si può decidere di applicare il CSS del phpBB sul foglio di stile XSL al posto di quello standard. In questo modo, tutte le tue personalizzazioni a tema come colore di sfondo e font o immagini saranno utilizzate anche nella produzione dello stile RSS.<br />Queste avranno effetto solo dopo che avrai eliminato la cache RSS dal menu "Manutenzione"...<br /> Se i file di stile RSS non sono presenti nello stile attuale, lo stile predefinito (sempre disponibile, basato su prosilver) sarà utilizzato.<br />Non cercare di usare modelli prosilver con un altro stile; è probabile che i CSS non corrispondano.',
	// Content
	'RSS_CONTENT' => 'Impostazione contenuto',
	'RSS_CONTENT_EXPLAIN' => 'Qui è possibile impostare diverse opzioni di filtraggio/formattazione dei contenuti.<br />Possono essere applicati a tutti i moduli Feed a seconda delle priorità sulle impostazioni RSS.',
	'RSS_ALLOW_CONTENT' => 'Abilita il contenuto dell’elemento',
	'RSS_ALLOW_CONTENT_EXPLAIN' => 'Qui puoi scegliere di consentire se il contenuto del messaggio sarà completamente o parzialmente visualizzato nel Feed RSS.<br /><b>NOTA:</b> Questa opzione, procurerà un po’ più di lavoro per il server. I limiti con l’uscita del contenuto devono essere inferiori a quelli senza il contenuto medesimo.',
	'RSS_SUMARIZE' => 'Riassunto degli elementi',
	'RSS_SUMARIZE_EXPLAIN' => 'È possibile riassumere il contenuto del messaggio inserito nel Feed.<br /> Il limite imposta la quantità massima di frasi, parole o caratteri, in base al metodo scelto qui di seguito. Inserisci 0 per tutti gli output relativi a questo.',
	'RSS_SUMARIZE_METHOD' => 'Metodo del riassunto',
	'RSS_SUMARIZE_METHOD_EXPLAIN' => 'È possibile scegliere fra tre diversi metodi per limitare il contenuto del messaggio nei Feed. <br />Per numero di righe, per numero di parole e numero di caratteri. I tag del BBCode e le parole non saranno suddivisi.',
	'RSS_ALLOW_PROFILE' => 'Mostra profili',
	'RSS_ALLOW_PROFILE_EXPLAIN' => 'Il nome dell’autore dell’elemento  può essere aggiunto ai Feed RSS, se si desidera.',
	'RSS_ALLOW_PROFILE_LINKS' => 'Link del profilo',
	'RSS_ALLOW_PROFILE_LINKS_EXPLAIN' => 'Se il nome dell’autore è incluso in output, si può decidere di collegarlo o meno alla corrispondente pagina del profilo del phpBB.',
	'RSS_ALLOW_BBCODE' => 'Consenti BBcode',
	'RSS_ALLOW_BBCODE_EXPLAIN' => 'Si può scegliere qui di analizzare, inviare o di omettere il BBcode.',
	'RSS_STRIP_BBCODE' => 'Elenco BBcode',
	'RSS_STRIP_BBCODE_EXPLAIN' => ' Da qui è possibile impostare un elenco di BBcode da escludere dall’analisi sintattica.<br />Il formato è semplice:<br /> <ul><li><b>elenco separato da virgole di BBcode:</b> Elimina i tag BBcode e mantiene il contenuto.<br /><b>Esempio:</b> <b>img,b,quote</b><br />In questo esempio, i BBCode immagine, grassetto e citazione non saranno analizzati, i tag BBcode stessi saranno eliminati e il contenuto all’interno dei tag BBcode sarà mantenuto.</li><li> <b>Elenco separato da virgole di BBcode con l’opzione due punti::</b> Elimina i tag BBcode e decidi circa il loro contenuto.<br/><b>Esempio::</b> <b>img: 1, b:0, quote, code: 1</b><br /> In questo esempio, il BBcode img e il link immagine verranno eliminati, il grassetto non sarà analizzato, ma il testo in grassetto sarà mantenuto, la citazione non verrà analizzata, ma il suo contenuto sarà conservato, il codice BBcode e il suo contenuto verrà eliminato dall’output.</ul> Il filtro funziona anche se il BBcode è vuoto. Utile per cancellare il codice contenuto dei tag e il link img in output per esempio<br /> Il filtraggio avviene prima che si riassuma.<br /> Il parametro Magic "tutti" (può essere tutti: 0 o tutti: 1 con l’elenco BBcode contenuto dei tag, naturalmente) si prenderà cura di tutti con un’unica azione.',
	'RSS_ALLOW_LINKS' => 'Abilita link attivi',
	'RSS_ALLOW_LINKS_EXPLAIN' => 'Si può scegliere qui di attivare o no i link utilizzati nelle voci del contenuto.<br /> Se disattivati, i link relativi alle e-mail saranno inclusi nel contenuto, ma non saranno cliccabili.',
	'RSS_ALLOW_EMAILS' => 'Abilita email',
	'RSS_ALLOW_EMAILS_EXPLAIN' => 'Si è scelto qui in output “e-mail del dominio DOT com” in luogo di “email@domain.com” nel contenuto degli elementi.',
	'RSS_ALLOW_SMILIES' => 'Abilita Emoticon',
	'RSS_ALLOW_SMILIES_EXPLAIN' => 'Si può scegliere qui se processare o ignorare le emoticon nei contenuti.',
	'RSS_NOHTML' => 'Filtro HTML',
	'RSS_NOHTML_EXPLAIN' => 'Filtro l’HTML, o no, nei Feed RSS. Se attivi questa opzione, i Feed RSS conterranno solo testo normale.',
	// Old URL handling
	'RSS_1XREDIR' => 'Regola GYM 1x per la riscrittura degli URL',
	'RSS_1XREDIR_EXPLAIN' => 'Attiva GYM 1x per rilevare gli URL riscritti. Il modulo visualizzerà un Feed personalizzato che fornisce il nuovo URL del Feed richiesto.<br /><b>Nota: </b><br /> Questa opzione richiede la compatibilità di riscrittura come spiegato nel file di installazione.',
));
?>