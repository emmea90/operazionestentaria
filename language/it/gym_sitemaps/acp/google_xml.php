<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-17
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* google_xml [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_XML' => 'Sitemap XML',
	'GOOGLE_XML_EXPLAIN' => 'Questi sono i parametri per il modulo Sitemap XML di Google. Puoi integrare completamente l’elenco degli URL da un file XML (un URL per riga) in GYM Sitemap e sfruttare tutte le funzionalità del modulo come stile e come cache XSLT.<br /> Alcune impostazioni possono essere sovrascritte a seconda della Google Sitemap e a seconda delle impostazioni principali di sovrascrittura.<br /> Ogni file XML aggiunto nella cartella gym_sitemaps/sources/ sarà preso in considerazione non appena avrete eliminato la cache del modulo PCA, utilizzando il link  di manutenzione qui sopra.<br /> Ogni elenco di URL di file XML deve essere composto da un URL completo per riga e dovrà seguire un modello di base per la denominazione del file: <b>google_</b>xml_file_name<b>.xml</b>.<br /> Una voce sarà creata nella SitemapIndex con URL <b>esempio.com/sitemap.php?xml=xml_file_nome</b> o <b>esempio.com/xml-xml_file_nome.xml</b> quando l’URL viene riscritto.<br /> Il nome del file sorgente deve necessariamente utilizzare caratteri alfanumerici (0-9a-z) più entrambi i separatori “_”  e  “-”.<p> Tu puoi anche utilizzare un URL completo della Sitemap generato da un’applicazione esterna, configurando il file gym_sitemaps/sources/xml_google_external.php (Leggi i commenti nel file per altri dettagli).</p><b style="color:red;">Nota:</b><br /> Si consiglia di memorizzzare la cache di questo modulo Sitemap per evitare inutili scansioni di file XML potenzialmente grandi.',
 	// Main
	'GOOGLE_XML_CONFIG' => 'Impostazioni Sitemap XML',
	'GOOGLE_XML_CONFIG_EXPLAIN' => 'Alcune impostazioni possono essere sovrascritte a seconda delle principali impostazioni di sovrascrittura di Google Sitemap.',
	'GOOGLE_XML_RANDOMIZE' => 'Rendi casuale',
	'GOOGLE_XML_RANDOMIZE_EXPLAIN' => 'È possibile mettere in ordine causale gli URL estratti dal file XML. Modificare l’ordine su base regolare può aiutare la scansione per un po’. Questa opzione è anche utile per esempio quando si vogliano limitare gli URL a 1000 per questo modulo e utilizzare file di testo di origine con 5000 URL; in questo caso tutti i 5000 URL saranno regolarmente visualizzati sulla corrispondente Sitemap.<br /><b>Nota:</b><br />Questa opzione richiede un’analisi completa del file di origine, si consiglia di usarlo quando è attiva la memorizzazione nella cache.',
	'GOOGLE_XML_UNIQUE' => 'Controlla duplicato',
	'GOOGLE_XML_UNIQUE_EXPLAIN' => 'Attiva per assicurarti che se alcuni URL vengono visualizzati più di una volta nell’XML d’origine, questi verrano visualizzati soltanto una volta nella Sitemap.<br /><b>Nota:</b><br />Questa opzione richiede un’analisi completa del file di origine, si consiglia di usarlo quando è attiva la memorizzazione nella cache.',
	'GOOGLE_XML_FORCE_LASTMOD' => 'Ultima modifica',
	'GOOGLE_XML_FORCE_LASTMOD_EXPLAIN' => 'Puoi forzare un momento dell’ultima modifica in base al ciclo di durata della cache (anche se la cache non è attivata) per tutti gli URL della Sitemap. Il modulo calcolerà anche le priorità e le frequenze di cambiamento basate su quest’ultima modifica. Per impostazione predefinita, nessun tag ultima modifica è stato aggiunto.<br /><b>Nota:</b><br />Questa opzione richiede un’analisi completa del file di origine, si consiglia di usarlo quando è attiva la memorizzazione nella cache.',
	'GOOGLE_XML_FORCE_LIMIT' => 'Forza limite',
	'GOOGLE_XML_FORCE_LIMIT_EXPLAIN' => 'Da qui è possibile fare in modo che non oltre una certa quantità massima di lista URL sia visualizzata nella Sitemap.<br /><b>Nota:</b><br />Questa opzione richiede un’analisi completa del file di origine, si consiglia di usarlo quando è attiva la memorizzazione nella cache.',
	// Reset settings
	'GOOGLE_XML_RESET' => 'Modulo Sitemap XML',
	'GOOGLE_XML_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della scheda “Impostazioni Sitemap XML” (principale) del modulo Sitemap.',
	'GOOGLE_XML_MAIN_RESET' => 'Impostazioni Sitemap XML',
	'GOOGLE_XML_MAIN_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della scheda “Impostazioni Sitemap XML” (principale) del modulo Sitemap.',
	'GOOGLE_XML_CACHE_RESET' => 'Cache Sitemap XML',
	'GOOGLE_XML_CACHE_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della cache del modulo Sitemap XML.',
	'GOOGLE_XML_GZIP_RESET' => 'XML Sitemap Gunzip',
	'GOOGLE_XML_GZIP_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni della cache del modulo Sitemap XML.',
	'GOOGLE_XML_LIMIT_RESET' => 'Limite Sitemap XML',
	'GOOGLE_XML_LIMIT_RESET_EXPLAIN' => 'Ripristina ai valori predefiniti tutte le opzioni limite del modulo Sitemap XML.',
));
?>