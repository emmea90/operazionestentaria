<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-19
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_html [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'HTML_MAIN' => 'Impostazioni HTML',
	'HTML_MAIN_EXPLAIN' => 'Queste sono le impostazioni principali per il modulo HTML.<br />Alcune di queste possono essere applicate a tutto il modulo HTML a seconda delle impostazioni di sovrascrittura HTML.',
	// Setup linking
	'HTML_LINKS_ACTIVATION' => 'Link forum',
	'HTML_LINKS_MAIN' => 'Link principali',
	'HTML_LINKS_MAIN_EXPLAIN' => 'Visualizza, o no, le News principali e la Sitemap nella parte bassa della pagina.<br /> Questo richiede che la funzione "Link principali" sia attivata nelle impostazioni principali.',
	'HTML_LINKS_INDEX' => 'Link sull’Indice',
	'HTML_LINKS_INDEX_EXPLAIN' => 'Visualizza, o no, i link disponibili di News e Sitemap per ogni forum dell’Indice. Questi link saranno aggiunti sotto la descrizione del forum.<br />Questa funzione richiede che l’opzione "Link sull’Indice" sia attivata nelle impostazioni principali.',
	'HTML_LINKS_CAT' => 'Link nella pagina del forum',
	'HTML_LINKS_CAT_EXPLAIN' => 'Visualizza, o no, i link disponibili di News e Sitemap nel forum corrente. Questi link sono aggiunti sotto il nome del forum.<br />Questa funzione richiede che l’opzione "Link nella pagina del forum" sia attivata nelle impostazioni principali.',
	// Ripristino impostazioni
	'HTML_ALL_RESET' => 'Tutti i moduli HTML',
	// Limits
	'HTML_RSS_NEWS_LIMIT' => 'Limiti principali pagine News',
	'HTML_RSS_NEWS_LIMIT_EXPLAIN' => 'Numero di voci visualizzate nella pagina principale delle News, selezionate tra quelle sorgente per gli RSS per la pagina principale News.',
	'HTML_MAP_TIME_LIMIT' => 'Limiti di tempo per il modulo Sitemap principale',
	'HTML_MAP_TIME_LIMIT_EXPLAIN' => 'Limite giornaliero. Età massima per le voci considerate nel conteggio durante la costruzione del modulo principale Sitemap. Può essere molto utile per alleggerire il carico del server su grandi quantità di dati. Inserisci 0 per disattivare il limite.',
	'HTML_CAT_MAP_TIME_LIMIT' => 'Limite di tempo per le categorie Sitemap',
	'HTML_CAT_MAP_TIME_LIMIT_EXPLAIN' => 'Limite giornaliero. Età massima per le voci considerate nel conteggio durante la costruzione del modulo della pagina delle categorie della Sitemap. Può essere molto utile per alleggerire il carico del server su grandi quantità di dati. Inserisci 0 per disattivare il limite.',
	'HTML_NEWS_TIME_LIMIT' => 'Limite di tempo per le News',
	'HTML_NEWS_TIME_LIMIT_EXPLAIN' => 'Età massima per le voci considerate nel conteggio durante la costruzione del modulo della pagina di News. Può essere molto utile per alleggerire il carico del server su grandi quantità di dati. Inserisci 0 per disattivare il limite.',
	'HTML_CAT_NEWS_TIME_LIMIT' => 'Limite di tempo per le categorie News',
	'HTML_CAT_NEWS_TIME_LIMIT_EXPLAIN' => 'Età massima per gli elementi considerati nel conteggio durante la costruzione del modulo pagina categorie News. Può essere molto utile per alleggerire il carico del server su grandi quantità di dati. Inserisci 0 per disattivare il limite.',
	// Ordinamento
	'HTML_MAP_SORT_TITLE' => 'Ordinamento Sitemap',
	'HTML_NEWS_SORT_TITLE' => 'Ordinamento News',
	'HTML_CAT_SORT_TYPE' => 'Ordinamento per categoria Sitemap',
	'HTML_CAT_SORT_TYPE_EXPLAIN' => 'Seguendo gli stessi criteri visti sopra, si applica al modulo pagina categorie Sitemap, per esempio una forum Sitemap per il modulo HTML forum.',
	'HTML_NEWS_SORT_TYPE' => 'Ordinamento per la pagina News',
	'HTML_NEWS_SORT_TYPE_EXPLAIN' => 'Seguendo gli stessi criteri visti sopra, si applica al modulo pagina News, per esempio una forum Sitemap per il modulo HTML forum.',
	'HTML_CAT_NEWS_SORT_TYPE' => 'Ordinamento per pagina categoria News',
	'HTML_CAT_NEWS_SORT_TYPE_EXPLAIN' => 'Seguendo gli stessi criteri visti sopra, si applica al modulo pagina categorie News, per esempio una forum Sitemap per il modulo HTML forum.',
	'HTML_PAGINATION_GEN' => 'Impaginazione principale',
	'HTML_PAGINATION_SPEC' => 'Impaginazione modulo',
	'HTML_PAGINATION' => 'Impaginazione Sitemap',
	'HTML_PAGINATION_EXPLAIN' => 'Attiva l’impaginazione della Sitemap. Puoi decidere di usarne solo una, o più pagine per la tua Sitemap.',
	'HTML_PAGINATION_LIMIT' => 'Voci per pagina',
	'HTML_PAGINATION_LIMIT_EXPLAIN' => 'Quando l’impaginazione della Sitemap è attivata, puoi scegliere quante voci per pagina visualizzare.',
	'HTML_NEWS_PAGINATION' => 'Impaginazione News',
	'HTML_NEWS_PAGINATION_EXPLAIN' => 'Attiva l’impaginazione della pagina News. Puoi decidere di usarne solo una, o più pagine per la tua Sitemap.',
	'HTML_NEWS_PAGINATION_LIMIT' => 'News per pagina',
	'HTML_NEWS_PAGINATION_LIMIT_EXPLAIN' => 'Quando l’impaginazione è attivata, puoi scegliere quante voci per pagina visualizzare.',
	'HTML_ITEM_PAGINATION' => 'Impaginazione voci',
	'HTML_ITEM_PAGINATION_EXPLAIN' => 'Da qui puoi decidere di avere in output i link impaginati (ove disponibili) provenienti dalla lista delle voci. Per esempio, il modulo può addizionalmente dare in output i link della pagina negli argomenti del forum.',
	// Impostazione base
	'HTML_SETTINGS' => 'Impostazioni di base',
	'HTML_C_INFO' => 'Informazioni copyright',
	'HTML_C_INFO_EXPLAIN' => 'Informazioni da mostrare nel meta tag copyright per la Sitemap e la pagina News. Per impostazione predefinita è il nome del sito di phpBB. Queste informazioni saranno utilizzate solo se è installata la MOD SEO Dynamic meta tag.',
	'HTML_SITENAME' => 'Nome del sito',
	'HTML_SITENAME_EXPLAIN' => 'Il nome del sito da visualizzare nella Sitemap e nella pagina News. Per impostazione predefinita è il nome del sito di phpBB.',
	'HTML_SITE_DESC' => 'Descrizione del sito',
	'HTML_SITE_DESC_EXPLAIN' => 'La descrizione del sito da visualizzare nella Sitemap e nella pagina News. Per impostazione predefinita è la descrizione del sito di phpBB.',
	'HTML_LOGO_URL' => 'Logo del sito',
	'HTML_LOGO_URL_EXPLAIN' => 'Il file immagine da usare come logo del sito nel nei Feed RSS nella cartella gym_sitemaps/images/.',
	'HTML_URL_EXPLAIN' => 'Inserisci l’URL completo del tuo file map.php, per esempio http://www.miosito.com/cartella_forum/ se il file map.php è installato in http://www.miosito.com/cartella_forum/.<br />Questa opzione è utile quando phpBB non è installato nella root del dominio e si vuole posizionare il file map.php nella root.',
	'HTML_RSS_NEWS_URL' => 'Sorgente principale della pagina RSS news',
	'HTML_RSS_NEWS_URL_EXPLAIN' => 'Inserisci l’URL completo del Feed RSS se vuoi visualizzarlo nella pagina News principale, esempio http://www.miosito.com/gymrss.php?news&amp; per visualizzare tutte le News del modulo RSS installato nella pagina principale HTML News.<br /> Puoi usare un Feed RSS 2.0 come sorgente per questa pagina.',
	'HTML_STATS_ON_NEWS' => 'Visualizza le statistiche del forum nella pagina News',
	'HTML_STATS_ON_NEWS_EXPLAIN' => 'Visualizza, o no, le statistiche del forum nella pagina News.',
	'HTML_STATS_ON_MAP' => 'Visualizza le statistiche forum Sitemap',
	'HTML_STATS_ON_MAP_EXPLAIN' => 'Visualizza, o no, le statistiche forum Sitemap.',
	'HTML_BIRTHDAYS_ON_NEWS' => 'Visualizza il Pannello compleanni nella pagina News',
	'HTML_BIRTHDAYS_ON_NEWS_EXPLAIN' => 'Visualizza, o no, il Pannello compleanni nella pagina News.',
	'HTML_BIRTHDAYS_ON_MAP' => 'Visualizza i Compleanni nella pagina Sitemap',
	'HTML_BIRTHDAYS_ON_MAP_EXPLAIN' => 'Visualizza, o no, i Compleanni nella Sitemap.',
	'HTML_DISP_ONLINE' => 'Visualizza Chi c’è in linea',
	'HTML_DISP_ONLINE_EXPLAIN' => 'Visualizza, o no, la lista degli utenti in linea nella Sitemap e nella pagina News.',
	'HTML_DISP_TRACKING' => 'Attiva tracciamento',
	'HTML_DISP_TRACKING_EXPLAIN' => 'Attiva, o no, il tracciamento delle voci (letto/non letto).',
	'HTML_DISP_STATUS' => 'Attiva stato',
	'HTML_DISP_STATUS_EXPLAIN' => 'Attiva, o no, il sistema stato dell’argomento (Annuncio, Importante, Bloccato, ecc. ...).',
	// Cache
	'HTML_CACHE' => 'Cache',
	'HTML_CACHE_EXPLAIN' => 'Da qui puoi definire diverse opzioni di cache per la modalità HTML. La cache HTML è separata dalle altre modalità (Google ed RSS). Questo modulo usa la cache standard di phpBB.<br />Questa opzione, invece, non può essere ereditata dal livello principale, e solo i contenuti pubblicamente visibili andranno nella cache. Tuttavia queste impostazioni possono essere trasmesse al modulo HTML a seconda delle impostazioni di sovrascrittura di questo.<br /><br />La cache è divisa in due tipi, una per ogni colonna nell’output: la colonna principale, contenente Sitemap e News, e quella opzionale, la quale, per esempio, può essere utilizzata per aggiungere la lista degli ultimi argomenti attivi nel modulo HTML del forum.',
	'HTML_MAIN_CACHE_ON' => 'Attiva la colonna principale della cache',
	'HTML_MAIN_CACHE_ON_EXPLAIN' => 'Da qui puoi attivare/disattivare la cache della colonna Sitemap e News.',
	'HTML_OPT_CACHE_ON' => 'Attiva la cache della colonna opzionale',
	'HTML_OPT_CACHE_ON_EXPLAIN' => 'Da qui puoi attivare/disattivare la cache della colonna opzionale.',
	'HTML_MAIN_CACHE_TTL' => 'Durata della cache principale',
	'HTML_MAIN_CACHE_TTL_EXPLAIN' => 'Totale ore nelle quali la cache della colonna principale sarà usata prima di essere aggiornata. Ogni file nella cache sarà aggiornato ogni volta che qualcuno lo visualizza e sono stati superati i limiti di tempo.',
	'HTML_OPT_CACHE_TTL' => 'Durata della cache colonna opzionale',
	'HTML_OPT_CACHE_TTL_EXPLAIN' => 'Totale ore nelle quali la cache della colonna opzionale sarà usata prima di essere aggiornata. Ogni file nella cache sarà aggiornato ogni volta che qualcuno lo visualizza e sono stati superati i limiti di tempo.',
	// Impostazioni autorizzazione
	'HTML_AUTH_SETTINGS' => 'Impostazioni autorizzazioni',
	'HTML_ALLOW_AUTH' => 'Autorizzazioni',
	'HTML_ALLOW_AUTH_EXPLAIN' => 'Attiva le autorizzazioni per la Sitemap e la pagina News. Se attivate, gli utenti loggati potranno visitare i contenuti privati e visualizzare gli argomenti dei forum privati se hanno le autorizzazioni richieste.',
	'HTML_ALLOW_NEWS' => 'Attiva le News',
	'HTML_ALLOW_NEWS_EXPLAIN' => 'Ogni modulo può avere una pagina News con la lista degli ultimi X argomenti attivi e dei loro contenuti, che possono essere filtrati. Per i forum, la pagina News del forum è generalmente una pagina che visualizza il riassunto dei primi messaggi degli utili 10 argomenti attivi provenienti da una selezione di argomenti pubblici e/o privati.',
	'HTML_ALLOW_CAT_NEWS' => 'Attiva le categorie News',
	'HTML_ALLOW_CAT_NEWS_EXPLAIN' => 'Seguendo gli stessi criteri del modulo pagina News, ogni modulo categoria può avere una pagina News.',
	// Contenuti
	'HTML_NEWS' => 'Impostazioni News',
	'HTML_NEWS_EXPLAIN' => 'Da qui puoi impostare opzioni filtri/formattazioni per i contenuti delle News.<br />Possono essere applicate a tutto il modulo HTML a seconda delle impostazioni di sovrascrittura di HTML.',
	'HTML_NEWS_CONTENT' => 'Impostazioni contenuti News',
	'HTML_SUMARIZE' => 'Riassunto argomenti',
	'HTML_SUMARIZE_EXPLAIN' => 'Puoi riassumente il contenuto del messaggio inserito nella pagina News.<br /> Il limite definisce il numero massimo di frasi, parole o caratteri, secondo il metodo indicato sotto. Inserisci 0 per visualizzare il messaggio completo.',
	'HTML_SUMARIZE_METHOD' => 'Metodo riassunto',
	'HTML_SUMARIZE_METHOD_EXPLAIN' => 'Puoi scegliere fra tre differenti metodi per limitare il contenuto del messaggio presente nei Feed.<br />Numero di righe, numero di parole e numero di caratteri. I tag BBcode saranno ignorati.',
	'HTML_ALLOW_PROFILE' => 'Mostra profilo',
	'HTML_ALLOW_PROFILE_EXPLAIN' => 'Il nome dell’autore può essere aggiunto all’output se si vuole.',
	'HTML_ALLOW_PROFILE_LINKS' => 'Link al profilo',
	'HTML_ALLOW_PROFILE_LINKS_EXPLAIN' => 'Se il nome dell’autore è incluso nell’output, puoi decidere di linkare o meno la corrispondente pagina phpBB del profilo.',
	'HTML_ALLOW_BBCODE' => 'Consenti BBcode',
	'HTML_ALLOW_BBCODE_EXPLAIN' => 'Seleziona questa opzione per visualizzare od omettere i BBcode in output.',
	'HTML_STRIP_BBCODE' => 'BBcode esclusi',
	'HTML_STRIP_BBCODE_EXPLAIN' => 'Da qui puoi impostare una lista di BBcode da escludere dall’analisi sintattica.<br />Il formato è semplicemente: <br /><ul><li> <b>Una lista di BBcode separati da virgola:</b> cancella i tag BBcode, conserva il contenuto. <br /><b>Esempio:</b> <b>img, b, quote</b> <br /> In questo esempio i bbcode img, grassetto e citazione saranno ignorati dall’analisi sintattica, i tag BBcode eliminati ed il contenuto conservato.</li><li> <b>Lista di BBcode separati da virgola con colonna opzione:</b> cancella il BBcode e decidi cosa fare del contenuto. <br /><b>Esempio:</b> <b>img:1, b:0, quote, code:1</b><br /> In questo esempio, il BBcode image ed il link img saranno cancellati, il grassetto non sarà processato, ma il testo in grassetto sarà conservato, la citazione non sarà processata, ma il contenuto salvato, il BBcode code ed il suo contenuto saranno eliminati dall’output.</ul>Il filtro funziona anche se il BBcode è vuoto. Utile per cancellare, ad esempio, i tag di codice ed i link dalle immagini dall’output.<br /> Il filtraggio viene eseguito prima di eseguire il riassunto.<br /> Il parametro Magic "Tutto" (può essere tutto: 0 oppure tutto: 1 per eliminare il contenuto dei tag BBcode comunque) farà tutto quanto previsto.',
	'HTML_ALLOW_LINKS' => 'Permetti i link attivi',
	'HTML_ALLOW_LINKS_EXPLAIN' => 'Da qui puoi scegliere di attivare, o no, i link presenti nei contenuti degli argomenti.<br /> Se disattivato, link ed email saranno inclusi ma non cliccabili.',
	'HTML_ALLOW_EMAILS' => 'Permetti email',
	'HTML_ALLOW_EMAILS_EXPLAIN' => 'Da qui puoi scegliere di avere in output "email AT dominio DOT com" in luogo di "email@dominio.com" nei contenuti degli argomenti.',
	'HTML_ALLOW_SMILIES' => 'Permetti emoticon',
	'HTML_ALLOW_SMILIES_EXPLAIN' => 'Da qui puoi scegliere di processare o ignorare le emoticon nei contenuti.',
	'HTML_ALLOW_SIG' => 'Permetti firma',
	'HTML_ALLOW_SIG_EXPLAIN' => 'Da qui puoi scegliere se visualizzare, o no, la firma nei contenuti.',
	'HTML_ALLOW_MAP' => 'Attiva il modulo Sitemap',
	'HTML_ALLOW_MAP_EXPLAIN' => 'Da qui puoi attivare/disattivare il modulo Sitemap.',
	'HTML_ALLOW_CAT_MAP' => 'Attiva modulo categorie Sitemap',
	'HTML_ALLOW_CAT_MAP_EXPLAIN' => 'Da qui puoi attivare/disattivare il modulo categorie Sitemap.',
));
?>