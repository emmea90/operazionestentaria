<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-27
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* install [Italian]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	// Install
	'SEO_INSTALL_PANEL'	=> 'GYM Sitemap &amp; RSS Pannello di installazione',
	'CAT_INSTALL_GYM_SITEMAPS' => 'Installa GYM Sitemap',
	'CAT_UNINSTALL_GYM_SITEMAPS' => 'Disinstalla GYM Sitemap',
	'CAT_UPDATE_GYM_SITEMAPS' => 'Aggiorna GYM Sitemap',
	'SEO_ERROR_INSTALL'	=> 'Si è verificato un errore durante il processo di installazione. Se vuoi ripetere l’installazione, devi prima disinstallare la MOD stessa.',
	'SEO_ERROR_INSTALLED'	=> 'Il modulo %s è già stato installato.',
	'SEO_ERROR_ID'	=> 'Il modulo %1$ non ha nessun ID.',
	'SEO_ERROR_UNINSTALLED'	=> 'Il modulo %s è già stato disinstallato.',
	'SEO_ERROR_INFO'	=> 'Informazione: ',
	'SEO_FINAL_INSTALL_GYM_SITEMAPS'	=> 'Login su PCA',
	'SEO_FINAL_UPDATE_GYM_SITEMAPS'	=> 'Login su PCA',
	'SEO_FINAL_UNINSTALL_GYM_SITEMAPS'	=> 'Torna all’Indice del Forum',
	'SEO_OVERVIEW_TITLE'	=> 'Panoramica di GYM sitemap &amp; RSS',
	'SEO_OVERVIEW_BODY'	=> '<p>Benvenuti nell’installazione di phpBB SEO GYM Sitemap &amp; RSS %1$s.</p> <p>Verifica l’<a href="%3$s" title="Guarda l’Annuncio di rilascio" target="_phpBBSEO"><b>Annuncio di rilascio</b></a> per ulteriori informazioni</p><p><strong style="text-transform: uppercase;">Nota:</strong> Devi avere già eseguito le modifiche al codice necessarie e devi aver caricato tutti i nuovi file prima di procedere con questa procedura di installazione guidata.</p> <p>Questo sistema di installazione ti guiderà attraverso il processo di installazione di GYM Sitemap &amp; RSS Pannello di Controllo Amministratore (PCA). Ti permetterà di generare efficienti Sitemap e Feed RSS ottimizzati per i motori di ricerca. Il suo design modulare consente di generare Sitemap e Feed RSS per ogni applicazione PHP/SQL installata sul tuo sito, utilizzando i plug-in dedicati. Per ulteriori informazioni visita il <a href="%3$s" title="Forum di Supporto" target="_phpBBSEO"><b>Forum di Supporto</b></a> per qualsiasi cosa riguardante il modulo GYM Sitemap &amp; RSS.</p> ',
	'CAT_SEO_PREMOD'	=> 'GYM Sitemap &amp; RSS',
	'SEO_INSTALL_INTRO'		=> 'Benvenuti sull’installazione di phpBB SEO GYM Sitemap &amp; RSS.',
	'SEO_INSTALL_INTRO_BODY'	=> '<p>Stai per installare la MOD %1$s %2$s. Questa installazione attiverà la GYM Sitemap &amp; RSS nel PCA.</p> <p>Una volta installata, sarà necessario andare nel PCA per scegliere le impostazioni appropriate.</p>
	<p><strong>Nota:</strong> Se è la prima volta che installi questa MOD, ti consigliamo di testarla preventivamente in locale oppure su un forum sperimentale, in modo da poter testare le varie configurazioni prima di andare OnLine.</p><br />
	<p>Requisiti:</p>
	<ul>
		<li>Apache server (Linux OS) con mod_rewrite per il modulo di riscrittura degli URL.</li>
		<li>IIS server (Windows OS) con isapi_rewrite per il modulo di riscrittura degli URL, ma saranno necessari adattamenti alle regole di scrittura nel file httpd.ini</li>
	</ul>',
	'SEO_INSTALL'		=> 'Installa',
	'UN_SEO_INSTALL_INTRO'		=> 'Benvenuto nella disinstallazione di  GYM Sitemap &amp; RSS',
	'UN_SEO_INSTALL_INTRO_BODY'	=> '<p>Stai per disinstallare la MOD %1$s %2$s.</p>
	<p><strong>Nota:</strong> Sitemap e Feed non saranno più disponibili dopo aver disinstallato il modulo.</p>',
	'UN_SEO_INSTALL'		=> 'Disinstalla',
	'SEO_INSTALL_CONGRATS'		=> 'Congratulazioni!',
	'SEO_INSTALL_CONGRATS_EXPLAIN'	=> '<p>Hai installato con successo la MOD %1$s %2$s. Vai su PCA di phpBB per configurare la MOD.</p>
	<p>Vedrai le Categorie di phpBB SEO; dove potrai:</p>
	<h2>Configurare con precisione la Sitemap e i Feed RSS</h2>
	<p>Potrai configurare Sitemap e Feed RSS in modo che supportino lo stile XSLT ed i CSS del phpBB, che potranno così essere applicati senza modificare una sola riga di codice.</p>
	<p>Potrai configurare Sitemap e Feed RSS in modo che rilevino automaticamente la riscrittura di phpBB SEO MOD e le relative impostazioni; usando un altro URL riscritto l’uso della MOD risulterà facile.</p>
	<h2>Generare un file .htaccess personalizzato.</h2>
	<p>Con il phpBB SEO mod rewrite una volta impostate le opzioni di cui sopra, sarai in grado di generare una file .htaccess personalizzato in modo rapido e di salvarlo direttamente sul server.</p><br /><h3>Log d’installazione:</h3>',
	'UN_SEO_INSTALL_CONGRATS'	=> 'Il modulo GYM Sitemap &amp; RSS è stato rimosso dal PCA.',
	'UN_SEO_INSTALL_CONGRATS_EXPLAIN'	=> '<p>Hai disinstallato con successo la MOD %1$s %2$s.<p>
	<p>Sitemap e Feed RSS non saranno più disponibili.</p>',
	'SEO_VALIDATE_INFO'	=> 'Info validazione:',
	'SEO_LICENCE_TITLE'	=> 'GNU LESSER GENERAL PUBLIC LICENSE',
	'SEO_LICENCE_BODY'	=> 'phpBB SEO GYM Sitemap &amp; RSS sono rilasciate sotto GNU LESSER GENERAL PUBLIC LICENSE.',
	'SEO_SUPPORT_TITLE'	=> 'Supporto',
	'SEO_SUPPORT_BODY'	=> 'Pieno supporto sarà dato su <a href="%1$s" title="Visita il %2$s forum" target="_phpBBSEO"><b>%2$s forum</b></a>. Ti forniremo risposte a domande sulle impostazioni generali, problemi di configurazione, e supporto a problemi comuni.</p> <p>Non dimenticate di visitare il nostro <a href="http://www.phpbb-seo.com/boards/" title="Forum SEO" target="_phpBBSEO"><b>Forum Ottimizzazione per i Motori di Ricerca</b></a>.</p> <p>Ti consigliamo di <a href="http://www.phpbb-seo.com/boards/profile.php?mode=register" title="Registrati su phpBB SEO" target="_phpBBSEO"><b>register</b></a>, eseguire il login e di <a href="%3$s" title="Avviso sugli aggiornamenti" target="_phpBBSEO"><b>sottoscrivere l’Annuncio di rilascio</b></a>  per essere informato via mail su ogni aggiornamento.',
	// Security
	'SEO_LOGIN'		=> 'Devi essere iscritto e connesso per visualizzare questa pagina.',
	'SEO_LOGIN_ADMIN'	=> 'Devi essere connesso come amministratore per visualizzare questa pagina.<br />La sessione è stata eliminata per motivi di sicurezza.',
	'SEO_LOGIN_FOUNDER'	=> 'Devi essere connesso come fondatore per visualizzare questa pagina.',
	'SEO_LOGIN_SESSION'	=> 'Controllo sessione non riuscito.<br />Le impostazioni non sono state modificate.<br />La sessione è stata eliminata per motivi di sicurezza.',
	// Cache status
	'SEO_CACHE_FILE_TITLE'	=> 'Stato della cache',
	'SEO_CACHE_STATUS'	=> 'La cartella è configurata come: <b>%s</b>',
	'SEO_CACHE_FOUND'	=> 'La cartella cache è stata trovata con successo.',
	'SEO_CACHE_NOT_FOUND'	=> 'La cartella cache non è stata trovata.',
	'SEO_CACHE_WRITABLE'	=> 'La cartella cache è scrivibile.',
	'SEO_CACHE_not writeable'	=> 'La cartella cache non è scrivibile. Devi impostare i permessi CHMOD su 0777.',
	'SEO_CACHE_FORUM_NAME'	=> 'Nome del Forum',
	'SEO_CACHE_URL_OK'	=> 'URL cache',
	'SEO_CACHE_URL_NOT_OK'	=> 'L’URL di questo forum non è memorizzato nella cache',
	'SEO_CACHE_URL'		=> 'URL finale',
	'SEO_CACHE_MSG_OK'	=> 'La cache è stata aggiornata con successo.',
	'SEO_CACHE_MSG_FAIL'	=> 'Si è verificato un errore durante l’aggiornamento della cache.',
	'SEO_CACHE_UPDATE_FAIL'	=> 'L’URL inserito non può essere utilizzato; la cache è stata lasciata intatta.',
	// Update
	'UPDATE_SEO_INSTALL_INTRO'		=> 'Benvenuti nell’aggiornamento di phpBB SEO GYM Sitemap &amp; RSS.',
	'UPDATE_SEO_INSTALL_INTRO_BODY'	=> '<p>Stai per aggiornare %1$s alla versione %2$s. Questo script aggiornerà il database di phpBB.<br />Le impostazioni correnti non saranno modificate.</p>
	<p><strong>Nota:</strong> Questo script non aggiorna i file fisici di GYM Sitemap &amp; RSS.<br /><br />Per aggiornare da tutte le versioni 2.0.x (phpBB3) <b>devi</b> caricare tutti i file nella <b>root/</b> della cartella in cui è contenuto il phpBB/ tramite ftp, dopo aver modificato manualmente tutti i file che devono essere modificati.<br /><br /> <b>Puoi</b> riavviare lo script di aggiornamento se desideri, semplicemente per visualizzare nuovamente le modifiche del codice di aggiornamento per i file del phpBB3.</p>',
	'UPDATE_SEO_INSTALL'		=> 'Aggiornamento',
	'SEO_ERROR_NOTINSTALLED'	=> 'GYM Sitemap &amp; RSS non è stata installata!',
	'SEO_UPDATE_CONGRATS_EXPLAIN'   => '<p>Ora hai aggiornato con successo %1$s alla versione %2$s.<p>
    <p><strong>Nota:</strong>Questo script non aggiorna i file fisici di GYM Sitemap &amp; RSS.</p><br />Devi attuare le modifiche in base ai codici elencati di seguito.<br /><h3>Log di aggiornamento:</h3>',
));
?>