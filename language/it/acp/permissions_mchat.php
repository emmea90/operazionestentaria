<?php
/**
* acp_permissions_mchat (phpBB Permission Set) [Italian]
*
* @package language
* @version $Id: permissions_mchat.php
* @copyright (c) 2010 rmcgirr83.org
* @copyright (c) 2009 phpbb3bbcodes.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
* @copyright (c) 2014 - www.phpbbitalia.net translated by Darkman on 2014-03-12
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Adding new category
$lang['permission_cat']['mchat'] = 'mChat';

// Adding the permissions
$lang = array_merge($lang, array(
	// User perms
	'acl_u_mchat_use'			=> array('lang' => 'Può usare mChat', 'cat' => 'mchat'),
	'acl_u_mchat_view'			=> array('lang' => 'Può vedere mChat', 'cat' => 'mchat'),
	'acl_u_mchat_edit'			=> array('lang' => 'Può modificare i messaggi di mChat ', 'cat' => 'mchat'),
	'acl_u_mchat_delete'		=> array('lang' => 'Può cancellare messaggi di mChat', 'cat' => 'mchat'),
	'acl_u_mchat_ip'			=> array('lang' => 'Può utilizzare la visualizzazione degli indirizzi IP di mChat ', 'cat' => 'mchat'),
	'acl_u_mchat_flood_ignore'	=> array('lang' => 'Può ignorare il Flood di mChat', 'cat' => 'mchat'),
	'acl_u_mchat_archive'		=> array('lang' => 'Può vedere l’Archivio', 'cat' => 'mchat'),
	'acl_u_mchat_bbcode'		=> array('lang' => 'Può usare i bbcode in mChat', 'cat' => 'mchat'),
	'acl_u_mchat_smilies'		=> array('lang' => 'Può usare le emoticon in mChat', 'cat' => 'mchat'),
	'acl_u_mchat_urls'			=> array('lang' => 'Può inserire URL in mChat', 'cat' => 'mchat'),

	// Admin perms
	'acl_a_mchat'				=> array('lang' => 'Può gestire le impostazioni di mChat', 'cat' => 'permissions'), // Using a phpBB category here
));

?>
