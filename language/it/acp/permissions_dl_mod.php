<?php

/**
*
* @mod package		Download Mod 6
* @file				permissions_dl_mod.php 1 2011/03/09 OXPUS
* @copyright		(c) 2005 oxpus (Karsten Ude) <webmaster@oxpus.de> http://www.oxpus.de
* @copyright mod	(c) hotschi / demolition fabi / oxpus
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-05-01
* @license			http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* Language pack for MOD permissions [Italian]
*/
 
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// Adding new category
$lang['permission_cat']['downloads'] = 'Download Panel';

// Download MOD Permissions
$lang = array_merge($lang, array(
	'acl_a_dl_overview'		=> array('lang' => 'Puoi vedere la schermata iniziale', 'cat' => 'downloads'),
	'acl_a_dl_config'		=> array('lang' => 'Puoi gestire le impostazioni generali', 'cat' => 'downloads'),
	'acl_a_dl_traffic'		=> array('lang' => 'Puoi gestire il traffico', 'cat' => 'downloads'),
	'acl_a_dl_categories'	=> array('lang' => 'Puoi gestire le categorie', 'cat' => 'downloads'),
	'acl_a_dl_files'		=> array('lang' => 'Puoi gestire i download', 'cat' => 'downloads'),
	'acl_a_dl_permissions'	=> array('lang' => 'Puoi gestire i permessi', 'cat' => 'downloads'),
	'acl_a_dl_stats'		=> array('lang' => 'Puoi vedere e gestire le statistiche', 'cat' => 'downloads'),
	'acl_a_dl_banlist'		=> array('lang' => 'Puoi gestire la lista ban', 'cat' => 'downloads'),
	'acl_a_dl_blacklist'	=> array('lang' => 'Puoi gestire la lista nera delle estensioni file', 'cat' => 'downloads'),
	'acl_a_dl_toolbox'		=> array('lang' => 'Puoi usare gli strumenti', 'cat' => 'downloads'),
	'acl_a_dl_fields'		=> array('lang' => 'Puoi gestire campi definiti dall’utente', 'cat' => 'downloads'),
	'acl_a_dl_browser'		=> array('lang' => 'Puoi gestire gli User Agent', 'cat' => 'downloads'),
));

?>