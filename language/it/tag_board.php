<?php
/**
*
* @package	language
* @version	3.0.7
* @license	GNU Public License
* @author	draghetto
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'TB_TB'					=> 'Tag Board',
	'TB_ACP'				=> 'Impostazioni Tag Board',
	'TB_INSTALL'			=> 'Tag Board installata con successo<br />Elimina il file tb_install.php',
	'TB_ACP_EXPLAIN'		=> 'Qui puoi settare le impostazioni di base.',
	'TB_CVERSION' 			=> 'Versione corrente',
	'TB_LVERSION' 			=> 'Ultima versione',
	'TB_CHECK' 				=> 'Verifica manualmente',
	'TB_AUTH' 				=> 'Permessi',
	'TB_GROUPS' 			=> 'Gruppi permessi',
	'TB_GROUPS_EXPLAIN' 	=> 'L’id dei gruppi cui mostrare la TB. Puoi inserirne più di uno separando gli id con una virgola.',
	'TB_DENIED' 			=> 'Utenti negati',
	'TB_DENIED_EXPLAIN' 	=> 'L’id degli utenti cui nascondere la TB. Puoi inserirne più di uno separando gli id con una virgola.',
	'TB_STANDARD' 			=> 'Tutti gli utenti possono LEGGERE ma solo gli iscritti possono SCRIVERE<br />',
	'TB_FULL' 				=> 'Tutti gli utenti possono LEGGERE e SCRIVERE<br />',
	'TB_LIMITED' 			=> 'Solo i registrati possono LEGGERE e SCRIVERE',
	'TB_GUEST' 				=> 'Nickname visualizzato per gli utenti non registrati',
	'TB_GUEST_EXPLAIN' 		=> 'Lascia il campo bianco per mostrare il suo IP.',
	'TB_LIMIT' 				=> 'Mostra gli ultimi',
	'TB_POSTS' 				=> 'messaggi',
	'TB_HEIGTH' 			=> 'Altezza della Tag Board',
	'TB_PX' 				=> 'px',
	'TB_MAXLENGTH' 			=> 'Caratteri massimi per messaggio',
	'TB_MAXLENGTH_EXPLAIN' 	=> 'Il numero massimo di caratteri permessi per ogni messaggio.',
	'TB_CHARS' 				=> 'caratteri',
	'TB_BBCODE' 			=> 'Permetti BBCode',
	'TB_CUSTOM' 			=> 'Mostra il bottone per visualizzare i BBCode personalizzati',
	'TB_FSIZE' 				=> 'Mostra il bottone per modificare la dimensione del messaggio',
	'TB_IMG' 				=> 'Permetti l’uso del tag BBCode <code>[IMG]</code>',
	'TB_FLASH' 				=> 'Permetti l’uso del tag BBCode <code>[FLASH]</code>',
	'TB_SMILIES' 			=> 'Permetti emoticon',
	'TB_URLS' 				=> 'Permetti link',
	'TB_URLS_EXPLAIN' 		=> 'Consente di rendere automaticamente cliccabili i collegamenti.',
	'TB_BUTTONS' 			=> 'Mostra i bottoni BBCode',
	'TB_DELETE' 			=> 'Permetti agli utenti di eliminare i propri messaggi',
	'TB_EDIT' 				=> 'Permetti agli utenti di modificare i propri messaggi',
	'TB_EDIT_TIME' 			=> 'Limite di tempo per modifica',
	'TB_EDIT_TIME_EXPLAIN' 	=> 'Limite di tempo disponibile per modificare un messaggio. Scegliendo 0 disabiliti questa funzione.',
	'TB_MINUTES' 			=> 'minuti',
	'TB_FLOOD' 				=> 'Intervallo di flood',
	'TB_FLOOD_EXPLAIN' 		=> 'Numero di secondi che un utente deve aspettare prima di inviare un nuovo messaggio. Scegliendo 0 disabiliti questa funzione.',
	'TB_SECONDS' 			=> 'secondi',
	'TB_PURGE' 				=> 'Cancella automaticamente i messaggi più vecchi di',
	'TB_PURGE_EXPLAIN' 		=> 'Scegliendo 0 disabiliti questa funzione.',
	'TB_DAYS' 				=> 'giorni',
	'TB_REFRESH' 			=> 'Aggiorna automaticamente la Tag Board ogni',
	'TB_REFRESH_EXPLAIN' 	=> 'Scegliendo 0 disabiliti questa funzione.',
	'TB_HISTORY' 			=> 'Numero massimo di messaggi salvati nel database',
	'TB_HISTORY_EXPLAIN' 	=> 'Scegliendo 0 disabiliti questa funzione.',
	'TB_DELETEALL' 			=> 'Cancella tutti i messaggi dalla Tag Board',
	'TB_DELETEALL_EXPLAIN' 	=> 'L’operazione non permette di tornare indietro.',
	'TB_FLOOD_ERROR' 		=> 'Non puoi inserire un altro messaggio dopo così poco tempo.',
	'TB_OFFLINE' 			=> 'Offline',
	'TB_INACTIVE' 			=> 'Inattivo',
	'TB_ONLINE' 			=> 'Online',
	'TB_GUESTNAME' 			=> 'Ospite',
	'TB_MORE_SMILIES' 		=> 'Mostra tutte le emoticon',
));

?>