<?php

/**
*
* @package phpBB3 - who was here MOD
* @version $Id: lang_wwh.php 61 2007-12-17 20:15:23Z nickvergessen $
* @copyright (c) nickvergessen ( http://www.flying-bits.org/ )
* Tradotto da Angolo
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/
if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
// for the normal sites
	'WHO_WAS_HERE'					=> 'Chi è stato qui?',
	'WHO_WAS_HERE_LATEST1'			=> 'Ultimo',
	'WHO_WAS_HERE_LATEST2'			=> '',//used for parts like o'clock in the timedisplay (last at vw:xy "o'clock")

	'WHO_WAS_HERE_TOTAL'			=> array(
		0		=> 'In totale ci sono stati <strong>0</strong> utenti connessi :: ',
		1		=> 'In totale c’è stato <strong>%d</strong> un utente connesso :: ',
		2		=> 'In totale ci sono stati <strong>%d</strong> utenti connessi :: ',
	),
	'WHO_WAS_HERE_REG_USERS'		=> array(
		0		=> '0 registrati',
		1		=> '%d registrato',
		2		=> '%d registrati',
	),
	'WHO_WAS_HERE_HIDDEN'			=> array(
		0		=> '0 nascosti',
		1		=> '%d nascosto',
		2		=> '%d nascosti',
	),
	'WHO_WAS_HERE_BOTS'				=> array(
		0		=> '0 Bot',
		1		=> '%d Bot',
		2		=> '%d Bot',
	),
	'WHO_WAS_HERE_GUESTS'			=> array(
		0		=> '0 ospiti',
		1		=> '%d ospite',
		2		=> '%d ospiti',
	),

	'WHO_WAS_HERE_WORD'				=> ' e',
	'WHO_WAS_HERE_EXP'				=> 'basato sugli utenti attivi di oggi',
	'WHO_WAS_HERE_EXP_TIME'			=> 'basato sugli utenti attivi degli ultimi ',
	'WWH_HOURS'						=> array(
		0		=> '',
		1		=> '%%s %1$s ora',
		2		=> '%%s %1$s ore',
	),
	'WWH_MINUTES'					=> array(
		0		=> '',
		1		=> '%%s %1$s minuto',
		2		=> '%%s %1$s minuti',
	),
	'WWH_SECONDS'					=> array(
		0		=> '',
		1		=> '%%s %1$s secondo',
		2		=> '%%s %1$s secondi',
	),
	'WHO_WAS_HERE_RECORD'			=> 'Utenti connessi ::  <strong>%1$s</strong>, %2$s',
	'WHO_WAS_HERE_RECORD_TIME'		=> 'Utenti connessi :: <strong>%1$s</strong>, tra %2$s e %3$s',
));

?>