<?php
/**
*
* gallery [Italian]
*
* @package phpBB Gallery
* @version $Id$
* @copyright (c) 2007 nickvergessen nickvergessen@gmx.de http://www.flying-bits.org
* @copyright (c) 2011 http://www.phpbbitalia.net
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
**/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ADD_UPLOAD_FIELD'				=> 'Aggiungi altri campi caricati',
	'ALBUM'							=> 'Album',
	'ALBUM_IS_CATEGORY'				=> 'L’album è un album-categoria.<br />Non puoi caricare categorie.',
	'ALBUM_LOCKED'					=> 'Bloccato',
	'ALBUM_NAME'					=> 'Nome album',
	'ALBUM_NOT_EXIST'				=> 'Questo album non esiste',
	'ALBUM_PERMISSIONS'				=> 'Permessi album',
	'ALBUM_REACHED_QUOTA'			=> 'Questo Album ha raggiunto il numero di immagini. Non è più possibile caricarne altre.<br />Si prega di contattare l\'amministratore per ulteriori informazioni.',
	'ALBUM_UPLOAD_NEED_APPROVAL'		=> 'Le immagini sono state caricate con successo.<br /><br />Ma la tua immagine deve essere approvata da un amministratore o un moderatore prima di essere resa pubblica.',
	'ALBUM_UPLOAD_NEED_APPROVAL_ERROR'	=> 'Alcune delle immagini sono state caricate con successo.<br /><br />Ma la tua immagine deve essere approvata da un amministratore o un moderatore prima di essere resa pubblica.<br /><br /><p class="error">%s</p>',
	'ALBUM_UPLOAD_SUCCESSFUL'		=> 'Le immagini sono state caricate con successo',
	'ALBUM_UPLOAD_SUCCESSFUL_ERROR'	=> 'Alcune delle immagini sono state caricate con successo.<br /><br /><span class="error">%s</span>',
	'ALBUMS_MARKED'					=> 'Tutti gli albums sono stati marcati come letti.',
	'ALL'							=> 'Tutto',
	'ALL_IMAGES'					=> 'Tutte le immagini',
	'ALLOW_COMMENTS'				=> 'Commenta questa immagine.',
	'ALLOW_COMMENTS_ARY'			=> array(
		0	=> 'Commenta questa immagine.',
		2	=> 'Commenta questa immagine.',
	),
	'ALLOWED_FILETYPES'				=> 'Tipi di file consentiti',
	'APPROVE'						=> 'Approva',
	'APPROVE_IMAGE'					=> 'Approva immagine',

	//@todo
	'ALBUM_COMMENT_CAN'			=> '<strong>Puoi</strong> scrivere commenti alle immagini in questo album',
	'ALBUM_COMMENT_CANNOT'		=> '<strong>Non puoi</strong> scrivere commenti alle immagini in questo album',
	'ALBUM_DELETE_CAN'			=> '<strong>Puoi</strong> cancellare le tue immagini in questo album',
	'ALBUM_DELETE_CANNOT'		=> '<strong>Non puoi</strong> cancellare le tue immagini in questo album',
	'ALBUM_EDIT_CAN'			=> '<strong>Puoi</strong> modificare le tue immagini in questo album',
	'ALBUM_EDIT_CANNOT'			=> '<strong>Non puoi</strong> modificare le tue immagini in questo album',
	'ALBUM_RATE_CAN'			=> '<strong>Puoi</strong> votare le immagini in questo album',
	'ALBUM_RATE_CANNOT'			=> '<strong>Non puoi</strong> votare le immagini in questo album',
	'ALBUM_UPLOAD_CAN'			=> '<strong>Puoi</strong> caricare nuove immagini in questo album',
	'ALBUM_UPLOAD_CANNOT'		=> '<strong>Non puoi</strong> caricare nuove immagini in questo album',
	'ALBUM_VIEW_CAN'			=> '<strong>Puoi</strong> vedere le immagini in questo album',
	'ALBUM_VIEW_CANNOT'			=> '<strong>Non puoi</strong> vedere le immagini in questo album',

	'BAD_UPLOAD_FILE_SIZE'			=> 'Il file caricato è troppo grande',
	'BBCODES'						=> 'BBCodes',
	'BROWSING_ALBUM'				=> 'Utenti che esplorano questo album: %1$s',
	'BROWSING_ALBUM_GUEST'			=> 'Utenti che esplorano questo album: %1$s e %2$d ospite',
	'BROWSING_ALBUM_GUESTS'			=> 'Utenti che esplorano questo album: %1$s e %2$d ospiti',

	'CHANGE_AUTHOR'					=> 'Cambia Autore',
	'CHANGE_IMAGE_STATUS'			=> 'Cambia stato immagine',
	'CHARACTERS'					=> 'caratteri',
	
	'CLICK_RETURN_ALBUM'			=> 'Clicca %squi%s per ritornare all’album',
	'CLICK_RETURN_IMAGE'			=> 'Clicca %squi%s per ritornare all’immagine',
	'COMMENT'						=> 'Commento',
	'COMMENT_IMAGE'					=> 'Scrivi un commento sull’immagine nell’album %s',
	'COMMENT_LENGTH'				=> 'Scrivi un commento qui, non può contenere più di <strong>%d</strong> caratteri.',
	'COMMENT_ON'					=> 'Commentato il',
	'COMMENT_STORED'				=> 'Il tuo commento è stato salvato.',
	'COMMENT_TOO_LONG'				=> 'Il tuo commento è troppo lungo.',
	'COMMENTS'						=> 'Commenti',
	'CONTEST_COMMENTS_STARTS'		=> 'Commenti sull’immagine in questo concorso sono consentiti da %s il.',
	'CONTEST_ENDED'					=> 'Il concorso è terminato il %s.',
	'CONTEST_ENDS'					=> 'Questo concorso è terminato il %s.',
	'CONTEST_RATING_STARTED'		=> 'Le votazioni per questo concorso sono iniziate il %s.',
	'CONTEST_RATING_STARTS'			=> 'Le votazioni per questo concorso iniziano il %s.',
	'CONTEST_RATING_ENDED'			=> 'Le votazioni per questo concorso sono terminate il %s.',
	'CONTEST_RATING_HIDDEN'			=> 'nascosto',
	'CONTEST_RESULT'				=> 'Concorso',
	'CONTEST_RESULT_1'				=> 'Vincitore',
	'CONTEST_RESULT_2'				=> 'Secondo',
	'CONTEST_RESULT_3'				=> 'Primo',
	'CONTEST_RESULT_HIDDEN'			=> 'La votazione per questa immagine è nascosta fino alla fine del concorso il %s.',
	'CONTEST_STARTED'				=> 'Il concorso è iniziato il %s.',
	'CONTEST_STARTS'				=> 'Il concorso inizia il %s.',
	'CONTEST_USERNAME'				=> '<strong>Concorso</strong>',
	'CONTEST_USERNAME_LONG'			=> '<strong>Concorso</strong> » Il nome utente è nascosto fino alla fine del concorso il %s.',
	'CONTEST_WINNERS_OF'			=> 'Vincitore del concorso di "%s"',

	'DELETE_COMMENT'				=> 'Cancella commento',
	'DELETE_COMMENT2'				=> 'Cancellare commento?',
	'DELETE_COMMENT2_CONFIRM'		=> 'Sei sicuro di voler cancellare il commento?',
	'DELETE_IMAGE'					=> 'Cancella',
	'DELETE_IMAGE2'					=> 'Cancella immagine?',
	'DELETE_IMAGE2_CONFIRM'			=> 'Sei sicuro di voler cancellare l’immagine?',
	'DELETED_COMMENT'				=> 'Commento cancellato',
	'DELETED_COMMENT_NOT'			=> 'Commento non cancellato',
	'DELETED_IMAGE'					=> 'Immagine cancellata',
	'DELETED_IMAGE_NOT'				=> 'Immagine non cancellata',
	'DESC_TOO_LONG'					=> 'La tua descrizione è troppo lunga',
	'DESCRIPTION_LENGTH'			=> 'Aggiungi la tua descrizione, può contenere una massimo di <strong>%d</strong> caratteri.',
	'DETAILS'						=> 'Dettagli',
	'DONT_RATE_IMAGE'				=> 'Non votare immagine',

	'EDIT_COMMENT'					=> 'Modifica commento',
	'EDIT_IMAGE'					=> 'Modifica',
	'EDITED_TIME_TOTAL'				=> 'Ultima modifica di %s il %s; modificato %d volte in totale',
	'EDITED_TIMES_TOTAL'			=> 'Ultime modifiche di %s il %s; modificati %d volte in totale',

	'FAVORITE_IMAGE'				=> 'Aggiungi ai favoriti',
	'FAVORITED_IMAGE'				=> 'L’immagine è stata aggiunta ai favoriti.',
	'FILE'							=> 'File',
	'FILE_SIZE'						=> 'Dimensione file',
	'FILETYPE_MIMETYPE_MISMATCH'	=> 'Il tipo di file di "<strong>%1$s</strong>" non corrisponde con il tipo mime (%2$s).',
	'FILETYPES_GIF'					=> 'gif',
	'FILETYPES_JPG'					=> 'jpg',
	'FILETYPES_PNG'					=> 'png',
	'FILETYPES_ZIP'					=> 'zip',

	'IMAGE'								=> 'Immagine',
	'IMAGE_#'							=> '1 immagine',
	'IMAGE_ALREADY_REPORTED'			=> 'L’immagine è già stata segnalata.',
	'IMAGE_BBCODE'						=> 'BBCode immagine',
	'IMAGE_COMMENTS_DISABLED'			=> 'I commenti per questa immagine sono disabilitati',
	'IMAGE_DAY'							=> '%.2f immagini per giorno',
	'IMAGE_DESC'						=> 'Descrizione immagine',
	'IMAGE_HEIGHT'						=> 'Altezza immagine',
	'IMAGE_INSERTED'					=> 'Imagine inserita',
	'IMAGE_LOCKED'						=> 'Questa immagine è bloccata, non puoi inserire commenti.',
	'IMAGE_NAME'						=> 'Nome immagine',
	'IMAGE_NOT_EXIST'					=> 'L’immagine non esiste.',
	'IMAGE_PCT'							=> '%.2f%% di tutte le immagini',
	'IMAGE_STATUS'						=> 'Stato',
	'IMAGE_URL'							=> 'URL immagine',
	'IMAGE_WIDTH'						=> 'Larghezza immagine',
	'IMAGES_#'							=> '%s immagini',
	'IMAGES_REPORTED_SUCCESSFULLY'		=> 'L’immagine è stata segnalata con successo',
	'IMAGES_UPDATED_SUCCESSFULLY'		=> 'L\’informazione dell\’immagine è stata aggiornata con successo',
	'INSERT_IMAGE_POST'					=> 'Inserisci l\'immagine nel post',
	'INVALID_USERNAME'					=> 'Il tuo nome utente non è valido',

	'LAST_COMMENT'					=> 'Ultimo commento',
	'LAST_IMAGE'					=> 'Ultima immagine',
	'LOGIN_EXPLAIN_UPLOAD'			=> 'Devi essere registrato e loggato per caricare immagini in questa galleria.',




	'MARK_ALBUMS_READ'				=> 'Marca gli albums come letti',
	'MAX_FILE_SIZE'					=> 'Dimensione massima file (bytes)',
	'MAX_HEIGHT'					=> 'Altezza massima immagine (pixels)',
	'MAX_WIDTH'						=> 'Larghezza massima immagine (pixels)',


	'MISSING_COMMENT'				=> 'Nessun messaggio inserito',
	'MISSING_IMAGE_NAME'			=> 'È necessario specificare un nome quando si modifica l\'immagine',
	'MISSING_MODE'					=> 'Nessun modo selezionato',
	'MISSING_REPORT_REASON'			=> 'E’ necessario scrivere una ragione per segnalare l’immagine.',
	'MISSING_SLIDESHOW_PLUGIN'		=> 'Non è stato trovato  slideshow-plugin. Contatta l’amministratore del forum per altre informazioni.',
	'MISSING_SUBMODE'				=> 'Nessun modo selezionato',
	'MISSING_USERNAME'				=> 'Nessun nome utente inserito',
	'MOVE_TO_ALBUM'					=> 'Sposta album',
	'MOVE_TO_PERSONAL'				=> 'Sposta album personale',
	'MOVE_TO_PERSONAL_MOD'			=> 'Se scegli l’opzione "si", l’immagine viene spostata nell’album personale dell’utente. Se l’utente non esiste ancora, viene creato automaticamente.',
	'MOVE_TO_PERSONAL_EXPLAIN'		=> 'Se scegli l’opzione "si", l’immagine viene spostata nell’album personale dell’utente. Se l’utente non esiste ancora, viene creato automaticamente.',

	'NEW_COMMENT'					=> 'Nuovo commento',
	'NEW_IMAGES'					=> 'Nuova immagine',
	'NEWEST_PGALLERY'				=> 'La galleria personale più recente è di %s',
	'NO_ALBUMS'						=> 'Non ci sono albums in questa galleria.',
	'NO_COMMENTS'					=> 'Nessun commento',
	'NO_IMAGES'						=> 'Nessun immagine',
	'NO_IMAGES_FOUND'				=> 'Nessun immagine trovata.',
	'NO_NEW_IMAGES'					=> 'Nessuna nuova immagine',
	'NO_IMAGES_LONG'				=> 'Non ci sono immagini in questo album.',
	'NOT_ALLOWED_FILE_TYPE'			=> 'Questo tipo di file non è consentito',
	'NOT_RATED'						=> 'nessun voto',

	'ORDER'							=> 'Ordine',
	'ORIG_FILENAME'					=> 'Considera nome file nome immagine (inserimento-campo non dispone di alcuna funzione)',
	'OUT_OF_RANGE_VALUE'			=> 'Il valore è compreso nell’intervallo consentito',
	'OWN_IMAGES'					=> 'La tua immagine',

	'PERCENT'						=> '%',
	'PERSONAL_ALBUMS'				=> 'Albums personali',
	'PIXELS'						=> 'pixel',
	'POST_COMMENT'					=> 'Scrivi un commento',
	'POST_COMMENT_RATE_IMAGE'		=> 'Scrivi un commento e vota l’immagine',
	'POSTER'						=> 'Poster',

	'QUOTA_REACHED'					=> 'Il caricamento di immagini consentito è stato raggiunto.',
	'QUOTE_COMMENT'					=> 'Quota il commento',
	'RANDOM_IMAGES'					=> 'immagini casuali',
	'RATE_IMAGE'					=> 'Vota l’immagine',
	'RATES_COUNT'					=> 'Numero di voti',
	'RATING'						=> 'Vota',
	'RATING_STRINGS'				=> array(
		0	=> 'nessun voto',
		1	=> '%2$s (1 voto)',
		2	=> '%2$s (%1$s voti)',
	),
	'RATING_STRINGS_USER'			=> array(
		1	=> '%2$s (1 voto, il tuo voto: %3$s)',
		2	=> '%2$s (%1$s voti, il tuo voto: %3$s)',
	),
	'RATING_SUCCESSFUL'				=> 'L’immagine è stata votata con successo.',
	'READ_REPORT'					=> 'Leggi messaggio segnalazione',
	'RECENT_COMMENTS'				=> 'Commenti recenti',
	'RECENT_IMAGES'					=> 'immagini recenti',
	'REPORT_IMAGE'					=> 'Segnala immagine',
	'RETURN_ALBUM'					=> '%sRitorna all’ultimo album visitato%s',
	'ROTATE_IMAGE'                  => 'Ruota immagine', 
    'ROTATE_LEFT'                   => 'di 90° a sinistra', 
    'ROTATE_NONE'                   => 'nessuna rotazione', 
    'ROTATE_RIGHT'                  => 'di 90° a destra', 
    'ROTATE_UPSIDEDOWN'             => 'di 180° sotto e sopra', 

	'SEARCH_ALBUM'					=> 'Cerca in questo album…',
	'SEARCH_ALBUMS'					=> 'Cerca negli albums',
	'SEARCH_ALBUMS_EXPLAIN'			=> 'Seleziona l’album o gli albums dove vuoi eseguire la ricerca. La ricerca nei sotto-albums sarà effettuata automaticamente se non hai disattivato “cerca in sotto-albums“.',
	'SEARCH_COMMENTS'				=> 'Solo commenti',
	'SEARCH_CONTEST'				=> 'Vincitori concorso',
	'SEARCH_IMAGE_COMMENTS'			=> 'Nomi immagini, descrizione e commenti',
	'SEARCH_IMAGE_VALUES'			=> 'Solo nomi immagini e descrizione',
	'SEARCH_IMAGENAME'				=> 'solo nomi immagini',
	'SEARCH_RANDOM'					=> 'Immagini casuali',
	'SEARCH_RECENT'					=> 'Immagini recenti',
	'SEARCH_RECENT_COMMENTS'		=> 'Commenti recenti',
	'SEARCH_SUBALBUMS'				=> 'Cerca in sotto-albums',
	'SEARCH_TOPRATED'				=> 'Immagini più votate',
	'SEARCH_USER_IMAGES'			=> 'Cerca in immagini utente',
	'SEARCH_USER_IMAGES_OF'			=> 'Immagini di %s',
	'SELECT_ALBUM'					=> 'Seleziona un Album',
	'SHOW_PERSONAL_ALBUM_OF'		=> 'Mostra album personale di %s',
	'SLIDE_SHOW'					=> 'Presentazione',
	'SLIDE_SHOW_HIGHSLIDE'			=> 'Per iniziare la presentazione, clicca sul nome dell’immagine e poi sull’icona "Avanti":',
	'SLIDE_SHOW_LYTEBOX'		    => 'Per iniziare la presentazione, clicca sul nome dell’immagine:',
	'SLIDE_SHOW_SHADOWBOX'          => 'Per iniziare la presentazione, clicca sul nome dell’immagine:',



	'SORT_ASCENDING'				=> 'Ascendente',
    'SORT_DEFAULT'                  => 'Default',	
	'SORT_DESCENDING'				=> 'Discendente',
	'STATUS'						=> 'Stato',
	'SUBALBUMS'						=> 'Sotto-albums',
	'SUBALBUM'						=> 'Sotto-album',

	'THUMBNAIL_SIZE'				=> 'Dimensione anteprima (pixels)',
	'TOTAL_COMMENTS_OTHER'			=> 'Totale commenti <strong>%d</strong>',
	'TOTAL_COMMENTS_ZERO'			=> 'Totale commenti <strong>0</strong>',
	'TOTAL_IMAGES'					=> 'Totale immagini',
	'TOTAL_PGALLERIES_OTHER'		=> 'Totale gallerie personali <strong>%d</strong>',
	'TOTAL_PGALLERIES_SHORT'		=> '%d gallerie personali ',
	'TOTAL_PGALLERIES_ZERO'			=> 'Totale gallerie personali <strong>0</strong>',

	'UNFAVORITE_IMAGE'				=> 'elimina dai favoriti',
	'UNFAVORITED_IMAGE'				=> 'L’immagine è stata eliminata dai favoriti.',
	'UNFAVORITED_IMAGES'			=> 'L’immagine è stata eliminata dai tuoi favoriti.',
	'UNLOCK_IMAGE'					=> 'Sblocca immagine',
	'UNWATCH_ALBUM'					=> 'Elimina sottoscrizione album',
	'UNWATCH_IMAGE'					=> 'Elimina sottoscrizione immagine',
	'UNWATCH_PEGAS'					=> 'Non sottoscrivere la nuova galleria personale',
	'UNWATCHED_ALBUM'				=> 'Non sei informato sulle nuove immagini in questo album.',
	'UNWATCHED_ALBUMS'				=> 'Non sei informato sulle nuove immagini in questi albums.',
	'UNWATCHED_IMAGE'				=> 'Non sei informato sui nuovi commenti in questa immagine.',
	'UNWATCHED_IMAGES'				=> 'Non sei informato sui nuovi commenti in queste immagini.',
	'UNWATCHED_PEGAS'				=> 'Non sei automaticamente più iscritto alla nuova gallerie personali.',
	'UPLOAD_ERROR'					=> 'Durante il caricamento „%1$s“ si è verificato il seguente errore:<br />&raquo; %2$s',
	'UPLOAD_IMAGE'					=> 'Carica immagine',
	'UPLOAD_IMAGE_SIZE_TOO_BIG'		=> 'La dimensione della tua immagine è troppo grande',
	'UPLOAD_NO_FILE'				=> 'Devi inserire il tuo percorso ed il nome del file',
	'UPLOADED_BY_USER'				=> 'Inviata da',
	'UPLOADED_ON_DATE'				=> 'Caricata',
	'USE_SAME_NAME'					=> 'Utilizzare lo stesso nome dell\'immagine e la descrizione di tutte le immagini.',
	'USE_NUM'						=> 'Aggiungi {NUM} per numeri. Inizio conteggio:',
	'USER_REACHED_QUOTA'			=> array(
		0	=> 'Non sei autorizzato a caricare <strong>qualsiasi</strong> immagine.<br />Contatta l\'mministratore per maggiori informazioni.',
		1	=> 'You are not allowed to upload more than <strong>1</strong> image.<br />Please contact the administrator for more information.',
		2	=> 'You are not allowed to upload more than <strong>%s</strong> images.<br />Please contact the administrator for more information.',
	),
	'USER_REACHED_QUOTA_SHORT'		=> array(
		0	=> 'You are not allowed to upload <strong>any</strong> images.',
		1	=> 'You are not allowed to upload more than <strong>1</strong> image.',
		2	=> 'You are not allowed to upload more than <strong>%s</strong> images.',
	),
	'USERNAME_BEGINS_WITH'			=> 'Nome utente che inizia con',
	'USERS_PERSONAL_ALBUMS'			=> 'Albums personali utenti',

	'VIEW_ALBUM'					=> 'Vedi album',
	'VIEW_ALBUM_IMAGE'				=> '1 immagine',
	'VIEW_ALBUM_IMAGES'				=> '%s immagini',
	'VIEW_IMAGE'					=> 'Vedi immagine',
	'VIEW_LATEST_IMAGE'				=> 'Vedi ultime immagini',
	'VIEW_SEARCH_RECENT'			=> 'Vedi immagini recenti',
	'VIEW_SEARCH_RANDOM'			=> 'Vedi immagini casuali',
	'VIEW_SEARCH_COMMENTED'			=> 'Vedi commenti recenti',
	'VIEW_SEARCH_CONTESTS'			=> 'Vedi vincitori concorso',
	'VIEW_SEARCH_TOPRATED'			=> 'Vedi immagini più votate',
	'VIEW_SEARCH_SELF'				=> 'Vedi tue immagini',
	'VIEWING_ALBUM'					=> 'Visualizzazione album %s',
	'VIEWING_IMAGE'					=> 'Visualizzazione immagine nell’album %s',
	'VIEWS'							=> 'Visualizzazioni',

	'WATCH_ALBUM'					=> 'Sottoscrivi album',
	'WATCH_IMAGE'					=> 'Sottoscrivi immagine',
	'WATCH_PEGAS'					=> 'Sottoscrivi le nuove gallerie personali',

	'WATCHING_ALBUM'				=> 'Ora sarai informato sulle nuove immagini in questo album.',
	'WATCHING_IMAGE'				=> 'Ora sarai informato sulle nuove immagini in quest’immagine.',
	'WATCHING_PEGAS'				=> 'Ora sei automaticamente iscritto alle nuove gallerie personali.',
	
	
	'YOUR_COMMENT'					=> 'Tuo commento',
	'YOUR_PERSONAL_ALBUM'			=> 'Tuo album personale',
	'YOUR_RATING'					=> 'Tue votazioni',
));

?>