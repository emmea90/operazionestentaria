<?PHP


/**
*
* @mod package		Download Mod 6
* @file				dl_help.php 16 2011/07/17 OXPUS
* @copyright		(c) 2005 oxpus (Karsten Ude) <webmaster@oxpus.de> http://www.oxpus.de
* @copyright mod	(c) hotschi / demolition fabi / oxpus
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-10-30
* @license			http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/** 
*
* @mod package		Download Mod 6
* @file				dl_help.php 14 2011/05/31 OXPUS
* @copyright		(c) 2005 oxpus (Karsten Ude) <webmaster@oxpus.de> http://www.oxpus.de
* @copyright mod	(c) hotschi / demolition fabi / oxpus
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-06-06
* @license			http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/*
* [ Italian ] language file for Download MOD 6
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'HELP_TITLE' => 'Aiuto OnLine MOD Download',
	
	'DL_NO_HELP_AVIABLE' => 'Non ci sono altri aiuti disponibili per questa opzione',

	'HELP_DL_ACTIVE_EXPLAIN'	=> 'Attiva il download in base alle seguenti opzioni On o Off.',
	'HELP_DL_ANTISPAM'		=> 'Questa opzione blocca i download per cui l’utente deve avere traffico, il numero di messaggi e il numero di messaggi richiesti nelle ultime ore.<br /><br />Esempio:<br />Le impostazioni contengono: 25 messaggi in 24 ore.<br />Sulla base di queste impostazioni i download saranno bloccati per l’utente che non ha inviato 25 nuovi messaggi nelle ultime 24 ore.<br />Questa opzione dovrebbe prevenire lo spam per i download, specialmente da parte di nuovi utenti, prima che un membro dello staff possa prendere provvedimenti.<br />Il download sarà mostrato comunque come disponibile per invogliare l’utente. L’utente riceverà solo un messaggio riguardo la mancanza dei permessi.<br /><br />Per disabilitarlo imposta uno o entrambi i valori a 0.',
	'HELP_DL_APPROVE' => 'Questo approverà il download immediatamente se invii questo modulo.<br />Altrimenti, il download sarà nascosto agli utenti.',
	'HELP_DL_APPROVE_COMMENTS' => 'Se disabiliti questa opzione, ogni nuovo commento dovrà essere approvato da un moderatore/amministratore del download prima che gli altri utenti possano vederlo.',

	'HELP_DL_BUG_TRACKER_CAT' => 'Abilita la segnalazione dei bug per questa categoria di download.<br />I bug possono essere segnalati e visualizzati da tutti gli utenti registrati per i relativi download, e per le altre categorie la segnalazione dei bug sarà attivata anche lì.<br />Solo gli amministratori e i moderatori della Board posso gestire i bug.<br />Per ogni modifica nel messaggio del bug, l’autore riceverà una email ed il membro delo staff che dovrebbe lavorare sul bug sarà così informato.',

	'HELP_DL_CAT_DESCRIPTION' => 'Una breve descrizione per questa categoria.<br />I BBCode non sono disponibili.<br />Questa descrizione sarà visualizzata nell’indice dei download e nelle sotto categorie.',
	'HELP_DL_CAT_ICON' => 'L’icona della categoria deve essere caricata nel Forum, per esempio nella cartella /images/dl_icons/ (Questa cartella deve essere creata prima che l’icona possa essere caricata).<br />Inserisci l’URL relativo della root del Forum, es. images/dl_icon.gif.<br /><br />Per favore, utilizza solo icone visualizzabili da un browser web.<br />Raccomandate per questo JPG, GIF o PNG.<br />Calcola la dimensione dell’immagine dell’icona per evitare di destrutturare l’indice dei download, perché l’icona non sarà ridimensionata prima di essere utilizzata.',
	'HELP_DL_CAT_NAME' => 'Questo è il nome della categoria che sarà visualizzato in ogni punto.<br />Ti si consiglia di evitare caratteri speciali per non avere voci confuse nel jumpbox.',
	'HELP_DL_CAT_PARENT' => 'Il livello principale o un’altra categoria possono essere uniti.<br />Con questo sistema dinamico a discesa puoi costruire una struttura gerarchica per i tuoi download.',
	'HELP_DL_CAT_PATH' => 'Qui devi inserire un percorso esistente per i tuoi download.<br />Questo valore deve essere il nome di una sottocartella sotto la cartella principale (es. download/) che hai definito nella configurazione principale.<br />Inserisci qui il nome della cartella con uno slash alla fine.<br />Per esempio, se esiste la cartella "download/mods/" devi inserire come percorso della categoria solo "mods/".<br />Se invii questo modulo, la categoria sarà controllata.<br />Assicurati che la sotto cartella inserita esista!<br />Se la cartella è una sotto cartella di una sotto cartella, inserisci la gerarchia completa qui.<br />Es. "download/mods/misc/" deve essere inserito come percorso categoria "mods/misc/".<br />Assicurati che ogni sotto cartella abbia i permessi CHMOD 777 e considera i nomi delle cartelle come <i>case sensitive</i> (se maiuscole, maiuscole; se minuscole minuscole) se usi Unix/Linux.',
	'HELP_DL_CAT_RULES' => 'Queste regole saranno mostrate sopra le sotto categorie e nei download mentre si visualizza la categoria.',
	'HELP_DL_CAT_TRAFFIC' => 'Inserisci il traffico mensile massimo per questa categoria.<br />Questo traffico non incrementa il traffico totale!<br />Inserisci 0 per disabilitare il limite.',
	'HELP_DL_CHOOSE_CATEGORY' => 'Scegli la categoria che includerà questo download.<br />Il file deve essere già salvato nella cartella che hai inserito nella gestione categorie prima di salvare questo download.<br />Altrimenti riceverai un messaggio di errore.',
	'HELP_DL_COMMENTS' => 'Attiva il sistema dei commenti per questa categoria.<br />Gli utenti che puoi abilitare con il trascinamento, possono vedere e/o inserire commenti in questa categoria.<br />Amministratori e Moderatori del download possono modificare e cancellare tutti i commenti. L’autore può gestire i propri testi.',
	'HELP_DL_COPY_PERMISSIONS' => 'Questo copia i permessi dalla categoria selezionata.<br />Se si seleziona la categoria superiore, questa categoria avrà i permessi della categoria che è stata selezionata in precedenza a cui questa categoria sarà unita.<br />Se la categoria superiore coincide con l’indice dei download questa categoria non avrà alcun permesso. In questo caso scegli un’altra categoria o imposta i permessi per questa attraverso il modulo dei permessi.',

	'HELP_DL_DELAY_AUTO_TRAFFIC' => 'Inserisci il numero dei giorni che devono trascorrere prima che un nuovo utente riceva il primo traffico automatico.<br />Il conteggio inizia dal giorno della registrazione.<br />Inserisci 0 per disabilitare questo conteggio.',
	'HELP_DL_DELAY_POST_TRAFFIC' => 'Inserisci il numero dei giorni che devono trascorrere prima che un nuovo utente riceva il traffico per i messaggi.<br />Il conteggio inizia dal giorno della registrazione.<br />Inserisci 0 per disabilitare questo conteggio.',
	'HELP_DL_DISABLE_EMAIL' => 'Con questa opzione puoi abilitare o disabilitare completamente le notifiche email sull’aggiunta di nuovi download o sulla modifica dei download.<br />Fin quando questa funzione è abilitata può essere disibilitata singolarmente per l’aggiunta o la modifica dei download.<br />Solo l’utente che ha abilitato nella sua configurazione dei download la notifica email sull’aggiunta dei nuovi download o la modifica di questi riceverà un’ email.<br />Tali utenti possono aprire la configurazione dal footer dei download.',
	'HELP_DL_DISABLE_EMAIL_FILES' => 'Seleziona questa opzione per disabilitare la notifica email.<br />Questo non avrà effetto sulle email e sulle notifiche popup e sui messaggi della Board o aggiornando l’ultima data di modifica.',
	'HELP_DL_DISABLE_POPUP' => 'Con questa opzione puoi abilitare o disabilitare completamente le notifiche popup o i messaggi della Board nell’header del Forum per quanto riguarda le nuove aggiunte o le modifiche dei download.<br />Fin quando questa funzione è abilitata, puoi disabilitarla singolarmente mentre inserisci o modifichi un download.<br />Solo gli utenti che hanno abilitato le notifiche nella configurazione dei download riceveranno popup/messaggi della Board.<br />Gli utenti possono aprire la loro configurazione dal footer dei download.',
	'HELP_DL_DISABLE_POPUP_FILES' => 'Seleziona questa opzione per disabilitare le notifiche popup e i messaggi della Board.<br />Questo non avrà effetto sulle notifiche popup o sui messaggi della Board o aggiornando l’ultima data di modifica.',
	'HELP_DL_DISABLE_POPUP_NOTIFY' => 'Se questa opzione è abilitata, puoi disabilitarla per cambiare la data di modifica mentre si modifica un download.',
	'HELP_DL_DROP_TRAFFIC_POSTDEL' => 'Se un messaggio o un argomento è cancellato, questa opzione decrementa anche il traffico dell’autore (cancellando un argomento solo l’autore sarà rispettato!).<br />L’autore può ottenere traffico inviando messaggi e lo perderà se viene eliminato un suo messaggio/argomento. Il totale del traffico per l’invio di un messaggio o la cancellazione può essere differente per lui/lei!',

	'HELP_DL_EDIT_OWN_DOWNLOADS' => 'Se abiliti questa opzione, ogni utente può modificare il proprio upload senza essere Amministratore o Moderatore del download.',
	'HELP_DL_EDIT_TIME' => 'Inserisci qui il numero dei giorni per cui un download sarà indicato come modificato.<br />Inserisci 0 per disabilitare questa opzione.',
    'HELP_DL_ENABLE_JUMPBOX' => 'Questa opzione mostra o nasconde il menu di navigazione rapida nel footer della MOD Download.<br />Disabilitare il menu di navigazione rapida aiuta ad aumentare la velocità della MOD Download.',  
	'HELP_DL_ENABLE_POST_TRAFFIC' => 'Imposta la quantità di traffico che un utente riceverà per la creazione di un argomento, per la risposta o citazione, come nelle prossime due opzioni.',
    'HELP_DL_ENABLE_RATE' => 'Con questa opzione puoi abilitare/disabilitare il sistema basato sui punti.<br />Se già ci sono punti accumulati questi non saranno cancellati e verranno mostrati nel momento in cui verrà attivato il sistema a punteggio.',  
	'HELP_DL_ENABLE_TOPIC'	=> 'Permetti di creare un argomento per ogni nuovo download che è aggiunto o caricato senza il pannello di amministrazione nei seguenti forum e con il testo fornito. Per i download che devono essere approvati prima di essere visualizzati, l’argomento sarà creato e visibile nel pannello di moderazione.',
	'HELP_DL_EXT_NEW_WINDOW' => 'Apri i download esterni in una nuova finestra del browser o caricali nella finestra corrente.',
	'HELP_DL_EXTERN' => 'Attiva questa funzione per i file esterni che si inseriranno nel campo qui sopra (http://www.esempio.com/media.mp3).<br />La funzione "free" diventa insignificante.',
	'HELP_DL_EXTERN_UP' => 'Attiva questa funzione per i file esterni che si inseriranno nel campo a destra (http://www.example.com/media.mp3).<br />La funzione "free" diventa insignificante.',

	'HELP_DL_FILE_DESCRIPTION' => 'Una breve descrizione per questo download.<br />Questa sarà visualizzata anche nella categoria dei download.<br />I BBCode non sono disponibili per questo testo.<br />Inserisci solo un breve testo per ridurre il carico dei dati all’apertura della categoria.',
	'HELP_DL_FILES_URL' => 'Il nome del file per questo download.<br />Inserisci questo nome senza il percorso del file o lo slash.<br />Il file deve esistere prima di salvare questo download o riceverai un messaggio di errore.<br />Nota estensione file vietata: ogni file che ha un’estensione vietata sarà bloccato.',

	'HELP_DL_GUEST_STATS_SHOW' => 'Questa opzione includerà o escluderà i dati statistici per gli ospiti, dalle statistiche delle categorie pubbliche.<br />Lo script raccoglierà tutti i dati.<br />Lo strumento statistiche del PCA mostrerà sempre i dati statistici completi.',

	'HELP_DL_HACK_AUTOR' => 'L’autore di questo file di download.<br />Lascia vuoto per nascondere questo valore nei dettagli del download e nella visualizzazione generale.',
	'HELP_DL_HACK_AUTOR_EMAIL' => 'L’indirizzo email dell’autore.<br />Se non viene inserito niente, sarà nascosto nei dettagli del download e nella visualizzazione generale.',
	'HELP_DL_HACK_AUTOR_WEBSITE' => 'Sito web dell’autore.<br />Questo URL dovrebbe essere il sito web dell’autore, non la pagina per il download (non sempre coincidono).<br />Per favore non inserire link a siti protetti o dal contenuto incerto.',
	'HELP_DL_HACK_DL_URL' => 'La pagina di un download alternativo per questo file.<br />Questa può essere il sito web dell’autore o un sito web alternativo.<br />Per favore non inserire i link per il download diretto se l’autore non lo permette.',
	'HELP_DL_HACK_VERSION' => 'Dichiarazioni sul rilascio del download.<br />Questo sarà sempre visualizzato accanto al download.<br />Non puoi effettuare ricerche su questo.',
	'HELP_DL_HACKLIST' => 'Se scegli Sì, con questa opzione puoi aggiungere il download alla lista dei trucchi (deve essere abilitata nelle impostazioni generali).<br />No, non si inserirà il download nella lista dei trucchi.<br />Mostra le informazioni extra per visualizzare questo blocco solo nei dettagli del download.',
	'HELP_DL_HOTLINK_ACTION' => 'Qui puoi decidere il comportamento dello script del download per prevenire link di download diretti (vedi anche l’ultima opzione).<br />Verrà visualizzato un messaggio (riduce il carico del server) o si verrà reindirizzati al download (produce ulteriore traffico).',

	'HELP_DL_ICON_FREE_FOR_REG' => 'Questa opzione assegna l’icona bianca per i download (download gratuito per gli utenti registrati) anche agli utenti ospiti.<br />Se disabiliti questa opzione, gli ospiti vedranno l’icona rossa invece di quella bianca.',
	'HELP_DL_IS_FREE' => 'Attiva questa opzione se vuoi che il download sia libero per tutti.<br />Non sarà utilizzato il traffico dell’account.<br />Scegli libero per utenti registrati per abilitare il download gratuito solo per gli utenti registrati.',

	'HELP_DL_KLICKS_RESET' => 'Con questa opzione riporti a zero il valore dei click mensili.<br />È utile se vuoi controllare il numero dei click dopo aver cambiato il rilascio del file.',
 
	'HELP_DL_LATEST_COMMENTS' => 'Questa opzione mostra gli ultimi X commenti nei dettagli del download. Inserisci 0 per disabilitare questo blocco.',
	'HELP_DL_LIMIT_DESC_ON_INDEX' => 'Taglia la descrizione dopo avere inserio il numero di caratteri.<br />Inserisci zero per disabilitare questa funzione.',
	'HELP_DL_LINKS_PER_PAGE' => 'Questa opzione controlla come molti download saranno visualizzati in ogni pagina di categoria e nelle statistiche del PCA.<br />Nella lista dei trucchi e nella lista generale sarà utilizzata l’impostazione della Board "Argomenti per pagina".',

	'HELP_DL_METHOD' => 'Il metodo "Vecchio" invierà i file direttamente al web client.<br />Questo metodo è compatibile con la maggior parte degli ambienti web, ma non è possibile visualizzare la dimensione del file reale durante il download; di conseguenza il client dell’utente non potrà calcolare il tempo rimanente per il download.<br />Il metodo "Nuovo" mostrerà la dimensione reale del file ma potrà produrre errori.<br />Usa il metodo "Vecchio" se hai problemi con il "Nuovo".<br />Se non funziona nulla, seleziona "Diretto" per collegare il download direttamente al file sul server qualora questo sia più grande del limite di memoria PHP.',
	'HELP_DL_METHOD_QUOTA' => 'Imposta qui la dimensione del file partizionato che sarà letto come file intero se hai scelto il "Nuovo" metodo.<br />Sotto questa dimensione il file sarà preso come file in lettura e inviato direttamente al client web.',
	'HELP_DL_MOD_DESC' => 'Descrizione dettagliata sulla MOD selezionata.<br />Puoi utilizzare BBCode ed Emoticon in questo testo.<br />Le righe dei Feed saranno formattate.<br />Questo testo sarà visualizzato solo nei dettagli del download.',
	'HELP_DL_MOD_DESC_ALLOW' => 'Abilita il blocco delle informazioni sulla MOD mentre si aggiunge o si modifica un download.',
	'HELP_DL_MOD_LIST' => 'Attiva questo blocco nei dettagli del download.<br />Se non abiliti questa opzione, l’intero blocco non sarà visualizzato.',
	'HELP_DL_MOD_REQUIRE' => 'Dichiarazione attraverso cui è richiesto di utilizzare questa MOD Download per altre MOD.<br />Questo testo sarà visualizzato solo nei dettagli del download.',
	'HELP_DL_MOD_TEST' => 'Dichiara che questa MOD è stata testata con successo su phpBB.<br />Inserisci semplicemente la release della Board test.<br />Lo script verrà visualizzato come phpBB X, così dovrai inserire qui solo X.<br />Questo testo sarà visualizzato solo nei dettagli del download.',
	'HELP_DL_MOD_TODO' => 'Qui puoi inserire i prossimi passi che hai programmato per questa MOD o che sono attualmente in lavorazione.<br />Questo creerà la lista delle attività che può essere aperta nella parte bassa della pagina dei download.<br />Con questo testo l’utente sarà informato sull’ultimo stato di questa MOD.<br />Le righe dei Feed saranno formattate, i BBcode non sono disponibili qui.<br />La lista delle cose da fare sarà riempita fino a quando questo blocco non sarà disabilitato.',
	'HELP_DL_MOD_WARNING' => 'Consigli importanti riguardanti l’installazione di questa MOD, e l’interazione con altre MOD.<br />Questo testo sarà visualizzato solo nei dettagli del download formattato con un altro colore (il rosso è il predefinito).<br />Le righe dei Feed saranno formattate.<br />I BBCode non sono disponibili qui.',
	'HELP_DL_MUST_APPROVE' => 'Attiva questa opzione per approvare ogni nuovo file caricato prima che il download venga visualizzato in questa categoria.<br />Amministratori e moderatori dei download riceveranno un’email per ogni download non approvato.',

	'HELP_DL_NAME' => 'Questo è il nome del download, che sarà mostrato in diversi posti.<br />Cerca di evitare i caratteri speciali per ridurre gli errori di visualizzazione.',
	'HELP_DL_NEW_TIME' => 'Indica per quanti giorni un download sarà segnato come nuovo.<br />Inserisci 0 per disabilitare questa funzione.',
	'HELP_DL_NEWTOPIC_TRAFFIC' => 'Per ogni nuovo argomento inviato all’autore sarà aggiunto un ammontare di traffico (al suo account) pari a quello qui inserito.',
	'HELP_DL_NO_CHANGE_EDIT_TIME' => 'Seleziona questa opzione per rimuovere la possibilità di modificare l’ultima data di modifica per questo download.<br />Questo non avrà effetto sulle email e sulle notifiche popup e sui messaggi della Board.',

	'HELP_DL_OFF_HIDE_EXPLAIN'			=> 'Nascondi link nella barra di navigazione.<br />Altrimenti l’area download visualizzerà solo un messaggio.',
	'HELP_DL_OFF_NOW_TIME_EXPLAIN'		=> 'Chiudi, o disattiva, immediamente i download regolarmente in questi periodi di tempo.',
	'HELP_DL_OFF_TIME_PERIOD_EXPLAIN'	=> 'Periodi di tempo nei quali i download saranno automenticamenti chiusi.',
	'HELP_DL_ON_ADMINS_EXPLAIN'			=> 'Consenti agli amministratori della Board di inserire download e lavorare con essi mentre la MOD download è disattivata.<br />Altrimenti anche gli amministratori saranno esclusi.',
	'HELP_DL_OVERALL_TRAFFIC' => 'Limite globale per tutti i download  che non può essere superato nel mese corrente per gli utenti registrati.<br />Dopo aver raggiunto questo limite, i download saranno bloccati e, se abilitati, anche gli upload saranno bloccati.',
	'HELP_DL_OVERALL_GUEST_TRAFFIC' => 'Limite globale per tutti i download per gli utenti ospiti che non può essere superato nel mese corrente.<br />Dopo aver raggiunto questo limite, i download saranno bloccati e, se abilitati, gli upload saranno impossibili.',

	'HELP_DL_PHYSICAL_QUOTA' => 'Limite globale fisico per cui la MOD sarà in grado di gestire e salvare i download.<br />Se viene raggiunto questo limite, i nuovi download potranno essere aggiunti tramite caricamento ftp e organizzati con la gestione file da PCA.',
	'HELP_DL_POSTS' => 'Ogni utente, anche ogni amministratore e moderatore dei Download, deve aver inviato questo numero minimo di messaggi per essere in grado di scaricare questo download non libero.<br />Si consiglia di installare anche una MOD Anti Spam per evitare gli spammer.<br />Inserirsci 0 per disabilitare la funzione; raccomandato per le Board appena create.',
	'HELP_DL_PREVENT_HOTLINK' => 'Abilita questa opzione se vuoi evitare link a download diretti esclusi i dettagli del download.<br />Questa opzione <b>non</b> costituisce una protezione per la cartella!',

    'HELP_DL_RATE_POINTS' => 'Imposta il numero massimo del punteggio che un utente può collegare a un download.<br /><br /><strong>Ricorda:</strong><br />Se modifichi questa impostazione tutti i punti dati fino ad ora verranno eliminati per consentire alla MOD di calcolare il punteggio corretto che rispetti il nuovo limite!',  

	'HELP_DL_REPLY_TRAFFIC' => 'Al traffico totale dell’utente sarà aggiunto l’ammontare di traffico qui inserito per ogni risposta o citazione data.',
	'HELP_DL_REPORT_BROKEN' => 'Attiva/disattiva la funzione per segnalare i download non funzionanti.<br />Se si è impostato "Non per gli ospiti", solo gli utenti registrati potranno segnalare i download.',
	'HELP_DL_REPORT_BROKEN_LOCK' => 'Se abiliti questa opzione il download sarà bloccato e segnalato come non funzionante.<br />Nasconde il pulsante download e nessun utente potrà scaricare il file fino a quando un amministratore o un moderatore del Download non avrà sbloccato il download stesso.',
	'HELP_DL_REPORT_BROKEN_MESSAGE' => 'Se un download è segnalato come non funzionante sarà visualizzato un messaggio<br />Se abiliti questa opzione il messaggio sarà riportato anche se il download è bloccato.<br />In questo caso non sotto, ma al posto del pulsante download.',
	'HELP_DL_REPORT_BROKEN_VC' => 'Abilita la conferma visuale se un utente segnala un download non funzionante.<br />Solo se il codice è corretto la segnalazione sarà salvata e gli amministratori e i moderatori del download saranno informati tramite email.',
	
	'HELP_DL_RSS_ENABLE'				=> 'Il Feed RSS per il download può essere abilitato/disabilitato completamente.<br />Quando questa funzione è disabilitata, le seguenti due opzioni definiscono ciò che l’utente vedrà in luogo del feed.',
	'HELP_DL_RSS_OFF_ACTION'			=> 'Con questa opzione è possibile definire cosa il Feed farà quando questo è disattivato.',
	'HELP_DL_RSS_OFF_TEXT'				=> 'Questo sarà il testo visualizzato in luogo delel voci di download del Feed RSS; se la funzione è stato disabilita e l’opzione precedente sarà impostata in base agli annunci di questo messaggio.<br />Nella precedente opzione si sarebbe dovuto creare un redirect, quindi questo testo rimarrà attivo, ma non visualizzato.',
	'HELP_DL_RSS_CATS'					=> 'Le voci sono visualizzate nel feed RSS da tutti o dalla lista visualizzata in categorie selezionate.<br />Per selezionare più categorie, si prega di tenere premuto il tasto CTRL.<br />Si può scegliere di includere le categorie selezionate o non selezionate per il Feed.',
	'HELP_DL_RSS_PERMS'					=> 'Nonostante la selezione con cui sono visualizzate le voci di categoria, può essere consigliabile verificare i permessi dell’utente, o addirittura chiudere per gli ospiti, e i Bot in modo da indicare nessun download nel feed, nell’account corrente.<br />L’impostazione "per gli ospiti" selezionerà solo le categorie che possono essere visualizzate dagli ospiti e dai Bot.<br />A meno che per l’utente ospite/Bot a causa delle categorie selezionate  non si impostino i permessi in modo diverso.',
	'DL_RSS_NEW_UPDATE'					=> 'Questa opzione segna download nuovi o modificati, così come la categoria mini-icona',
	'HELP_DL_RSS_NUMBER'				=> 'Numero massimo di download, visualizzati nel Feed.',
	'HELP_DL_RSS_SELECT'				=> 'Questa opzione determina se i download devono essere visualizzati nel Feed, a seconda della categoria scelta, e dei permessi.',


	'HELP_DL_RSS_DESC_LENGTH'			=> 'Con questa opzione, è possibile lasciare le descrizioni per visualizzare i download, oppure scegliere una presentazione breve (secondo l’impostazione per l’Indice del download).<br /><br /><strong>Attenzione:</strong><br />Poiché non tutti i lettori di Feed riconoscono anche il codice HTML, può accadere che il testo appaia difettoso. In questo caso, sarà bene che l’utente provi ad utilizzare un altro lettore.',
	'HELP_DL_RSS_DESC_LENGTH_SHORTEN'	=> 'Taglia dalla descrizione del download dopo i caratteri impostati se la descrizione sarà visualizzata in modo ridotto (vedi opzione precedente).<br />Scrivi 0 per non visualizzare la descrizione!',

	'HELP_DL_SHORTEN_EXTERN_LINKS' => 'Inserisci la lunghezza del link dei download esterni visualizzati sui dettagli del download.<br />Sulla base della lunghezza del link, sarà tagliato a metà o accorciato a partire dal lato destro.<br />Lascia questo campo vuoto o inserisci 0 per disabilitare questa funzione.',
	'HELP_DL_SHOW_FOOTER_LEGEND' => 'Questa opzione consente di attivare la legenda con le icone di stato On e Off relative al download, nella parte bassa della pagina dei download.<br />Le icone che si trovano accanto al download non saranno modificate da questa opzione.',
	'HELP_DL_SHOW_FOOTER_STAT' => 'Questa opzione consente di attivare le statistiche con On e Off nella parte bassa della pagina del download.<br />Le statistiche incrementeranno altri dati, se l’interruttore è in Off.',
	'HELP_DL_SHOW_REAL_FILETIME' => 'Con questa opzione è possibile visualizzare il tempo reale dell’ultima modifica dei file di download nel dettaglio download.<br />Questa è anche la stima del tempo esatto con cui i file vengono caricati con un client ftp o non aggiornati durante la modifica dei download.',
	'HELP_DL_SORT_PREFORM' => 'L’opzione "Predefinito" ordina tutti i download in tutte le categorie per tutti gli utenti come sono ordinate nel PCA.<br />Con l’opzione "Utente" ogni utente può scegliere come i download saranno ordinati per lui/lei e se questo ordinamento sarà fissato o esteso con criteri di altro tipo.',
	'HELP_DL_STAT_PERM' => 'Seleziona qui quale livello utente può essere visualizzato dagli utenti dalla pagina della statistica del download.<br />Es.: se abiliti il download per i moderatori, ogni amministratore della Board e i moderatori del download (NON i moderatori del Forum!) potranno aprire e visualizzare la pagina.<br />Nota che questa pagina può produrre un carico pesante, così raccomandiamo di non renderla accessibile alle masse, se hai una Board grande e/o se gestisci molti download.',
	'HELP_DL_STATISTICS' => 'Abilita le statistiche dettagliate sui file.<br />Nota che queste statistiche produrranno query supplementari nel database, in una tabella seperata.',
	'HELP_DL_STATS_PRUNE' => 'Inserisci il numero di righe nei dati di statistiche che questa categoria può raggiungere.<br />Ogni nuova riga cancellerà la più vecchia.<br />Inserisci 0 per disabilitare la cancellazione automatica.',
	'HELP_DL_STOP_UPLOADS' => 'Puoi abilitare/disabilitare l’upload con questa opzione.<br />Se si disattiva questa opzione, solo gli amministratori saranno in grado di caricare nuovi file.<br />Attiva questa opzione per consentire agli utenti di caricare i file a seconda dei permessi della categoria e del gruppo.',

	'HELP_DL_THUMB' => 'In questo campo puoi caricare una piccola immagine (nota le dimensioni dei file e l’immagine visualizzata sotto questo campo) per visualizzarla nei dettagli dei download.<br />Se esiste già una miniatura, è possibile caricarne una nuova per sostituirla.<br />Controlla la miniatura esistente, per "cancellarla".',
	'HELP_DL_THUMB_CAT' => 'Questa opzione permetterà le miniature sui download in questa categoria.<br />Le dimensioni di queste immagini si baseranno sulle impostazioni di configurazione principali di questa MOD.',
	'HELP_DL_THUMB_MAX_DIM' => 'Questi valori limiteranno le dimensioni delle possibili immagini delle miniature caricate.<br />Le miniature devono essere grandi al massimo 150 x 100 pixel ed è possibile visualizzare le immagini caricate in una finestra popup se si clicca sulla miniatura.<br /><br />Inserisci 0 per disabilitare le miniature (non raccomandato).<br />Le miniature esistenti saranno visualizzate fino a quando la dimensione dei file non sarà portata a 0.',
	'HELP_DL_THUMB_MAX_SIZE' => 'Inserisci 0 come dimensione dei file per disabilitare le miniature in tutte le categorie.<br />Se vuoi consentire le miniature, inserisci la dimensione massima dell’immagine per i file caricati da cui verranno create nuove miniature.<br />Se si disattivano le miniature non è possibile vedere nei dettagli del download le miniature esistenti.',
	'HELP_DL_TODO_LINK' => 'Scegli On o Off per le cose da fare nella lista della parte bassa della pagina del download. La gestione delle voci non sarà interessata modificando questa opzione.',
	'HELP_DL_TOPIC_DETAILS' => 'Mostra la descrizione del download, il nome del file, la dimensione o gli URL dell’argomento del Forum su download esterni.<br />Il testo può essere collocato sopra o dopo il testo immesso in precedenza.<br />Se l’argomento sarà creato nelle categorie dei download, l’opzione nella configurazione generale sarà ignorata.',
	'HELP_DL_TOPIC_FORUM'	=> 'Il forum che consente di visualizzare i nuovi argomenti per i download.<br />Se si seleziona in un forum l’opzione "Scegli una categoria", l’argomento verrà inserito nel forum selezionato delle categorie.',
	'HELP_DL_TOPIC_FORUM_C'	=> 'Il forum che consente di visualizzare i nuovi argomenti sui download di questa categoria.',
	'HELP_DL_TOPIC_TEXT'	=> 'Testo libero per la creazione di argomenti per i download. BBCode, HTML ed emoticon non sono ammessi qui, perché il testo deve essere utilizzato solo per introdurre l’argomento.',
	'HELP_DL_TOPIC_USER'	=> 'Scegli se l’utente debba essere l’autore degli argomenti dei download.<br />Se l’utente corrente dovesse essere l’autore dell’argomento, seleziona l’opzione utente corrente. L’opzione selezionata per categoria, consente di selezionare un utente per argomento. Questo può ancora essere l’utente corrente o più ID utenti selezionati, inseriti nel campo sul lato destro della tendina. Questo è consigliato per l’opzione "Seleziona utente per ID".<br /><br /><strong>Attenzione:</strong><br />L’ID dell’utente non sarà controllato dalla MOD download, quindi un ID non esistente può compromettere le funzioni!',
	'HELP_DL_TRAFFIC' => 'Il massimo traffico che un file potrà produrre.<br />Il valore 0 disattiva il controllo del traffico',
	'HELP_DL_TRAFFIC_OFF' => 'Attiva l’intera gestione del traffico nell’area download e disattiva tutte le opzioni per il traffico.<br />L’attivazione di questa opzione nasconderà tutti i testi per il traffico di download nel forum e non considererà i limiti di traffico. Allo stesso modo durante il download e l’upload i dati sul traffico non sarannno cambiati più.<br />Modifiche al traffico degli utenti durante la creazione o l’eliminazione dei messaggi, non sono ammesse negli account utente.<br />Il traffico assegnato automaticamente non sarà più assegnato agli utenti, se questa opzione è disattivata. Tuttavia per gli utenti o membri del gruppo è ancora possibile ottenere traffico con il modulo di gestione del traffico del PCA.<br />Inoltre, nella zona amministrazione tutti i moduli, i testi e le funzioni per la gestione del traffico saranno ancora invariati.',

	'HELP_DL_UCONF_LINK' => 'Seleziona On oppure Off nel link per la configurazione degli utenti nella parte bassa della pagina del download. La configurazione degli utenti non sarà interessata mentre si modifica questa opzione.',
	'HELP_DL_UPLOAD_FILE' => 'L’upload di file dal tuo computer.<br />Certo, la dimensione del file sarà più piccola come mostrato e l’estensione del file non sarà inclusa nella lista come si può vedere in questo campo.',
	'HELP_DL_UPLOAD_TRAFFIC_COUNT' => 'Se l’opzione è attivata, gli upload saranno inferiori come traffico complessivo mensile.<br />Dopo il raggiungimento del limite complessivo, nessun upload sarà possibile e i nuovi download potranno essere caricati con un client ftp e aggiunti nel PCA.',
	'HELP_DL_USE_EXT_BLACKLIST' => 'Se si attiva la lista nera tutti i tipi di file inseriti saranno bloccati per i nuovi download caricati o modificati.',
	'HELP_DL_USE_HACKLIST' => 'Questa opzione può abilitare o disabilitare la lista dei trucchi con On o con Off.<br />Se è attiva, è possibile inserire informazioni trucchi mentre aggiungi o modifichi un download per inserirlo nella lista dei trucchi.<br />Se disattivi la lista dei trucchi, sarà completamente nascosta per ogni utente, come se non fosse mai stata installata, ma è possibile attivarla ogni volta.<br />Nota che ogni informazione relativa ai trucchi sui download andrà persa, se vuoi modificare il file dopo che l’elenco è stato disabilitato.',
	'HELP_DL_USER_TRAFFIC_ONCE' => 'Scegli se i download debbano solo diminuire il traffico degli utenti solo per la prima volta scaricando il file.<br /><b>Nota:</b><br />questa opzione NON modifica lo stato del download!',

	'HELP_DL_VISUAL_CONFIRMATION' => 'Attiva questa opzione per consentire all’utente di inserire una codice di conferma visuale di 5 cifre per accedere al download.<br />Se l’utente non inserirà il codice o il codice sarà scorretto, la MOD visualizzerà un messaggio e interromperà il download.',

	'HELP_DOWNLOAD_PATH' => 'Il relativo percorso nella root della cartella del phpBB.<br />Alla prima installazione di questo pacchetto troverai qui il valore "dl_mod/downloads/".<br />Per quanto riguarda i nomi sono <i>case sensitive</i> se si utilizza Unix/Linux.<br />Lo Slash alla fine del nome della cartella sarà importante, ma non aggiungere lo Slash all’inizio.<br />Questa e le incluse sottocartelle devono avere anche i permessi CHMOD a 777 perché tutto funzioni correttamente.<br />Nell’ambito di questa cartella è necessario creare una o più sottocartelle che contengano tutti i file fisici.<br />Devi creare una sottocartella per ogni gruppo logico di categorie.<br />Questa sottocartella deve essere inserita come percorso nella categoria con la stessa sintassi come la cartella principale, ma senza usare la cartella principale (per saperne di più leggi l’aiuto nella gestione delle categorie).<br />Puoi creare ulteriori sottocartelle utilizzando un client ftp o utilizzando la casella degli strumenti (vedi il link in alto a destra di questa pagina).',

	'HELP_NUMBER_RECENT_DL_ON_PORTAL' => 'Il numero di download più recenti che l’utente vede sul portale.<br />Il blocco utilizza l’ultima modifica temporale per questa lista, in modo che possa essere possibile avere il download più vecchio in cima a questa lista.',
));

?>