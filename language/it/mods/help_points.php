<?php
/**
*
* help_points [Italian]
*
* @package language
* @version $Id: help_points.php 639 2010-02-21 06:04:51Z femu $
* copyright (c) 2009 wuerzi & femu
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if ( empty($lang) || !is_array($lang) )
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » „ “ — …
//

$help = array(
	array(
		0 => '--',
		1 => 'Generale'
	),
	array(
		0 => 'Aggiunta Messaggi',
		1 => 'Se un membro pubblica un messaggio, i punti saranno ricalcolati e aggiunti solo dopo la pubblicazione. <br /><br />Questa azione si abilita se i punti sono autorizzati ( > 0 ) per i punti nell\'ACP e che il Comutatoure sia "Si"'
	),
	array(
		0 => 'Aggiunta Argomenti',
		1 => 'Se un membro aggiunge argomenti (il suo primo messaggio nell\'argomento), riceverà punti solo per la generazione di un nuovo argomento. <br />Questo vuol dire che l\'argomento verr&agrave; ricalcolato completamente.
<br />Ma, il messaggio sar&agrave; ricalcolato solo se i punti sono autorizzati ( > 0 ) per i nuovi argomenti nell\'ACP e che il Comutatore sia su "Si".'
	),
	array(
		0 => 'Aggiunta Allegati',
		1 => 'Se un membro aggiunge o cancella un allegato solo i punti per l\'allegato sono calcolati e aggiunti al totale del suo conto.
<br /><br />Se si cancellano tutti gli allegati, i punti saranno dedotti. Gli allegati sono sempre calcolati e non hanno niente a che fare con con i messaggi o nuovi argomenti!'
	),
	array(
		0 => 'Aggiunta di Sondaggi',
		1 => 'Se un membro aggiunge sondaggi, i sondaggi saranno completamente ricalcolati e ricever&agrave; soltanto i punti del sondaggio finale. <br />Se il sondaggio viene cancellato, i punti del sondaggio saranno ritirati (il membro ricever&agrave; solamente, i punti corrispondenti al nuovo argomento).
<br /><br />Ma, l\'argomento sar&agrave; ricalcolato solo se i punti sono autorizzati ( > 0 ) per i nuovi argomenti nell\'ACP e che il  Comutatore sia su "Si". Poich&egrave; i sondaggi sono sempre nuovi argomenti!'
	),
	array(
		0 => 'Cancellazione di Messaggi',
		1 => 'Se un membro cancella i suoi messaggi, i punti per questo messaggio saranno ritirati dal suo conto.
<br /><br />Se un moderatore cancella i messaggi, i punti non saranno ritirati. <br />Inoltre se avete il pruning attivo, i punti non saranno ritirati.'
	),
	array(
		0 => 'bbCodes',
		1 => 'Tutti i caratteri all\'interno di un bbCode saranno contati. <br />I bbCode in s&egrave; non sar&agrave; contato.'
	),
	array(
		0 => 'Codice Blocco',
		1 => 'Tutto all\'interno del Codice ( [code] [/code] ) non sar&agrave; contato!'
	),
	array(
		0 => 'Smilies',
		1 => 'Gli smiles non sono contati. <br /><br />Attenzione, gli spazi tra gli smiles sono contati come carattere!'
	),
	array(
		0 => 'Caratteri e caratteri speciali',
		1 => 'Ogni singolo carattere sar&agrave; contato, se attivate il conteggio dei caratteri. <br /><br />I caratteri sono tutte le lettere, spazi, numeri, caratteri speciali e spazi vuoti'
	),
	array(
		0 => 'Citazioni',
		1 => 'Tutto quello contenuto nel codice Quote ( [quote] / [/quote] ) non sar&agrave; contato! <br /><br />Solamente i messaggi in s&egrave; ed il testo fuori alla citazione &egrave; contato.'
	),
	array(
		0 => 'Note Importante !',
		1 => 'Solo il testo dentro il codice bbCode sar&agrave; contato, ma non il bbCode in s&egrave;. <br /><br />Il bbCode non sar&agrave; letto !!<br />il codice finale ( [/xxx] ) &egrave; importante, se non indicato, solo il testo dopo il codice ( [xxx] ) sar&agrave; contato.
<br /><br />
Se un membro edita un messaggio per il quale non ha ricevuto punti (per esempio dopo l\'installazione di una MOD o la funzione non era attivata), non ricever&agrave; punti.
<br /><br />
Se un membro edita un messaggio o un argomento per il quale non ha ricevuto punti (per esempio dopo l\'installazione di una  MOD o la funzione non era attiva), questo messaggio o argomento non sar&agrave; contato'
	),
	array(
		0 => '--',
		1 => 'Settaggio Punti'
	),
	array(
		0 => 'Generale',
		1 => 'Qui potete indicare differenti nomi per i vostri "punti", attivare/disattivare Ultimate Points System e mettere un messaggio di disattivazione.
<br /><br />Potete anche attivare/disattivare le differenti parti d\'Ultimate Points System e altre possibili funzioni che sono auto-explicite.'
	),
	array(
		0 => 'Trasferimento di Gruppi',
		1 => 'Con questa opzione,nel settaggio dei punti avete la possibilit&agrave; di trasferire una certa somma di punti ad un gruppo. Se riempite il campo "Soggetto" e "Testo" potete inviare  un MP a tutti i membri del gruppo. Potete utilizzare i bbCodes, ma solamente se presenti nella minibox.'
	),
	array(
		0 => 'Reset Punteggio dei membri',
		1 => 'Con questa opzione regolazione punti, avete la possibilit&agrave; di cancellare tutti i punti degli utenti. <br /><br />Ma attenzione ! Questa azione &egrave; irreversibile!'
	),
	array(
		0 => 'Reset Log utenti',
		1 => 'Con questa opzione di regolazione punti, avete la possibilit&agrave; di cancellare tutti i log degli utenti. <br /><br />Ma attenzione ! Questa azione &egrave; irreversibile!'
	),
	array(
		0 => '--',
		1 => 'Settaggio Avanzato Punti'
	),
	array(
		0 => 'Allegati',
		1 => 'Potete dare dei punti per gli argomenti e messaggi con degli allegati. I punti principali sono donati 1 sola volta e i punti supplementari sono dati per ogni allegato.
<br /><br /><strong>NON POTETE</strong> disattivare gli allegati di base per forum!'
	),
	array(
		0 => 'Sondaggi',
		1 => 'Potete donare punti per i sondaggi (per una sola volta) e dei punti per ogni opzione del sondaggio.
<br /><br />Poich&eacute; i sondaggi sono possibili soltanto in occasione della creazione di un argomento, i punti saranno donati se autorizzati ( > 0 ) per i nuovi argomenti nell\'ACP e che il Comutatore sia "Si" !'
	),
	array(
		0 => 'Nuovi Argomenti',
		1 => 'Oltre ai punti per argomento, messa a punto dei forums, potete donare dei punti per ogni parola o i punti per ogni carattere.
<br /><br />Se regolate 0 nella regolazione del forum o se disattivate i punti per i nuovi argomenti, i punti supplementari non saranno aggiunti!'
	),
	array(
		0 => 'Repliche ai nuovi Messaggi',
		1 => 'Oltre ai punti per messaggio, messa a punto dei forums, potete donare dei punti per ogni parola o i punti per ogni carattere.
<br /><br />Se regolate 0 nella regolazione del forum o se disattivate i punti per i nuovi messaggi, i punti supplementari non saranno aggiunti !'
	),
	array(
		0 => 'Download e Allegati',
		1 => 'Qui potete impostare i costi di download. <br />Se settate 0 qui, il download &egrave; gratuito. <br />Se l\'utente non ha abbastanza punti, non potr&agrave; scaricare l\'allegato!

<br /><br />Nota Importante! Per esempio una foto (in parte unita) in un messaggio essendo direttamente visibile, i punti corrispondenti all\'immagine saranno sotratti. Se un utente non ha abbastanza punti, non vedr&agrave; la foto!'
	),
	array(
		0 => 'Punti per Avvertimenti',
		1 => 'Se un utente riceve un avvertimento, avete la possibilit&agrave; di ritirargli dei punti. <br /><br />Se non ha abbastanza punti, saranno comunque ritirati e passer&agrave; in saldo negativo!'
	),
	array(
		0 => 'Punti per Iscrizione',
		1 => 'Qui potete settare i punti che un utente ricever&agrave; dopo l\'iscrizione al forum. <br />Questo permetter&agrave; all\'utente di avere un piccolo capitale iniziale. Questi punti sono donati solo una volta!'
	),
	array(
		0 => 'Voci per pagina',
		1 => 'Qui potete impostare le voci per pagina pubblicare nei logs o la cronostoria della lotteria. 5 sono le voci minime.'
	),
	array(
		0 => 'Numero degli utenti pi&uacute; ricchi',
		1 => 'Qui potete impostare in numero degli utenti pi&ugrave; ricchi visualizzati. Visualizzerete questo numero di posti multipli nell\'index, nella banca e nella vista globale.
<br />0 per disattivare questa opzione. <br />Nessuna visualizzazione sull\'index e nella banca e nella vista globale un messaggio corrispondente sar&agrave; visualizzato.'
	),
	array(
		0 => '--',
		1 => 'Settaggio Punti Forum'
	),
	array(
		0 => 'Generale',
		1 => 'I punti dei forum sono indipendenti dalle altre regolazioni dei punti e sono quindi supplementari. Potete impostare i punti per forum, questo permetter&agrave; di impostare con precisione i punti che riceveranno gli utenti. Trovate questo settaggio nell\' ACP - Forums - Manage Forums - Forum che volete settare.'
	),
	array(
		0 => 'Tasti di Comutazione',
		1 => 'Con i comutatori di punti dei forum, si attiva/disattiva globalmente i punti per i forum. <br /><br />Se si disattivano i punti per argomento, messaggio o editor, i punti NON SARANNO aggiunti in tutti i forum. <br /><br />In pi&ugrave; i setup avanzati non saranno esaminati, tanto che i tasti di comutazione non saranno su "Si".'
	),
	array(
		0 => 'Settaggio Globale Punti Forum',
		1 => 'Qui potete impostare globalmente i punti per tutti i forum in una sola azione. <br /><br />Questi cambiamenti sovrascriveranno i dati immessi in precedenza per l\'utente! <br /><br />Se utilizzate questa opzione dovete riverificare tutte le regolazioni dentro ai forum!'
	),
	array(
		0 => 'Nuovi Argomenti',
		1 => 'Qui potete impostare quanti punti un utente ricever&agrave; dopo aver compilato un nuovo argomento. <br />Potete impostare globalmente o individualmente via ACP -> Forums.
<br /><br />Se segliete 0, le impostazioni avanzate dei punti (parole e caratteri) NON SARANNO AGGIUNTE.'
	),
	array(
		0 => 'Nuovi Messaggi',
		1 => 'Qui potete impostare quanti punti un utente ricever&agrave; dopo aver compilato un nuovo messaggio o reply. <br />Potete impostare globalmente o individualmente via  ACP -> Forums.
<br /><br />Se segliete 0, le regolazioni avanzate dei punti (parole e caratteri) NON SARANNO AGGIUNTE.'
	),
	array(
		0 => 'Editazione di Argomenti/Messaggi',
		1 => 'Qui potete impostare quanti punti un utente ricever&agrave; dopo aver editato un argomento o un messaggio.'
	),
	array(
		0 => '--',
		1 => 'Banca'
	),
	array(
		0 => 'Generale',
		1 => 'Se la banca &egrave; attivata, gli utenti vedranno una sezione supplementare nel men&ugrave; principale di Ultimate Points e potranno aprire un conto. In pi&ugrave; troveranno delle informazioni nel profilo e nella parte viewtopic, gli Amministratori/Moderatori avranno la possibilit&agrave; di modificare i punti in banca, se autorizzati.'
	),
	array(
		0 => 'Tassi di Interesse',
		1 => 'Qui potete settare i Tassi di interesse da 0 al 100 percento per il periodo di pagamento. Il periodo di pagamento &egrave; fissato al "giorno". Dopo questo periodo gli utenti otterranno il tasso di interesse pagato automaticamente. Potete anche definire a che importo i pagamenti termineranno. Se un utente possiede pi&ugrave; di questo importo definito, non verranno toccati gli interessi.'
	),
	array(
		0 => 'Costi Bancari',
		1 => 'Qui potete settare i costi per il prelievo di punti dal conto da 0 al 100 percento. In pi&ugrave; potete fissare un valore di costo fisso per il mantenimento del conto  nel periodo. Questo avr&agrave; lo stesso periodo del tasso di interesse.'
	),
	array(
		0 => '--',
		1 => 'Lotteria'
	),
	array(
		0 => 'Generali',
		1 => 'Se la Lotteria &egrave; attivata, gli utenti avranno accesso al modulo Lotteria. Vedranno una sezione supplementare nel men&ugrave; principale di Ultimate Points e potranno giocare alla lotteria.
Se disattivate questo modulo, la Lotteria funziona comunque in background, ma gli utenti non ne avranno accesso.'
	),
	array(
		0 => 'Come funziona la Lotteria',
		1 => 'Con un calcolo casuale, un biglietto tra tutti quelli comprati verr&agrave; sorteggiato come possibile vincitore. <br />In seguito un altro calcolo casuale definisce - in funzione del numero di possibilit&agrave; di uscita - se il biglietto &egrave; realmente vincente o no.
<br />Se non &egrave; vincente l\'importo viene aggiunto al Jackpot fino a che nessuno vince.'
	),
	array(
		0 => 'Jackpot',
		1 => 'La Lotteria funziona con un sistema di Jackpot. 
L\'importo di tutti gli acquisti di biglietti aumenta il valore del Jackpot oltre al valore di partenza, che potete definire. <br />Se nessuno vince il Jackpot &egrave; rimesso in gioco ed aumenter&agrave; con i biglietti della prossima estrazione.'
	),
	array(
		0 => 'Probabilit&agrave; di vincita',
		1 => 'Qui potete impostare la  probabilit&agrave; di vincita. Gli utenti non vedranno questo valore!!<br />Pi&ugrave; il valore &egrave; alto, pi&ugrave; sono le probalilit&agrave; di vincere. <br />0 non vincer&agrave; mai nessuno, 100 indicher&agrave; che ci sar&agrave; un vincitore al prossimo sorteggio.'
	),
	array(
		0 => 'Periodo di Sorteggi',
		1 => 'Qui potete impostare il periodo di sorteggio in ore. <br />Questa impostazione ha effetto immediato!
<br />Se settato a 0 i pagamenti saranno bloccati.
<br />Gli utenti non possono comprare nessun biglietto e la posta rimane del valore attuale.
<br />Potete utilizzare questa impostazione per forzare un pagamento! <br />Impostando un nuovo valore, il pagamento sar&agrave; visualizzato e partir&agrave; al prossimo richiamo della pagina.'
	),
	array(
		0 => 'Invio ID',
		1 => 'Qui potete impostare l\'ID degli utenti, verr&agrave; inviato all\'utente fortunato un messaggio di vincita via MP. Se regolato su 0 l\'utente ricever&agrave; un messaggio da s&egrave; stesso.'
	),
	array(
		0 => '--',
		1 => 'Furto'
	),
	array(
		0 => 'Generale',
		1 => 'Con il modulo furto, un utente pu&ograve; rubare dei punti dal conto di un altro utente!. Se il furto &egrave; attivato, gli utenti vedranno una sezione supplementare nel men&ugrave; principale di Ultimate Points.'
	),
	array(
		0 => 'Settaggio MP',
		1 => 'Qui potete impostare se gli utenti saranno informati di un tentativo di furto. Se un utente ha impostato nelle prefernze di non voler ricevere MP, non ricever&agrave; MP dal modulo di Furto.
Se il ladro &egrave; bloccato per l\'invio di MP, un MP sar&agrave; comunque inviato all\'utente. Questo messaggio &egrave; predefinito ed il ladro non avr&agrave; nessuna influenza...'
	),
	array(
		0 => 'Possibilit&agrave; di successo sul furto',
		1 => 'Qui potete impostare la possibilit&agrave; di riuscire un furto in percentuale. Potete impostare il valore da 0 e 100 percento.'
	),
	array(
		0 => 'Penalit&agrave; sul fallimento di un furto',
		1 => 'Qui potete impostare la penalit&agrave; che il ladro dovr&agrave; pagare se il furto fallisce.
Il ladro dovr&agrave; pagare una percentuale della somma che ha tentato di rubare. Se il ladro dovr&agrave; pagare verr&agrave; indicato nella pagina dei Furti. Potete fissare il valore fra 0 e 100 percento.'
	),
	array(
		0 => 'Valore massimo che pu&ograve; essere rubato in 1 volta',
		1 => 'Qui potete impostare la percentuale massima di punti che pu&ograve; essere rubata in 1 volta. Questo valore sar&agrave; indicato nella pagina dei Furti. Potete impostare il valore 0 a 100 percento.'
	),
	array(
		0 => '--',
		1 => 'Trasferimenti / Donazioni'
	),
	array(
		0 => 'Generale',
		1 => 'Se i Trasferimenti/Donazioni &egrave; attivato, gli utenti vedranno una sezione supplementare nel men&ugrave; principale di Ultimate Points. Se gli utenti hanno il permesso, potranno effettuare dei trasferimenti o donazioni di punti tra di loro. Questo potr&agrave; essere eseguito dalla pagina del modulo Trasferimenti, argomenti o i profili.'
	),
	array(
		0 => 'Trasferimenti con MP',
		1 => 'Potete attivare/disattivare questa opzione dalla pagina principale dei punti nell\'ACP. Se un utente &egrave; bloccato per l\'invio di MP, non potr&agrave; aggiungere commenti al suo trasferimento.'
	),
	array(
		0 => 'Logs',
		1 => 'Tutti i trasferimenti saranno visualizzati nella pagina dei Logs. questa funzione pu&ograve; essere attivata/disattivata dalla pagina principale dei punti dall\'ACP. Avete anche la possibilit&agrave; di cancellare TUTTI i logs. <br /><br />Ma, attenzione questa azione &egrave; irreversibile!'
	),
	array(
		0 => '--',
		1 => 'Permessi'
	),
	array(
		0 => 'Permessi Amministratore',
		1 => 'Potete dare all\'amministratore il consenso di amministrare Ultimate Points System, dalla pagina dell\'ACP -> Permessi Globali -> Amministratori -> Permessi avanzati.'
	),
	array(
		0 => 'Permessi Globali Moderatori',
		1 => 'Potete dare ai Moderatori il consenso di cambiare i punti e il conto bancario degli utenti  degli utenti, pagina ACP -> Permessi Globali -> Moderatori Globali -> Permessi avanzati..'
	),
	array(
		0 => 'Permessi Utenti e Gruppi',
		1 => 'Potete dare agli utenti il consenso di utilizzare Ultimate Points System, dalla pagina ACP -> Permessi -> Pernessi Utenti/Gruppi. Vedi sotto:
<ul>
	<li>Pu&ograve; utilizzare Ultimate Points</li> 			
	<li>Pu&ograve; utilizzare il Modulo Banca</li>  	 	 	
	<li>Pu&ograve; utilizzare il Modulo Logs</li> 			
	<li>Pu&ograve; utilizzare il Modulo Lotteria</li> 			
	<li>Pu&ograve; utilizzare il Modulo Furti</li> 			
	<li>Pu&ograve; utilizzare il Modulo Trasferimenti</li>
</ul>'
	),
	array(
		0 => '--',
		1 => 'AddOns e compatibilit&agrave; con altre MODs'
	),
	array(
		0 => 'Generale',
		1 => 'Ultimate Points System &egrave; attualmente compatibile con altre MODs.'
	),
	array(
		0 => 'phpBB Arcade',
		1 => 'La Sala Giochi di Jeff ( <a href="http://www.jeffrusso.net/">JeffRusso.net</a> ) &egrave; compatibile con Ultimate Points System.<br />
Questa Salagiochi rileva se Ultimate Points System &egrave; installato. Potete stabilire il costo per gioco e di Jackpot.'
	),
	array(
		0 => 'phpbb Gallery',
		1 => 'La phpBB Gallery di nickvergessen ( <a href="http://www.flying-bits.org/">Flying-bits.org</a> ) &egrave; compatibile con Ultimate Points.<br />
Non appena avete installato la Galleria, vedrete un campo supplementare nella pagina principale dei punti.<br /><br />Dovete copiare l\'addon incluso nel pacchetto UPS in ( contrib/AddOns/Gallery_Integration/root/gallery/includes/hookup_gallery.php ) verso il pacchetto gallery !<br /><br /><strong>Nota Importante !</strong> Se utilizzate uno dei box d\'immagine (Highslide, Lightbox, Shadowbox.), i punti saranno dedotti 2 volte per motivi tecnici. Se volete vedere un immagine, vi verranno dedotti, dovete indicare 1 punto qui! <br />Per il box Highslide, potete trovare un correttivo <a href="http://highslide.com/forum/viewtopic.php?p=18498#p18498">qui</a><br /><br />In parallelo potete attivare/disattivare la possibilit&agrave; per un utente di vedere un immagine comunque anche se il totale dei suoi punti &egrave; negativo.'
	),
	array(
		0 => 'Medal System MOD',
		1 => 'Le Medal System de Gremlinn ( <a href="http://test.dupra.net/">Gremlinn\'s Mod Support Site</a> ) &egrave; compatibile con Ultimate Points.<br />
Nell\'Acp de Medal Mod, trovate un campo dove potete impostare il numero di punti da assegnare alla medaglia attribuita.'
	),
	array(
		0 => 'Sudoku',
		1 => 'Il Sudoku MOD di el_teniente ( <a href="http://vfalcone.ru/">vfalcone.ru</a> ) &eacute; compatibile con Ultimate Points.<br />
Anche se possiede un sistema di punti interno, potete definire i punti che gli utenti riceveranno dal sistema di ricompensa.'
	),
	array(
		0 => 'F1 Webtipp',
		1 => 'La Formula 1 Webtipp di Dr.Death ( <a href="http://www.lpi-clan.de/">LPI-Clan</a> ) &eacute; compatibile con  Ultimate Points.<br />
Potete impostare il numero di punti da assegnare per i tipps.'
	),
	array(
		0 => 'DM Video',
		1 => 'La DM Video MOD di femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ) &eacute; compatibile con Ultimate Points.<br />
Potete impostare il numero di punti da assegnare per l\'aggiunta del video. Questo importo sar&agrave; naturalmente sottratto quando l\'utente canceller&agrave; un video.'
	),
	array(
		0 => 'Shop Mod',
		1 => 'La Shop Mod v1.0.4 Beta di Adrian &eacute; compatibile con Ultimate Points.<br />
Per ulteriori informazioni, andate sul sito di Adrian<a href="http://phpbbgods.org/community/index.php]phpbbgods.org">phpbbgods.org</a>.<br />Utilizzate ugualmente questo sito per un supporto sulla Shop Mod!'
	),
	array(
		0 => 'User Blog Mod',
		1 => 'La User Blog Mod di EXreaction ( <a href="http://www.lithiumstudios.org/">Lithiumstudios.org</a> ) &eacute; compatibile con Ultimate Points.<br />

Prima di utilizzare  User Blog Mod con l\'UPS, dovete installare il plugins incluso nel pacchetto UPS ( contrib/AddOns/User_Blog_Mod_Plugin ).
Dopo questa installazione, troverete dei settaggi supplementari nel settaggio di Blog Mod.'
	),
	array(
		0 => 'Board3 Portal',
		1 => 'Troverete nel pacchetto UPS ( contrib/AddOns/Board3_Portal_AddOns ) un addon che permetter&agrave; di visualizzare la Lotteria in  Board3 Portal ( <a href="http://www.board3.de/">Board3 Portal</a> ).'
	),	
	array(
		0 => 'DM Easy Download System',
		1 => 'La DM EDS di femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ) &eacute; compatibile con Ultimate Points..<br />
		La DM EDS &egrave; un sistema di Download molto semplice, dove potete fissare i costi per ogni trasferimento individualmente.
		<br /><br />
		Se gli utenti non hanno punti sufficienti, non potranno scaricare nessun file.'
	),
	array(
		0 => 'DM Quotes Collection',
		1 => 'DM Quotes Collection &egrave; una semplice utility di femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ), dove avete la possibilit&agrave; di iniziare il commercio di collezioni che saranno visualizzate in ordine casuale nell\'index del forum. >br />
		Tutte le citazioni sono gestite via ACP. <br /><br />Appena un utente inserisce una citazione ricever&agrave; i punti corrispondenti, regolati nell\'ACP.'
	),
	array(
		0 => 'Knuffel (Giochi di Matrici)',
		1 => 'Ultimate Points &eacute; compatibile con Knuffel (gioco clone di Kniffel) di Wuerzi ( <a href="http://www.spieleresidenz.de/">Spieleresidenz</a> ). In questo gioco dovete realizzare degli esempi di Matrici (come il Yahtzee) per avere il punteggio pi&ugrave; alto.<br />
		Nell\'ACP potete impostare il costo per gioco e anche per il Jackpot.'
	),
	array(
		0 => 'phpBB Ajax Partners',
		1 => 'La phpbb Ajax Partners di djchrisnet ( <a href="http://djchrisnet.de/?page=partners">djchrisnet Webdesign</a> ) &eacute; compatibile con Ultimate Points..<br />
		Con l\'Ajax Partners Mod, Potete aggiungere una zona di partners, dove potete ricevere i punti per le osservazioni, valutazioni, etc. <br />
<strong>In Sviluppo!</strong>'
	),
	array(
		0 => 'Invita un Amico',
		1 => 'L\'Invite A Friend Mod di Bycoja ( <a href="http://bycoja.by.funpic.de/">Bycoja\'s Bugs</a> ) partendo dalla versione 0.5.3 &eacute; compatibile con Ultimate Points.
. Invita un Amico, &egrave; un supplemento per phpBB3, questo permette ai vostri utenti di raccomandare i loro amici alla vostra board.'
	),
	array(
		0 => 'Hangman',
		1 => 'The Hangman game ( <a href="http://area53.die-muellers.org/">Our Mod Support Site</a> ) &eacute; compatibile con Ultimate Points. All\'interno di questo gioco gli utenti possono guadagnare i punti inviando un nuovo Hangmans o indovinando le parole.'
	),
);

?>