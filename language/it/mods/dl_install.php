<?PHP

/**
*
* @mod package		Download Mod 6
* @file				dl_install.php 3 2011/06/17 OXPUS
* @copyright		(c) 2008 oxpus (Karsten Ude) <webmaster@oxpus.de> http://www.oxpus.de
* @copyright mod	(c) hotschi / demolition fabi / oxpus
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-10-24
* @license			http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/


/*
* [ Italian ] language file for Download MOD 6
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'DOWNLOAD_MOD'						=> 'MOD Download',

	'INSTALL_DOWNLOAD_MOD'				=> 'Installa MOD Download',
	'INSTALL_DOWNLOAD_MOD_CONFIRM'		=> 'Da qui puoi installare la MOD Donwload nel tuo forum.<br />Clicca sul pulsante per continuare.',
	'UPDATE_DOWNLOAD_MOD'				=> 'Aggiorna MOD Download',
	'UPDATE_DOWNLOAD_MOD_CONFIRM'		=> 'Da qui puoi aggiornare la MOD Donwload nel tuo Forum.<br />Clicca sul pulsante per continuare.',
	'UNINSTALL_DOWNLOAD_MOD'			=> 'Disinstalla MOD Download',
	'UNINSTALL_DOWNLOAD_MOD_CONFIRM'	=> 'Da qui puoi disinstallare completamente la MOD Donwload dal tuo Forum.<br />Clicca sul pulsante per continuare.',
	'DL_MOVE_CONFIGURATION'				=> 'Sposta la configurazione della Board',
	'DL_MOVE_TRAFFIC_VALUES'			=> 'Sposta i dati del traffico',
	'DL_DROP_OLD_VERSION'				=> 'Elimina il vecchio numero di versione della MOD',
	'DL_DROP_OLD_CAPTCHA'				=> 'Elimina i vecchi dati captcha',
	'DL_CONVERT_STATISTICAL_DATA'		=> 'Converti le statistiche',
	'DL_ADD_REAL_FILE'					=> 'Aggiungi i nomi reali',
));

?>