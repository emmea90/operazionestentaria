<?php
/**
*
* SafeGamerTag acp module [Italian]
*
* @package language
* @version 1.0
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// NOTE TO TRANSLATORS:  Text in parenthesis refers to keys on the keyboard

$lang = array_merge($lang, array(
	'ACP_CAT_SAFEGT'	=> 'Safe GamerTag',
	'ACP_SAFEGT_CFG'	=> 'Configura Safe GamerTag',

	'LOG_SAFEGT_CONFIG_EDIT'	=> '<strong>Aggiornato il config di Safe GamerTag.</strong><br />%s',
));

$lang = array_merge($lang, array(
	'acl_a_safegt'	=> array('lang' => 'Può configurare Safe GamerTag e moduli relativi', 'cat' => 'misc')
));
?>