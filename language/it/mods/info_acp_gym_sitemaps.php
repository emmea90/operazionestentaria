<?php
/**
*
* info_acp_gym_sitemaps [Italian]
*
* @package phpBB SEO GYM Sitemaps
* @version $Id$
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @copyright (c) 2011 - www.phpbbitalia.net translated on 2011-04-16
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License 
*
*/
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_GYM_SITEMAPS' => 'GYM sitemap &amp; RSS',
	'ACP_GYM_MAIN' => 'Configurazione principale',
	'ACP_GYM_GOOGLE_MAIN' => 'Google sitemap',
	'ACP_GYM_RSS_MAIN' => 'Feed RSS',
	'ACP_GYM_YAHOO_MAIN' => 'Yahoo! Lista URL',
	'ACP_GYM_HTML_MAIN' => 'HTML sitemap',
	'GYM_LOG_CONFIG_MAIN' => '<strong>Impostazioni GYM sitemap &amp; RSS modificate</strong><br />&raquo; Impostazioni principali',
	'GYM_LOG_CONFIG_GOOGLE' => '<strong>Impostazioni GYM sitemap &amp; RSS modificate</strong><br />&raquo; Google sitemap',
	'GYM_LOG_CONFIG_RSS' => '<strong>Impostazioni GYM sitemap &amp; RSS modificate</strong><br />&raquo; Feed RSS',
	'GYM_LOG_CONFIG_HTML' => '<strong>Impostazioni GYM sitemap &amp; RSS modificate</strong><br />&raquo; Sitemap HTML',
	'GYM_LOG_CONFIG_YAHOO' => '<strong>Impostazioni GYM sitemap &amp; RSS modificate</strong><br />&raquo; Yahoo! Liste URL',
	// Install Logs
	'SEO_LOG_INSTALL_GYM_SITEMAPS' => '<strong>GYM sitemap &amp; RSS V%s installata</strong>',
	'SEO_LOG_INSTALL_GYM_SITEMAPS_FAIL' => '<strong>Tentativo di installazione fallito di GYM sitemap &amp; RSS</strong><br />%s',
	'SEO_LOG_UNINSTALL_GYM_SITEMAPS' => '<strong>Disinstallazione di GYM sitemap &amp; RSS V%s</strong>',
	'SEO_LOG_UNINSTALL_GYM_SITEMAPS_FAIL' => '<strong>Tentativo di disinstallazione fallito di GYM sitemap &amp; RSS</strong><br />%s',
	'SEO_LOG_UPDATE_GYM_SITEMAPS' => '<strong>Aggiorna GYM sitemap &amp; RSS alla V%s</strong>',
	'SEO_LOG_UPDATE_GYM_SITEMAPS_FAIL' => '<strong>Tentativo di aggiornamento fallito di GYM sitemap &amp; RSS</strong><br />%s',
));

?>