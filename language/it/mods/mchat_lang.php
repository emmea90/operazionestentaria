<?php
/**
*
* @package mChat
* @version $Id: mchat_lang.php 
* @copyright (c) RMcGirr83 ( http://www.rmcgirr83.org/ )
* @copyright (c) djs596 ( http://djs596.com/ ), (c) Stokerpiller ( http://www.phpbb3bbcodes.com/ )
* @copyright (c) By Shapoval Andrey Vladimirovich (AllCity) ~ http://allcity.net.ru/
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
* @copyright (c) 2014 - www.phpbbitalia.net translated by Darkman on 2014-03-12
**/

/**
* DO NOT CHANGE!
*/
if (!defined('IN_PHPBB'))
{
  exit;
}

if (empty($lang) || !is_array($lang))
{
  $lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste (Unicode characters):
// ’ » “ ” …
//

$lang = array_merge($lang, array(

	// MCHAT
	'MCHAT_ADD'					=> 'Invia',
	'MCHAT_ANNOUNCEMENT'		=> 'Annuncio',
	'MCHAT_ARCHIVE'				=> 'Archivio',	
	'MCHAT_ARCHIVE_PAGE'		=> 'Archivio Mini-Chat',	
	'MCHAT_BBCODES'				=> 'BBCode',
	'MCHAT_CLEAN'				=> 'Pulisci',
	'MCHAT_CLEANED'				=> 'Tutti i messaggi sono stati rimossi con successo',
	'MCHAT_CLEAR_INPUT'			=> 'Ripristina',
	'MCHAT_COPYRIGHT'			=> '&copy; <a href="http://www.rmcgirr83.org/">RMcGirr83.org</a>',
	'MCHAT_CUSTOM_BBCODES'		=> 'Personalizza BBCode',
	'MCHAT_DELALLMESS'			=> 'Rimuovi tutti i messaggi?',
	'MCHAT_DELCONFIRM'			=> 'Confermi la rimozione?',
	'MCHAT_DELITE'				=> 'Elimina',
	'MCHAT_EDIT'				=> 'Modifica',
	'MCHAT_EDITINFO'			=> 'Modifica il messaggio e clicca su OK',
	'MCHAT_ENABLE'				=> 'Siamo spiacenti, la Mini-Chat non è al momento disponibile',	
	'MCHAT_ERROR'				=> 'Errore',	
	'MCHAT_FLOOD'				=> 'Non puoi inviare un altro messaggio così presto dopo l’ultimo da te inserito',	
	'MCHAT_FOE'					=> 'Questo messaggio è stato inviato da <strong>%1$s</strong> che è attualmente nella tua lista di ignorati.',
	'MCHAT_HELP'				=> 'Regole mChat',
	// uncomment and translate the following line for languages for the rules in the chat area
// <br /> signifies a new line, see above for Unicode characters to use
		//'MCHAT_RULES'				=> 'Non insultare<br />Non fare pubblicità al tuo forum o sito senza autorizzazione<br />Non lasciare messaggi  consecutivi senza senso<br /><br />Non lasciare messaggi con sole emoticons',	
	'MCHAT_HIDE_LIST'			=> 'Nascondi Lista',	
	'MCHAT_HOUR'				=> 'ora ',
	'MCHAT_HOURS'				=> 'ore ',
	'MCHAT_IP'					=> 'IP whois per',
	
	'MCHAT_MINUTE'				=> 'minuto ',
	'MCHAT_MINUTES'				=> 'minuti ',
	'MCHAT_MESS_LONG'			=> 'Il tuo messaggio è troppo lungo.\nPer favore limitalo a %s caratteri',	
	'MCHAT_NO_CUSTOM_PAGE'		=> 'La pagina personalizzata mChat non è attivata in questo momento!',	
	'MCHAT_NOACCESS'			=> 'Non hai il permesso di inserire un messaggio nella mChat',
	'MCHAT_NOACCESS_ARCHIVE'	=> 'Non hai il permesso di visualizzare l’Archivio',	
	'MCHAT_NOJAVASCRIPT'		=> 'Il tuo browser non supporta JavaScript o JavaScript è disabilitato',		
	'MCHAT_NOMESSAGE'			=> 'Nessun messaggio',
	'MCHAT_NOMESSAGEINPUT'		=> 'Non hai inserito un messaggio',
	'MCHAT_NOSMILE'				=> 'Smilie non trovata',
	'MCHAT_NOTINSTALLED_USER'	=> 'La mChat non è installata. Si prega di contattare il fondatore della Board.',
	'MCHAT_NOT_INSTALLED'		=> 'Sono assenti voci di database della mChat.<br />Si prega lanciare l’%sinstaller%s per apportare le modifiche del database della MOD.',
	'MCHAT_OK'					=> 'OK',
	'MCHAT_PAUSE'				=> 'In pausa',
	'MCHAT_LOAD'				=> 'Loading',   
	'MCHAT_PERMISSIONS'			=> 'Modificare i permessi agli utenti',
	'MCHAT_REFRESHING'			=> 'Refreshing...',
	'MCHAT_REFRESH_NO'			=> 'Autoaggiornamento non attivo',
	'MCHAT_REFRESH_YES'			=> 'Autoaggiornamento ogni <strong>%d</strong> secondi',
	'MCHAT_RESPOND'				=> 'Rispondi all’utente',
	'MCHAT_RESET_QUESTION'		=> 'Pulire l’area di testo?',
	'MCHAT_SESSION_OUT'			=> 'La sessione della chat è scaduta',	
	'MCHAT_SHOW_LIST'			=> 'Vedi lista',
	'MCHAT_SECOND'				=> 'secondo ',
	'MCHAT_SECONDS'				=> 'secondi ',
	'MCHAT_SESSION_ENDS'		=> 'La sessione di chat terminata in',
	'MCHAT_SMILES'				=> 'Emoticon',

	'MCHAT_TOTALMESSAGES'		=> 'Messaggi totali: <strong>%s</strong>',
	'MCHAT_USESOUND'			=> 'Vuoi usare un suono?',
	
// commentare e tradurre la seguente riga per le lingue per il messaggio statico nell’area di chat
//	'STATIC_MESSAGE'			=> 'Scrivete quello che volete qui',
	// whois chatting stuff

	'MCHAT_ONLINE_USERS_TOTAL'			=> 'In totale ci sono <strong>%d</strong> utenti in chat ',
	'MCHAT_ONLINE_USER_TOTAL'			=> 'In totale c’è <strong>%d</strong> utente in  chat ',
	'MCHAT_NO_CHATTERS'					=> 'Nessuno è in chat',
	'MCHAT_ONLINE_EXPLAIN'				=> '(basato sugli utenti attivi negli ultimi %s)',
	
	'WHO_IS_CHATTING'			=> 'Chi è in chat',
	'WHO_IS_REFRESH_EXPLAIN'	=> 'Refresh ogni <strong>%d</strong> secondi',
	'MCHAT_NEW_TOPIC'			=> '<strong>Nuovo argomento</strong>',		
	
	// UCP
	'UCP_PROFILE_MCHAT'	=> 'Preferenze mChat',
	
	'DISPLAY_MCHAT' 	=> 'Visualizza mChat nell’Indice',
	'SOUND_MCHAT'		=> 'Attiva il suono mChat',
	'DISPLAY_STATS_INDEX'	=> 'Visualizza le statistiche Chi è in Chat  nell’Indice',
	'DISPLAY_NEW_TOPICS'	=> 'Visualizza nuovi argomenti nella Chat',
	'DISPLAY_AVATARS'	=> 'Visualizza gli avatar nella Chat',
	'CHAT_AREA'		=> 'Tipo di input',
	'CHAT_AREA_EXPLAIN'	=> 'Scegli quale tipo di area da usare per immettere una chat:<br />Un’area di testo oppure<br />un’area di input',
	'INPUT_AREA'		=> 'Area di input',
	'TEXT_AREA'			=> 'Area di testo',	

	// ACP
	'USER_MCHAT_UPDATED'	=> 'Le preferenze mchat degli utenti sono state aggiornate',
));
?>
