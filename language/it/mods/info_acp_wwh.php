<?php

/**
*
* @package phpBB3 - who was here MOD
* @version $Id: info_acp_wwh.php 61 2007-12-17 20:15:23Z nickvergessen $
* @copyright (c) nickvergessen ( http://www.flying-bits.org/ )
* Tradotto da Angolo
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/
if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
if (!isset($phpbb_root_path) && defined('IN_ADMIN'))
{
	$phpbb_root_path = '../';
}
else if (!isset($phpbb_root_path))
{
	$phpbb_root_path = './';
}

$lang = array_merge($lang, array(
	'WWH_CONFIG'				=> 'Configura "Chi è stato qui?"',
	'WWH_TITLE'					=> 'Chi è stato qui?',

	'WWH_DISP_SET'				=> 'Impostazioni di visualizzazione',
	'WWH_DISP_BOTS'				=> 'Mostra Bot',
	'WWH_DISP_BOTS_EXP'			=> 'Qualche utente potrebbe chiedersi cosa sono i Bot e temerli.',
	'WWH_DISP_GUESTS'			=> 'Mostra ospiti',
	'WWH_DISP_GUESTS_EXP'		=> 'Vuoi visualizzare gli ospiti nel contatore?',
	'WWH_DISP_HIDDEN'			=> 'Mostra gli utenti nascosti',
	'WWH_DISP_HIDDEN_EXP'		=> 'Gli utenti nascosti dovrebbero visualizzarsi nella lista (autorizzazione necessaria)',
	'WWH_DISP_TIME'				=> 'Mostra l’orario',
	'WWH_DISP_TIME_FORMAT'		=> 'Formato dell’orario',
	'WWH_DISP_HOVER'			=> 'Visualizza oltre',
	'WWH_DISP_TIME_EXP'			=> 'Tutti gli utenti vedranno l’orario o nessuno. Nessuna funzione speciale per gli amministratori',
	'WWH_DISP_IP'				=> 'Mostra l’IP degli utenti',
	'WWH_DISP_IP_EXP'			=> 'Solo per gli utenti con le autorizzazioni amministrative, come per viewonline.php',

	'WWH_INSTALLED'				=> 'Installata la MOD "Chi è stato qui?" v%s',

	'WWH_RECORD'				=> 'Record',
	'WWH_RECORD_EXP'			=> 'Visualizza e salva il record',
	'WWH_RECORD_TIMESTAMP'		=> 'Formato data per il record',
	'WWH_RESET'					=> 'Ripristina il record',
	'WWH_RESET_EXP'				=> 'Ripristina l’orario e il contatore del Record di chi è stato qui',
	'WWH_RESET_TRUE'			=> 'Se invii questo form,\n il record sarà azzerato.',// \n is the beginning of a new line
									//no space after it

	'WWH_SAVED_SETTINGS'		=> 'La configurazione è stata modificata con successo.',
	'WWH_SORT_BY'				=> 'Ordina gli utenti per',
	'WWH_SORT_BY_EXP'			=> 'In quale ordine devono essere visualizzati gli utenti?',
	'WWH_SORT_BY_0'				=> 'Nome utente A -> Z',
	'WWH_SORT_BY_1'				=> 'Nome utente Z -> A',
	'WWH_SORT_BY_2'				=> 'Orario della visita crescente',
	'WWH_SORT_BY_3'				=> 'Orario della visita decrescente',
	'WWH_SORT_BY_4'				=> 'ID-Utente crescente',
	'WWH_SORT_BY_5'				=> 'ID-Utente decrescente',

	'WWH_UPDATE_NEED'			=> 'Aggiorna la MOD "Chi è stato qui?". Quindi esegui l’<a style="font-weight: bold;" href="' . $phpbb_root_path . 'install/index.php">install/index.php</a>.<br />Se già lo hai fatto, devi cancellare la cartella di installazione.',

	'WWH_VERSION'				=> 'Visualizzati utenti di... ',
	'WWH_VERSION_EXP'			=> 'Visualizzati gli utenti di oggi, o del termine fissato nella prossima opzione.',
	'WWH_VERSION1'				=> 'Oggi',
	'WWH_VERSION2'				=> 'Periodo di tempo',
	'WWH_VERSION2_EXP'			=> 'scrivi 0, se vuoi visualizzare gli utenti nelle ultime 24h',
	'WWH_VERSION2_EXP2'			=> 'disabilitato, se hai scelto "Oggi"',
	'WWH_VERSION2_EXP3'			=> 'secondi',

	'WWH_MOD'					=> 'MOD "Chi è stato qui?"',
	'INSTALL_WWH_MOD'			=> 'Installa la MOD "Chi è stato qui?"',
	'INSTALL_WWH_MOD_CONFIRM'	=> 'Sei sicuro di volere installare la MOD "Chi è stato qui?"',
	'UPDATE_WWH_MOD'			=> 'Aggiorna la MOD "Chi è stato qui?"',
	'UPDATE_WWH_MOD_CONFIRM'	=> 'Sei sicuro di voler aggiornare la MOD "Chi è stato qui?"',
	'UNINSTALL_WWH_MOD'			=> 'Disinstalla la MOD "Chi è stato qui?"',
	'UNINSTALL_WWH_MOD_CONFIRM'	=> 'Sei sicuro di voler disinstallare la MOD "Chi è stato qui?"',
));

?>