<?php

/**
*
* @package - mChat
* @version $Id: info_acp_mchat.php
* @copyright (c) 2010 RMcGirr83 ( http://www.rmcgirr83.org/ )
* @copyright (c) 2009 phpbb3bbcodes.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
* @copyright (c) 2014 - www.phpbbitalia.net translated by Darkman on 2014-03-12
*
*/
if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …


$lang = array_merge($lang, array(

	// UMIL stuff
	'ACP_MCHAT_CONFIG'				=> 'Configurazione',
	'ACP_CAT_MCHAT'					=> 'mChat',
	'ACP_MCHAT_TITLE'				=> 'Mini-Chat',
	'ACP_MCHAT_TITLE_EXPLAIN'		=> 'Una mini chat (alias “box messaggi”) per il tuo forum',
	'MCHAT_TABLE_DELETED'			=> 'La tabella mChat è stata cancellata con successo',
	'MCHAT_TABLE_CREATED'			=> 'La tabella mChat è stata creata con successo',
	'MCHAT_TABLE_UPDATED'			=> 'La tabella mChat è stata aggiornata con successo ',
	'MCHAT_NOTHING_TO_UPDATE'		=> 'Niente da fare .... continua',
	'UCP_CAT_MCHAT'					=> 'mChat Preferenze',
	'UCP_MCHAT_CONFIG'				=> 'mChat Preferenze Utenti',

	// ACP entries
	'ACP_MCHAT_RULES'				=> 'Regole',
	'ACP_MCHAT_RULES_EXPLAIN'		=> 'Inserisci le regole del forum qui. Ogni regola su una nuova linea.<br />Devi limitarti a 255 caratteri. <br /><strong>Questo messaggio può essere tradotto.</strong> (Puoi modificare il file mchat_lang.php e leggi le istruzioni).',
	'LOG_MCHAT_CONFIG_UPDATE'		=> '<strong>Aggiornata la configurazione di mChat </strong>',
	'MCHAT_CONFIG_SAVED'			=> 'La configurazione di mChat è stata aggiornata',
	'MCHAT_TITLE'					=> 'mChat',
	'MCHAT_VERSION'					=> 'Versione:',
	'MCHAT_ENABLE'					=> 'Abilita mChat MOD',
	'MCHAT_ENABLE_EXPLAIN'			=> 'Abilita o disabilita la MOD a livello globale.',
	'MCHAT_AVATARS'					=> 'Visualizza gli avatar',
	'MCHAT_AVATARS_EXPLAIN'			=> 'Se imposti SI, gli avatar ridimensionati degli utenti saranno visualizzati.',	
	'MCHAT_ON_INDEX'				=> 'mChat nell’Indice',
	'MCHAT_ON_INDEX_EXPLAIN'		=> 'Consenti la visualizzazione di mChat sulla pagina Indice.',
	'MCHAT_INDEX_HEIGHT'			=> 'Altezza Pagina Indice',
	'MCHAT_INDEX_HEIGHT_EXPLAIN'	=> 'L’altezza della finestra della chat in pixel della pagina Indice del forum. <br /> <em>I limiti dei valori sono da 50 a 1000 </em>.',
	'MCHAT_LOCATION'				=> 'Posizione nel Forum',
	'MCHAT_LOCATION_EXPLAIN'		=> 'Scegli la posizione di mChat nella pagina dell’Indice.',
	'MCHAT_TOP_OF_FORUM'			=> 'Top del Forum',
	'MCHAT_BOTTOM_OF_FORUM'			=> 'Fondo del Forum',
	'MCHAT_REFRESH'					=> 'Refresh',
	'MCHAT_REFRESH_EXPLAIN'			=> 'Numero di secondi prima che la chat effettui il refresh automaticamente. <br /> <em>I limiti dei valori sono da 5 a 60 secondi</em>.',
	'MCHAT_PRUNE'					=> 'Abilita Prune',
	'MCHAT_PRUNE_EXPLAIN'			=> 'Imposta SI per attivare la funzione di prune. <br /><em>si verifica solo se un utente visualizza le personalizzazioni o le pagine dell’archivio</em>.',
	'MCHAT_PRUNE_NUM'				=> 'Numero Prune',
	'MCHAT_PRUNE_NUM_EXPLAIN'		=> 'Il numero di messaggi di mantenere in chat.',	
	'MCHAT_MESSAGE_LIMIT'			=> 'Limite messaggi',
	'MCHAT_MESSAGE_LIMIT_EXPLAIN'	=> 'Il numero massimo di messaggi da mostrare nella chat. <br /> <em>Si consiglia da 10 a 30</em>.',
	'MCHAT_MESSAGE_NUM'				=> 'Limite messaggi nella pagina Indice',
	'MCHAT_MESSAGE_NUM_EXPLAIN'	=> 'Il massimo numero di messaggi da visualizzare nell’area  della chat nella pagina dell’Indice.<br /><em>Raccomandato da 10 a 50</em>.',
	'MCHAT_ARCHIVE_LIMIT'			=> 'Limite Archivio',
	'MCHAT_ARCHIVE_LIMIT_EXPLAIN'	=> 'Il numero massimo di messaggi da mostrare per ogni pagina nella pagina dell’Archivio. <br /> <em>Si consiglia da 25 a 50</em>.',
	'MCHAT_FLOOD_TIME'				=> 'Limite Flood',
	'MCHAT_FLOOD_TIME_EXPLAIN'		=> 'Il numero di secondi che un utente deve attendere prima di inviare un altro messaggio nella chat. <br /> <em>Si consiglia da 5 a 30, impostare su 0 per disabilitare</em>.',
	'MCHAT_MAX_MESSAGE_LENGTH'			=> 'Lunghezza massima dei messaggi',
	'MCHAT_MAX_MESSAGE_LENGTH_EXPLAIN'	=> 'Il numero massimo di caratteri consentiti per messaggio inviato. <br /> <em>Si consiglia da 100 a 500, impostare su 0 per disabilitare</em>.',
	'MCHAT_CUSTOM_PAGE'				=> 'Personalizza pagina',
	'MCHAT_CUSTOM_PAGE_EXPLAIN'		=> 'Consenti l’utilizzo della pagina personalizzata',
	'MCHAT_CUSTOM_HEIGHT'			=> 'Personalizza altezza pagina',
	'MCHAT_CUSTOM_HEIGHT_EXPLAIN'	=> 'L’altezza della finestra della chat in pixel della pagina mChat separata. <br /> <em>I limiti dei valori sono da  50 a 1000 </em>.',
	'MCHAT_DATE_FORMAT'				=> 'Formato data',
	'MCHAT_DATE_FORMAT_EXPLAIN'		=> 'La sintassi usata è identica al PHP <a href="http://www.php.net/date">date()</a> function.',
	'MCHAT_CUSTOM_DATEFORMAT'		=> 'Personalizza...',
	'MCHAT_WHOIS'					=> 'Whois',
	'MCHAT_WHOIS_EXPLAIN'			=> 'Consenti la visualizzazione degli utenti presenti in chat',
	'MCHAT_WHOIS_REFRESH'			=> 'Aggiorna Whois',
	'MCHAT_WHOIS_REFRESH_EXPLAIN'	=> 'Numero di secondi prima che la funzione whois aggiorni le statistiche.  <br /> <em>I limiti dei valori sono da 30 e 300 secondi</em>.',
	'MCHAT_BBCODES_DISALLOWED'		=> 'BBCode disabilitati',
	'MCHAT_BBCODES_DISALLOWED_EXPLAIN'	=> 'Qui puoi inserire i BBCode che <strong>NON</strong> devono essere essere utilizzati nel messaggio. <br /> Separate i bbcode con una barra verticale, per esempio: <br />b|i|u|code|list|list=|flash|quote e/o un %svostro bbcode%s',
	'MCHAT_STATIC_MESSAGE'			=> 'Messaggio statico',
	'MCHAT_STATIC_MESSAGE_EXPLAIN'	=> 'Qui è possibile definire un messaggio statico da far visualizzare agli utenti della chat. <br /> Lasciare il campo vuoto per disabilitare la visualizzazione. Il limite è di 255 caratteri. <br /> <strong>Questo messaggio può essere tradotto.</strong> (è necessario modificare il file mchat_lang.php e leggere le istruzioni).',
	'MCHAT_USER_TIMEOUT'			=> 'Tempo massimo Utente',
	'MCHAT_USER_TIMEOUT_EXPLAIN'	=> 'Imposta la durata di tempo, in secondi, sino a quando una sessione di utenti nella chat termina. Imposta a 0 per non avere nessun tempo massimo.<br /><em>I limiti dei valori sono della %s configurazione forum impostata per le sessioni %s che è attualmente di %s secondi</em>',
	'MCHAT_OVERRIDE_SMILIE_LIMIT'	=> 'Ignora limite smilie',
	'MCHAT_OVERRIDE_SMILIE_LIMIT_EXPLAIN'	=> 'Imposta SI per ignorare l’impostazione del limite smilie fissato per messaggi chat',
	'MCHAT_OVERRIDE_MIN_POST_CHARS'	=> 'Ignora limite minimo dei caratteri',
	'MCHAT_OVERRIDE_MIN_POST_CHARS_EXPLAIN'	=> 'Imposta SI per ignorare limite minimo dei caratteri per i messagi di chat',
	'MCHAT_NEW_POSTS'				=> 'Visualizza Nuovi Argomenti',
	'MCHAT_NEW_POSTS_EXPLAIN'		=> 'Imposta SI per permettere che I nuovi argomenti del forum siano pubblicati in area chat <br /><strong>E’ necessario avere installato l’add-on per le notifiche dei nuovi argomenti aperti</strong> (all’interno della cartella contrib della MOD).',
	'MCHAT_MAIN'					=> 'Configurazione principale',
	'MCHAT_STATS'					=> 'Chi sta chattando',
    'MCHAT_STATS_INDEX'				=> 'Statistiche sull’indice',
	'MCHAT_STATS_INDEX_EXPLAIN'		=> 'Mostra con chi sta chattando nella sezione statistiche del forum',
	'MCHAT_MESSAGES'				=> 'Impostazione Messaggio',
	'MCHAT_PAUSE_ON_INPUT'			=> 'Pausa su input ',
	'MCHAT_PAUSE_ON_INPUT_EXPLAIN'	=> 'Se imposti SI, la chat non potrà effettuare l’autoaggiornamento se un utente che sta scrivendo nell’area di invio',
	
	// error reporting
	'MCHAT_NEEDS_UPDATING'	=> 'La MOD mChat deve essere aggiornata.  Si prega un fondatore del forum di visitare questa sezione per eseguire l’installazione.',
	'MCHAT_WRONG_VERSION'	=> 'La versione installata è inadatta. Si prega di lanciare l’%sinstaller%s per la nuova versione della modifica.',
	'WARNING'	=> 'Attenzione',
	'TOO_LONG_DATE'		=> 'Il formato della data è troppo lungo.',
	'TOO_SHORT_DATE'	=> 'Il formato della data è troppo corto.',
	'TOO_SMALL_REFRESH'	=> 'Il valore refresh è troppo piccolo.',
	'TOO_LARGE_REFRESH'	=> 'Il valore refresh è troppo grande.',
	'TOO_SMALL_MESSAGE_LIMIT'	=> 'Il valore del limite messaggio è troppo piccolo.',
	'TOO_LARGE_MESSAGE_LIMIT'	=> 'Il valore del limite messaggio è troppo grande.',
	'TOO_SMALL_ARCHIVE_LIMIT'	=> 'Il valore del limite Archivio è troppo piccolo.',
	'TOO_LARGE_ARCHIVE_LIMIT'	=> 'Il valore del limite Archivio è troppo grande.',
	'TOO_SMALL_FLOOD_TIME'	=> 'Il valore del limite Flood è troppo piccolo.',
	'TOO_LARGE_FLOOD_TIME'	=> 'Il valore del limite Flood è troppo grande.',
	'TOO_SMALL_MAX_MESSAGE_LNGTH'	=> 'Il valore di lunghezza massima messaggio è troppo piccolo.',
	'TOO_LARGE_MAX_MESSAGE_LNGTH'	=> 'Il valore di lunghezza massima messaggio è troppo grande.',
	'TOO_SMALL_MAX_WORDS_LNGTH'	=> 'Il valore della lunghezza massima parole è troppo piccolo.',
	'TOO_LARGE_MAX_WORDS_LNGTH'	=> 'Il valore della lunghezza massima parole è troppo grande.',
	'TOO_SMALL_WHOIS_REFRESH'	=> 'Il valore aggiorna whois è troppo piccolo.',
	'TOO_LARGE_WHOIS_REFRESH'	=> 'Il valore aggiorna whois è troppo grande.',	
	'TOO_SMALL_INDEX_HEIGHT'	=> 'Il valore dell’altezza dell’Indice è troppo piccolo.',
	'TOO_LARGE_INDEX_HEIGHT'	=> 'Il valore dell’altezza dell’Indice è troppo grande.',
	'TOO_SMALL_CUSTOM_HEIGHT'	=> 'Il valore dell’altezza personalizzata è troppo piccolo.',
	'TOO_LARGE_CUSTOM_HEIGHT'	=> 'Il valore dell’altezza personalizzata è troppo grande.',
	'TOO_SHORT_STATIC_MESSAGE'	=> 'Il valore del messaggio statico è troppo corto.',
	'TOO_LONG_STATIC_MESSAGE'	=> 'Il valore del messaggio statico è troppo lungo.',	
	'TOO_SMALL_TIMEOUT'	=> 'Il valore del tempo massimo utente è troppo piccolo.',
	'TOO_LARGE_TIMEOUT'	=> 'Il valore del tempo massimo utente è troppo grande.',
	
));

?>
