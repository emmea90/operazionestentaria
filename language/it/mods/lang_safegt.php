<?php
/**
*
* safegamertag [Italian]
*
* @package language
* @version $Id: lang_safegt.php 9933 2011-01-07 11:20:51Z Soshen $
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'UCP_PROFILE_SAFEGT'			=> 'Modifica le tue Safe GamerTags',
	'EU'							=> 'EU',
	'USA'							=> 'USA',
	'XBOXGT'						=> 'XboX Live gamertag',
	'XBOXGT_STYLE'					=> 'XboX gamertag style',
	'DEFAULT'						=> 'Default',
	'CYLINDER'						=> 'Cylinder',
	'GEL'							=> 'Gel',
	'NXE'							=> 'NXE',
	'PSNGT'							=> 'PSN gamertag',
	'PSN_ZONE'						=> 'Zona di gioco PSN',
	'WIIGT'							=> 'WII Codice Amico',
	'WII_URL'						=> 'URL immagine WII Gamertag',
	'STEAM_NAME'					=> 'Steam username',
	'STEAMID'						=> 'Steam Community ID',
	'ANTEPRIMA'						=> 'Anteprima',
	'XFIRE_NAME'					=> 'Xfire Username',
	'XFIRE_SKIN'					=> 'Xfire Skin',
	'SHADOW'						=> 'Shadow',
	'COMBAT'						=> 'Combat',
	'SHIFI'							=> 'Sci-fi',
	'FANTASY'						=> 'Fantasy',
	'PSNZONE_DESC'					=> 'Le GamerCard PSN sono case sensitive.<br />Dovete aver creato il vostro ID PORTATILE: <a href="http://us.playstation.com/portableid/">US</a> / <a href="http://it.playstation.com/psn/support/ps3/detail/item139890/Creazione-di-un-ID-portatile/">EU</a>!',
	'WIIURL_DESC'					=> 'Potete inserire l’url di un immagine creata da voi o usare siti appositi che creano Wii gamertag come <a href="http://www.nipponart.org/wiigt.php">NipponArt.org</a> o <a href="http://www.wiinnertag.com">Wiinnertag.com</a><br /><strong>Si accettano solo immagini jpg o png di max 410px X 200px.</strong>',
	'STEAMID_DESC'					=> 'Per usare questa gamertag dovete registrarvi su <a href="http://steamprofile.com">steamprofile.com</a>. Creato il vostro utente avrete una pagina che ha nell’ URL una stringa <em>?steamid=XXXX</em>. Il numero al posto di XXXX è il vostro Steam Community ID.',
	'XFIRE_DESC'					=> 'Per usare questa gamertag dovete registrarvi a <a href="http://www.xfire.com/miniprofile/">Xfire</a>.',
	'NON_ALFANUMERICO_XBOX'			=> 'Nella GamerTag XboX Live hai inserito caratteri non validi!',
	'NON_ALFANUMERICO_PSN'			=> 'Nella GamerTag PSN hai inserito caratteri non validi!',
	'NON_ALFANUMERICO_WII'			=> 'Nel Codice Amico Wii hai inserito caratteri non validi o non è completo di 4 serie di numeri!',
	'NON_ALFANUMERICO_STEAM'		=> 'Nella GamerTag Steam (Steam Community ID) hai inserito caratteri non validi!',
	'NON_ALFANUMERICO_STEAM_NOME'	=> 'Hai inserito caratteri non validi nel campo Steam Username. Sono ammessi solo numeri, lettere, spazio e il simbolo _.',
	'NON_ALFANUMERICO_XFIRE'		=> 'Nell’ Username Xfire hai inserito caratteri non validi!',
	'STEAM_NOT_FILLED_ALL'			=> 'Entrambi i campi Steam sono obbligatori per la relativa gamercard!',
	'WII_NOT_FILLED_ALL'			=> 'Entrambi i campi Wii sono obbligatori per la relativa gamercard!',
	'IMMAGINE_TROPPO_GRANDE'		=> 'L’immagine che hai inserito è troppo grande! Le dimensioni massime sono 410px X 200px.',
	'AGGIORNATE_INFO_GIOCATORE_GT'	=> 'Le tue gamertags sono state aggiornate correttamente. Fra pochi istanti potrai controllare la loro l’anteprima.',
	'WII_URL_NO_IMAGE'				=> 'L’url che hai inserito come WII Gamertag non corrisponde a una immagine.',
	'SAFEGTDC'						=> '<a href="http://www.nipponart.org">Safe GamerTag by Soshen</a>',
	'SAFE_GT_LOGO'					=> 'Safe GamerTag logo',
	'SAFE_GT_HOME'					=> 'Safe GamerTag Home',
	// 2.3.0
	'WRONG_DATA'					=> 'Errore su un campo:',
	'GAMERTAG'						=> 'My GamerTags',
	// 2.3.1
	'ORIGIN'						=> 'Origin ID',
	'NON_ALFANUMERICO_ORIGIN'		=> 'Nel Origin ID hai inserito caratteri non validi!',
	'ACP_SAFEGT_CONFIG'				=> 'Configura Safe GamerTag',
	'ACP_SAFEGT_CFG_TXT'			=> 'Scegli quale sistema di GamerTag o modulo aggiuntivo attivare.',
	'ACP_GAMERCARD_SYSTEM'			=> 'Spunta per attivare',
	'ACP_XBOX_GT'					=> 'Abilita la Gamercard XboX Live',
	'ACP_PSN_GT'					=> 'Abilita il PSN portable ID',
	'ACP_WII_GT'					=> 'Abilita il Codice Amico Wii',
	'ACP_STEAM_GT'					=> 'Abilita le Steam ID',
	'ACP_XFIRE_GT'					=> 'Abilita la gamertag XFire',
	'ACP_ORIGIN_GT'					=> 'Abilita l’Origin ID',
	'SAFEGT_CONFIG_EDITED'			=> 'Configurazione di Safe GamerTag aggiornata.',
	'ACP_COOP_ADDON'				=> 'Attiva il CoOp addon',
	'ACP_XBOXLD_ADDON'				=> 'Attiva XboX Live Leaderboard addon',
	'ACP_PSNLD_ADDON'				=> 'Attiva PSN Leaderboard addon',
	'DOWNLOAD_MODULE'				=> 'scarica il modulo',
	'OLD_VERSION'					=> 'ATTENZIONE! È disponibile una nuova versione di Safe GamerTag. La versione installata è la',
	'LATEST_VERSION'				=> 'mentre l’ultima disponibile è la',
	'DOWNLOAD_LATEST'				=> 'Aggiorna ora',
	'SAFEGT_UMIL_NOT_ACTIVE'		=> 'Nessun sistema gamercard è attivo o non sono state installate le definizioni per il database.<br />%sTorna alla pagina principale del pannello di controllo.%s',
	'CREDIT1'						=> 'Safe GamerTag e i suoi addon (da installare a parte) COOP System, Simple Xbox Live Leaderboard, Simple PSN Leaderboard sono disponibili su %shttp://www.nipponart.org%s.',
	// 2.3.2
	'XBOX_GT_INACTIVE'				=> 'Hai inserito una GamerCard XboX inesistente!',
	'PSN_URL_NO_IMAGE'				=> 'Hai inserito un ID Portable PSN inesistente. Controlla di aver scritto bene maiuscole e miniscole.',
	'WII_URL_NO_CORRECT_URL'		=> 'Hai scritto un url con caratteri non ammessi nalla gamercard Wii o incompleto o con un estensione diversa da una immagine.',
	'STEAM_URL_NO_IMAGE'			=> 'Hai inserito uno SteamID inesistente!',
	'XFIRE_URL_NO_IMAGE'			=> 'Hai inserito un user XFire inesistente!',
	'ACP_XBOX_ACCURATE_VALIDATION'	=> 'XboX controllo accurato',
	'ACP_XBOX_ACCURATE_VALID_TXT'	=> 'Il sistema controlla se una gamertag esiste per davvero. Necessita di cURL attivo. Dovrebbe essere attivo se usate l’addon XboX Leaderboard.',
	'ACP_PSN_ACCURATE_VALIDATION'	=> 'PSN controllo accurato portable ID',
	'ACP_PSN_ACCURATE_VALID_TXT'	=> 'Il sistema controlla se un utente ha creato il suo Portable ID. Dovrebbe essere attivo se usate l’addon PSN Leaderboard.',
	'SHOW_INACTIVE_TOO'				=> 'Mostra anche i sistemi gamertag al momento inattivi',
	'SAFEGT_FORUM_LIMITER'			=> 'Limita visualizzazione gamertags nel forum',
	'SELECT_GAMERTAG_NOT_DISPLAYED'	=> 'Seleziona le gamertag che NON vuoi appaiano nel forum',
	'SELECT_GT_DESC'				=> 'Tieni premuto Ctrl (windows) / Command (Mac) per le selezioni multiple.',
	'XFIRE'							=> 'X-Fire gamercard',
	// 2.3.3
	'CURL_UNACTIVE'					=> 'Il tuo server non ha il cURL attivo nel suo php.ini. Disattiva il controllo gamertag PSN / XboX in ACP o attiva il modulo necessario.',
	// 2.3.4
	'GT_ALREADY_USED'				=> 'La %s che hai scelto è già in uso ad un utente',
));

?>