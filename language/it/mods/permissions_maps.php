<?php
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

if (empty($lang) || !is_array($lang))
{
    $lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Adding new category
$lang['permission_cat']['maps'] = 'Maps Editor';

// Adding the permissions
$lang = array_merge($lang, array(
	'acl_u_can_access_maps'    => array('lang' => 'Può accedere all\'editor', 'cat' => 'maps'),
	'acl_u_can_view_tracks'    => array('lang' => 'Può accedere alle proprie tracce', 'cat' => 'maps'),
	'acl_u_can_view_tours'    => array('lang' => 'Può accedere ai tour', 'cat' => 'maps'),
	'acl_u_can_view_all_tracks'    => array('lang' => 'Può vedere tutte le tracce', 'cat' => 'maps'),
	'acl_u_can_view_all_tours'    => array('lang' => 'Può vedere tutti i tour', 'cat' => 'maps'),
	'acl_u_can_view_debug_stuff'    => array('lang' => 'Può vedere i lavori in corso', 'cat' => 'maps'),
	'acl_m_can_edit_tracks'    => array('lang' => 'Può modificare tutte le tracce', 'cat' => 'maps'),
	'acl_m_can_edit_tours'    => array('lang' => 'Può modificare tutte i tour', 'cat' => 'maps'),
	'acl_m_can_change_track_authors'    => array('lang' => 'Può cambiare l\'autore delle tracce', 'cat' => 'maps'),
	'acl_a_can_manage_maps'    => array('lang' => 'Può gestre l\'editor di tappe', 'cat' => 'maps'),
));
?>