<?php
/**
*
* skype [German]
*
* @package language
* @version $Id: skype.php,v 1.1 2009/01/02 22:56:58 rmcgirr83 Exp $
* @copyright (c) 2008 Richard McGirr 
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @translation by Fleshgrinder
* @copyright (c) 2008 Richard Fussenegger
* @conatact admin@nervenhammer.com
* @website http://www.nervenhammer.com/
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
// do not translate these next two lines!!  Else skype will not be able to connect
  'SKYPE_CHAT'  => 'Chat',
  'SKYPE_CALL'  => 'Call',
// translate following
  'IM_SKYPE'            => 'Bitte bedenke, dass Mitglieder möglicherweise den Empfang von nicht autorisierten Kontakten deaktiviert haben könnten.',
  'SEND_SKYPE_MESSAGE'  => 'Skype Nachricht senden',
  'SKYPE'               => 'Skype',
  'SKYPE_ADD'           => 'Kontakt hinzufügen',
  'SKYPE_EXPLAIN'       => 'Bitte lese die %sFAQ%s um mehr zu erfahren.',
  'SKYPE_FAQ'           => 'Skype FAQ',
  'SEND_SKYPE_MESSAGE'  => 'Skype Nachricht senden',
  'SKYPE_SENDFILE'      => 'Datei senden...',
  'SKYPE_USERINFO'      => 'Skype Profil ansehen',
  'SKYPE_VOICEMAIL'     => 'Skype Anrufbeantworter',
  'SKYPE_TYPE'          => 'Skype Nachrichtentyp',
  'SKYPE_CALL_TRANS'    => 'Anrufen',
	'SKYPE_CHAT_TRANS'		=> 'Sofortnachricht senden...',
  'TOO_LONG_SKYPE'      => 'Der angegebene Skype Name ist zu lang.',
  'TOO_SHORT_SKYPE'     => 'Der angegebene Skype Name ist zu kurz.',
  'UCP_SKYPE'           => 'Skype Name',
));

?>