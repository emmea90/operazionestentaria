<?php
/**
*
* @package	language
* @version	3.0.7
* @license	GNU Public License
* @author	draghetto
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'TB_TB'					=> 'Tag Board',
	'TB_ACP'				=> 'Tag Board settings',
	'TB_INSTALL'			=> 'Tag Board succesfully installed<br />Please delete tb_install.php file',
	'TB_ACP_EXPLAIN'		=> 'Here you can determine the basic operation of your Tag Board.',
	'TB_CVERSION' 			=> 'Current version',
	'TB_LVERSION' 			=> 'Latest version',
	'TB_CHECK' 				=> 'Check manually',
	'TB_AUTH' 				=> 'Permissions',
	'TB_GROUPS' 			=> 'Allowed groups',
	'TB_GROUPS_EXPLAIN' 	=> 'Id of group who can show TB. You can set more than one id separate with a comma.',
	'TB_DENIED' 			=> 'Users denied',
	'TB_DENIED_EXPLAIN' 	=> 'Id of users who can’t show TB. You can set more than one id separate with a comma.',
	'TB_STANDARD' 			=> 'All users can READ but only registered users can WRITE<br />',
	'TB_FULL' 				=> 'All users can READ and WRITE<br />',
	'TB_LIMITED' 			=> 'Only registered users can READ and WRITE',
	'TB_GUEST' 				=> 'Guestname',
	'TB_GUEST_EXPLAIN' 		=> 'Leave blank to use IP as Guestname.',
	'TB_LIMIT' 				=> 'Show last',
	'TB_POSTS' 				=> 'posts',
	'TB_HEIGTH' 			=> 'Tag Board heigth',
	'TB_PX' 				=> 'px',
	'TB_MAXLENGTH' 			=> 'Maximum characters per post',
	'TB_MAXLENGTH_EXPLAIN' 	=> 'The number of characters allowed within a post.',
	'TB_CHARS' 				=> 'chars',
	'TB_BBCODE' 			=> 'Allow BBCode',
	'TB_CUSTOM' 			=> 'Show custom BBCode button',
	'TB_FSIZE' 				=> 'Show font size button',
	'TB_IMG' 				=> 'Allow use of <code>[IMG]</code> BBCode tag',
	'TB_FLASH' 				=> 'Allow use of <code>[FLASH]</code> BBCode tag',
	'TB_SMILIES' 			=> 'Allow smilies',
	'TB_URLS' 				=> 'Allow links',
	'TB_URLS_EXPLAIN' 		=> 'If disallowed the automatic/magic URLs are disabled.',
	'TB_BUTTONS' 			=> 'Show buttons',
	'TB_DELETE' 			=> 'Allow user to delete own posts',
	'TB_EDIT' 				=> 'Allow user to edit own posts',
	'TB_EDIT_TIME' 			=> 'Limit editing time',
	'TB_EDIT_TIME_EXPLAIN' 	=> 'Limits the time available to edit a new post. Setting the value to 0 disables this behaviour.',
	'TB_MINUTES' 			=> 'minutes',
	'TB_FLOOD' 				=> 'Flood interval',
	'TB_FLOOD_EXPLAIN' 		=> 'Number of seconds a user must wait between posting new messages. Setting the value to 0 disables this behaviour.',
	'TB_SECONDS' 			=> 'seconds',
	'TB_PURGE' 				=> 'Auto prune old posts',
	'TB_PURGE_EXPLAIN' 		=> 'This will delete old posts within the number of days you select. Setting the value to 0 disables this behaviour.',
	'TB_DAYS' 				=> 'days',
	'TB_REFRESH' 			=> 'Auto refresh',
	'TB_REFRESH_EXPLAIN' 	=> 'Number of seconds elapsed for auto refreshing Tag Board. Setting the value to 0 disables this behaviour.',
	'TB_HISTORY' 			=> 'Maximum posts stored on database',
	'TB_HISTORY_EXPLAIN' 	=> 'Setting the value to 0 store all posts.',
	'TB_DELETEALL' 			=> 'Delete all Tag Board posts',
	'TB_DELETEALL_EXPLAIN' 	=> 'Please note that deleting is final, they cannot be recovered.',
	'TB_FLOOD_ERROR' 		=> 'You cannot make another post so soon after your last.',
	'TB_OFFLINE' 			=> 'Offline',
	'TB_INACTIVE' 			=> 'Inactive',
	'TB_ONLINE' 			=> 'Online',
	'TB_GUESTNAME' 			=> 'Guest',
	'TB_MORE_SMILIES' 		=> 'View more smilies',
));

?>