<?php
/**
*
* safegamertag [English]
*
* @package language
* @version $Id: lang_safegt.php 9933 2011-01-08 22:10:01Z Soshen $
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'UCP_PROFILE_SAFEGT'			=> 'Edit your Safe GamerTags',
	'EU'							=> 'EU',
	'USA'							=> 'USA',
	'XBOXGT'						=> 'XboX Live gamertag',
	'XBOXGT_STYLE'					=> 'XboX gamertag style',
	'DEFAULT'						=> 'Default',
	'CYLINDER'						=> 'Cylinder',
	'GEL'							=> 'Gel',
	'NXE'							=> 'NXE',
	'PSNGT'							=> 'PSN gamertag',
	'PSN_ZONE'						=> 'PSN region',
	'WIIGT'							=> 'WII Friend Code',
	'WII_URL'						=> 'Image URL of a WII Gamertag',
	'STEAM_NAME'					=> 'Steam username',
	'STEAMID'						=> 'Steam Community ID',
	'ANTEPRIMA'						=> 'Preview',
	'XFIRE_NAME'					=> 'Xfire Username',
	'XFIRE_SKIN'					=> 'Xfire Skin',
	'SHADOW'						=> 'Shadow',
	'COMBAT'						=> 'Combat',
	'SHIFI'							=> 'Sci-fi',
	'FANTASY'						=> 'Fantasy',
	'PSNZONE_DESC'					=> 'The PSN gamercard are case-sensitive.<br />You must have created your PORTABLE ID: <a href="http://us.playstation.com/portableid/">US</a> / <a href="http://uk.playstation.com/psn/support/ps3/detail/item139661/Creating-a-Portable-ID/">EU</a>!',
	'WIIURL_DESC'					=> 'You can enter the URL of an image created by you or use sites creating Wii gamertag like <a href="http://www.nipponart.org/wiigt.php">NipponArt.org</a> or <a href="http://www.wiinnertag.com">Wiinnertag.com</a><br /><strong>We only accept images .jpg or .png, max 410px X 200px.</strong>',
	'STEAMID_DESC'					=> 'To use this gamertag, you must register on <a href="http://steamprofile.com">steamprofile.com</a>. When you have created your user you will have a page that has a URL string <em>?steamid=XXXX</em>. The number in place of XXXX is your Steam Community ID.',
	'XFIRE_DESC'					=> 'To use this gamertag, you must register on <a href="http://www.xfire.com/miniprofile/">Xfire</a>.',
	'NON_ALFANUMERICO_XBOX'  		=> 'You entered invalid characters in the Xbox Live GamerTag!',
	'NON_ALFANUMERICO_PSN'   		=> 'You entered invalid characters in the PSN GamerTag!',
	'NON_ALFANUMERICO_WII'   		=> 'You entered invalid characters in the Wii Friend code or is not complete with 4 sets of numbers!',
	'NON_ALFANUMERICO_STEAM' 		=> 'You entered invalid characters in the Steam GamerTag (Steam Community ID)!',
	'NON_ALFANUMERICO_STEAM_NOME'	=> 'You entered invalid characters in Steam username. Only numbers, letters, space and _ symbol are allowed.',
	'NON_ALFANUMERICO_XFIRE' 		=> 'You entered invalid characters in the Xfire username!',
	'STEAM_NOT_FILLED_ALL'			=> 'Both Steam field must be filled!',
	'WII_NOT_FILLED_ALL'			=> 'Both Wii field must be filled!',
	'IMMAGINE_TROPPO_GRANDE' 		=> 'The image you have entered is too big! The maximum size is 410px X 200px.',
	'AGGIORNATE_INFO_GIOCATORE_GT'	=> 'Your Gamertags have been updated correctly. In a few moments you can check out their preview.',
	'WII_URL_NO_IMAGE'       		=> 'You entered on the URL of WII Gamertag a file that is not a image!',
	'SAFEGTDC'          			=> '<a href="http://www.nipponart.org">Safe GamerTag by Soshen</a>',
	'SAFE_GT_LOGO'      			=> 'Safe GamerTag logo',
	'SAFE_GT_HOME'      			=> 'Safe GamerTag Home',
	// 2.3.0
	'WRONG_DATA'					=> 'Field Error:',
	'GAMERTAG'						=> 'My GamerTags',
	// 2.3.1
	'ORIGIN'						=> 'Origin ID',
	'NON_ALFANUMERICO_ORIGIN'		=> 'You entered invalid characters in the Origin ID!',
	'ACP_SAFEGT_CONFIG'				=> 'Safe GamerTag config',
	'ACP_SAFEGT_CFG_TXT'			=> 'Choose which GamerTag system or addon modules active in your forum.',
	'ACP_GAMERCARD_SYSTEM'			=> 'Set for active it',
	'ACP_XBOX_GT'					=> 'Active XboX Live gamercard',
	'ACP_PSN_GT'					=> 'Active PSN portable ID',
	'ACP_WII_GT'					=> 'Active Wii friend code',
	'ACP_STEAM_GT'					=> 'Active Steam ID',
	'ACP_XFIRE_GT'					=> 'Active XFire gamercard',
	'ACP_ORIGIN_GT'					=> 'Active Origin ID',
	'SAFEGT_CONFIG_EDITED'			=> 'Safe GamerTag configuration updated.',
	'ACP_COOP_ADDON'				=> 'Active CoOp addon',
	'ACP_XBOXLD_ADDON'				=> 'Active XboX Live Leaderboard addon',
	'ACP_PSNLD_ADDON'				=> 'Active PSN Leaderboard addon',
	'DOWNLOAD_MODULE'				=> 'download module',
	'OLD_VERSION'					=> 'IMPORTANT! Is available a new version of Safe GamerTag. Your current version is',
	'LATEST_VERSION'				=> 'and the latest one is',
	'DOWNLOAD_LATEST'				=> 'Update now',
	'SAFEGT_UMIL_NOT_ACTIVE'		=> 'Any gamercard system are active or the databases definition aren’t being installed.<br />%sGo back to the user control panel.%s',
	'CREDIT1'						=> 'Safe GamerTag and its addons (addons must be installed separately) COOP System, Simple Xbox Live Leaderboard, Simple PSN Leaderboard are available on %shttp://www.nipponart.org%s.',
	// 2.3.2
	'XBOX_GT_INACTIVE'				=> 'You entered a XboX GamerCard that does not exist!',
	'PSN_URL_NO_IMAGE'				=> 'You entered a PSN gamercard / portable ID that does not exist. Check for uppercase and lowcase letters too.',
	'WII_URL_NO_CORRECT_URL'		=> 'You entered in Wii gamercard a url with illegal characters or incomplete or with an extension other than an image.',
	'STEAM_URL_NO_IMAGE'			=> 'You entered a SteamID that does not exist!',
	'XFIRE_URL_NO_IMAGE'			=> 'You entered a XFire user that does not exist!',
	'ACP_XBOX_ACCURATE_VALIDATION'	=> 'XboX accurate validation',
	'ACP_XBOX_ACCURATE_VALID_TXT'	=> 'If you activate this the system check if the gamertag exist for real. It need cURL active. It should be on if you have installed the XboX Leaderboard addon.',
	'ACP_PSN_ACCURATE_VALIDATION'	=> 'PSN portable ID accurate validation',
	'ACP_PSN_ACCURATE_VALID_TXT'	=> 'If you activate this the system check if the portable ID exist for real. It should be on if you have installed the PSN Leaderboard addon.',
	'SHOW_INACTIVE_TOO'				=> 'Show inactive gamertag platform too',
	'SAFEGT_FORUM_LIMITER'			=> 'Limit the gamertags visibility for this forum',
	'SELECT_GAMERTAG_NOT_DISPLAYED'	=> 'Select the gamertags che you do NOT want to be displayed',
	'SELECT_GT_DESC'				=> 'Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.',
	'XFIRE'							=> 'X-Fire gamercard',
	// 2.3.3
	'CURL_UNACTIVE'					=> 'Your server does not have the cURL active in his php.ini. Disable the control of PSN Gamertag / XboX in ACP or active the necessary php module.',
	// 2.3.4
	'GT_ALREADY_USED'				=> 'The choice %s has been already taken by a user!',
));

?>