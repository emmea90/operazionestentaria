<?php
/**
*
* safegamertag install [English]
*
* @package language
* @version $Id: umil_safegt_install.php 4933 2011-01-09 14:40:11Z Soshen $
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_SAFE_GAMERTAG'                     => 'Safe GamerTag',
	'INSTALL_ACP_SAFE_GAMERTAG'             => 'Install Safe GamerTag mod',
	'INSTALL_ACP_SAFE_GAMERTAG_CONFIRM'     => 'Are you ready to install the Safe GamerTag mod?',
	'UNINSTALL_ACP_SAFE_GAMERTAG'			=> 'Uninstall Safe GamerTag Mod',
	'UNINSTALL_ACP_SAFE_GAMERTAG_CONFIRM'	=> 'Are you ready to uninstall the Safe GamerTag Mod? All settings and data saved by this mod will be removed!',
	'UPDATE_ACP_SAFE_GAMERTAG'				=> 'Update Safe GamerTag Mod',
	'UPDATE_ACP_SAFE_GAMERTAG_CONFIRM'		=> 'Are you ready to update the Safe GamerTag Mod?',
));

?>