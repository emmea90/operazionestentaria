<?php

/**
*
* help_skype [English]
*
* @package language
* @version $Id: help_skype.php,v 1.1 2008/12/11 14:41:29 rmcgirr83 Exp $
* @copyright (c) Richard McGirr
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$help = array(
	array(
		0 => '--',
		1 => 'Skype'
	),
	array(
		0 => 'What is Skype?',
		1 => '<a href="http://www.skype.com">Skype</a> lets you make free calls over the internet to anyone else who also has the service. It is free and easy to download and use, and it works with most computers. Skype offers free global telephony and unlimited voice calls with its next-generation peer-to-peer software.',
	),
	array(
		0 => 'How can I add users to my Skype account?',
		1 => 'You can add users to your Skype account by simply clicking on the links found when either viewing a topic or viewing a registered users profile. The icon shown will change depending on the users status on Skype when viewing a topic.'
	),
	array(
		0 => 'Okay, I entered my Skype address but the icon is not showing my status!',
		1 => 'Once you have inputted your Skype address into your profile, you then need to change your Skype program privacy settings (Skype->Options->Privacy->Show Advanced Options->Allow my status to be shown on the web).  Your status on Skype will then be shown to all registered users.'
	),
	array(
		0 => 'What does the topic button do?',
		1 => 'If you have Skype installed, you will place either a <b><i>call</i></b> or send a <b><i>chat</i></b> message to that user depending on that users preferences.'
	),
	array(
		0 => 'What does the profile button/link do?',
		1 => 'It allows a user to perform a multitude of functions with you via Skype.  The user will be able to Add you as a contact, send you a file, send you a voicemail, check your information at skype, make a call or make a chat.'
	),
);
?>
