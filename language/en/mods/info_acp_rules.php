<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @package Advanced Rules Page
* @version $Id$
* @copyright (c) 2012 Dugi ( http://imgit.org )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_RULES_MOD'				=> 'Advanced Rules Page',
	'ACP_RULES'					=> 'Board rules',
	'ACP_CAT_RULES'				=> 'Rules',
	'ACP_CAT_CATS'				=> 'Categories',
	'ACP_PARENT_RULES'			=> 'Rules',
	'ACP_CREATE_RULE'			=> 'Create rule',
	'ACP_CREATE_CAT'			=> 'Create category',
	'ACP_CAT_EDIT'				=> 'Edit category',
	'ACP_RULE_EDIT'				=> 'Edit rule',
	'RULES_ADMIN'				=> 'Rules administration',
	'RULES_ADMIN_EXPLAIN'		=> 'From here you can set up your board rules. To create an understandable rule set, create categories and then create the corresponding rules inside it. You can limit categories to groups. To do this, simply choose your desired groups while creating or updating a category.',
	'RULES_MANAGE'				=> 'Manage rules',
	'RULE_MANAGE'				=> 'Manage rule',
	'CAT_MANAGE'				=> 'Manage category',
	'RULE'						=> 'Rule',
	'CAT'						=> 'Category',
	'CREATE_RULE'				=> 'Create rule',
	'CREATE_CAT'				=> 'Create category',
	'RULE_NAME'					=> 'Rule name',
	'CAT_NAME'					=> 'Category name',
	'RULE_DESCRIPTION'			=> 'Description',
	'RULE_DESCRIPTION_EXPLAIN' 	=> 'Any HTML markup entered here will be displayed as is.',
	'MANAGE_RULE_EXPLAIN'		=> 'Using the form below you can update an existing rule or category which will be displayed to your users.',
	'CREATE_RULE_EXPLAIN'		=> 'Using the form below you can create a new rule or category which will be displayed to your users.',
	'RULE_PARENT'				=> 'Parent category',
	'RULE_ENABLE'				=> 'Enable rule',
	'RULE_DISABLE'				=> 'Disable rule',
	'CAT_ENABLE'				=> 'Enable category',
	'CAT_DISABLE'				=> 'Disable category',
	'RULE_ENABLE_EXPLAIN'		=> 'Switching this option to <em>No</em> hiddes this rule or category from the public.',
	'RULES_ENABLE'				=> 'Enable board rules',
	'RULES_ENABLE_EXPLAIN'		=> 'Turn on or off board rules. If disabled users will get a message saying that the page is being updated.',
	'RULES_PAGE_MANAGE'			=> 'Manage rules page',
	'SET_GROUPS'				=> 'Applies to',
	'SET_GROUPS_EXPLAIN'		=> 'Select the groups that should see this category in the rules page. Please note that even if an user does not have any of the selected groups as his default group (but is part of the group), he still would see this category and its rules.',
	'RULE_SETTINGS'				=> 'Rule settings',
	'CAT_SETTINGS'				=> 'Category settings',
	
	// Logs and messages
	'LOG_ALTERED_RULE'		=> '<strong>Altered rule</strong><br />» %s',
	'LOG_ALTERED_CAT'		=> '<strong>Altered rule category</strong><br />» %s',
	'LOG_CAT_CREATED'		=> '<strong>Created new rule category</strong><br />» %s',
	'LOG_RULE_CREATED'		=> '<strong>Created new rule</strong><br />» %s',
	'LOG_DELETED_RULE'		=> '<strong>Deleted rule</strong><br />» %s',
	'LOG_DELETED_CAT'		=> '<strong>Deleted rule category</strong><br />» %s',
	'LOG_ENABLED_RULE'		=> '<strong>Enabled rule</strong><br />» %s',
	'LOG_DISABLED_RULE'		=> '<strong>Disabled rule</strong><br />» %s',
	'RULE_UPDATED'			=> 'Rule succesfully updated.',
	'CAT_UPDATED'			=> 'Category succesfully updated.',
	'RULE_DELETED'			=> 'Rule succesfully deleted.',
	'CAT_DELETED'			=> 'Category succesfully deleted.',
	'RULE_CREATED'			=> 'New rule successfully created.',
	'CAT_CREATED'			=> 'New category successfully created.',
	'EMPTY_FIELDS'			=> 'You left all fields blank.',
	'NO_RULE'				=> 'No rule specified.',
	'RULE_TITLE_TOO_LONG'	=> 'Rule or category name exceeds the maximal allowed character size.',
	'RULE_DESC_TOO_LONG'	=> 'Description field exceeded the maximum allowed character size.',
	'RULE_TOO_SHORT'		=> 'Rule name or description field doesn’t include at least 3 characters.',
	'CAT_TITLE_TOO_SHORT'	=> 'The category name should be at least 3 characters long.',
	'CANNOT_MOVE'			=> 'Rule could not be moved.',
	'CONFIRM_CAT_DELETE' 	=> 'Deleting this category deletes all rules inside it too. Continue?',
	'CONFIRM_RULE_DELETE' 	=> 'Are you sure you wish to delete this rule?',
	'RULE_ENABLED'			=> 'Rule successfully enabled.',
	'RULE_DISABLED'			=> 'Rule successfully disabled.',
	'RULES_PAGE_ENABLED'	=> 'Board rules successully enabled',
	'RULES_PAGE_DISABLED'	=> 'Board rules successully disabled',
	'DESCRIPTION_REQUIRED'	=> 'Description is required when creating rules.',
	'TITLE_REQUIRED'		=> 'Title is required when creating rules.',
	'RULES_NOT_INSTALLED'	=> 'You need to run the installer in order to see this page.',
	'CANNOT_ENABLE'			=> 'Rule cannot not be enabled because its parent is disabled.',
	'GROUP_IDS_POPULATED'	=> 'Populating the new group_ids column',
	'GROUP_IDS_UNPOPULATED'	=> 'Emptying the group_ids column',
	'MAXIMUM_RULES'			=> 'This category reached its maximum allowed space for rules.',
	
	'acl_a_rules'	=> array('lang' => 'Can manage board rules', 'cat' => 'misc'),
));
?>