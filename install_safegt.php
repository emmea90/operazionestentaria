<?php
/**
*
* @package umil
* @version $Id install_safegt.php
* @copyright (c) 2011 Soshen <nipponart.org>
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('UMIL_AUTO', true);
define('IN_PHPBB', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
$user->session_begin();
$auth->acl($user->data);
$user->setup();

if (!file_exists($phpbb_root_path . 'umil/umil_auto.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}
/*
* The name of the config variable which will hold the currently installed version
* You do not need to set this yourself, UMIL will handle setting and updating the version itself.
*/
$version_config_name = 'safegt_version';

$language_file = 'mods/umil_safegt_install';

// The name of the mod to be displayed during installation.
$mod_name = 'ACP_SAFE_GAMERTAG';

/*
* The array of versions and actions within each.
* You do not need to order it a specific way (it will be sorted automatically), however, you must enter every version, even if no actions are done for it.
*
* You must use correct version numbering.  Unless you know exactly what you can use, only use X.X.X (replacing X with an integer).
* The version numbering must otherwise be compatible with the version_compare function - http://php.net/manual/en/function.version-compare.php
*/
$versions = array(
	// Version 2.2
	'2.2'	=> array(
	//Add table
		'table_column_add' => array(
			array("phpbb_users", 'user_xboxgt', array('VCHAR_UNI:20', '')),
			array("phpbb_users", 'user_psngt', array('VCHAR_UNI:20', '')),
			array("phpbb_users", 'user_psnzone', array('TINT:1', 0)),
			array("phpbb_users", 'user_wiigt', array('VCHAR_UNI:20', '')),
			array("phpbb_users", 'user_wiiurl', array('VCHAR_UNI:255', '')),
			array("phpbb_users", 'user_steamgt', array('VCHAR_UNI:100', '')),
			array("phpbb_users", 'user_steamid', array('VCHAR_UNI:20', '')),
			array("phpbb_users", 'user_xfiregt', array('VCHAR_UNI:26', '')),
			array("phpbb_users", 'user_xfireskin', array('VCHAR_UNI:2', 'sh'))
		),	

	//Add module
		'module_add' => array(
			// Now we will add the modules manually
            array('ucp', 'UCP_PROFILE', array(
					'module_basename'   => 'profile',
					'module_langname'   => 'UCP_PROFILE_SAFEGAMERTAG',
					'module_mode'       => 'safegt',
					'module_auth'       => '',
				),
			),
		),	
	), // end of array 2.2
	
	// Version 2.3.0
	'2.3.0' => array(
		//Add module
		'module_add' => array(
			// Now we will add the modules manually
            array('acp', 'ACP_CAT_USERS', array(
					'module_basename'   => 'users',
					'module_langname'   => 'ACP_USER_GAMERCARD',
					'module_mode'       => 'gamercard',
					'module_auth'       => 'acl_a_user',
					'module_display'    => 0,
				),
			),
		),
	), // end of array 2.3.0
	
	// Version 2.3.1
	'2.3.1' => array(
		//Add table
		'table_column_add' => array(
			array("phpbb_users", 'user_originid', array('VCHAR_UNI:20', '')),
		),
		// Add config values
		'config_add' => array(
			array('safegt_xbox', 1),
			array('safegt_psn', 1),
			array('safegt_wii', 1),
			array('safegt_steam', 1),
			array('safegt_xfire', 1),
			array('safegt_origin', 1),
		),
		// Add permission data
		'permission_add' => array(
			array('a_safegt', true),
		),
		// Set permission to admin
		'permission_set' => array(
			// Global Group permissions
			//array('ADMINISTRATORS', 'a_safegt', 'group'), /* Removed cose with UMIL 1.0.3 this affect the mods too. */
			// Global Role permissions for admins
			array('ROLE_ADMIN_FULL', 'a_safegt'),
		),
		//Add module
		'module_add' => array(
			array('acp', 'ACP_CAT_DOT_MODS', 'ACP_CAT_SAFEGT'),
			array('acp', 'ACP_CAT_SAFEGT',
				array('module_basename'	=> 'safegt'),
			),
		),
		//Clear cache
		'cache_purge' => array(''),
	), // end of array 2.3.1
	
	// Version 2.3.2
	'2.3.2' => array(
		// Add config values
		'config_add' => array(
			array('safegt_psncheck', 1),
			array('safegt_xboxcheck', 0),
		),
		//Add table
		'table_column_add' => array(
			array("phpbb_forums", 'forum_gamertags', array('VCHAR_UNI:255', ''))
		),
		//Clear cache
		'cache_purge' => array(''),
	), // end of array 2.3.2
	
	// Version 2.3.3
	'2.3.3' => array(
		//Clear cache
		'cache_purge' => array(''),
	), // end of array 2.3.3
	
	// Version 2.3.3
	'2.3.4' => array(
		//Clear cache
		'cache_purge' => array(''),
	), // end of array 2.3.4

);

// Include the UMIF Auto file and everything else will be handled automatically.
include($phpbb_root_path . 'umil/umil_auto.' . $phpEx);

?>