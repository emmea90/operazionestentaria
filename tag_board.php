<?php
/**
*
* @package	core
* @version	3.0.7c
* @license	GNU Public License
* @author	draghetto
*
*/

/**
* @ignore
*/
define('IN_PHPBB',true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'),1);
include($phpbb_root_path . 'common.' . $phpEx);
define('TB_TABLE', $table_prefix . 'tag_board');

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('tag_board');

// Grab data
$tb_id 		= request_var('id',0);
$tb_mode 	= request_var('mode','');
$tb_view 	= request_var('view','');
$tb_audio 	= request_var('audio',0);
$tb_start 	= request_var('start',0);
$tb_error	= request_var('error','');
$tb_cookie	= request_var($config['cookie_name'] . '_tb', 0, false, true);

// Get some variables
$tb_auth = ($config['tb_groups']  && !$auth->acl_get('a_')) ? preg_match('/([^0-9]*' . $user->data['group_id'] . '[^0-9]*)/', $config['tb_groups']) : 1;
$tb_denied = ($config['tb_denied'] && !$auth->acl_get('a_')) ? preg_match('/([^0-9]*' . $user->data['user_id'] . '[^0-9]*)/', $config['tb_denied']) : 0;
$cookie_expire	= time() + 31536000;

// Delete all posts
if($config['tb_deleteall'])
{
	$sql = 'TRUNCATE TABLE ' . TB_TABLE;
	$db->sql_query($sql);
	set_config('tb_deleteall',0);
	$user->set_cookie('tb', 0, 0);
}

// Prune old posts
if($config['tb_purge'])
{
	$dif = time() - ($config['tb_purge'] * 86400);
	$sql = 'DELETE
		FROM ' . TB_TABLE . "
		WHERE tb_post_time < $dif";
	$db->sql_query($sql);
}

// Keep only $config['tb_history'] posts in db
function delete_tags()
{
	global $db, $config;
		$sql = "SELECT tb_post_id
			FROM " . TB_TABLE . "
			ORDER BY tb_post_id DESC";
		$result = $db->sql_query_limit($sql,($config['tb_history'] - 1) . ',' . 1);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if($row)
		{
			$sql = 'DELETE
				FROM ' . TB_TABLE . "
				WHERE tb_post_id < " . $row['tb_post_id'];
			$db->sql_query($sql);
		}
}

// Get user id var function
function get_user_id_var($v, $u)
{
	global $db;
		$u = (int) $u;
		$sql = "SELECT $v
				FROM " . USERS_TABLE . "
				WHERE user_id = $u";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
	return $row[$v];
}

// Read posts
function is_read($id)
{
	global $tb_cookie;
		if(!$tb_cookie || $tb_cookie < $id)
		{
			return false;
		}
		else
		{
			return true;
		}
}

// Is online
function is_online($u)
{
	global $db,$config;
		$u = (int) $u;
		$sql = 'SELECT *
				FROM ' . SESSIONS_TABLE . "
				WHERE session_user_id = $u
				AND session_viewonline = 1";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
	return (time() - ($config['load_online_time'] * 60)) > $row['session_time'] ? 'offline' : ((time() - ($config['load_online_time'] * 30)) > $row['session_time'] ? 'inactive' : 'online');
}

// Is author
function is_author($u,$id)
{
	global $db;
		$id = (int) $id;
		$sql = 'SELECT *
				FROM ' . TB_TABLE . "
				WHERE tb_post_id = $id";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
	return ($u == $row['tb_poster_id'] && $u != ANONYMOUS) ? true : false;
}

// Flood control
function flood($u)
{
	global $db, $config;
		$u = (int) $u;
		$sql = 'SELECT *
				FROM ' . TB_TABLE . "
				WHERE tb_poster_id = $u
				ORDER BY tb_post_time DESC";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
	return (time() > ($row['tb_post_time'] + $config['tb_flood'])) ? true : false;
}

switch($tb_mode)
{
	case 'write':
		if(check_form_key('tb_form') && !$tb_denied && $tb_auth && ($config['tb_auth'] == 'FULL' || $user->data['is_registered']))
		{
			$tb_post = utf8_normalize_nfc(request_var('tb_post', '', true));
			if($tb_post)
			{
				if(flood($user->data['user_id']) || $auth->acl_get('a_') || $auth->acl_get('m_'))
				{
					if(!$config['tb_img'])
					{
						$tb_post = str_replace('[img]', '&#91;img&#93;', $tb_post);
						$tb_post = str_replace('[/img]', '&#91;/img&#93;', $tb_post);
					}
					if(!$config['tb_flash'])
					{
						$tb_post = str_replace('[flash]', '&#91;flash&#93;', $tb_post);
						$tb_post = str_replace('[/flash]', '&#91;/flash&#93;', $tb_post);
					}
					generate_text_for_storage($tb_post,$uid,$bitfield,$flags,$config['tb_bbcode'],$config['tb_urls'],$config['tb_smilies']);
					$sql_ary = array(
						'tb_poster_id' 			=> $user->data['user_id'],
						'tb_post_time' 			=> time(),
						'tb_post_username' 		=> $user->data['is_registered'] ? $user->data['username'] : $user->ip,
						'tb_post_text' 			=> $tb_post,
						'tb_bbcode_bitfield' 	=> $bitfield,
						'tb_bbcode_uid' 		=> $uid,
						'tb_flags' 				=> $flags,
					);
					$sql = 'INSERT INTO ' . TB_TABLE . ' ' . $db->sql_build_array('INSERT', $sql_ary);                           
					$db->sql_query($sql);
					set_config_count('total_tb', 1);
				}
				else
				{
					header("Location: " . append_sid("{$phpbb_root_path}tag_board.$phpEx", "audio={$tb_audio}&error=flood"));
					break;
				}
			}
			header("Location: " . append_sid("{$phpbb_root_path}tag_board.$phpEx", "audio=$tb_audio"));
		}
		else
		{
			header("Location: " . append_sid("{$phpbb_root_path}tag_board.$phpEx", "audio={$tb_audio}&error=1"));
		}
	break;
	
	case 'palette':
		page_header('',false);
		$template->set_filenames(array('body' => 'tag_board_palette.html'));
		page_footer();
	break;
	
	case 'smilies':
		$sql = 'SELECT COUNT(smiley_id) AS total
			FROM ' . SMILIES_TABLE . "
			WHERE display_on_posting <> 1";
		$result = $db->sql_query($sql);
		$total = $db->sql_fetchfield('total');
		$db->sql_freeresult($result);
		if ($total && $tb_view != 'all')
		{
			$template->assign_var('TB_MORE', 1);
		}
		if($tb_view == 'all')
		{
			$sql = 'SELECT MIN(code) as code, smiley_url, emotion
				FROM ' . SMILIES_TABLE . "
				GROUP BY smiley_url
				ORDER BY smiley_order";
		}
		else
		{
			$sql = 'SELECT MIN(code) as code, smiley_url, emotion
				FROM ' . SMILIES_TABLE . "
				WHERE display_on_posting = 1
				GROUP BY smiley_url
				ORDER BY smiley_order";
		}
		$result = $db->sql_query($sql, 3600);
		while ($row = $db->sql_fetchrow($result))
		{
			$template->assign_block_vars('tb_smilies', array(
				'SMILEY_CODE'	=> addslashes($row['code']),
				'SMILEY_IMG'	=> $phpbb_root_path . $config['smilies_path'] . '/' . $row['smiley_url'],
				'SMILEY_DESC'	=> $row['emotion'])
			);
		}
		$db->sql_freeresult($result);
		page_header('',false);
		$template->set_filenames(array('body' => 'tag_board_smilies.html'));
		page_footer();
	break;

	case 'bbcode':
		$sql = 'SELECT *
				FROM ' . BBCODES_TABLE . "
				WHERE display_on_posting = 1";
		$result = $db->sql_query($sql, 3600);
		while($row = $db->sql_fetchrow($result))
		{
			$template->assign_block_vars('tb_bbcodes',array(
				'TB_VALUE'	=> addslashes($row['bbcode_tag']),
				'TB_END'	=> substr($row['bbcode_tag'], -1) === '=' ? substr($row['bbcode_tag'], 0, -1) : addslashes($row['bbcode_tag']),
				'TB_TITLE'	=> $row['bbcode_helpline'],
			));
		}
		$db->sql_freeresult($result);
		page_header('',false);
		$template->set_filenames(array('body' => 'tag_board_bbcodes.html'));
		page_footer();
	break;

	case 'edit':
		if($auth->acl_get('m_') || $auth->acl_get('a_') || ($config['tb_edit'] && is_author($user->data['user_id'],$tb_id)))
		{
			$tb_id = (int) $tb_id;
			$sql = 'SELECT *
					FROM ' . TB_TABLE . "
					WHERE tb_post_id = $tb_id";
			$result = $db->sql_query($sql);
			$row = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);
			$tb_post2 = generate_text_for_edit($row['tb_post_text'],$row['tb_bbcode_uid'],$row['tb_flags']);
			$tb_textarea = $config['tb_heigth'] - 50;
			$template->assign_vars(array(
					'TB_ID' 		=> $tb_id,
					'TB_POST2' 		=> $tb_post2['text'],
					'TB_TEXTAREA' 	=> $tb_textarea,
					'TB_POST_TIME' 	=> $row['tb_post_time'],
			));
			add_form_key('tb_form');
			page_header('',false);
			$template->set_filenames(array('body' => 'tag_board_edit.html'));
			page_footer();
		}
		header("Location: " . append_sid("{$phpbb_root_path}tag_board.$phpEx", "audio=$tb_audio"));
	break;

	case 'edit_submit':
		$tb_post_time = request_var('tb_post_time',0);
		if(check_form_key('tb_form') && $auth->acl_get('m_') || $auth->acl_get('a_') || ($config['tb_edit'] && is_author($user->data['user_id'],$tb_id) && (!$config['tb_edit_time'] || time() < ($tb_post_time + ($config['tb_edit_time'] * 60)))))
		{
			$tb_post2 = utf8_normalize_nfc(request_var('tb_post2', '', true));
			if($tb_post2)
			{
				generate_text_for_storage($tb_post2,$uid,$bitfield,$flags,$config['tb_bbcode'],$config['tb_urls'],$config['tb_smilies']);
				$sql_ary = array(
					'tb_post_text' 			=> (string) $tb_post2,
					'tb_bbcode_bitfield' 	=> (string) $bitfield,
					'tb_bbcode_uid' 		=> (string) $uid,
					'tb_flags' 				=> (int) $flags,
				);
				$sql = 'UPDATE ' . TB_TABLE . ' SET ' . $db->sql_build_array('UPDATE', $sql_ary) . ' WHERE tb_post_id = ' . $tb_id;
				$db->sql_query($sql);
			}
		}
		header("Location: " . append_sid("{$phpbb_root_path}tag_board.php", "audio=$tb_audio"));
	break;

	case 'delete':
		if(check_form_key('tb_form') && $auth->acl_get('m_') || $auth->acl_get('a_') || ($config['tb_delete'] && is_author($user->data['user_id'],$tb_id)))
		{
			$tb_id = (int) $tb_id;
			$sql = 'DELETE FROM ' . TB_TABLE . "
					WHERE tb_post_id = $tb_id";
			$db->sql_query($sql);
			set_config_count('total_tb', -1);
		}
		header("Location: " . append_sid("{$phpbb_root_path}tag_board.$phpEx", "audio=$tb_audio"));
	break;

	default:
		if(!$tb_denied && $tb_auth && ($config['tb_auth'] == 'STANDARD' || $config['tb_auth'] == 'FULL' || $user->data['is_registered']))
		{
			if($tb_error)
			{
				if($tb_error == 'flood')
				{
					$template->assign_var('TB_ERROR', $user->lang['TB_FLOOD_ERROR']);
				}
				else
				{
					$template->assign_var('TB_ERROR', $user->lang['FORM_INVALID']);
				}
			}
			if($config['tb_history'])
			{
				delete_tags();
			}
			$flag = $tb_start ? false : true;
			$limit = $tb_start ? ($config['tb_limit'] * $tb_start) : $config['tb_limit'];
			$sql = 'SELECT *
				FROM ' . TB_TABLE . "
				ORDER BY tb_post_time DESC";
			$result = $db->sql_query_limit($sql,$limit);
			while($row = $db->sql_fetchrow($result))
			{
				$display = 1;
				$tb_post = generate_text_for_display($row['tb_post_text'],$row['tb_bbcode_uid'],$row['tb_bbcode_bitfield'],$row['tb_flags']);
				if(preg_match('/(' . $_SERVER['HTTP_HOST'] . ')(.+)(php\?f=)([0-9]+)/', $tb_post, $tb_forum_id))
				{
					$display = $auth->acl_get('f_read', $tb_forum_id[4]);
				}
				if(preg_match('/(\.\/)(.+)(php\?f=)([0-9]+)/', $tb_post, $tb_forum_id))
				{
					$display = $auth->acl_get('f_read', $tb_forum_id[4]);
				}
				if($flag)
				{
					if(!is_read($row['tb_post_id']) && $tb_audio && $display)
					{
						$template->assign_var('TB_UNREAD', 1);
						$user->set_cookie('tb', $row['tb_post_id'], $cookie_expire);
					}
					$flag = false;
				}
				if($display)
				{
					$tb_quote = generate_text_for_edit($row['tb_post_text'],$row['tb_bbcode_uid'],$row['tb_flags']);
					$tb_quote_user = $row['tb_poster_id'] != ANONYMOUS ? urlencode($row['tb_post_username']) : ($config['tb_guest'] ? urlencode($config['tb_guest']) : urlencode($row['tb_post_username']));
					$tb_quote_text = urlencode($tb_quote['text']);
					$template->assign_block_vars('tagrow',array(
						'TB_ID' 			=> $row['tb_post_id'],
						'TB_IS_READ'		=> is_read($row['tb_post_id']),
						'TB_USER_COLOUR' 	=> get_username_string('colour', $row['tb_poster_id'], $row['tb_post_username'], get_user_id_var('user_colour', $row['tb_poster_id'])),
						'TB_USER_STATUS' 	=> $row['tb_poster_id'] != ANONYMOUS ? is_online($row['tb_poster_id']) : 'guest',
						'TB_USER_PROFILE' 	=> $row['tb_poster_id'] != ANONYMOUS ? append_sid("{$phpbb_root_path}memberlist.$phpEx", 'mode=viewprofile&u=' . $row['tb_poster_id']) : null,
						'TB_POST_TIME' 		=> $user->format_date($row['tb_post_time']),
						'TB_POST_USERNAME' 	=> $row['tb_poster_id'] != ANONYMOUS ? $row['tb_post_username'] : ($config['tb_guest'] ? $config['tb_guest'] : $row['tb_post_username']),
						'TB_POST_TEXT' 		=> $tb_post,
						'TB_QUOTE_TEXT' 	=> ($config['tb_bbcode'] && ($config['tb_auth'] == 'FULL' || $user->data['is_registered'])) ? $tb_quote_text : '',
						'TB_QUOTE_USER' 	=> $tb_quote_user,
						'TB_POST_EDIT' 		=> ($auth->acl_get('m_') || $auth->acl_get('a_') || ($config['tb_edit'] && is_author($user->data['user_id'],$row['tb_post_id']) && (!$config['tb_edit_time'] || time() < ($row['tb_post_time'] + ($config['tb_edit_time'] * 60))))) ? true : false,
						'TB_POST_DELETE' 	=> ($auth->acl_get('m_') || $auth->acl_get('a_') || ($config['tb_delete'] && is_author($user->data['user_id'],$row['tb_post_id']))) ? true : false,
					));
				}
			}
			$db->sql_freeresult($result);
			
			// Determino se ci sono more tags
			$sql = 'SELECT COUNT(tb_post_id) AS num_tb_posts FROM ' . TB_TABLE;
			$result = $db->sql_query($sql);
			$more = ($db->sql_fetchfield('num_tb_posts') / $config['tb_limit']);
			$db->sql_freeresult($result);
			if ($more > 1)
			{
				$int = ceil($more);
				if(!$tb_start)
				{
					$more = 2;
				}
				else
				{
					$more = $tb_start + 1;
				}
				if ($more < $int + 1)
				{
					$template->assign_var('TB_MORE', $more);
				}
			}

			add_form_key('tb_form');
			page_header('',false);
			$template->set_filenames(array('body' => 'tag_board_layout.html'));
			page_footer();
		}
	break;
}

?>