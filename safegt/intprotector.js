/**
 * Intprotector for GamerCards
 *
 * @version 1.0
 * @license GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html
 * @author  Soshen, http://www.nipponart.org
 * @created 2011
 */

function Pxbox(obj) {
  v = obj.value.replace(/[^a-z\^A-Z\^0-9\ ]/g,"");
  obj.value = v;
}
function Ppsn(obj) {
  v = obj.value.replace(/[^a-z\^A-Z\^0-9\-_]/g,"");
  obj.value = v;
}
function Pwii(obj) {
  v = obj.value.replace(/[^0-9\- ]/g,"");
  obj.value = v;
}
function Psteamid(obj) {
  v = obj.value.replace(/[^0-9]/g,"");
  obj.value = v;
}
function Pxfireu(obj) {
  v = obj.value.replace(/[^a-z\^A-Z\^0-9]/g,"");
  obj.value = v;
}
function Porigin(obj) {
  v = obj.value.replace(/[^a-z\^A-Z\^0-9\-_]/g,"");
  obj.value = v;
}