<?php
/**
* phpBB_SEO Class
* www.phpBB-SEO.com
* @package Advanced phpBB3 SEO mod Rewrite
*/
if (!defined('IN_PHPBB')) {
	exit;
}
$this->cache_config['settings'] = array ( 'url_rewrite' => true, 'modrtype' => 3, 'sql_rewrite' => true, 'profile_inj' => true, 'profile_vfolder' => true, 'profile_noids' => true, 'rewrite_usermsg' => true, 'rewrite_files' => true, 'rem_sid' => true, 'rem_hilit' => true, 'rem_small_words' => true, 'virtual_folder' => true, 'virtual_root' => true, 'cache_layer' => true, 'rem_ids' => true, 'copyrights' => array ( 'img' => true, 'txt' => '', 'title' => '', ), 'no_dupe' => array ( 'on' => true, ), 'zero_dupe' => array ( 'on' => true, 'strict' => true, 'post_redir' => 'guest', ), );
$this->cache_config['forum'] = array ( 4 => 'sezioni-private', 3 => 'amministrazione', 5 => 'benvenuti', 6 => 'regolamento', 15 => 'presentazioni', 7 => 'moderazione', 8 => 'pcm14', 11 => 'story', 9 => 'modding', 10 => 'multiplayer', 12 => 'tour-france', 13 => 'sezione-ciclismo', 14 => 'altri-sport', );
?>