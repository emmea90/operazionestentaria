<?php
/**
*
* @package DM Video
* @version $Id: common_dm_video.php 251 2010-12-29 17:14:51Z femu $
* @copyright (c) 2008, 2009 femu - http://die-muellers.org
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Some mod related variables and includes
$dm_video_path = (defined('DM_VIDEO_ROOT_PATH')) ? DM_VIDEO_ROOT_PATH : 'dm_video/';
$user->add_lang('mods/dm_video');
$video_unapproved = 0;

// Get config values
$sql_array = array(
    'SELECT'    => 'config_name, config_value',

    'FROM'      => array(
        DM_VIDEO_CONFIG_TABLE => 'vc'
    ),
);
$sql = $db->sql_build_query('SELECT', $sql_array);
$result = $db->sql_query($sql);

while ($row = $db->sql_fetchrow($result))
{
    $dmv_config[$row['config_name']] = $row['config_value'];
}
$db->sql_freeresult($result);

// Some additional template variables
$template->assign_vars(array(
	'S_IN_DMV'			=> true,
	'S_DMV_NEW_VIDEOS'	=> false,
	'U_DMV_INDEX'		=> append_sid("{$phpbb_root_path}{$dm_video_path}index.$phpEx"),
	'DMV_COPY_NOTE'		=> sprintf($user->lang['DMV_COPY_NOTE'], $dmv_config['copyright_email'], $dmv_config['copyright_show']),
));

?>