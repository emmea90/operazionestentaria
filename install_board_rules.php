<?php
/**
*
* @author Dugi (Dukagjin Surdulli) dugagjin.s@gmail.com
* @version $Id$
* @copyright (c) 2012 Dugi
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('UMIL_AUTO', true);
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

include($phpbb_root_path . 'common.' . $phpEx);
$user->session_begin();
$auth->acl($user->data);
$user->setup();

if (!file_exists($phpbb_root_path . 'umil/umil_auto.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}

$mod_name 				= 'ACP_RULES_MOD';
$version_config_name 	= 'advanced_rules_page_version';
$language_file 			= 'mods/info_acp_rules';
$logo_img 				= 'adm/images/rules_mod_logo.gif';

// Setup install data
$cat_module_data = array(
	'module_enabled'	=> 1,
	'module_display'	=> 1,
	'module_basename'	=> '',
	'module_langname'	=> '',
	'module_mode'		=> '',
	'module_langname'	=> 'ACP_CAT_RULES',
	'module_auth'		=> 'acl_a_rules',
);

$parent_module_data = array(
	'module_enabled'	=> 1,
	'module_display'	=> 1,
	'module_langname'	=> 'ACP_PARENT_RULES',
	'module_auth'		=> 'acl_a_rules',
);

$front_module_data = array(
	'module_enabled'	=> 1,
	'module_display'	=> 1,
	'module_langname'	=> 'RULES_MANAGE',

	'module_basename'	=> 'rules',
	'module_mode'		=> 'manage',
	'module_auth'		=> 'acl_a_rules',
);

// Versioning
$versions = array(
	'0.0.1' => array(

		'table_add' => array(
			array(RULES_TABLE, array(
				'COLUMNS' => array(
					'rule_id' => array('UINT', NULL, 'auto_increment'),
					'parent_id' => array('UINT', 0),
					'cat_position' => array('UINT', 0),
					'rule_position' => array('UINT', 0),
					'rule_type' => array('VCHAR:4', ''),
					'rule_title' => array('VCHAR', ''),
					'rule_description' => array('MTEXT', ''),
					'public' => array('BOOL', 0),
					'parse_bbcode' => array('BOOL', 0),
					'parse_links' => array('BOOL', 0),
					'parse_smilies' => array('BOOL', 0),
				),
				
				'PRIMARY_KEY' => array('rule_id'),
			)),

		),
		
		// Insert some samples
		'table_insert' => array(
        	array(RULES_TABLE, array(
					'parent_id' => 0,
					'cat_position' => 0,
					'rule_position'	=> 0,
					'rule_type'	=> 'cat',
					'rule_title' => 'Sample Category 1',
					'rule_description' => '',
					'public' => true,
					'parse_bbcode' => false,
					'parse_links' => false,
					'parse_smilies' => false,
            	),
            ),
			
			array(RULES_TABLE, array(
					'parent_id' => 1,
					'cat_position' => 0,
					'rule_position'	=> 0,
					'rule_type'	=> 'rule',
					'rule_title' => 'Sample Rule 1',
					'rule_description' => 'This is a sample rule description',
					'public' => true,
					'parse_bbcode' => true,
					'parse_links' => true,
					'parse_smilies' => true,
            	),
            ),
		),
		
		'permission_add' => array(
			array('a_rules', true),
		),
		
		'permission_set' => array(
			array('ROLE_ADMIN_STANDARD', 'a_rules'),
		),
		
		'module_add' => array(
			array('acp', '', $cat_module_data),
			array('acp', 'ACP_CAT_RULES', $parent_module_data),
			array('acp', 'ACP_PARENT_RULES', $front_module_data)
		),
		
		'config_add' => array(
			array('enable_board_rules', false),
		),
	),
	
	'0.0.2' => array(),
	
	// Skipped some versions
	'0.1.0' => array(
		'table_column_add' => array(
			array(RULES_TABLE, 'group_ids', array('VCHAR:255', '')), 
		),
		
		'custom' => 'populate_group_ids',
	),
	
	'0.1.1' => array(),
	'0.1.2' => array(),
	
	'0.1.3' => array(
		'cache_purge' => array('auth', 0),
		'cache_purge' => array('template', 'theme'),
	),
	
	// Skipped some versions
	'1.0.0' => array(),
);

// Include the UMIL Auto file, it handles the rest
include($phpbb_root_path . 'umil/umil_auto.' . $phpEx);

/*
* Adds data to the new column `group_ids`
*
* @param string $action The action (install|update|uninstall) will be sent through this.
* @param string $version The version this is being run for will be sent through this.
*/
function populate_group_ids($action, $version)
{
	global $db, $table_prefix, $umil;

	if ($action == 'install' || $action == 'update')
	{
		if ($umil->table_exists($table_prefix . 'rules'))
		{
			$sql = 'UPDATE ' . RULES_TABLE .
				" SET group_ids = '1 2 7'";
			$db->sql_query($sql);

	        return array('command' => 'GROUP_IDS_POPULATED', 'result' => 'SUCCESS');
		}
	}
	else
	{
		 return array('command' => 'GROUP_IDS_UNPOPULATED', 'result' => 'SUCCESS');	
	}
}