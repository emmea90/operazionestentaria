<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file definisce le seguenti configurazioni: impostazioni MySQL,
 * Prefisso Tabella, Chiavi Segrete, Lingua di WordPress e ABSPATH.
 * E' possibile trovare ultetriori informazioni visitando la pagina: del
 * Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Editing wp-config.php}. E' possibile ottenere le impostazioni per
 * MySQL dal proprio fornitore di hosting.
 *
 * Questo file viene utilizzato, durante l'installazione, dallo script
 * di creazione di wp-config.php. Non � necessario utilizzarlo solo via
 * web,� anche possibile copiare questo file in "wp-config.php" e
 * rimepire i valori corretti.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - E? possibile ottenere questoe informazioni
// ** dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'my_operazionestentaria');

/** Nome utente del database MySQL */
define('DB_USER', 'operazionestentaria');

/** Password del database MySQL */
define('DB_PASSWORD', '');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha
idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * E' possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * E' possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ci� forzer� tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MZlJu9e0|a2$1d_Ng-w|/u:|d}+U$}4O)GcB~ukpwZ`+LBDkL^+]15}2-D@c2:;[');
define('SECURE_AUTH_KEY',  'Za0(rRC(cG+@)Y*KgJ(m ?QQn%W!Ot@$I~dl(Rzz8c-zL*70h8$+p/MK)1AYecxg');
define('LOGGED_IN_KEY',    'nh*,4@y%8W}|q77Sn`ay;mW{k2KP$,Dq!JV.}hvf;*rKHsiy#+cAt9@ptC+!k;e8');
define('NONCE_KEY',        '|ngfcO2l)i&-/dk(ZE/@5;3 _>g)j<PlJj0%CW:Tgd^=;Vh-qwHv2.slLck!}{n,');
define('AUTH_SALT',        'e(Hys+<Swn{$Ys49e8?#5?>IcLfX~-p#N =E~0SN ;CZnN/+28]%;M;sR<.5z6!J');
define('SECURE_AUTH_SALT', 'MY(=x9uJ$?e#Q]EVV,zY.qKrGI+JPJ~3_sI|2RvOM1iu,QyM_+Y XND2-u4bme|h');
define('LOGGED_IN_SALT',   '0jW;]:Vypx~(0h&RSAZ/~9KMzU.3@S{H=1`r2c$zW%4E%e-cAGaB-1T5+v[gAjO(');
define('NONCE_SALT',       'g+$;z[W>X*Qk~+z%UsAb?aYU(5+n8Hpcjg$U1<4|a]<pwR@coHJ54i1Mf|8U%,a-');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress .
 *
 * E' possibile avere installazioni multiple su di un unico database if you give each a unique
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Lingua di Localizzazione di WordPress, di base Inglese.
 *
 * Modificare questa voce per localizzare WordPress. Occorre che nella cartella
 * wp-content/languages sia installato un file MO corrispondente alla lingua
 * selezionata. Ad esempio, installare de_DE.mo in to wp-content/languages ed
 * impostare WPLANG a 'de_DE' per abilitare il supporto alla lingua tedesca.
 *
 * Tale valore � gi� impostato per la lingua italiana
 */
define('WPLANG', 'it_IT');

/**
 * Per gli sviluppatori: modalit� di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * E' fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all'interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta lle variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
